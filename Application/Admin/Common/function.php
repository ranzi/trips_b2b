<?php
function getJob($id) {
    return D('Job')->where(array('id' => $id))->getField('title');
}

function getDepartment($id) {
    return D('Department')->where(array('id' => $id))->getField('title');
}

function getTypes($id) {
    return D('Types')->where(array('id' => $id))->getField('title');
}

function getCategory($id) {
    return D('Category')->where(array('id' => $id))->getField('title');
}

function getMenus($id) {
    $id = intval($id);
    if ($id == 0) {
        return '';
    }
    return D('Menu')->where(array('id' => $id))->getField('title');
}

function getZsysData($id) {
    return M('Zsysdata')->where(array('id' => $id))->getField('title');
}

function getTitleById($model, $id) {
    return $model->where(array('id' => $id))->getField('title');
}

function getAdmin($id) {
    return D('Admin')->where(array('id' => $id))->getField('username');
}
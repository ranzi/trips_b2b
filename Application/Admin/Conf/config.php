<?php
return array(
    'B2C_ENCRY'           => 'Uex$8YnM98',
    'AUTO_UPLOAD_REMOTE'  => TRUE,
    'AUTO_UPLOAD_FILE'    => TRUE,
    'ALLOW_FILE_EXT'      => array('jpg', 'gif', 'png'),
    'DEFAULT_PAGE_SIZE'   => 15,
    'COOKIE_HTTPONLY'     => TRUE,
    'TMPL_ACTION_SUCCESS' => 'Public/success',
    'TMPL_ACTION_ERROR'   => 'Public/error',
);
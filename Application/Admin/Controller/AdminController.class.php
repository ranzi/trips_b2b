<?php
namespace Admin\Controller;

class AdminController extends AuthController {

    public function _initialize() {
        parent::_initialize();
    }

    public function _before_index() {
        $this->assign('bar', array('curpos' => '管理员列表', 'menu' => '管理员', 'url' => U('Admin/index')));
    }

    public function _before_add() {
        $this->assign('bar', array('curpos' => '添加管理员', 'menu' => '管理员', 'url' => U('Admin/index')));
        $groups = D('Group')->toSelect();
        $this->assign('groups', $groups);
        $depts = D('Department')->toTree();
        $this->assign('depts', $depts);
        $jobs = D('Job')->toSelect(array('status' => 1));
        $this->assign('jobs', $jobs);
    }

    public function _before_edit() {
        $uid = I('get.id', 0, 'intval');
        if (!$uid) {
            $this->error('参数错误');
        }
        $this->assign('bar', array('curpos' => '添加管理员', 'menu' => '管理员', 'url' => U('Admin/index')));
        $groups = D('Group')->toSelect();
        $this->assign('groups', $groups);
        $hasGroups = D('Admingroup')->field('group_id')->where(array('uid' => $uid))->select();
        $selected  = array();
        foreach ($hasGroups as $group) {
            $selected[] = $group['group_id'];
        }
        $this->assign('selected', $selected);
        $depts = D('Department')->toTree();
        $this->assign('depts', $depts);
        $hasDepts = D('AdminDept')->getAllDepts($uid);
        $this->assign('depted', $hasDepts);
        $jobs = D('Job')->toSelect(array('status' => 1));
        $this->assign('jobs', $jobs);
    }

    public function profile() {
        $this->assign('bar', array('curpos' => '个人资料'));
        $this->assign('user', session('admin'));
        $this->display();
    }

    public function getPos() {
        if (!IS_AJAX) {
            $this->redirect('Index/index');
        }
        $id   = I('post.id', 0, 'intval');
        $poss = D('Position')->getAll(array('status' => 1, 'belong' => $id));
        $this->ajaxReturn(array('status' => 1, 'data' => $poss));
    }
}
<?php
namespace Admin\Controller;

class AuthController extends BaseController {
    public function _initialize() {
        parent::_initialize();
        $this->_checkAuth();
        $this->getAuthCategory();
    }

    public function _checkAuth() {
        #是否登录
        if (!session('admin_id') || session('admin_id') <= 0) {
            $this->redirect('Login/index');
        }
        if (IS_AJAX && IS_POST && !in_array(ACTION_NAME, array('edit', 'delete'))) {
            return TRUE;
        }
        if (!session('groups')) {
            $groups = D('Admingroup')->alias('ag')->join('__GROUP__ g ON ag.group_id=g.id', 'LEFT')->select();
            if (empty($groups)) {
                $this->redirect('Login/logout');
            }
            session('groups', $groups);
            $groupids = array();
            foreach ($groups as $group) {
                $groupids[] = $group['id'];
            }
            session('groupids', $groupids);
        }

        if (!session('groups')) {
            $this->redirect('Login/logout');
        }

        $grants = D('Grant')->where("(auth_id=" . session('admin_id') . " AND auth_type='user') OR (auth_id IN (" . implode(',', session('groupids')) . ") AND auth_type='group') AND `status`=1")->select();

        $auths = array();
        foreach ($grants as $grant) {
            if ($grant['auth']) {
                $auths = array_merge($auths, explode(',', $grant['auth']));
            }
        }
        if ($auths) {
            $priveleges = D('Privelege')->where(array('status' => 1, 'id' => array('IN', $auths)))->select();
        }

        #是否有当前访问路径的操作权限
        $allow = false;
        foreach ($priveleges as $pri) {
            if (strcasecmp($pri['model'], MODULE_NAME) === 0 && strcasecmp($pri['ctrl'], CONTROLLER_NAME) === 0 && strcasecmp($pri['action'], ACTION_NAME) === 0) {
                if (isset($pri['op']) && !empty($pri['op'])) {
                    if (I('request.op', '', 'trim') == $pri['op']) {
                        $allow = true;
                        break;
                    }
                } else {
                    $allow = true;
                    break;
                }
            }
        }

        if (!$allow) {
            if (IS_AJAX) {
                $this->ajaxReturn(array('status' => 0, 'msg' => '您没有权限进行此项操作'));
            }
            $this->error('您没有权限进行此项操作');
        }

        $authMenus = array();
        foreach ($priveleges as $pri) {
            if ($pri['menu_id']) {
                $authMenus[] = $pri['menu_id'];
            }
        }
        $authMenus = array_unique($authMenus);
        #获取对应操作菜单
        $menus = D('Menu')->toTree();
        foreach ($menus as $key => $menu) {
            #若父菜单没有权限，子级全部移除
            if (!in_array($menu['id'], $authMenus)) {
                unset($menus[$key]);
            } else {
                if ($menu['child']) {
                    foreach ($menu['child'] as $k => $item) {
                        if (!in_array($item['id'], $authMenus) || (!APP_DEBUG && $item['is_dev'] == 1)) {
                            unset($menus[$key]['child'][$k]);
                        } elseif (strcasecmp($item['url'], CONTROLLER_NAME . '/' . ACTION_NAME) === 0) {
                            $menus[$key]['class']              = 'active open';
                            $menus[$key]['child'][$k]['class'] = 'active';
                        }
                    }
                } else {
                    if (strcasecmp($menu['url'], CONTROLLER_NAME . '/' . ACTION_NAME) === 0) {
                        $menus[$key]['class'] = 'active';
                    }
                }

            }
        }
        $this->assign('authMenus', $menus);
    }

    /**
     *desc:获取授权的分类
     *@param $admin_id Integer admin ID,if null given, will be the current user
     *@return categorys
     *@author qiisyhx<sihangshi2011@gmail.com>
     **/
    public function getAuthCategory($admin_id = 0) {
        if (!$admin_id) {
            $admin_id = session('admin_id');
        }
        $departid = D('AdminDept')->field('dept_id')->where(array('uid' => $admin_id))->select();
        if (empty($departid)) {
            $dept_id = -1;
        } else {
            $temp = array();
            foreach ($departid as $row) {
                $temp[] = $row['dept_id'];
            }
            $dept_id = implode(',', $temp);
        }
        $auth_cate = D('GrantCate')->field('distinct cate_id')->where("(`type`='user' AND `grant_to`={$admin_id}) OR (`type`='department' AND `grant_to` IN ($dept_id))")->select();
        if (empty($auth_cate)) {
            $auth_cate = array(-1);
        } else {
            $temp = array();
            foreach ($auth_cate as $cate) {
                $temp[] = $cate['cate_id'];
            }
            $auth_cate = $temp;
        }
        // session('auth_cate', $auth_cate);
        return $auth_cate;
    }
}
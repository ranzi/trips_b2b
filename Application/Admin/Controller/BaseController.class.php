<?php
namespace Admin\Controller;
use Think\Controller;

class BaseController extends Controller {
    protected $upfile = array();

    public function __construct() {
        parent::__construct();
    }

    /*init system*/
    public function _initialize() {
        //client ip address
        define('CLIENT_IP', get_client_ip(1));
        //auto upload files
        // if (C('AUTO_UPLOAD_FILE') && isset($_FILES) && !empty($_FILES) && ACTION_NAME != 'frameUpload' && ACTION_NAME != 'uploadify') {
        //     $this->autoUploadFile();
        // }
        $this->_autoLogin();
        $this->cfgs = F('cfgs');
        $this->assign('cfgs', $this->cfgs);
    }

    /**
     *@author Chunlei Wang<sihangshi2011@gmail.com>
     *description:
     *create data list
     *@param Object $model data module, created by function D()
     *@param HashMap $map the key=>value array of data filter
     *@param String $sort order by
     *@param Boolean $asc use asc sort, default false
     **/
    protected function _list($model, $map, $sort = '', $asc = false) {
        if (!($order = I('request._order', '', 'trim'))) {
            $order = !empty($sort) ? $sort : $model->getPk();
        }
        if (!($sort = I('request._sort', '', 'trim'))) {
            $sort = $asc ? 'asc' : 'desc';
        }
        $psize = I('request.psize', 0, 'intval') ?: C('DEFAULT_PAGE_SIZE', NULL);
        $total = $model->where($map)->count();
        $pager = new \Think\Page($total, $psize);
        if ($total > 0) {
            $list = $model->where($map)->order($order . ' ' . $sort)->limit($pager->firstRow . ',' . $pager->listRows)->select();
        }
        #处理搜索条件
        $get = I('get.');
        unset($get['_order'], $get['_sort'], $get['p']);
        $this->assign('_condition', http_build_query($get));
        $this->assign('list', $list);
        $this->assign('page', $pager->show());
        $this->assign('total', $total);
        $this->assign('_sort', $sort);
        $this->assign('_order', $order);
    }

    /**
     *@author Chunlei Wang<sihangshi2011@gmail.com>
     *description
     *export csv file to download
     *@param Array $data source data
     *@param Mixed $title title, usage a,b,c or array(a, b, c)
     *@param String $filename export file name
     *@param Array $fields filter of data filed
     **/
    public function export_csv($data, $title = array(), $filename, $fields = array()) {
        if (!$data || !is_array($data)) {
            $this->show('No result');
        } else {
            $content = '';
            if (is_array($title)) {
                $title = implode(',', $title);
            }
            $content .= iconv('utf-8', 'gbk', $title) . "\n";
            foreach ($data as $key => $val) {
                $temp = array();
                foreach ($val as $field => $item) {
                    if (empty($fields) || in_array($field, $fields)) {
                        $temp[$field] = iconv('utf-8', 'gbk', '"' . $item . '"');
                    }
                }
                $content .= implode(',', $temp) . "\n";
            }
            if (!$filename) {
                $filename = date('YmdHis');
            }
            header('Content-Disposition: attachment; filename=' . $filename . '.csv');
            echo $content;
        }
        exit;
    }

    /**
     *@author Chunlei Wang<sihangshi2011@gmail.com>
     *说明：
     *图片文件自动上传，可通过C('AUTO_UPLOAD_FILE')打开或关闭自动上传功能
     **/
    public function autoUploadFile() {
        $model = strtolower(MODULE_NAME);
        $ctrl  = strtolower(CONTROLLER_NAME);
        $path  = $_SERVER['DOCUMENT_ROOT'] . '/uploads/' . $model . '/' . $ctrl . '/';
        if (!file_exists($path)) {
            @mkdir($path, 0777, true);
        }
        $upload           = new \Think\Upload();
        $upload->exts     = array('jpg', 'gif', 'png', 'jpeg');
        $upload->subName  = date('Ymd');
        $upload->rootPath = './uploads/' . $model . '/' . $ctrl . '/';
        $info             = $upload->upload();
        if ($info) {
            foreach ($info as $key => $val) {
                $this->upfile[$key] = '/uploads/' . $model . '/' . $ctrl . '/' . $info[$key]['savepath'] . $info[$key]['savename'];
            }
            if (IS_AJAX) {
                $this->ajaxReturn(array('status' => 1, 'result' => $this->upfile, 'msg' => ''));
            }
        } else {
            if (IS_AJAX) {
                $this->ajaxReturn(array('status' => 0, 'msg' => 'upload error'));
            }
        }
    }

    /**
     *@author Chunlei Wang<sihangshi2011@gmail.com>
     *说明：
     *使用iframe方式上传图片
     **/
    public function frameUpload() {
        $size = I('request.size', '', 'trim');
        $dir  = I('request.dir', '', 'trim');
        if (!empty($dir)) {
            $path = $dir . '/';
        } else {
            $model = strtolower(MODULE_NAME);
            $ctrl  = 'ajax';
            $path  = $model . '/' . $ctrl . '/';
        }
        $nowater = I('request.nowater', 0, 'intval');

        if (isset($_FILES) && !empty($_FILES) && $_FILES['upfile']['error'] == 0) {
            $realpath = $_SERVER['DOCUMENT_ROOT'] . '/uploads/' . $path;
            if (!file_exists($realpath)) {
                @mkdir($realpath, 0777, true);
            }

            $upload           = new \Think\Upload();
            $upload->exts     = array('jpg', 'gif', 'png', 'jpeg');
            $upload->subName  = date('Ymd');
            $upload->rootPath = './uploads/' . $path;
            $info             = $upload->upload();

            if ($info) {
                $returnFile = '/uploads/' . $path . $info['upfile']['savepath'] . $info['upfile']['savename'];
                // list($fname, $ext) = explode('.', $info['upfile']['savename']);
                // if (!empty($size)) {
                //     $image = new \Think\Image();
                //     $image->open($realpath . $info['upfile']['savepath'] . $info['upfile']['savename']);
                //     list($thumbWidth, $thumbHeight) = explode('x', $size);
                //     $image->thumb($thumbWidth, $thumbHeight, \Think\Image::IMAGE_THUMB_CENTER);
                //     $image->save('./uploads/' . $path . $info['upfile']['savepath'] . $fname . '_' . $thumbWidth . 'x' . $thumbHeight . '.' . $ext);
                //     $returnFile = '/uploads/' . $path . $info['upfile']['savepath'] . $fname . '_' . $thumbWidth . 'x' . $thumbHeight . '.' . $ext;
                // } else {
                //     $returnFile = '/uploads/' . $path . $info['upfile']['savepath'] . $info['upfile']['savename'];
                // }
                // $this->frameCallback(I('request.callback'), json_encode(array('status' => 1, 'file' => $returnFile)));
            } else {
                $msg = $upload->getError();
            }
        } elseif (($url = I('request.url', '', 'trim'))) {
            // $info = pathinfo($url);
            $imageType = exif_imagetype($url);
            if ($imageType === FALSE || !in_array($imageType, array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG, IMAGETYPE_BMP))) {
                $msg = '图片格式错误';
            } else {
                switch ($imageType) {
                    case IMAGETYPE_GIF:
                        $ext = '.gif';
                        break;
                    case IMAGETYPE_PNG:
                    case IMAGETYPE_BMP:
                        $ext = '.png';
                        break;
                    case IMAGETYPE_JPEG:
                        $ext = '.jpg';
                        break;
                }
                $savepath = '/uploads/' . $path . date('Ymd') . '/';
                if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $savepath)) {
                    @mkdir($_SERVER['DOCUMENT_ROOT'] . $savepath, 0777, true);
                }
                $fileName = md5($savepath . date('Ymd') . microtime() . mt_rand()) . $ext;
                $ch       = curl_init();
                $fp       = fopen($_SERVER['DOCUMENT_ROOT'] . $savepath . $fileName, 'wb');

                curl_setopt($ch, CURLOPT_URL, $url);
                // curl_setopt($ch, CURLOPT_FILE, $fp);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
                curl_setopt($ch, CURLOPT_TIMEOUT, 20);

                $rawdata = curl_exec($ch);
                curl_close($ch);
                if ($rawdata !== false) {
                    fwrite($fp, $rawdata);
                    $returnFile = $savepath . $fileName;
                } else {
                    @unlink($_SERVER['DOCUMENT_ROOT'] . $savepath . $fileName);
                    $msg = '图片下载失败';
                }
                fclose($fp);
            }
            // $this->frameCallback(I('request.callback'), json_encode(array('status' => 1, 'file' => $savepath . $fileName)));
        }
        if ($returnFile && $msg == '') {
            // list($fname, $ext) = explode('.', $info['upfile']['savename']);
            $ext = pathinfo($returnFile, PATHINFO_EXTENSION);
            if (!empty($size)) {
                list($thumbWidth, $thumbHeight) = explode('x', $size);
            }
            if (!$thumbWidth) {
                $thumbWidth = $this->cfgs['thumb_width'];
            }
            if (!$thumbHeight) {
                $thumbHeight = $this->cfgs['thumb_height'];
            }
            $original = $returnFile;
            if ($thumbHeight && $thumbWidth) {
                $image = new \Think\Image();
                $image->open($_SERVER['DOCUMENT_ROOT'] . $returnFile);
                $image->thumb($thumbWidth, $thumbHeight, \Think\Image::IMAGE_THUMB_CENTER);
                $returnFile = preg_replace("/\." . $ext . "$/", '_' . $thumbWidth . "x" . $thumbHeight . "." . $ext, $returnFile);
                $image->save($_SERVER['DOCUMENT_ROOT'] . $returnFile);
            }
            if ($nowater != 1 && $this->cfgs['watermark'] == 1) {

                $image = new \Think\Image();
                $image->open($_SERVER['DOCUMENT_ROOT'] . $returnFile);
                if ($this->cfgs['watertype'] == 'image') {
                    if (empty($this->cfgs['waterimage'])) {
                        $msg        = '请先上传水印图片';
                        $returnFile = '';
                    } else {
                        $image->water($_SERVER['DOCUMENT_ROOT'] . $this->cfgs['waterimage'])->save($_SERVER['DOCUMENT_ROOT'] . $returnFile);
                    }
                } else {
                    if (empty($this->cfgs['watertext'])) {
                        $msg        = '请先设置水印文字';
                        $returnFile = '';
                    } else {
                        $image->text($this->cfgs['watertext'], './Public/font/hanyi.ttf', 10, '#000000')->save($_SERVER['DOCUMENT_ROOT'] . $returnFile);
                    }
                }
            }
            if ($returnFile) {
                $this->frameCallback(I('request.callback'), json_encode(array('status' => 1, 'file' => $returnFile, 'original' => $original)));
            }
        }
        $this->frameCallback(I('request.callback'), json_encode(array('status' => 0, 'msg' => $msg ?: '图片上传失败')));
    }

    /**
     *@author Chunlei Wang<sihangshi2011@gmail.com>
     *说明:
     *index方法为默认的列表方法
     *控制器对应数据库表名
     */
    public function index() {
        $model = $this->_model ?: D(CONTROLLER_NAME);
        $map   = $this->getMaps($model);
        // $total = $model->where($map)->count();
        // $pager = new \Think\Page($total, C('DEFAULT_PAGE_SIZE', NULL, 20));
        // if ($total > 0) {
        //     $list = $model->where($map)->limit($pager->firstRow . ',' . $pager->listRows)->select();
        //     $this->assign('list', $list);
        // }
        // $this->assign('total', $total);
        // $this->assign('page', $pager->show());
        $this->_list($model, $map);
        $this->display();
    }

    /**
     *@author Chunlei Wang<sihangshi2011@gmail.com>
     *说明：
     *删除数据
     *
     */
    public function delete() {
        $id    = I('request.id');
        $model = D(I('request.table', CONTROLLER_NAME, 'trim'));
        $pk    = I('request.pk', $model->getPk(), 'trim');
        if (!$id) {
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => '参数错误'));
            $this->error($this->error('参数错误'));
        }
        $id = explode(',', $id);
        foreach ($id as &$i) {
            $i = intval($i);
        }
        $model->where(array($pk => array('IN', implode(',', $id))))->limit(count($id))->delete();
        IS_AJAX && $this->ajaxReturn(array('status' => 1, 'msg' => '删除成功'));
        $this->success('删除成功');
    }

    /**
     *@author  Chunlei Wang<sihangshi2011@gmail.com>
     *说明：
     *添加数据操作
     */
    public function add() {
        $model = $this->_model ?: D(CONTROLLER_NAME);
        if (IS_POST) {
            $result = $model->_add();
            if ($result['status'] == 1) {
                $this->success($result['msg'], U(CONTROLLER_NAME . '/index'));
            } else {
                $this->error($result['msg']);
            }
        }
        $this->display();
    }

    /**
     *@author  Chunlei Wang<sihangshi2011@gmail.com>
     *说明：
     *编辑数据操作
     */
    public function edit() {
        $model = $this->_model ?: D(CONTROLLER_NAME);
        if (IS_AJAX) {
            $field  = I('post.field', '', 'trim');
            $id     = I('post.id', '', 'trim');
            $pk     = I('post.pk', '', 'trim');
            $value  = I('post.value', '', 'trim');
            $status = 0;
            $msg    = '编辑失败';
            if ($field && $id & $pk && $value) {
                $status = 1;
                $msg    = '编辑成功';
                $model->where(array($pk => $id))->save(array($field => $value));
            }
            $this->ajaxReturn(array('msg' => $msg, 'status' => $status));
        } elseif (IS_POST) {
            $result = $model->_save();
            if ($result['status'] == 1) {
                $this->success($result['msg'], U(CONTROLLER_NAME . '/index'));
            } else {
                $this->error($result['msg']);
            }
        }

        $id = I('get.id', 0, 'intval');
        if (!$id) {
            $this->error('参数错误');
        }
        $info = $model->where(array($model->getPk() => $id))->find();
        if (!$info) {
            $this->error('您所查看的信息不存在');
        }
        $this->assign('info', $info);
        $this->display();
    }

    /**
     *@author  Chunlei Wang<sihangshi2011@gmail.com>
     *说明：
     *自动构建map
     */
    protected function getMaps($model) {
        $map     = array();
        $request = I('request.');
        $fields  = $model->getDbFields();
        foreach ($fields as $key => $val) {
            if (isset($request[$val]) && $request[$val] !== '') {
                $map[$val] = $request[$val];
            }
        }
        return $map;
    }

    /**
     *@author Chunlei Wang<sihangshi2011@gmail.com>
     *说明：
     *上传图片至远程服务器
     */
    public function autoUploadImageToRemote() {
        if (C('AUTO_UPLOAD_REMOTE') && C('IMAGE_REMOTE_URL')) {

        }
    }

    /**
     *@author Chunlei Wang<sihangshi2011@gmail.com>
     *iframe 上传图片回调函数
     **/
    public function frameCallback($callback, $val) {
        echo '<script type="text/javascript">window.parent.' . $callback . '(' . $val . ');</script>';
        exit;
    }

    /**
     *@author Chunlei Wang<sihangshi2011@gmail.com>
     *自动登录
     **/
    public function _autoLogin() {
        if (cookie('b2c_autologin')) {
            $account = cookie('b2c_username');
            $key     = cookie('b2c_login_key');
            if ($key && $account) {
                $map = array(
                    '_complex' => array(
                        'username' => $account,
                        'mobile'   => $account,
                        '_logic'   => 'OR',
                    ),
                );
                $user_data = D('Admin')->where($map)->find();
                if ($user_data) {
                    if ($key == sha1($user_data['addtime'] . $user_data['pwd'] . $user_data['salt'])) {
                        session('admin_id', $user_data['id']);
                        session('admin_name', $user_data['realname'] ?: $user_data['username']);
                        session('admin', $user_data);
                    } else {
                        cookie('b2c_autologin', NULL);
                        cookie('b2c_username', NULL);
                        cookie('b2c_login_key', NULL);
                    }
                }
            }
        }
    }

    /**
     *desc:load images album by types
     */
    public function loadImages() {
        $id      = I('post.id', 0, 'intval');
        $map     = array('type_id' => $id);
        $cate_id = I('post.cate_id', 0, 'intval');
        if ($cate_id) {
            $map['cate_id'] = $cate_id;
        }
        $arrive = I('post.arrive', '', 'trim');
        if ($arrive) {
            $id          = D('District')->where(array('title' => $arrive))->getField('id');
            $map['dist'] = $id;
        }
        $key = I('post.key', '', 'trim');
        if ($key) {
            $map['title'] = array('LIKE', '%' . $key . '%');
        }
        $page  = I('post.page', 1, 'intval');
        $page  = max($page, 1);
        $size  = 20;
        $limit = ($page - 1) * $psize;
        $total = D('Images')->where($map)->count();
        $list  = D('Images')->where($map)->limit($limit . ',' . $size)->select();
        $this->ajaxReturn(array('status' => 1, 'data' => $list, 'page' => $page, 'total' => $total, 'size' => $size, 'leave' => ($page * $size) < $total));
    }

    /**
     *get album images
     **/
    public function loadImagesDetail() {
        $id   = I('post.id', 0, 'intval');
        $map  = array('pid' => $id);
        $list = D('ImagesSrc')->getAll($map);
        $this->ajaxReturn(array('status' => 1, 'data' => $list));
    }

    public function loadDirs() {
        $dir    = I('post.dir', '', 'trim');
        $parent = I('post.parent', '', 'trim');
        $root   = THINK_PATH . '../uploads/attachment';
        $dir    = empty($parent) && empty($dir) ? '/' : $parent . '/' . $dir;
        $parent = $dir;
        $dir    = $root . $dir;
        $list   = scandir($dir);
        $result = array();
        foreach ($list as $key => $_dir) {
            if ($_dir != '.' && $_dir != '..') {
                $result[] = array(
                    'name'   => $_dir,
                    'isdir'  => is_dir($dir . '/' . $_dir),
                    'parent' => $parent,
                );
            }
        }
        sort($list);
        $this->ajaxReturn(array('status' => 1, 'data' => $result));
    }

    public function uploadify() {
        if (isset($_FILES) && !empty($_FILES) && $_FILES['Filedata']['error'] == 0) {
            $ext = pathinfo($_FILES['Filedata']['name'], PATHINFO_EXTENSION);
            if ('jpg' == $ext || 'gif' == $ext || 'png' == $ext) {
                $sub  = 'images';
                $type = 4;
            } elseif ('xls' == $ext || 'xlsx' == $ext) {
                $sub  = 'Excel';
                $type = 2;
            } elseif ('doc' == $ext || 'docx' == $ext) {
                $sub  = 'Doc';
                $type = 1;
            } elseif ('txt' == $ext) {
                $sub  = 'Txt';
                $type = 3;
            } else {
                $sub = 'Miscell';
            }
            $path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/attachment/' . $sub . '/';
            if (!file_exists($path)) {
                @mkdir($path, 0777, true);
            }
            $upload           = new \Think\Upload();
            $upload->exts     = array('jpg', 'gif', 'png', 'jpeg', 'txt', 'doc', 'docx', 'xls', 'xlsx');
            $upload->subName  = date('Ymd');
            $upload->rootPath = './uploads/attachment/' . $sub . '/';
            $info             = $upload->upload();
            if ($info) {
                $url        = '/uploads/attachment/' . $sub . '/' . $info['Filedata']['savepath'] . $info['Filedata']['savename'];
                $attachment = I('post.attachment', 0, 'intval');
                if (!$attachment) {
                    $attachment = D('Attachment')->add(array('addtime' => NOW_TIME, 'counts' => 0, 'downloads' => 0));
                }
                $showSize = $this->sizeFmt($info['Filedata']['size']);
                $subid    = D('AttachmentUrl')->add(array('title' => $_FILES['Filedata']['name'], 'url' => $url, 'downloads' => 0, 'addtime' => NOW_TIME, 'admin_id' => session('admin_id'), 'type' => $type, 'pid' => $attachment, 'size' => $info['Filedata']['size'], 'sizeShow' => $showSize));
                $data     = array(
                    'file'       => $url,
                    'showname'   => $_FILES['Filedata']['name'],
                    'attachment' => $attachment,
                    'subid'      => $subid,
                    'size'       => $showSize,
                );
                $this->ajaxReturn(array('status' => 1, 'data' => $data));
            } else {
                $this->ajaxReturn(array('status' => 0, 'msg' => '文件上传失败'));
            }
        }
    }

    public function sizeFmt($size) {
        $size = intval($size);
        if ($size > 1024) {
            $bit  = $size % 1024;
            $byte = floor($size / 1024);
            if ($byte > 1024) {
                $mb   = floor($byte / 1024);
                $byte = $byte % 1024;
                $str  = round(floatval($mb . '.' . $byte), 2) . 'MB';
            } else {
                $str = round(floatval($byte . '.' . $bit), 2) . 'KB';
            }
        } else {
            if ($size < 1000) {
                $str = $size . 'B';
            } else {
                $str = round($size / 1024, 2) . 'KB';
            }
        }
        return $str;
    }

    public function delAttachment() {
        $i = I('post.i', 0, 'intval');
        $j = I('post.j', 0, 'intval');
        if ($i) {
            $map['pid'] = $i;
            if ($j) {
                $map['id'] = $j;
            }
            $file = D('AttachmentUrl')->where($map)->find();
            if ($file['downloads'] > 0) {
                D('AttachmentUrl')->where($map)->save(array('status' => 0));
            } else {
                @unlink($_SERVER['DOCUMENT_ROOT'] . $file['url']);
                D('AttachmentUrl')->where($map)->delete();
            }

        }
        $this->ajaxReturn(array('status' => 1, 'msg' => '删除成功'));
    }

    public function downloadAttachment() {
        $i = I('get.i', 0, 'intval');
        $j = I('get.j', 0, 'intval');
        if (!$i) {
            $this->_404();
        }
        $file = D('AttachmentUrl')->where(array('pid' => $i, 'id' => $j, 'status' => 1))->find();
        if (!$file) {
            $this->_404();
        }
        $filename = $_SERVER['DOCUMENT_ROOT'] . $file['url'];
        if (!file_exists($filename)) {
            $this->_404();
        }
        // D('Attachment')->where(array('id' => $i))->save(array('downloads' => 'downloads+1'));
        $model = new \Think\Model();
        $model->query("update __ATTACHMENT__ set `downloads`=`downloads`+1 where id='{$i}' limit 1", false, false);
        D('AttachmentUrl')->where(array('pid' => $i, 'id' => $j, 'status' => 1))->save(array('downloads' => $file['downloads'] + 1));
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . $file['title']);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filename));
        ob_clean();
        flush();
        readfile($filename);
        ob_end_clean();
        echo '<script type="text/javascript">window.colse();</script>';
        exit();
    }
}
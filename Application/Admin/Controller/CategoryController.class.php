<?php
namespace Admin\Controller;

class CategoryController extends AuthController {
    public function _initialize() {
        parent::_initialize();
    }

    public function _before_index() {
        $this->assign('bar', array('curpos' => '分类列表', 'menu' => '产品分类', 'url' => U('Category/index')));
    }

    public function _before_add() {
        $this->assign('bar', array('curpos' => '添加分类', 'menu' => '产品分类', 'url' => U('Category/index')));
        $parents = D('Category')->toSelect();
        $this->assign('parents', $parents);
        $this->assign('types', D('Types')->toSelect());
        $this->assign('admins', D('Admin')->where(array('status' => 1))->getField('id, realname'));
    }

    public function _before_edit() {
        $this->assign('bar', array('curpos' => '添加分类', 'menu' => '产品分类', 'url' => U('Category/index')));
        $parents = D('Category')->toSelect();
        $this->assign('parents', $parents);
        $this->assign('types', D('Types')->toSelect());
        $this->assign('admins', D('Admin')->where(array('status' => 1))->getField('id, realname'));
    }
}
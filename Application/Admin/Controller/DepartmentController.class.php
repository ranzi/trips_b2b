<?php
namespace Admin\Controller;

class DepartmentController extends AuthController {
    public function _initialize() {
        parent::_initialize();
    }

    public function _before_index() {
        $this->assign('bar', array('curpos' => '部门列表', 'menu' => '部门管理', 'url' => U('Department/index')));
    }

    public function _before_add() {
        $this->assign('bar', array('curpos' => '添加部门', 'menu' => '部门管理', 'url' => U('Department/index')));

        $this->assign('depts', D('Department')->toSelect());
    }

    public function _before_edit() {
        $this->assign('bar', array('curpos' => '编辑部门', 'menu' => '部门管理', 'url' => U('Department/index')));

        $this->assign('depts', D('Department')->toSelect());
    }
}
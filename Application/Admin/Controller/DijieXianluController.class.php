<?php
/**
 * 地接线路-资源管理
 * liubi 2015-08-26
 */
namespace Admin\Controller;

class DijieXianluController extends AuthController {
    public function _initialize() {
        parent::_initialize();
    }

	public function index() {
		$Types	= D('Types');
        
        $map	= array(
        			'type'		=> 3,
        			'deletebs'	=> 0
        		);
        $field 	= 'id,type,title,ordid,status';
        $order  = array(
        			'ordid'	=> 'DESC',
        			'id'	=> 'DESC'
        		);
        $getlist 	= $Types->getList($Types, $map, $field, $order);
        
        $this->assign('bar', array('curpos' => '列表', 'menu' => '结算方式', 'url' => U('Settlementset/index')));
        $this->assign('list', $getlist['list']);
        $this->assign('page', $getlist['page']);
		$this->display();
	}

	public function add() {
		if (IS_POST) {
			$Types = D('Types');
			
			$data = array(
				'title'		=> I('post.title'),
				'ordid'		=> intval(I('post.ordid')),
				'type'		=> intval(I('post.type')),
				'status'	=> intval(I('post.status'))==1?:0,
			);
			$result = $Types->insert($data);

            if ($result['status'] == 1) {
                $this->success($result['msg'], U('Settlementset/index'));
            } else {
                $this->error($result['msg']);
            }
		}

		$this->assign('type',3);
    	$this->assign('bar', array('curpos' => '添加', 'menu' => '结算方式', 'url' => U('Settlementset/index')));
		$this->display();
	}

	public function edit() {
		$model = D('Types');
        if (IS_AJAX) {
            $field  = I('post.field', '', 'trim');
            $id     = I('post.id', '', 'trim');
            $pk     = I('post.pk', '', 'trim');
            $value  = I('post.value', '', 'trim');
            $status = 0;
            $msg    = '编辑失败';
            if ($field && $id & $pk && $value) {
                $status = 1;
                $msg    = '编辑成功';
                $model->where(array($pk => $id))->save(array($field => $value));
            }
            $this->ajaxReturn(array('msg' => $msg, 'status' => $status));
        } elseif (IS_POST) {
            $data = array(
                'title'         => I('post.title'),
                'ordid'         => intval(I('post.ordid')),
                'industryid'    => intval(I('post.industryid'))?:0,
                'status'        => intval(I('post.status'))==1?:0,
            );

            $result = $model->update($data);
            if ($result['status'] == 1) {
                $this->success($result['msg'], U(CONTROLLER_NAME . '/index'));
            } else {
                $this->error($result['msg']);
            }
        }

        $id = I('get.id', 0, 'intval');
        if (!$id) {
            $this->error('参数错误');
        }

        $data = array(
        	'id' => $id,
        	'deletebs' => 0,
        );

        $info = $model -> where($data) -> find();
        if (!$info) {
            $this->error('数据不存在');
        }
        $this->assign('info', $info);

        $this->display();
	}
	public function delete() {
		$id    = I('request.id');
        $model = D('Types');
        $pk    = I('request.pk', $model->getPk(), 'trim');
        if (!$id) {
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => '参数错误'));
            $this->error($this->error('参数错误'));
        }
        $id = explode(',', $id);
        foreach ($id as &$i) {
            $i = intval($i);
        }
        $data = array(
			'deletebs'=>1,
		);
        $model->where(array($pk => array('IN', implode(',', $id))))->limit(count($id))->save($data);
        IS_AJAX && $this->ajaxReturn(array('status' => 1, 'msg' => '删除1成功'));
        $this->success('删除成2功');
	}
}
<?php
namespace Admin\Controller;

class DistrictController extends AuthController {
    public function _initalize() {
        parent::_initalize();
    }

    public function _before_index() {
        $this->assign('bar', array('curpos' => '地区列表', 'menu' => '地区', 'url' => U('District/index')));
    }

    public function _before_add() {
        $this->assign('bar', array('curpos' => '地区列表', 'menu' => '地区', 'url' => U('District/index')));
        if (!IS_POST) {
            $this->assign('districts', D('District')->toTree());
        }
    }

    public function _before_edit() {
        $this->assign('bar', array('curpos' => '地区列表', 'menu' => '地区', 'url' => U('District/index')));
        if (!IS_POST) {
            $this->assign('districts', D('District')->toTree());
        }
    }
}
<?php
namespace Admin\Controller;

class ExampleController extends BaseController {

    public function _initialize() {
        parent::_initialize();
    }

    public function index() {
        $this->show('Examples');
    }

    public function download() {
        $data = array(
            array('id' => 1, 'name' => 'alex', 'age' => 23),
            array('id' => 2, 'name' => 'bill', 'age' => 32),
            array('id' => 3, 'name' => 'bob', 'age' => 33),
        );
        $title = array('编号', '姓名', '年龄');
        parent::export_csv($data, $title);
    }
}
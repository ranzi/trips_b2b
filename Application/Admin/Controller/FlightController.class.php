<?php
/**
 * 机票产品-资源管理
 */
namespace Admin\Controller;

class FlightController extends AuthController {
    public function _initialize() {
        parent::_initialize();
    }

	public function index() {
		$model	 = D('Flight');
        $map['le.deletebs'] = 0;
        $field 	 = 'le.id,le.departuretime,le.arrivaltime,le.flightnumber,le.departure_id,le.arrival_id,le.airline_id,le.seatnum,le.price,le.discount,le.industry_id,le.partners_id,le.stops,le.remark,le.ordid,le.status,le.confirm_bs,le.confirm_admin_id,le.confirm_time,p.title as t_partners_id';
        $order   = 'le.ordid desc,le.id';
        
        $sch = array();
        if(I('get.schbs')==1){
            $sch['schbs']         = I('get.schbs');
            $sch['flightnumber']  = I('get.flightnumber', '', 'trim');
            $sch['partners']      = I('get.partners', '', 'trim');
            $sch['departuretime'] = I('get.departuretime', '', 'trim');
            
            //保险名称
            if($sch['flightnumber']){
                $map['le.flightnumber'] = array('like','%'.$sch['flightnumber'].'%');
            }
            //合作单位
            if($sch['partners']){
                $partnerids = D('Partners')->get_partnerids_liketitle($sch['partners']);
                if($partnerids){
                    $map['le.partners_id'] = array('in',$partnerids);
                } else {
                    $map['le.partners_id'] = -1;
                }
            }
            if($sch['departuretime']){
                $map["DATE_FORMAT(FROM_UNIXTIME(departuretime),'%Y-%m-%d')"] = $sch['departuretime'];
            }
            /*
            if($sch['start_price']){
                if($sch['end_price']){
                    $map['price']  = array('between',$sch['start_price'].','.$sch['end_price']);
                }else{
                    $map['price']  = $sch['start_price'];
                    $sch['end_price'] = '';
                }
            } else if ($sch['end_price']){
                $map['price']  = array('between','0,'.$sch['end_price']);
                $sch['start_price'] = 0;
            } else {
                $sch['start_price'] = '';
                $sch['end_price'] = '';
            }
            */
        }

        $getlist = $model->getList($map, $field, $order);
        
        $this->assign('bar', array('curpos' => '列表', 'menu' => '机票产品', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('list', $getlist['list']);
        $this->assign('page', $getlist['page']);
        $this->assign('sch', $sch);
		$this->display();
	}

	public function add() {
		if (IS_POST) {
			if(intval(I('post.industry_id')) <= 0 || intval(I('post.partners_id')) <= 0){
                $this->error('请选择合作单位!');
            }
            if(strtotime(I('post.departuretime'))>=strtotime(I('post.arrivaltime'))){
                $this->error('到达时间不能<=出发时间!');
            }

            $Flight = D('Flight');
			
			$data = array(
				'departuretime' => strtotime(I('post.departuretime')),
                'arrivaltime'   => strtotime(I('post.arrivaltime')),
                'flightnumber'  => I('post.flightnumber', '', 'trim'),
                'departure_id'  => I('post.departure_id', 0, 'intval'),
                'arrival_id'    => I('post.arrival_id', 0, 'intval'),
                'airline_id'    => I('post.airline_id', 0, 'intval'),
                'seatnum'       => I('post.seatnum', 0, 'intval'),
                'price'         => I('post.price', 0, 'floatval'),
                'discount'      => I('post.discount', 0, 'floatval'),
                'industry_id'   => I('post.industry_id', 0, 'intval'),
                'partners_id'   => I('post.partners_id', 0, 'intval'),
                'stops'         => I('post.stops', '', 'trim'),
                'remark'        => I('post.remark', '', 'trim'),
				'ordid'		    => I('post.ordid', 0, 'intval'),
				'admin_id'		=> session('admin_id'),
				'status'	    => I('post.status')==1?:0,
			);

			$result = $Flight->_add($data);

            if ($result['status'] == 1) {
                $this->success($result['msg'], U(CONTROLLER_NAME . '/index'));
            } else {
                $this->error($result['msg']);
            }
		}

		$types_data = D('Types')->get_typesdata(array('type' => 'industry'));
    	$this->assign('bar', array('curpos' => '添加', 'menu' => '机票产品', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('industrys', $types_data['industry']);
		$this->display();
	}

	public function edit() {
		$model = D('Flight');
        if (IS_AJAX) {
            $field  = I('post.field', '', 'trim');
            $id     = I('post.id', '', 'trim');
            $pk     = I('post.pk', '', 'trim');
            $value  = I('post.value', '', 'trim');
            $status = 0;
            $msg    = '编辑失败';
            if ($field && $id & $pk && $value) {
                $status = 1;
                $msg    = '编辑成功';
                $model->where(array($pk => $id))->save(array($field => $value));
            }
            $this->ajaxReturn(array('msg' => $msg, 'status' => $status));
        } elseif (IS_POST) {
            if (!I('post.id')) {
                return array('status' => 0, 'msg' => '传参有误!');
            }
            if(intval(I('post.industry_id')) <= 0 || intval(I('post.partners_id')) <= 0){
                $this->error('请选择合作单位!');
            }
            if(strtotime(I('post.departuretime'))>=strtotime(I('post.arrivaltime'))){
                $this->error('到达时间不能<=出发时间!');
            }
            $data = array(
                'id'            => I('post.id', 0, 'intval'),
                'departuretime' => strtotime(I('post.departuretime')),
                'arrivaltime'   => strtotime(I('post.arrivaltime')),
                'flightnumber'  => I('post.flightnumber', '', 'trim'),
                'departure_id'  => I('post.departure_id', 0, 'intval'),
                'arrival_id'    => I('post.arrival_id', 0, 'intval'),
                'airline_id'    => I('post.airline_id', 0, 'intval'),
                'seatnum'       => I('post.seatnum', 0, 'intval'),
                'price'         => I('post.price', 0, 'floatval'),
                'discount'      => I('post.discount', 0, 'floatval'),
                'industry_id'   => I('post.industry_id', 0, 'intval'),
                'partners_id'   => I('post.partners_id', 0, 'intval'),
                'stops'         => I('post.stops', '', 'trim'),
                'remark'        => I('post.remark', '', 'trim'),
                'ordid'         => I('post.ordid', 0, 'intval'),
                'admin_id'      => session('admin_id'),
                'status'        => I('post.status')==1?:0,
            );
            /**
             * 当控位数改变时要记录到位子日志表(未处理)
             */
            $result = $model->_save($data);
            if ($result['status'] == 1) {
                $this->success($result['msg'], U(CONTROLLER_NAME . '/edit?id='.I('post.id')));
            } else {
                $this->error($result['msg']);
            }
        }

        $id = I('get.id', 0, 'intval');
        if (!$id) {
            $this->error('参数错误');
        }

        $data = array(
        	'id' => $id,
        	'deletebs' => 0,
        );

        $info = $model -> where($data) -> find();
        if (!$info) {
            $this->error('数据不存在');
        }

        $types_data = D('Types')->get_typesdata(array('type' => 'industry'));
        $this->assign('bar', array('curpos' => '编辑', 'menu' => '机票产品', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('industrys', $types_data['industry']);
        $this->assign('info', $info);
        $this->display();
	}
	public function delete() {
		/**
         * 删除航班产品之前需要检查当前位子是否被分配使用，如果已分配则提示取消分配后再删除(未处理)
         */
        $id    = I('request.id');
        $model = M('Flight');
        if (!$id) {
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => '参数错误'));
            $this->error($this->error('参数错误'));
        }
        $id = explode(',', $id);
        foreach ($id as &$i) {
            $i = intval($i);
        }
        $data = array(
            'deletebs' => 1,
            'admin_id' => session('admin_id'),
            'addtime'  => NOW_TIME,
        );
        
        $model->where(array('id' => array('IN', implode(',', $id))))->limit(count($id))->save($data);
        IS_AJAX && $this->ajaxReturn(array('status' => 1, 'msg' => '删除成功'));
        $this->success('删除成功');
	}
}
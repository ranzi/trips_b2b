<?php
/**
 *@author qiisyhx<sihangshi2011@gmail.com>
 *@version $Id 2015-09-19
 *少年不识愁滋味
 *爱上层楼
 *爱上层楼
 *为赋新词强说愁
 *而今识尽愁滋味
 *欲说还休
 *欲说还休
 *却道天凉好个秋
 **/
namespace Admin\Controller;

class FlowController extends AuthController {
    public function _initialize() {
        parent::_initialize();
    }

    public function _before_index() {
        $this->assign('bar', array('curpos' => '流程列表', 'menu' => '流程管理', 'url' => U(CONTROLLER_NAME . '/index')));
    }

    public function _before_add() {
        $this->assign('bar', array('curpos' => '新建流程', 'menu' => '流程管理', 'url' => U(CONTROLLER_NAME . '/index')));
        $tables = D('Tables')->getAll(array('status' => 1));
        $this->assign('tables', $tables);
    }

    public function _before_edit() {
        if (!IS_POST) {
            $this->assign('bar', array('curpos' => '编辑流程', 'menu' => '流程管理', 'url' => U(CONTROLLER_NAME . '/index')));
            $tables = D('Tables')->getAll(array('status' => 1));
            $this->assign('tables', $tables);
            $fid   = I('get.id', 0, 'intval');
            $steps = D('FlowStep')->getAll(array('fid' => $fid));
            if ($steps) {
                $groups = D('Group')->toSelect(array('status' => 1));
                $depts  = D('Department')->toSelect(array('status' => 1));
                $users  = D('Admin')->field("id,CASE WHEN realname<>'' THEN realname ELSE username END name")->where(array('status' => 1))->select();
                foreach ($steps as &$step) {
                    if ($step['groups']) {
                        $tmp    = array();
                        $_group = explode(',', $step['groups']);
                        foreach ($groups as $id => $title) {
                            if (in_array($id, $_group)) {
                                $tmp[] = $title;
                            }
                        }
                        $step['show_groups'] = implode(',', $tmp);
                    }
                    if ($step['depts']) {
                        $tmp    = array();
                        $_depts = explode(',', $step['depts']);
                        foreach ($depts as $id => $title) {
                            if (in_array($id, $_depts)) {
                                $tmp[] = $title;
                            }
                        }
                        $step['show_depts'] = implode(',', $tmp);
                    }
                    if ($step['users']) {
                        $tmp    = array();
                        $_users = explode(',', $step['users']);
                        foreach ($users as $id => $row) {
                            if (in_array($row['id'], $_users)) {
                                $tmp[] = $row['name'];
                            }
                        }
                        $step['show_users'] = implode(',', $tmp);
                    }
                    if ($step['types'] == 'ACTIVE') {
                        $step['show_types'] = '正常';
                    } elseif ($step['types'] == 'COMPLETE') {
                        $step['show_types'] = '完成';
                    }
                }
            }
            $this->assign('steps', $steps);
        }
    }

    public function getPriv() {
        $type = I('post.type', '', 'trim');
        if (!in_array($type, array('user', 'group', 'dept'))) {
            $type = 'user';
        }
        if ('user' == $type) {
            $data = D('Admin')->field("id,CASE WHEN realname<>'' THEN realname ELSE username END name, 0 as pid")->where(array('status' => 1))->select();
        } elseif ('group' == $type) {
            $data = D('Group')->field('id,title as name,0 as pid')->where(array('status' => 1))->select();
        } elseif ('dept' == $type) {
            $data = D('Department')->field('id,title as name,pid')->where(array('status' => 1))->select();
        }
        $this->ajaxReturn(array('status' => 1, 'data' => $data));
    }

    public function addStep() {
        $sid  = I('post.sid', 0, 'intval');
        $data = D('FlowStep')->create(I('post.'));
        if (false !== $data) {
            if ($sid) {
                D('FlowStep')->where(array('sid' => $sid))->save();
            } else {
                $sid         = D('FlowStep')->add();
                $data['sid'] = $sid;
            }
            $this->ajaxReturn(array('status' => 1, 'data' => $data));
        }
        $this->ajaxReturn(array('status' => 0, 'msg' => D('FlowStep')->getError()));
    }

    public function delStep() {
        $sid = I('sid', 0, 'intval');
        $fid = I('fid', 0, 'intval');
        if ($sid && $fid) {
            D('FlowStep')->where(array('fid' => $fid, 'sid' => $sid))->limit(1)->delete();
            D('FlowStepButton')->where(array('fid' => $fid, 'sid' => $sid))->delete();
        }
        $this->ajaxReturn(array('status' => 1, 'msg' => '删除成功'));
    }

    public function buttons() {
        $fid = I('get.fid', 0, 'intval');
        $sid = I('get.sid', 0, 'intval');
        if (!$fid || !$sid) {
            $this->error('参数错误');
        }
        $steps = D('FlowStep')->where(array('fid' => $fid))->select();
        $this->assign('steps', $steps);
        $flow = D('Flow')->get($fid);
        $this->assign('flow', $flow);
        $item = array();
        foreach ($steps as $step) {
            if ($step['sid'] == $sid) {
                $item = $step;
                break;
            }
        }
        $this->assign('item', $item);
        $buttons = D("FlowStepButton")->getAll(array('fid' => $fid, 'sid' => $sid));
        $this->assign('buttons', $buttons);
        $this->assign('bar', array('curpos' => '流程关系配置', 'menu' => '流程管理', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->display();
    }

    public function addButton() {
        $model = D('FlowStepButton');
        $data  = $model->create(I('post.'));
        if (false === $data) {
            $this->ajaxReturn(array('status' => 0, 'msg' => $model->getError()));
        } else {
            $id = I('post.id', 0, 'intval');
            if ($id) {
                $model->where(array('id' => $id))->save();
            } else {
                $id         = $model->add();
                $data['id'] = $id;
            }
            $this->ajaxReturn(array('status' => 1, 'msg' => '添加成功', 'data' => $data));
        }
    }

    public function deleteButtons() {
        $fid = I('post.fid', 0, 'intval');
        $sid = I('post.sid', 0, 'intval');
        $id  = I('post.id', 0, 'intval');
        if (!($fid && $sid && $id)) {
            $this->ajaxReturn(array('status' => 0, 'msg' => '参数错误'));
        } else {
            D('FlowStepButton')->where(array('fid' => $fid, 'sid' => $sid, 'id' => $id))->limit(1)->delete();
            $this->ajaxReturn(array('status' => 1, 'msg' => '删除成功'));
        }
    }

    public function fields() {
        if (IS_POST) {
            $fid    = I('post.fid', 0, 'intval');
            $sid    = I('post.sid', 0, 'intval');
            $tid    = I('post.tid', 0, 'intval');
            $fields = I('post.fields', array());
            $status = I('post.status', array());
            $ids    = I('post.id', array());
            $isshow = I('post.isshow', array());
            $model  = D('FlowStepFields');
            foreach ($fields as $key => $val) {
                $data = array(
                    'sid'    => $sid,
                    'tid'    => $tid,
                    'fid'    => $fid,
                    'fields' => $val,
                    'status' => $status[$key],
                    'isshow' => $isshow[$key],
                );
                $id = $ids[$key];
                if ($id) {
                    $model->where(array('id' => $id))->save($data);
                } else {
                    $model->add($data);
                }
            }
            $this->success('编辑成功', U('Flow/fields', array('fid' => $fid, 'sid' => $sid)));
        }
        $fid = I('get.fid', 0, 'intval');
        $sid = I('get.sid', 0, 'intval');
        if (!$fid || !$sid) {
            $this->error('参数错误');
        }
        $flow = D('Flow')->get($fid);
        if (!$flow) {
            $this->error('流程错误');
        }
        if (!$flow['tid']) {
            $this->error('业务表未配置');
        }
        $this->assign('sid', $sid);
        $this->assign('flow', $flow);
        $table = D('Tables')->get($flow['tid']);
        $this->assign('table', $table);
        $fields = D('TablesFields')->getAll(array('tid' => $flow['tid'], 'status' => 1));
        $this->assign('fields', $fields);
        $this->assign('bar', array('curpos' => '流程字段管理', 'menu' => '流程管理', 'url' => U(CONTROLLER_NAME . '/index')));
        $fsfs = D('FlowStepFields')->getAll(array('fid' => $fid, 'sid' => $sid));
        $temp = array();
        foreach ($fsfs as $row) {
            $temp[$row['fields']] = $row;
        }
        $fsfs = $temp;
        unset($temp);
        $this->assign('fsfs', $fsfs);
        $this->display();
    }
}
<?php
namespace Admin\Controller;

class GradeController extends AuthController {
    public function __construct() {
        parent::__construct();
    }

    public function _initialize() {
        parent::_initialize();
    }

    public function _before_index() {
        $this->assign('bar', array('curpos' => '价格等级列表', 'menu' => '价格等级管理', 'url' => U('Grade/index')));
    }
}
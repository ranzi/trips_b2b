<?php
namespace Admin\Controller;

class GrantCateController extends AuthController {
    public function _initialize() {
        parent::_initialize();
    }

    public function index() {
        if (IS_POST) {
            $cates = I('post.cates', '', 'trim');
            $cates = $cates ? json_decode($cates, true) : array();
            $type  = I('post.auth_type', '', 'trim');
            $id    = I('post.id', 0, 'intval');
            if (!in_array($type, array('user', 'department')) || !$id) {
                $this->error('参数错误');
            }
            $model = D('GrantCate');
            $model->where(array('grant_to' => $id, 'type' => $type))->delete();
            foreach ($cates as $row) {
                $data = array(
                    'grant_to' => $id,
                    'type'     => $type,
                    'type_id'  => $row['type_id'],
                    'cate_id'  => $row['id'],
                );
                $model->add($data);
            }
            $this->success('保存成功');
        }
        $this->assign('bar', array('curpos' => '产品授权'));
        $id   = I('get.id', 0, 'intval');
        $type = I('get.type', '', 'trim');
        if (!$id || !in_array($type, array('user', 'department'))) {
            $this->error('参数错误');
        }
        if ($type == 'department') {
            $info = D('department')->where(array('id' => $id))->find();
        } elseif ($type == 'user') {
            $info = D('Admin')->get($id);
        }
        $auths  = D('GrantCate')->field('type_id, cate_id')->where(array('type' => $type, 'grant_to' => $id))->select();
        $gtypes = $gcates = array();
        if ($auths) {
            foreach ($auths as $row) {
                $gtypes[] = $row['type_id'];
                $gcates[] = $row['cate_id'];
            }
        }
        $typeids = D('Types')->field('id,title')->where(array('status' => 1, 'type' => 0))->select();
        $cates   = D('Category')->field('id, title as name, type_id, pid')->select();

        $temp = array();
        if ($cates) {
            foreach ($cates as $row) {
                if (in_array($row['id'], $gcates)) {
                    $row['checked'] = true;
                }
                $temp[$row['type_id']][] = $row;
            }
        }
        $cates = $temp;
        unset($temp, $gcates, $gtypes);
        $this->assign('cates', $cates);
        // $this->assign('gtypes', $gtypes);
        // $this->assign('gcates', $gcates);
        $this->assign('typeids', $typeids);
        $this->assign('auth_type', $type);
        $this->assign('info', $info);
        $this->display();
    }
}
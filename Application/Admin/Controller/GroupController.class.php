<?php
/**
 *@author Chunlei Wang <sihangshi2011@gmail.com>
 *
 **/
namespace Admin\Controller;

class GroupController extends AuthController {
    public function _initialize() {
        parent::_initialize();
    }

    public function _before_index() {
        $this->assign('bar', array('curpos' => '用户组列表', 'menu' => '用户组', 'url' => U('Group/index')));
    }

    public function _before_add() {
        $this->assign('bar', array('curpos' => '添加用户组', 'menu' => '用户组', 'url' => U('Group/index')));
    }

    public function _before_edit() {
        $this->assign('bar', array('curpos' => '添加用户组', 'menu' => '用户组', 'url' => U('Group/index')));
    }
}
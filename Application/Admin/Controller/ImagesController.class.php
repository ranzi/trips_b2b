<?php
/**
 *desc: images upload and manager
 *@author qiisyhx<sihangshi2011@gmail.com>
 *@version $Id 2015-09-09
 *林花谢了春红
 *太匆匆
 *无奈朝来寒雨、晚来风
 *胭脂泪
 *相留醉
 *几时重
 *人生自是长恨、水长东
 **/
namespace Admin\Controller;

class ImagesController extends AuthController {
    public function _initialize() {
        parent::_initialize();
    }

    public function _before_index() {
        $this->assign('bar', array('curpos' => '图片列表', 'menu' => '图片管理', 'url' => U('Images/index')));
    }

    public function _before_add() {
        if (IS_POST) {
            $images = D('Images');
            $data   = $images->create(I('post.'));
            $id     = $images->add();
            if ($id) {
                $imgs = I('post.imgs', array());
                foreach ($imgs as $item) {
                    $sub = array(
                        'pid'      => $id,
                        'src'      => preg_replace("/\_\d{1,}x\d{1,}(?=\.(jpg|gif|png|jpeg))/", "", $item),
                        'thumb'    => $item,
                        'addtime'  => NOW_TIME,
                        'admin_id' => session('admin_id'),
                    );
                    D('ImagesSrc')->add($sub);
                }
            }
            $this->success('添加图集成功', U('Images/index'));
        }
        $this->assign('bar', array('curpos' => '添加图片', 'menu' => '图片管理', 'url' => U('Images/index')));
        $types = D('Zsysdata')->toSelect(array('type' => 'images'));
        $this->assign('types', $types);
        $district = D('District')->toTree(array('status' => 1));
        $this->assign('district', $district);
        $authCates = $this->getAuthCategory();
        $cates     = D('Category')->toTree(array('id' => array('IN', $authCates)));
        $this->assign('cates', $cates);
    }

    public function _before_edit() {
        if (IS_POST) {
            $images = D('Images');
            $data   = $images->create(I('post.'));
            $id     = I('post.id', 0, 'intval');
            $images->where(array('id' => $id))->save();
            $imgs = I('post.imgs', array());
            foreach ($imgs as $item) {
                $sub = array(
                    'pid'      => $id,
                    'src'      => preg_replace("/\_\d{1,}x\d{1,}(?=\.(jpg|gif|png|jpeg))/", "", $item),
                    'thumb'    => $item,
                    'addtime'  => NOW_TIME,
                    'admin_id' => session('admin_id'),
                );
                D('ImagesSrc')->add($sub);
            }

            $this->success('编辑图集成功', U('Images/index'));
        }
        $this->assign('bar', array('curpos' => '编辑图片', 'menu' => '图片管理', 'url' => U('Images/index')));
        $types = D('Zsysdata')->toSelect(array('type' => 'images'));
        $this->assign('types', $types);
        $id    = I('get.id', 0, 'intval');
        $ilist = D('ImagesSrc')->getAll(array('pid' => $id));
        $this->assign('ilist', $ilist);
        $district = D('District')->toTree(array('status' => 1));
        $this->assign('district', $district);
        $authCates = $this->getAuthCategory();
        $cates     = D('Category')->toTree(array('id' => array('IN', $authCates)));
        $this->assign('cates', $cates);
    }

    /**
     *desc: delete images from album
     **/
    public function delImages() {
        $id  = I('post.id', 0, 'intval');
        $pid = I('post.pid', 0, 'intval');
        if ($id && $pid) {
            // $img = D('ImagesSrc')->where(array('id' => $id, 'pid' => $pid))->find();
            // @unlink(THINK_PATH . '../' . $img['src']);
            // @unlink(THINK_PATH . '../' . $img['thumb']);
            D('ImagesSrc')->where(array('id' => $id, 'pid' => $pid))->delete();
        }
        IS_AJAX && $this->ajaxReturn(array('status' => 1, 'msg' => '删除图片成功'));
        $this->success('删除图片成功');
    }
}
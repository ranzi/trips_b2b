<?php
namespace Admin\Controller;

class IndexController extends AuthController {
    public function __construct() {
        parent::__construct();
    }

    public function _initialize() {
        parent::_initialize();
    }

    public function index() {
        $this->assign('bar', array('curpos' => '控制台'));
        $this->display();
    }
}
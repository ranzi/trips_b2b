<?php
/**
 * 保险产品-资源管理
 */
namespace Admin\Controller;

class InsuranceController extends AuthController {
    public function _initialize() {
        parent::_initialize();
    }

	public function index() {
        $model  = D('Insurance');
        $map['le.deletebs'] = 0;

        $field  = 'le.id,le.title,le.timelimit,le.price,le.industry_id,le.partners_id,le.ordid,le.status,p.title as t_partners_id';
        $order  = array();
        
        $sch = array();
        if(I('get.schbs')==1){
            $sch['schbs']       = I('get.schbs');
            $sch['title']       = I('get.title', '', 'trim');
            $sch['partners']    = I('get.partners', '', 'trim');
            $sch['start_price'] = I('get.start_price', '', 'floatval');
            $sch['end_price']   = I('get.end_price', '', 'floatval');
            
            //保险名称
            if($sch['title']){
                $map['le.title'] = array('like','%'.$sch['title'].'%');
            }
            //合作单位
            if($sch['partners']){
                $partnerids = D('Partners')->get_partnerids_liketitle($sch['partners']);
                if($partnerids){
                    $map['le.partners_id'] = array('in',$partnerids);
                } else {
                    $map['le.partners_id'] = -1;
                }
            }
            if($sch['start_price']){
                if($sch['end_price']){
                    $map['price']  = array('between',$sch['start_price'].','.$sch['end_price']);
                }else{
                    $map['price']  = $sch['start_price'];
                    $sch['end_price'] = '';
                }
            }else if($sch['end_price']){
                $map['price']  = array('between','0,'.$sch['end_price']);
                $sch['start_price'] = 0;
            } else {
                $sch['start_price'] = '';
                $sch['end_price'] = '';
            }
        }


        $getlist    = $model->getList($map, $field, $order);
        
        $this->assign('bar', array('curpos' => '列表', 'menu' => '保险产品', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('list', $getlist['list']);
        $this->assign('page', $getlist['page']);
        $this->assign('sch', $sch);
        $this->display();
    }

    public function add() {
        if (IS_POST) {
            if(intval(I('post.industry_id')) <= 0 || intval(I('post.partners_id')) <= 0){
                $this->error('请选择合作单位!');
            }
            $model  = D('Insurance');
            $data = array(
                'title'         => I('post.title', '', 'trim'),
                'intro'         => I('post.intro', '', 'trim'),
                'explain'       => I('post.explain', '', 'trim'),
                'timelimit'     => I('post.timelimit', '', 'trim'),
                'industry_id'   => I('post.industry_id', 0, 'intval'),
                'partners_id'   => I('post.partners_id', 0, 'intval'),
                'price'         => I('post.price', 0, 'floatval'),
                'remark'        => I('post.remark', '', 'trim'),
                'ordid'         => I('post.ordid', 0, 'intval'),
                'admin_id'      => session('admin_id'),
                'status'        => I('post.status')==1?:0,
            );

            $result = $model->_add($data);

            if ($result['status'] == 1 && $result['id']) {
                //新增成本价格行
                $starttime  = I('post.starttime', array());
                $endtime    = I('post.endtime', array());
                $cost_price = I('post.cost_price', array());
                
                if(is_array($starttime)){
                    foreach ($starttime as $key => $val) {
                        if($val == ''){
                            continue;
                        }

                        $data = array(
                            'insurance_id' => $result['id'],
                            'starttime'    => strtotime($starttime[$key]),
                            'endtime'      => strtotime($endtime[$key]),
                            'cost_price'   => floatval($cost_price[$key]),
                        );
                        D('InsurancePrice')->_add($data);
                    }
                }
                $this->success($result['msg'], U(CONTROLLER_NAME . '/index'));
            } else {
                $this->error($result['msg']);
            }
        }
        $types_data = D('Types')->get_typesdata(array('type' => 'industry'));
        $this->assign('bar', array('curpos' => '添加', 'menu' => '保险产品', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('industrys', $types_data['industry']);
        $this->display();
    }

    public function edit() {
        $model = D('Insurance');
        if (IS_AJAX) {
            $field  = I('post.field', '', 'trim');
            $id     = I('post.id', '', 'trim');
            $pk     = I('post.pk', '', 'trim');
            $value  = I('post.value', '', 'trim');
            $status = 0;
            $msg    = '编辑失败';
            if ($field && $id & $pk && $value) {
                $status = 1;
                $msg    = '编辑成功';
                $model->where(array($pk => $id))->save(array($field => $value));
            }
            $this->ajaxReturn(array('msg' => $msg, 'status' => $status));
        } elseif (IS_POST) {
            if (!I('post.id')) {
                return array('status' => 0, 'msg' => '传参有误!');
            }
            if(intval(I('post.industry_id')) <= 0 || intval(I('post.partners_id')) <= 0){
                $this->error('请选择合作单位!');
            }
            $data = array(
                'id'            => I('post.id', 0, 'intval'),
                'title'         => I('post.title', '', 'trim'),
                'intro'         => I('post.intro', '', 'trim'),
                'explain'       => I('post.explain', '', 'trim'),
                'timelimit'     => I('post.timelimit', '', 'trim'),
                'industry_id'   => I('post.industry_id', 0, 'intval'),
                'partners_id'   => I('post.partners_id', 0, 'intval'),
                'price'         => I('post.price', 0, 'floatval'),
                'remark'        => I('post.remark', '', 'trim'),
                'ordid'         => I('post.ordid', 0, 'intval'),
                'admin_id'      => session('admin_id'),
                'status'        => I('post.status')==1?:0,
            );
            $result = $model->_save($data);
            if ($result['status'] == 1) {
                $this->success($result['msg'], U(CONTROLLER_NAME . '/edit?id='.I('post.id')));
            } else {
                $this->error($result['msg']);
            }
        }

        $id = I('get.id', 0, 'intval');
        if (!$id) {
            $this->error('参数错误');
        }

        $data = array(
            'id' => $id,
            'deletebs' => 0,
        );

        $info = $model -> where($data) -> find();
        if (!$info) {
            $this->error('数据不存在');
        }
        $types_data = D('Types')->get_typesdata(array('type' => 'industry'));
        $this->assign('bar', array('curpos' => '编辑', 'menu' => '保险产品', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('industrys', $types_data['industry']);
        $this->assign('info', $info);
        $this->display();
    }
    public function delete() {
        $id    = I('request.id');
        $model = M('Insurance');
        if (!$id) {
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => '参数错误'));
            $this->error($this->error('参数错误'));
        }
        $id = explode(',', $id);
        foreach ($id as &$i) {
            $i = intval($i);
        }
        $data = array(
            'deletebs' => 1,
            'admin_id' => session('admin_id'),
            'addtime'  => NOW_TIME,
        );
        
        $model->where(array('id' => array('IN', implode(',', $id))))->limit(count($id))->save($data);
        IS_AJAX && $this->ajaxReturn(array('status' => 1, 'msg' => '删除成功'));
        $this->success('删除成功');
    }

    public function price() {
        if (IS_POST) {
            //删除价格行
            if(I('post.deleteprice')==1){
                $flag = M('insurance_price')->where(array('id' => I('post.id'), 'insurance_id' => I('post.insurance_id')))->save(array('deletebs'=>1));
                if($flag){
                    IS_AJAX && $this->ajaxReturn(array('status' => 1, 'msg' => '删除成功'));
                    $this->success('删除成功');
                }else{
                    IS_AJAX && $this->ajaxReturn(array('status' => -1, 'msg' => '删除失败'));
                    $this->success('删除失败');
                }
            }

            //新增修改价格行
            $model = D('InsurancePrice');

            $starttime           = I('post.starttime', array());
            $endtime             = I('post.endtime', array());
            //$market_price        = I('post.market_price', array());
            //$price               = I('post.price', array());
            $cost_price          = I('post.cost_price', array());
            $insurance_id        = I('post.insurance_id', 0, 'intval');
            $edit_starttime      = I('post.edit_starttime', array());
            $edit_endtime        = I('post.edit_endtime', array());
            //$edit_market_price   = I('post.edit_market_price', array());
            //$edit_price          = I('post.edit_price', array());
            $edit_cost_price     = I('post.edit_cost_price', array());

            if (!$insurance_id) {
                $this->error('传参错误');
            }
            
            //修改价格行
            if(is_array($edit_starttime)){
                foreach($edit_starttime as $key => $val){
                    if(strtotime($edit_starttime[$key]) >= strtotime($edit_endtime[$key]) || $edit_starttime[$key]==''){
                        continue;
                    }
                    $data = array(
                        'id'            => $key,
                        'starttime'     => strtotime($edit_starttime[$key]),
                        'endtime'       => strtotime($edit_endtime[$key]),
                        'cost_price'    => floatval($edit_cost_price[$key]),
                        //'market_price'  => floatval($edit_market_price[$key]),
                        //'price'         => floatval($edit_price[$key]),
                    );
                    $model -> _save($data);
                }
            }

            //新增
            if(is_array($starttime)){
                foreach ($starttime as $key => $val) {
                    if(strtotime($starttime[$key]) >= strtotime($endtime[$key]) || $starttime[$key]==''){
                        continue;
                    }
                    
                    //新增开始日期需与已有最大的结束日期相接 如最大的结束日期为 2015-09-30 则 新增开始日期只能为 2015-10-01
                    //当前未做完整性处理

                    $data = array(
                        'insurance_id'  => $insurance_id,
                        'starttime'     => strtotime($starttime[$key]),
                        'endtime'       => strtotime($endtime[$key]),
                        'cost_price'    => floatval($cost_price[$key]),
                        //'market_price'  => floatval($market_price[$key]),
                        //'price'         => floatval($price[$key]),   
                    );
                    $model->_add($data);
                }
            }
            $this->success('保存成功', U(CONTROLLER_NAME . '/price?id='.$insurance_id));
        }

        $this->assign('bar', array('curpos' => '保险价格', 'menu' => '保险产品', 'url' => U(CONTROLLER_NAME . '/index')));
        $id = I('get.id', 0, 'intval');
        if (!$id) {
            $this->error('传参错误');
        }
        $insurance = M('insurance') -> where(array('id' => $id,'deletebs' => 0)) -> find();
        if (!$insurance) {
            $this->error('主题不存在');
        }
        $list = D('InsurancePrice') -> getpricelist(array('insurance_id' => $id, 'deletebs' => 0));
        $this->assign('list', $list);
        $this->assign('insurance', $insurance);
        $this->display();
    }
}
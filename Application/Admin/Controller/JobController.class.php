<?php
namespace Admin\Controller;

class JobController extends AuthController {
    public function _initialize() {
        parent::_initialize();
    }

    public function _before_index() {
        $this->assign('bar', array('curpos' => '职务列表', 'menu' => '职务管理', 'url' => U('Department/index')));
    }

    public function _before_add() {
        $this->assign('bar', array('curpos' => '添加职务', 'menu' => '职务管理', 'url' => U('Department/index')));
    }

    public function _before_edit() {
        $this->assign('bar', array('curpos' => '编辑职务', 'menu' => '职务管理', 'url' => U('Department/index')));
    }
}
<?php
namespace Admin\Controller;

class LineController extends AuthController {
    public function _initialize() {
        parent::_initialize();
    }

    public function _before_index() {
        $this->assign('bar', array('curpos' => '线路列表', 'menu' => '线路管理', 'url' => U('Line/index')));
    }

    public function _before_add() {
        if (IS_POST) {
            $line   = D('line');
            $expand = D('LineExpand');
            $trips  = D('LineTrips');
            $prices = D('LinePrices');
            $lattrs = D('LineAttr');

            $data_line = array(
                'title'       => I('post.title', '', 'trim'),
                'subtitle'    => I('post.subtitle', '', 'trim'),
                'img'         => I('post.img', '', 'trim'),
                'status'      => I('post.status') == 1 ? 1 : 0,
                'ordid'       => I('post.ordid', 0, 'intval'),
                'isnew'       => I('post.isnew') == 1 ? 1 : 0,
                'type_id'     => I('post.type_id', 0, 'intval'),
                'cate_id'     => I('post.cate_id', 0, 'intval'),
                'ishot'       => I('post.ishot') == 1 ? 1 : 0,
                'ispromotion' => I('post.ispromotion') == 1 ? 1 : 0,
                'iscoupon'    => I('post.iscoupon') == 1 ? 1 : 0,
                'allowpay'    => I('post.allowpay') == 1 ? 1 : 0,
                'buycounts'   => 0,
                'cmts'        => 0,
                'hits'        => 0,
                'badminid'    => session('admin_id'),
                'bdeptid'     => session('admin.dept_id'),
                'departure'   => I('post.departure', '', 'trim'),
                'arrive'      => I('post.arrive', '', 'trim'),
                'attachment'  => I('post.attachment', 0, 'intval'),
            );
            if ($data_line['iscoupon'] == 1) {
                $data_line['begintime'] = I('post.begintime', NOW_TIME, 'strtotime');
                $data_line['endtime']   = I('post.endtime', 0, 'strtotime');
            } else {
                $data_line['begintime'] = 0;
                $data_line['endtime']   = 0;
            }
            $lid = $line->add($data_line);
            if ($lid > 0) {
                $trips_data = I('post.trips', array());
                $count_days = 0;
                foreach ($trips_data['days'] as $day) {
                    $row = array(
                        'lid'       => $lid,
                        'days'      => $day,
                        'title'     => $trips_data['title'][$day],
                        'breakfast' => $trips_data['breakfast'][$day] == 1 ? 1 : 0,
                        'lunch'     => $trips_data['lunch'][$day] == 1 ? 1 : 0,
                        'dinner'    => $trips_data['dinner'][$day] == 1 ? 1 : 0,
                        'contents'  => $trips_data['contents'][$day],
                        'hotel'     => $trips_data['hotel'][$day],
                        'trips_img' => implode(',', $trips_data['trips_img'][$day]),
                        'hotel_img' => implode(',', $trips_data['hotel_img'][$day]),
                        'eat_img'   => implode(',', $trips_data['eat_img'][$day]),
                    );
                    if ($row['title']) {
                        $count_days++;
                        $trips->add($row);
                    }
                }
                $line->where(array('id' => $lid))->limit(1)->save(array('days' => $count_days));

                $expand_data = array(
                    'lid'         => $lid,
                    'contents'    => I('post.contents', '', 'trim'),
                    'imgs'        => implode(',', I('post.imgs', array())),
                    'feature'     => I('post.feature', '', 'trim'),
                    'feeinclude'  => I('post.feeinclude', '', 'trim'),
                    'feeexclude'  => I('post.feeexclude', '', 'trim'),
                    'notice'      => I('post.notice', '', 'trim'),
                    'tips'        => I('post.tips', '', 'trim'),
                    'children'    => I('post.children', '', 'trim'),
                    'shopping'    => I('post.shopping', '', 'trim'),
                    'optional'    => I('post.optional', '', 'trim'),
                    'gift'        => I('post.gift', '', 'trim'),
                    'remark'      => I('post.remark', '', 'trim'),
                    'together'    => I('post.together', '', 'trim'),
                    'place'       => I('post.place', '', 'trim'),
                    'local_man'   => I('post.local_man', '', 'trim'),
                    'open_man'    => I('post.open_man', '', 'trim'),
                    'open_mobile' => I('post.open_mobile', '', 'trim'),
                    'dateline'    => I('post.dateline', NOW_TIME, 'strtotime'),
                );
                $expand->add($expand_data);
                $_prices = I('post.prices', array());
                $prices->where(array('lineid' => $lid))->delete();
                foreach ($_prices['starttime'] as $key => $item) {
                    $price = array(
                        'lineid'           => $lid,
                        'starttime'        => strtotime($item),
                        'endtime'          => strtotime($_prices['endtime'][$key]),
                        'gradeid'          => $_prices['gradeid'][$key],
                        'adult_price'      => floatval($_prices['adult_price'][$key]),
                        'child_price'      => floatval($_prices['child_price'][$key]),
                        'baby_price'       => floatval($_prices['baby_price'][$key]),
                        'adult_profit'     => floatval($_prices['adult_profit'][$key]),
                        'child_profit'     => floatval($_prices['child_profit'][$key]),
                        'baby_profit'      => floatval($_prices['baby_profit'][$key]),
                        'singleroom_price' => floatval($_prices['singleroom_price'][$key]),
                    );
                    $prices->add($price);
                }

                $typeitem = I('post.typeitem', array());
                if ($typeitem) {
                    foreach ($typeitem as $key => $val) {
                        $row = array(
                            'lid'     => $lid,
                            'type_id' => $data_line['type_id'],
                            'item_id' => $key,
                            'attr'    => $val,
                        );
                        $lattrs->add($row);
                    }
                }
                #grade
                $gradeids = I('post.gradeids', '', 'trim');
                if ($gradeids) {
                    D('Grade')->where(array('id' => array('IN', $gradeids)))->save(array('lid' => $lid));
                }
            }
            $this->success('添加线路成功', U('Line/index'));
        }
        $this->assign('bar', array('curpos' => '添加线路', 'menu' => '线路管理', 'url' => U('Line/index')));
        $types = D('Types')->toSelect(array('type' => 0));
        #district
        $froms = D('District')->where(array('pid' => 1, 'status' => 1))->order('ordid DESC')->getField('id, title');
        $tos   = D('District')->where(array('pid' => 0, 'status' => 1))->order('ordid DESC')->getField('id, title');
        $this->assign('froms', $froms);
        $this->assign('tos', $tos);
        $this->assign('types', $types);
        // $this->assign('grades', D('Grade')->toSelect());
        $albums = D('Zsysdata')->getAll(array('type' => 'images', 'status' => 1));
        $this->assign('albums', $albums);
    }

    public function edit() {
        $op = I('get.op');
        if ('price' == $op) {
            $this->editPrice();
        }
        $line   = D('Line');
        $expand = D('LineExpand');
        $prices = D('LinePrices');
        $trips  = D('LineTrips');
        $lattrs = D('LineAttr');

        $id = I('request.id', 0, 'intval');
        if (!$id) {
            $this->error('参数错误');
        }
        if (IS_POST) {
            // dump(I('post.'));exit;
            $data_line = array(
                'title'       => I('post.title', '', 'trim'),
                'subtitle'    => I('post.subtitle', '', 'trim'),
                'img'         => I('post.img', '', 'trim'),
                'status'      => I('post.status') == 1 ? 1 : 0,
                'ordid'       => I('post.ordid', 0, 'intval'),
                'isnew'       => I('post.isnew') == 1 ? 1 : 0,
                'type_id'     => I('post.type_id', 0, 'intval'),
                'cate_id'     => I('post.cate_id', 0, 'intval'),
                'ishot'       => I('post.ishot') == 1 ? 1 : 0,
                'ispromotion' => I('post.ispromotion') == 1 ? 1 : 0,
                'iscoupon'    => I('post.iscoupon') == 1 ? 1 : 0,
                'allowpay'    => I('post.allowpay') == 1 ? 1 : 0,
                'departure'   => I('post.departure', '', 'trim'),
                'arrive'      => I('post.arrive', '', 'trim'),
                'attachment'  => I('post.attachment', 0, 'intval'),
            );
            if ($data_line['iscoupon'] == 1) {
                $data_line['begintime'] = I('post.begintime', NOW_TIME, 'strtotime');
                $data_line['endtime']   = I('post.endtime', 0, 'strtotime');
            } else {
                $data_line['begintime'] = 0;
                $data_line['endtime']   = 0;
            }
            $line->where(array('id' => $id))->save($data_line);

            #trips
            $trips_data = I('post.trips', array());
            $trips->where(array('lid' => $id))->delete();
            $count_days = 0;
            foreach ($trips_data['days'] as $day) {
                $row = array(
                    'lid'       => $id,
                    'days'      => $day,
                    'title'     => $trips_data['title'][$day],
                    'breakfast' => $trips_data['breakfast'][$day] == 1 ? 1 : 0,
                    'lunch'     => $trips_data['lunch'][$day] == 1 ? 1 : 0,
                    'dinner'    => $trips_data['dinner'][$day] == 1 ? 1 : 0,
                    'contents'  => $trips_data['contents'][$day],
                    'hotel'     => $trips_data['hotel'][$day],
                    'trips_img' => implode(',', $trips_data['trips_img'][$day]),
                    'hotel_img' => implode(',', $trips_data['hotel_img'][$day]),
                    'eat_img'   => implode(',', $trips_data['eat_img'][$day]),
                );
                if ($row['title']) {
                    $trips->add($row);
                    $count_days++;
                }
            }
            $line->where(array('id' => $id))->save(array('days' => $count_days));
            // dump($line->_sql());exit;
            #expand
            $expand_data = array(
                'contents'    => I('post.contents', '', 'trim'),
                'imgs'        => implode(',', I('post.imgs', array())),
                'feature'     => I('post.feature', '', 'trim'),
                'feeinclude'  => I('post.feeinclude', '', 'trim'),
                'feeexclude'  => I('post.feeexclude', '', 'trim'),
                'notice'      => I('post.notice', '', 'trim'),
                'tips'        => I('post.tips', '', 'trim'),
                'children'    => I('post.children', '', 'trim'),
                'shopping'    => I('post.shopping', '', 'trim'),
                'optional'    => I('post.optional', '', 'trim'),
                'gift'        => I('post.gift', '', 'trim'),
                'remark'      => I('post.remark', '', 'trim'),
                'together'    => I('post.together', '', 'trim'),
                'place'       => I('post.place', '', 'trim'),
                'local_man'   => I('post.local_man', '', 'trim'),
                'open_man'    => I('post.open_man', '', 'trim'),
                'open_mobile' => I('post.open_mobile', '', 'trim'),
                'dateline'    => I('post.dateline', NOW_TIME, 'strtotime'),
            );
            $expand->where(array('lid' => $id))->save($expand_data);
            $_prices = I('post.prices', array());
            $prices->where(array('lineid' => $id))->delete();
            foreach ($_prices['starttime'] as $key => $item) {
                $price = array(
                    'lineid'           => $id,
                    'starttime'        => strtotime($item),
                    'endtime'          => strtotime($_prices['endtime'][$key]),
                    'gradeid'          => $_prices['gradeid'][$key],
                    'adult_price'      => floatval($_prices['adult_price'][$key]),
                    'child_price'      => floatval($_prices['child_price'][$key]),
                    'baby_price'       => floatval($_prices['baby_price'][$key]),
                    'adult_profit'     => floatval($_prices['adult_profit'][$key]),
                    'child_profit'     => floatval($_prices['child_profit'][$key]),
                    'baby_profit'      => floatval($_prices['baby_profit'][$key]),
                    'singleroom_price' => floatval($_prices['singleroom_price'][$key]),
                );
                $prices->add($price);
            }
            #typeitem
            $typeitem = I('post.typeitem', array());
            $lattrs->where(array('lid' => $id))->delete();
            if ($typeitem) {
                foreach ($typeitem as $key => $val) {
                    $row = array(
                        'lid'     => $id,
                        'type_id' => $data_line['type_id'],
                        'item_id' => $key,
                        'attr'    => $val,
                    );
                    $lattrs->add($row);
                }
            }
            $this->success('更新线路完成', U('Line/index'));
        }
        #line
        $info = $line->get($id);
        $this->assign('info', $info);
        #type
        $types = D('Types')->toSelect(array('type' => 0));
        $this->assign('types', $types);
        $typeItems = array();
        $cates     = array();
        if ($info['type_id']) {
            #type item
            $typeItems = D('TypesItem')->where(array('status' => 1, 'type_id' => $info['type_id']))->order('ordid DESC, id DESC')->select();
            foreach ($typeItems as &$value) {
                $value['attr'] = unserialize($value['attr']);
            }
            #category
            $auth_cate = $this->getAuthCategory();
            $cates     = D('Category')->toSelect(array('type_id' => $info['type_id'], 'status' => 1, 'pid' => array('NEQ', 0), 'id' => array('IN', $auth_cate)));
        }
        $this->assign('typeItems', $typeItems);
        $this->assign('cates', $cates);
        #expand
        $expand_data = $expand->get($id);
        $this->assign('expand', $expand_data);
        $this->assign('imgs', $expand_data['imgs'] ? explode(',', $expand_data['imgs']) : array());
        #trips
        $trips_data = $trips->where(array('lid' => $id))->order('days asc')->select();
        foreach ($trips_data as &$item) {
            if ($item['hotel_img']) {
                $tmp = explode(',', $item['hotel_img']);
                if (!empty($tmp)) {
                    $item['hotel_img_src'] = D('ImagesSrc')->field('id,thumb')->where(array('id' => array('IN', $tmp)))->select();
                }

            }
            if ($item['trips_img']) {
                $tmp = explode(',', $item['trips_img']);
                if (!empty($tmp)) {
                    $item['trips_img_src'] = D('ImagesSrc')->field('id,thumb')->where(array('id' => array('IN', $tmp)))->select();
                }

            }
            if ($item['eat_img']) {
                $tmp = explode(',', $item['eat_img']);
                if (!empty($tmp)) {
                    $item['eat_img_src'] = D('ImagesSrc')->field('id,thumb')->where(array('id' => array('IN', $tmp)))->select();
                }

            }
        }
        $this->assign('trips', $trips_data);
        #prices
        $plist = $prices->where(array('lineid' => $id))->select();
        $this->assign('plist', $plist);
        #attrs
        $attrs = $lattrs->getAll(array('lid' => $id));
        $temp  = array();
        foreach ($attrs as $key => $val) {
            $temp[$val['type_id']][$val['item_id']] = $val['attr'];
        }
        $attrs = $temp;
        $this->assign('attrs', $attrs);
        $this->assign('bar', array('curpos' => '编辑线路', 'menu' => '线路管理', 'url' => U('Line/index')));
        $this->assign('grades', D('Grade')->toSelect(array('lid' => $id)));
        $this->assign('glist', D('Grade')->where(array('lid' => $id))->select());
        $albums = D('Zsysdata')->getAll(array('type' => 'images', 'status' => 1));
        $this->assign('albums', $albums);
        $this->display();
    }

    public function getTypeCates() {
        if (!IS_POST || !IS_AJAX) {
            $this->error('error');
        }
        $type_id   = I('post.type_id', 0, 'intval');
        $auth_cate = $this->getAuthCategory();
        $cates     = D('Category')->where(array('type_id' => $type_id, 'pid' => array('NEQ', 0), 'status' => 1, 'id' => array('IN', $auth_cate)))->select();
        $this->ajaxReturn(array('status' => 1, 'data' => $cates), 'JSON');
    }

    public function getTypesItem() {
        if (!IS_POST || !IS_AJAX) {
            $this->error('error');
        }
        $type_id = I('post.type_id', 0, 'intval');
        $items   = D('TypesItem')->where(array('status' => 1, 'type_id' => $type_id))->order('ordid DESC, id DESC')->select();
        foreach ($items as &$value) {
            $value['attr'] = unserialize($value['attr']);
        }
        $this->ajaxReturn(array('status' => 1, 'data' => $items), 'JSON');
    }

    public function copy() {
        $id = I('request.id', 0, 'intval');
        if (!$id) {
            $this->ajaxReturn(array('status' => 0, 'msg' => '参数错误'));
        }
        $line   = D('Line');
        $expand = D('LineExpand');
        $trips  = D('LineTrips');
        $prices = D('LinePrice');

        $data_line = $line->get($id);
        if (!$data_line) {
            $this->ajaxReturn(array('status' => 0, 'msg' => '线路不存在'));
        }
        unset($data_line['id']);
        $data_line['title'] .= '--复制';
        $lid = $line->add($data_line);
        #expand
        $expand_data        = $expand->get($id);
        $expand_data['lid'] = $lid;
        $expand->add($expand_data);
        #trips
        $trips_data = $trips->getAll(array('lid' => $id));
        foreach ($trips_data as $row) {
            $row['lid'] = $lid;
            $trips->add($row);
        }
        #prices
        $prices_data = $prices->getAll(array('lid' => $id));
        foreach ($prices_data as $key => $row) {
            $row['lid'] = $lid;
            $prices->add($row);
        }
        #attrs
        $attrs = D('LineAttr')->getAll(array('lid' => $id));
        foreach ($attrs as $row) {
            $row['lid'] = $id;
            D('LineAttr')->add($row);
        }
        $this->ajaxReturn(array('status' => 1));
    }

    /**
     *desc: 获取地区下级分类
     **/
    public function getDist() {
        $id     = I('post.id', 0, 'intval');
        $childs = D('District')->field('id,title')->where(array('status' => 1, 'pid' => $id))->select();
        $this->ajaxReturn(array('status' => 1, 'data' => $childs), 'JSON');
    }

    public function delete() {
        $id = I('request.id');
        if (!$id) {
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => '参数错误'));
            $this->error('参数错误');
        }
        $ids = explode(',', $id);
        foreach ($ids as &$i) {
            $i = intval($i);
        }
        $limit  = count($ids);
        $result = D('Line')->where(array('id' => array('IN', $ids)))->limit($limit)->delete();
        if ($result) {
            D('LineExpand')->where(array('lid' => array('IN', $ids)))->limit($limit)->delete();
            D('LineTrips')->where(array('lid' => array('IN', $ids)))->delete();
            D('LinePrice')->where(array('lid' => array('IN', $ids)))->delete();
            D('LineAttr')->where(array('lid' => array('IN', $ids)))->delete();
            IS_AJAX && $this->ajaxReturn(array('status' => 1, '删除成功'));
            $this->success('删除成功');
        } else {
            IS_AJAX && $this->ajaxReturn(array('status' => 1, '删除失败'));
            $this->error('删除失败');
        }
    }

    public function addGrade() {
        $title    = I('post.title', '', 'trim');
        $contents = I('post.contents', '', 'trim');
        $lid      = I('post.lid', 0, 'intval');
        if (!$title) {
            $this->ajaxReturn(array('status' => 0, 'msg' => '标题不能为空'));
        }
        $id = D('Grade')->add(array('title' => $title, 'contents' => $contents, 'lid' => $lid, 'status' => 1));
        $this->ajaxReturn(array('status' => 1, 'data' => array('id' => $id, 'title' => $title)));
    }

    public function editGrade() {
        $data = I('post.data', array());
        if ($data) {
            $grade = D('Grade');
            foreach ($data as $key => $value) {
                $grade->where(array('id' => $value['id']))->save(array($value['field'] => $value['val']));
            }
        }
    }

    public function editPrice() {
        $id = I('request.id', 0, 'intval');
        if (!$id) {
            $this->error('参数错误');
        }
        $line   = D('Line');
        $prices = D('LinePrices');
        if (IS_POST) {
            $_prices = I('post.prices', array());
            $prices->where(array('lineid' => $id))->delete();
            foreach ($_prices['starttime'] as $key => $item) {
                $price = array(
                    'lineid'           => $id,
                    'starttime'        => strtotime($item),
                    'endtime'          => strtotime($_prices['endtime'][$key]),
                    'gradeid'          => $_prices['gradeid'][$key],
                    'adult_price'      => floatval($_prices['adult_price'][$key]),
                    'child_price'      => floatval($_prices['child_price'][$key]),
                    'baby_price'       => floatval($_prices['baby_price'][$key]),
                    'adult_profit'     => floatval($_prices['adult_profit'][$key]),
                    'child_profit'     => floatval($_prices['child_profit'][$key]),
                    'baby_profit'      => floatval($_prices['baby_profit'][$key]),
                    'singleroom_price' => floatval($_prices['singleroom_price'][$key]),
                );
                $prices->add($price);
            }
            $this->success('线路改价成功', U('Line/index'));
        }
        $info  = $line->get($id);
        $plist = $prices->where(array('lineid' => $id))->select();
        $this->assign('plist', $plist);
        $this->assign('info', $info);
        $this->assign('bar', array('curpos' => '编辑价格', 'menu' => '线路管理', 'url' => U('Line/index')));
        $this->assign('grades', D('Grade')->toSelect(array('lid' => $id)));
        $this->assign('glist', D('Grade')->where(array('lid' => $id))->select());
        $this->display('price');
        exit;
    }

    public function preview() {
        $id = I('get.id', 0, 'intval');
        if (!$id) {
            $this->error('参数错误');
        }
        $line = D('line')->where(array('id' => $id))->find();
        $this->assign('line', $line);
        $expand = D('LineExpand')->where(array('lid' => $id))->find();
        $this->assign('expand', $expand);
        $imgs = explode(',', $expand['imgs']);
        $this->assign('imgs', $imgs);
        $trips = D('LineTrips')->where(array('lid' => $id))->order('days asc')->select();
        foreach ($trips as &$row) {
            $row['hotel_img'] = explode(',', $row['hotel_img']);
            $row['trips_img'] = explode(',', $row['trips_img']);
            $row['eat_img']   = explode(',', $row['eat_img']);
            $srcs             = array_unique(array_merge($row['hotel_img'], $row['trips_img'], $row['eat_img']));
            $row['imgs']      = D('ImagesSrc')->field('thumb,src')->where(array('id' => array('IN', $srcs)))->select();
        }
        $this->assign('trips', $trips);
        $attrs = D('LineAttr')->alias('a')->field('a.*,ti.title')->join('__TYPES_ITEM__ ti on a.type_id=ti.type_id and a.item_id=ti.id')->where(array('lid' => $id))->select();
        $this->assign('attrs', $attrs);
        $tripdate = D('Tripdate')->field('tripdate,SUM(seatnum) seatnum, SUM(seatused) seatused, MIN(min_price) min_price')->where(array('lineid' => $id))->group('tripdate')->order('tripdate ASC')->select();
        $event    = '[';
        foreach ($tripdate as $key => $row) {
            $y = date('Y', $row['tripdate']);
            $m = date('n', $row['tripdate']) - 1;
            $d = date('j', $row['tripdate']) + 1;
            $event .= $event != '[' ? ',' : '';
            $event .= '{"title":"' . ($row['seatnum'] - $row['seatused']) . '余位","start":new Date(' . $y . ',' . $m . ',' . $d . '),"className":"label-success", "allDay":true},{"title":"￥' . $row['min_price'] . '元","start":new Date(' . $y . ',' . $m . ',' . $d . '),"className":"label-primary", "allDay":true}';
        }
        $event .= ']';
        $this->assign('tripdate', $event);
        $this->assign('bar', array('menu' => $line['title'], 'curpos' => '线路预览'));
        $this->display();
    }

    public function getTripdate() {
        $lid   = I('post.lid', 0, 'intval');
        $day   = I('post.day', '', 'trim');
        $start = strtotime($day . ' 00:00:00');
        $end   = strtotime($day . ' 23:59:59');
        $res   = D('Tripdate')->alias('td')->field('td.id,td.seatnum,td.seatused,te.traffic_gotxt,te.traffic_backtxt')->join('__TRIPDATE_EXPAND__ te ON td.id=te.tripdateid', 'LEFT')->where(array('td.lineid' => $lid, 'td.tripdate' => array('BETWEEN', $start . ',' . $end)))->select();
        foreach ($res as &$item) {
            $item['topic'] = D('TripdateTopics')->alias('tt')->field('tt.district_title,tt.linegrade_txt,tt.adult_saleprice,tt.child_saleprice,tt.singleroom_price')->where(array('tripdateid' => $item['id']))->select();
        }
        $this->ajaxReturn(array('status' => 1, 'data' => $res));
    }
}
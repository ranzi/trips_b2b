<?php
namespace Admin\Controller;

class LoginController extends BaseController {
    public function __construct() {
        parent::__construct();
    }

    public function _initialize() {
        parent::_initialize();
        if (session('admin_id') > 0) {
            if (in_array(ACTION_NAME, array('index', 'doLogin'))) {
                $this->redirect('Index/index');
            }
        }
    }

    public function index() {
        $this->assign('bar', array('curpos' => '登录'));
        $this->display();
    }

    public function doLogin() {
        if (!IS_POST && !IS_AJAX) {
            $this->redirect('Index/index');
        }
        $account  = I('post.account', '', 'trim');
        $password = I('post.password', '', 'trim');
        $holder   = I('post.holder', 0, 'intval');
        if (!$account) {
            $this->ajaxReturn(array('status' => 0, 'msg' => '账号不能为空'));
        }
        if (!$password) {
            $this->ajaxReturn(array('status' => 0, 'msg' => '密码不能为空'));
        }
        $map = array(
            '_complex' => array(
                'username' => $account,
                'mobile'   => $account,
                '_logic'   => 'OR',
            ),
        );
        $user_data = D('Admin')->where($map)->find();
        if (!$user_data) {
            $this->ajaxReturn(array('status' => 0, 'msg' => '用户名或密码错误'));
        }
        if ($user_data['status'] != 1) {
            $this->ajaxReturn(array('status' => 0, 'msg' => '用户无权登录'));
        }
        if ($user_data['pwd'] != sha1(C('B2C_ENCRY') . $password . $user_data['salt'])) {
            $this->ajaxReturn(array('status' => 0, 'msg' => '密码错误'));
        }
        D('Admin')->where(array('id' => $user_data['id']))->where(array('last_id' => CLIENT_IP, 'last_time' => NOW_TIME));
        session('admin_id', $user_data['id']);
        session('admin_name', $user_data['realname'] ?: $user_data['username']);
        session('admin', $user_data);
        if ($holder) {
            cookie('b2c_autologin', 1, 30 * 604800);
            cookie('b2c_username', $user_data['username'], 30 * 604800);
            cookie('b2c_login_key', sha1($user_data['addtime'] . $user_data['pwd'] . $user_data['salt']), 30 * 604800);
        }
        $this->ajaxReturn(array('status' => 1, 'msg' => '登录成功'));
    }

    public function logout() {
        session(NULL);
        cookie('b2c_autologin', NULL);
        cookie('b2c_username', NULL);
        cookie('b2c_login_key', NULL);
        $this->redirect('Login/index');
    }
}
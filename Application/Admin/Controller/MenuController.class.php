<?php
/**
 *@author Chunlei Wang <sihangshi2011@gmail.com>
 *
 **/
namespace Admin\Controller;

class MenuController extends AuthController {
    public function __construct() {
        parent::__construct();
    }

    public function _initialize() {
        parent::_initialize();
    }

    public function _before_index() {
        $this->assign('bar', array('curpos' => '菜单列表', 'menu' => '菜单管理', 'url' => U('Menu/index')));
    }

    public function _before_add() {
        $this->assign('bar', array('curpos' => '添加菜单', 'menu' => '菜单管理', 'url' => U('Menu/index')));
        $parents = D('menu')->toSelect();
        $this->assign('parents', $parents);
    }

    public function _before_edit() {
        $this->assign('bar', array('curpos' => '添加菜单', 'menu' => '菜单管理', 'url' => U('Menu/index')));
        $parents = D('menu')->toSelect();
        $this->assign('parents', $parents);
    }
}
<?php
/**
 * 合作单位
 */
namespace Admin\Controller;

class PartnersController extends AuthController {
    public function _initialize() {
        parent::_initialize();
    }

    public function index() {
        $model = D('Partners');
        $map['deletebs'] = 0;
        $sch = array();
        if(I('get.schbs')==1){
            $sch['schbs']             = I('get.schbs');
            $sch['title']             = I('get.title', '', 'trim');
            $sch['industry']          = I('get.industry', '', 'intval');
            $sch['settlement']        = I('get.settlement', '', 'intval');
            $sch['start_creditquota'] = I('get.start_creditquota', '', 'floatval');
            $sch['end_creditquota']   = I('get.end_creditquota', '', 'floatval');
            

            if($sch['title']){
                $map['title'] = array("like",'%'.$sch['title'].'%');
            }
            if($sch['industry']){
                $map['type_id'] = $sch['industry'];
            }
            if($sch['settlement']){
                $map['settlement_id'] = $sch['settlement'];
            }
            if($sch['start_creditquota']){
                if($sch['end_creditquota']){
                    $map['creditquota']  = array('between',$sch['start_creditquota'].','.$sch['end_creditquota']);
                }else{
                    $map['creditquota']  = $sch['start_creditquota'];
                    $sch['end_creditquota'] = '';
                }
            }else if($sch['end_creditquota']){
                $map['creditquota']  = array('between','0,'.$sch['end_creditquota']);
                $sch['start_creditquota'] = 0;
            } else {
                $sch['start_creditquota'] = '';
                $sch['end_creditquota'] = '';
            }
        }
        
        $field  = '';
        $order  = array(
            'ordid' => 'DESC',
            'id'    => 'ASC'
        );
        $types_data = D('Types')->get_typesdata(array('type' =>array('IN','industry,settlement')),1);
        $getlist   = $model->getList($map, $field, $order,$types_data);

        $this->assign('bar', array('curpos' => '列表', 'menu' => '合作单位', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('list', $getlist['list']);
        $this->assign('page', $getlist['page']);
        $this->assign('sch', $sch);
        $this->assign('industrys', $types_data['industry']);
        $this->assign('settlements', $types_data['settlement']);
        $this->display();
    }

    public function add() {

        if (IS_POST) {
            $result = D('Partners')->_add();
            if($result['status']==1){
                $this->success('添加成功', U(CONTROLLER_NAME . '/index'));
            }else{
                $this->error($result['msg']);
            }
        }

        $types_data = D('Types')->get_typesdata(array('type' =>array('IN','industry,settlement')));
        $this->assign('bar', array('curpos' => '添加', 'menu' => '合作单位', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('industrys', $types_data['industry']);
        $this->assign('settlements', $types_data['settlement']);
        $this->display();
    }

    public function edit() {
        $model = D('Partners');
        if (IS_AJAX) {
            $field  = I('post.field', '', 'trim');
            $id     = I('post.id', '', 'trim');
            $pk     = I('post.pk', '', 'trim');
            $value  = I('post.value', '', 'trim');
            $status = 0;
            $msg    = '编辑失败';
            if ($field && $id & $pk && $value) {
                $status = 1;
                $msg    = '编辑成功';
                $model->where(array($pk => $id))->save(array($field => $value));
            }
            $this->ajaxReturn(array('msg' => $msg, 'status' => $status));
        } elseif (IS_POST) {
            $result = $model->_save();
            if ($result['status'] == 1) {
                $this->success($result['msg'], U(CONTROLLER_NAME . '/edit?id='.I('post.id')));
            } else {
                $this->error($result['msg']);
            }
        }

        $id = I('get.id', 0, 'intval');
        if (!$id) {
            $this->error('传参有误!');
        }

        $data = array(
            'id'       => $id,
            'deletebs' => 0,
        );

        $info = $model -> where($data) -> find();
        if (!$info) {
            $this->error('数据不存在');
        }

        $types_data = D('Types')->get_typesdata(array('type' =>array('IN','industry,settlement')));
        $this->assign('bar', array('curpos' => '编辑', 'menu' => '合作单位', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('industrys', $types_data['industry']);
        $this->assign('settlements', $types_data['settlement']);
        $this->assign('info', $info);
        $this->display();
    }

    public function delete() {
        $id    = I('request.id');
        $model = M('Partners');
        if (!$id) {
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => '参数错误'));
            $this->error($this->error('参数错误'));
        }
        $id = explode(',', $id);
        foreach ($id as &$i) {
            $i = intval($i);
        }
        $data = array(
            'deletebs' => 1,
        );
        
        $model->where(array('id' => array('IN', implode(',', $id))))->limit(count($id))->save($data);
        IS_AJAX && $this->ajaxReturn(array('status' => 1, 'msg' => '删除成功'));
        $this->success('删除成功');
    }

    //结算账号
    public function bank(){
        $model = D('Partners_bank');
        if (IS_POST) {

            //伪删除行
            if(I('post.deleteprice')==1){
                $flag = $model->_save(array('deletebs' => 1),array('id' => I('post.id'),'partners_id' => I('post.partners_id')));
                if($flag){
                    IS_AJAX && $this->ajaxReturn(array('status' => 1, 'msg' => '删除成功'));
                    $this->success('删除成功');
                }else{
                    IS_AJAX && $this->ajaxReturn(array('status' => -1, 'msg' => '删除失败'));
                    $this->success('删除失败');
                }
                
            }

            $title            = I('post.title', array());
            $bankaccount      = I('post.bankaccount', array());
            $accountname      = I('post.accountname', array());
            $remark           = I('post.remark', array());
            $ordid            = I('post.ordid', array());
            $partners_id      = I('post.partners_id', 0, 'intval');
            $edit_title       = I('post.edit_title', array());
            $edit_bankaccount = I('post.edit_bankaccount', array());
            $edit_accountname = I('post.edit_accountname', array());
            $edit_remark      = I('post.edit_remark', array());
            $edit_ordid       = I('post.edit_ordid', array());

            if (!$partners_id) {
                $this->error('传参错误');
            }
            
            //修改
            if(is_array($edit_title)){
                foreach($edit_title as $key => $val){
                    if($edit_title[$key]==''){
                        continue;
                    }
                    $data = array(
                        'id'          => $key,
                        'title'       => $edit_title[$key],
                        'bankaccount' => $edit_bankaccount[$key],
                        'accountname' => $edit_accountname[$key],
                        'remark'      => $edit_remark[$key],
                        'ordid'       => intval($edit_ordid[$key]),
                    );
                    $model -> _save($data);
                }
            }

            //新增
            if(is_array($title)){
                foreach ($title as $key => $val) {
                    if($title[$key]==''){
                        continue;
                    }
                    $data = array(
                        'title'       => $title[$key],
                        'bankaccount' => $bankaccount[$key],
                        'accountname' => $accountname[$key],
                        'remark'      => $remark[$key],
                        'ordid'       => intval($ordid[$key]),
                        'partners_id' => $partners_id,
                        'status' => 1,
                    );
                    $model -> _add($data);
                }
            }
            $this->success('保存成功');
        }

        $id = I('get.id', 0, 'intval');
        if (!$id) {
            $this->error('传参错误');
        }
        
        $info = M('Partners') -> where(array('id' => $id)) -> find();
        if (!$info) {
            $this->error('数据不存在');
        }
        $list = $model -> getbanklist(array('partners_id' => $id, 'deletebs' => 0));
        
        $this->assign('bar', array('curpos' => '结算账号', 'menu' => '合作单位', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('list', $list);
        $this->assign('info', $info);
        $this->display();
    }

    //联系方式(合作单位)
    public function contact(){
        $model = D('Partners_contact');
        if (IS_POST) {
            $partners_id = I('post.partners_id', 0, 'intval');
            if (!$partners_id) {
                $this->error('传参错误');
            }

            //伪删除行
            if(I('post.deleteprice') == 1){
                $flag = $model->_save(array('deletebs' => 1),array('id' => I('post.id'),'partners_id' => I('post.partners_id')));
                if($flag){
                    IS_AJAX && $this->ajaxReturn(array('status' => 1, 'msg' => '删除成功'));
                    $this->success('删除成功');
                }else{
                    IS_AJAX && $this->ajaxReturn(array('status' => -1, 'msg' => '删除失败'));
                    $this->success('删除失败');
                }
                
            }

            $realname      = I('post.realname', array());
            $mobile        = I('post.mobile', array());
            $mobile2       = I('post.mobile2', array());
            $tel           = I('post.tel', array());
            $fax           = I('post.fax', array());
            $qq            = I('post.qq', array());
            $weixin        = I('post.weixin', array());
            $ordid         = I('post.ordid', array());
            $edit_realname = I('post.edit_realname', array());
            $edit_mobile   = I('post.edit_mobile', array());
            $edit_mobile2  = I('post.edit_mobile2', array());
            $edit_tel      = I('post.edit_tel', array());
            $edit_fax      = I('post.edit_fax', array());
            $edit_qq       = I('post.edit_qq', array());
            $edit_weixin   = I('post.edit_weixin', array());
            $edit_ordid    = I('post.edit_ordid', array());
            
            //修改
            if(is_array($edit_realname)){
                foreach($edit_realname as $key => $val){
                    if($val == ''){
                        continue;
                    }
                    $data = array(
                        'id'       => $key,
                        'realname' => $edit_realname[$key],
                        'mobile'   => $edit_mobile[$key],
                        'mobile2'  => $edit_mobile2[$key],
                        'tel'      => $edit_tel[$key],
                        'fax'      => $edit_fax[$key],
                        'qq'       => $edit_qq[$key],
                        'weixin'   => $edit_weixin[$key],
                        'ordid'    => intval($edit_ordid[$key]),
                        'admin_id' => session('admin_id'),
                    );
                    $model -> _save($data);
                }
            }

            //新增
            if(is_array($realname)){
                foreach ($realname as $key => $val) {
                    if($val == ''){
                        continue;
                    }

                    $data = array(
                        'realname'    => $realname[$key],
                        'mobile'      => $mobile[$key],
                        'mobile2'     => $mobile2[$key],
                        'tel'         => $tel[$key],
                        'fax'         => $fax[$key],
                        'qq'          => $qq[$key],
                        'weixin'      => $weixin[$key],
                        'ordid'       => intval($ordid[$key]),
                        'partners_id' => $partners_id,
                        'status'      => 1,
                        'admin_id' => session('admin_id'),
                    );
                    $model -> _add($data);
                }
            }
            $this->success('保存成功');
        }

        $id = I('get.id', 0, 'intval');
        if (!$id) {
            $this->error('传参错误');
        }
        $info = M('Partners') -> where(array('id' => $id)) -> find();
        if (!$info) {
            $this->error('数据不存在');
        }
        $list = $model -> getlist(array('partners_id' => $id, 'deletebs' => 0));
        
        $this->assign('bar', array('curpos' => '联系方式', 'menu' => '合作单位', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('list', $list);
        $this->assign('info', $info);
        $this->display();
    }

    /**
     * 通过行业类型获取对应的合作单位
     */
    public function getPartnersByIndustry() {
        if (!IS_AJAX || !IS_POST) {
            $this->redirect('Index/index');
        }
        $industry  = I('post.industry', 0, 'intval');
        if (!$industry) {
            $this->ajaxReturn(array('status' => 0, 'msg' => '请选择合作单位类型'));
        }
        $map['deletebs'] = 0;
        $map['type'] = 1;
        $map['type_id'] = $industry;
        $list = M('Partners')->field('id,title')->where($map)->order('ordid desc,id')->select();
        $result  = array();
        if($list){
            foreach($list as $key => $val){
                $result[] = array($val['id'],$val['title']);
            }
        }
        $this->ajaxReturn(array('status' => 1, 'data' => $result));
    }
}
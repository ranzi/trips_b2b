<?php
namespace Admin\Controller;

class PositionController extends AuthController {
    public function _initialize() {
        parent::_initialize();
    }

    public function _before_index() {
        $this->assign('bar', array('curpos' => '职位列表', 'menu' => '职位管理', 'url' => U('Position/index')));
    }

    public function _before_add() {
        $jobs = D('Job')->toSelect();
        if (empty($jobs)) {
            $this->error('请先维护职务', U('Job/add'));
        }
        $this->assign('jobs', $jobs);
        $this->assign('bar', array('curpos' => '添加职位', 'menu' => '职位管理', 'url' => U('Position/index')));
    }

    public function _before_edit() {
        $jobs = D('Job')->toSelect();
        if (empty($jobs)) {
            $this->error('请先维护职务', U('Job/add'));
        }
        $this->assign('jobs', $jobs);
        $this->assign('bar', array('curpos' => '编辑职位', 'menu' => '职位管理', 'url' => U('Position/index')));
    }
}
<?php
namespace Admin\Controller;

class PrivelegeController extends AuthController {
    public function _initialize() {
        parent::_initialize();
        $this->assign('models', array('admin' => '后台管理'));
    }

    public function _before_index() {
        $this->assign('bar', array('curpos' => '权限管理', 'menu' => '权限管理', 'url' => 'javascript:history.go(-1);'));

    }

    public function _before_add() {
        $this->assign('bar', array('curpos' => '添加权限', 'menu' => '权限管理', 'url' => U('Privelege/index')));
        $menus = D('Menu')->toSelect();
        $this->assign('menus', $menus);
        $authType = D('Privelege')->field('distinct type')->select();
        foreach ($authType as $type) {
            if ($type['type'] != NULL) {
                $auths[$type['type']] = $type['type'];
            }

        }
        $this->assign('auths', $auths);
    }

    public function _before_edit() {
        $this->assign('bar', array('curpos' => '添加权限', 'menu' => '权限管理', 'url' => U('Privelege/index')));
        $menus = D('Menu')->toSelect();
        $this->assign('menus', $menus);
        $authType = D('Privelege')->field('distinct type')->select();
        foreach ($authType as $type) {
            if ($type['type'] != NULL) {
                $auths[$type['type']] = $type['type'];
            }

        }
        $this->assign('auths', $auths);
    }

    public function getCtrl() {
        if (!IS_AJAX || !IS_POST) {
            $this->redirect('Index/index');
        }
        $model = I('post.model', '', 'trim');
        if (!$model) {
            $this->ajaxReturn(array('status' => 0, 'msg' => '请选择模块'));
        }
        $exclude = array('.', '..', 'Base', 'Auth');
        $list    = scandir(APP_PATH . strtoupper($model) . '/' . C('DEFAULT_C_LAYER') . '/');
        $ctrl    = array();
        foreach ($list as $item) {
            list($c) = explode(C('DEFAULT_C_LAYER'), $item);
            if (!in_array($c, $exclude)) {
                $ctrl[] = $c;
            }

        }
        $this->ajaxReturn(array('status' => 1, 'data' => $ctrl));
    }

    public function getAction() {
        if (!IS_AJAX || !IS_POST) {
            $this->redirect('Index/index');
        }
        $model = I('post.model', '', 'trim');
        $ctrl  = I('post.ctrl', '', 'trim');
        if (!$model) {
            $this->ajaxReturn(array('status' => 0, 'msg' => '请选择模块'));
        }
        if (!$ctrl) {
            if (!$model) {
                $this->ajaxReturn(array('status' => 0, 'msg' => '请选择模型'));
            }
        }
        $class   = A(ucfirst($model) . '/' . ucfirst($ctrl));
        $method  = get_class_methods($class);
        $exclude = array('show', 'ajaxReturn', 'checkAuth', 'error', 'success', 'redirect', 'buildHtml', 'theme', 'assign', 'get', 'set', 'frameCallback', 'display', 'frameUpload', 'autoUploadFile', 'getMaps', 'autoUploadImageToRemote', 'fetch');
        $result  = array();
        foreach ($method as $item) {
            if ($item[0] != '_' && !in_array($item, $exclude)) {
                $result[] = $item;
            }
        }
        $this->ajaxReturn(array('status' => 1, 'data' => $result));
    }

    public function grant() {
        if (IS_POST) {
            if (I('post.id', 0, 'intval')) {
                $result = D('Grant')->_save();
            } else {
                $result = D('Grant')->_add();
            }
            if ($result['status'] == 1) {
                $this->success($result['msg'], U('Privelege/grant', array('type' => I('post.auth_type', '', 'trim'), 'id' => I('post.auth_id', 0, 'intval'))));
            } else {
                $this->error($result['msg']);
            }
        }
        $this->assign('bar', array('curpos' => '授权管理'));
        $type = I('get.type', '', 'trim');
        $id   = I('get.id', 0, 'intval');
        if (!$type || !$id) {
            $this->error('参数错误');
        }
        $this->assign('auth_type', $type);
        $this->assign('auth_id', $id);

        if ('group' == $type) {
            $authFor = D('Group')->field('title')->where(array('id' => $id))->find();
        } elseif ('user' == $type) {
            $authFor          = D('Admin')->field('username,realname')->where(array('id' => $id))->find();
            $authFor['title'] = $authFor['realname'] ?: $authFor['username'];
        }
        $this->assign('authFor', $authFor);

        #grant info
        $info = D('Grant')->where(array('auth_id' => $id, 'auth_type' => $type))->find();
        $this->assign('info', $info);
        $auths = array();
        if ($info) {
            $auths = explode(',', $info['auth']);
        }
        $this->assign('auths', $auths);

        #privelege
        $list = D('Privelege')->getAll();
        $temp = array();
        foreach ($list as $row) {
            $temp[$row['type']][] = $row;
        }
        $this->assign('list', $temp);
        $this->display();
    }
	
}
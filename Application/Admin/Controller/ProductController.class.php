<?php
namespace Admin\Controller;

class ProductController extends AuthController {
    public function _initialize() {
        parent::_initialize();
    }

    public function _before_index() {
        $this->assign('bar', array('curpos' => '产品列表', 'menu' => '产品管理', 'url' => U('Product/index')));
    }

    public function _before_add() {
        $this->assign('bar', array('curpos' => '添加产品', 'menu' => '产品管理', 'url' => U('Product/index')));
    }

    public function _before_edit() {
        $this->assign('bar', array('curpos' => '编辑产品', 'menu' => '产品管理', 'url' => U('Product/index')));
    }
}
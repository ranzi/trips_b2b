<?php
/**
 * 结算方式-财务管理
 * 如按月结算 满额结算 现付
 */
namespace Admin\Controller;

class SettlementsetController extends AuthController {
    public function _initialize() {
        parent::_initialize();
    }

	public function index() {
		$model	= D('Types');
        $map['type']	 = 'settlement';
        $map['deletebs'] = 0;
        $field 	= 'id,type,title,ordid,status';
        $order   = 'ordid desc,id';
        $getlist 	= $model->getList($map, $field, $order);
        
        $this->assign('bar', array('curpos' => '列表', 'menu' => '结算方式', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('list', $getlist['list']);
        $this->assign('page', $getlist['page']);
		$this->display();
	}

	public function add() {
		if (IS_POST) {
			$model = D('Types');
			
			$data = array(
				'type'      => 'settlement',
                'title'		=> I('post.title', '', 'trim'),
				'ordid'		=> I('post.ordid', 0, 'intval'),
				'status'	=> I('post.status')==1?:0,
			);
			$result = $model->insert($data);

            if ($result['status'] == 1) {
                $this->success($result['msg'], U(CONTROLLER_NAME . '/index'));
            } else {
                $this->error($result['msg']);
            }
		}
    	$this->assign('bar', array('curpos' => '添加', 'menu' => '结算方式', 'url' => U(CONTROLLER_NAME . '/index')));
		$this->display();
	}

	public function edit() {
		$model = D('Types');
        if (IS_AJAX) {
            $field  = I('post.field', '', 'trim');
            $id     = I('post.id', '', 'trim');
            $pk     = I('post.pk', '', 'trim');
            $value  = I('post.value', '', 'trim');
            $status = 0;
            $msg    = '编辑失败';
            if ($field && $id & $pk && $value) {
                $status = 1;
                $msg    = '编辑成功';
                $model->where(array($pk => $id))->save(array($field => $value));
            }
            $this->ajaxReturn(array('msg' => $msg, 'status' => $status));
        } elseif (IS_POST) {
            if (!I('post.id')) {
                return array('status' => 0, 'msg' => '传参有误!');
            }
            $data = array(
                'id'     => I('post.id', 0, 'intval'),
                'title'  => I('post.title', '', 'trim'),
                'ordid'  => I('post.ordid', 0, 'intval'),
                'status' => I('post.status')==1?:0,
            );

            $result = $model->update($data);
            if ($result['status'] == 1) {
                $this->success($result['msg'], U(CONTROLLER_NAME . '/edit?id='.I('post.id')));
            } else {
                $this->error($result['msg']);
            }
        }

        $id = I('get.id', 0, 'intval');
        if (!$id) {
            $this->error('参数错误');
        }

        $data = array(
        	'id' => $id,
        	'deletebs' => 0,
        );

        $info = $model -> where($data) -> find();
        if (!$info) {
            $this->error('数据不存在');
        }
        $this->assign('bar', array('curpos' => '编辑', 'menu' => '结算方式', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('info', $info);
        $this->display();
	}
	public function delete() {
		$id    = I('request.id');
        $model = M('Types');
        if (!$id) {
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => '参数错误'));
            $this->error($this->error('参数错误'));
        }
        $id = explode(',', $id);
        foreach ($id as &$i) {
            $i = intval($i);
        }
        $data = array(
			'deletebs'=>1,
		);
        $model->where(array('id' => array('IN', implode(',', $id))))->limit(count($id))->save($data);
        IS_AJAX && $this->ajaxReturn(array('status' => 1, 'msg' => '删除成功'));
        $this->success('删除成功');
	}
}
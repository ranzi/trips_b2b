<?php
/**
 * 接送站信息
 * 只记录出发地目的地，接送范围，售价(没按日期段)
 * 真正结算单位与结算价，需要另开一个栏目，专门添加当天的车型，供应商，预定人数(和订单关联的人数)，成本价....
 */
namespace Admin\Controller;

class ShuttleController extends AuthController {
    public function _initialize() {
        parent::_initialize();
    }

	public function index() {
        $model  = D('Shuttle');
        
        $map    = array(
                    'deletebs'  => 0
                );
        $field  = '';
        $order  = array(
                    'ordid' => 'DESC',
                    'id'    => 'DESC'
                );
        $getlist    = $model->getList($map, $field, $order);
        
        $this->assign('bar', array('curpos' => '列表', 'menu' => '接送信息', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('list', $getlist['list']);
        $this->assign('page', $getlist['page']);
        $this->display();
    }

    public function add() {
        if (IS_POST) {
            $model = D('Shuttle');
            $data = array(
                'start_district' => I('post.start_district', '', 'trim'),
                'end_district'   => I('post.end_district', '', 'trim'),
                'message'        => I('post.message'),
                'price'          => I('post.price', 0, 'floatval'),
                'ordid'          => I('post.ordid', 0, 'intval'),
                'admin_id'       => session('admin_id'),
                'status'         => I('post.status')==1?:0,
                'addtime'        => NOW_TIME,
            );

            $result = $model->_add($data);

            //如果添加成功
            if ($result['status'] == 1 && $result['id']) {
                $this->success($result['msg'], U(CONTROLLER_NAME . '/index'));
            } else {
                $this->error($result['msg']);
            }
        }

        $this->assign('bar', array('curpos' => '添加', 'menu' => '接送信息', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->display();
    }

    public function edit() {
        $model = D('Shuttle');
        if (IS_AJAX) {
            $field  = I('post.field', '', 'trim');
            $id     = I('post.id', '', 'trim');
            $pk     = I('post.pk', '', 'trim');
            $value  = I('post.value', '', 'trim');
            $status = 0;
            $msg    = '编辑失败';
            if ($field && $id & $pk && $value) {
                $status = 1;
                $msg    = '编辑成功';
                $model->where(array($pk => $id))->save(array($field => $value));
            }
            $this->ajaxReturn(array('msg' => $msg, 'status' => $status));
        } elseif (IS_POST) {
            
            $data = array(
                'id'             => I('post.id', 0, 'intval'),
                'start_district' => I('post.start_district', '', 'trim'),
                'end_district'   => I('post.end_district', '', 'trim'),
                'message'        => I('post.message'),
                'price'          => I('post.price', 0, 'floatval'),
                'ordid'          => I('post.ordid', 0, 'intval'),
                'status'         => I('post.status')==1?:0,
            );
            
            $result = $model->_save($data);
            if ($result['status'] == 1) {
                $this->success($result['msg'], U(CONTROLLER_NAME . '/index'));
            } else {
                $this->error($result['msg']);
            }
        }

        $id = I('get.id', 0, 'intval');
        if (!$id) {
            $this->error('传参有误!');
        }

        $data = array(
            'id' => $id,
            'deletebs' => 0,
        );

        $info = $model -> where($data) -> find();
        if (!$info) {
            $this->error('数据不存在');
        }

        $this->assign('bar', array('curpos' => '编辑', 'menu' => '接送信息列表', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('info', $info);
        $this->display();
    }
    public function delete() {
        $id    = I('request.id');
        $model = M('Shuttle');
        if (!$id) {
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => '参数错误'));
            $this->error($this->error('参数错误'));
        }
        $id = explode(',', $id);
        foreach ($id as &$i) {
            $i = intval($i);
        }
        $data = array(
            'deletebs' => 1,
        );
        
        $model->where(array('id' => implode(',', $id)))->save($data);
        IS_AJAX && $this->ajaxReturn(array('status' => 1, 'msg' => '删除成功'));
        $this->success('删除成功');
    }
}
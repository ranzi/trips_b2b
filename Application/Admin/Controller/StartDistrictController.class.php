<?php
/**
 * 接团地区
 * 记录接团地区，接送范围，接送价格(没按日期段)
 * 真正合作单位与结算价，需要另开一个栏目，专门添加当天的车型，供应商，预定人数(和订单关联的人数)，成本价....
 */
namespace Admin\Controller;

class StartDistrictController extends AuthController {
    public function _initialize() {
        parent::_initialize();
    }

	public function index() {
        $model  = D('StartDistrict');
        
        $map['deletebs'] = 0;
        $field  = 'le.*,d.title as dist_title';
        $order  = 'le.ordid desc,id';
        $getlist    = $model->getList($map, $field, $order);
        
        $this->assign('bar', array('curpos' => '列表', 'menu' => '接团地区', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('list', $getlist['list']);
        $this->assign('page', $getlist['page']);
        $this->display();
    }

    public function add() {
        if (IS_POST) {
            $model = D('StartDistrict');
            $start_district = I('post.start_district', '', 'trim');
            if(!$start_district){
                return array('status' => 0, 'msg' => '请选择接团地区!');
            }

            $start_districts = M('District')->field('id')->where(array('title' => $start_district))->find();
            $start_districtid = intval($start_districts['id']);
            if($start_districtid <= 0){
                return array('status' => 0, 'msg' => '选择地区不存在!');
            }
            $data = array(
                'start_districtid' => $start_districtid,
                'start_district'   => $start_district,
                'message'          => I('post.message', '', 'trim'),
                'adult_price'      => I('post.adult_price', 0, 'floatval'),
                'child_price'      => I('post.child_price', 0, 'floatval'),
                'ordid'            => I('post.ordid', 0, 'intval'),
                'admin_id'         => session('admin_id'),
                'status'           => I('post.status')==1?:0,
            );

            $result = $model->_add($data);

            //如果添加成功
            if ($result['status'] == 1 && $result['id']) {
                $this->success($result['msg'], U(CONTROLLER_NAME . '/index'));
            } else {
                $this->error($result['msg']);
            }
        }

        $this->assign('bar', array('curpos' => '添加', 'menu' => '接团地区', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->display();
    }

    public function edit() {
        $model = D('StartDistrict');
        if (IS_AJAX) {
            $field  = I('post.field', '', 'trim');
            $id     = I('post.id', '', 'trim');
            $pk     = I('post.pk', '', 'trim');
            $value  = I('post.value', '', 'trim');
            $status = 0;
            $msg    = '编辑失败';
            if ($field && $id & $pk && $value) {
                $status = 1;
                $msg    = '编辑成功';
                $model->where(array($pk => $id))->save(array($field => $value));
            }
            $this->ajaxReturn(array('msg' => $msg, 'status' => $status));
        } elseif (IS_POST) {
            if (!I('post.id')) {
                return array('status' => 0, 'msg' => '传参有误!');
            }
            $start_district = I('post.start_district', '', 'trim');
            if(!$start_district){
                return array('status' => 0, 'msg' => '请选择接团地区!');
            }

            $start_districts = M('District')->field('id')->where(array('title' => $start_district))->find();
            $start_districtid = intval($start_districts['id']);
            if($start_districtid <= 0){
                return array('status' => 0, 'msg' => '选择地区不存在!');
            }
            $data = array(
                'id'               => I('post.id', 0, 'intval'),
                'start_districtid' => $start_districtid,
                'start_district'   => $start_district,
                'message'          => I('post.message', '', 'trim'),
                'adult_price'      => I('post.adult_price', 0, 'floatval'),
                'child_price'      => I('post.child_price', 0, 'floatval'),
                'ordid'            => I('post.ordid', 0, 'intval'),
                'admin_id'         => session('admin_id'),
                'status'           => I('post.status')==1?:0,
            );
            
            $result = $model->_save($data);
            if ($result['status'] == 1) {
                $this->success($result['msg'], U(CONTROLLER_NAME . '/edit?id='.I('post.id')));
            } else {
                $this->error($result['msg']);
            }
        }

        $id = I('get.id', 0, 'intval');
        if (!$id) {
            $this->error('传参有误!');
        }

        $data = array(
            'le.id' => $id,
            'le.deletebs' => 0,
        );

        $info = M('StartDistrict')->field('le.*,d.title as district_title')->alias('le')
                ->join('__DISTRICT__ d ON le.start_districtid = d.id', 'LEFT')
                ->where($data)->find();
        if (!$info) {
            $this->error('数据不存在');
        }

        $this->assign('bar', array('curpos' => '编辑', 'menu' => '接团地区', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('info', $info);
        $this->display();
    }
    public function delete() {
        $id    = I('request.id');
        $model = M('StartDistrict');
        if (!$id) {
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => '参数错误'));
            $this->error($this->error('参数错误'));
        }
        $id = explode(',', $id);
        foreach ($id as &$i) {
            $i = intval($i);
        }
        $data = array(
            'deletebs' => 1,
        );
        
        $model->where(array('id' => implode(',', $id)))->save($data);
        IS_AJAX && $this->ajaxReturn(array('status' => 1, 'msg' => '删除成功'));
        $this->success('删除成功');
    }
}
<?php
namespace Admin\Controller;

class SyscfgController extends AuthController {
    public function __construct() {
        parent::__construct();
    }

    public function _initialize() {
        parent::_initialize();
    }

    public function index() {
        $this->assign('bar', array('curpos' => '网站设置'));
        $cfgs = D('Syscfg')->getField('varname, value');
        $this->assign('cfgs', $cfgs);
        $this->display();
    }

    public function save() {
        if (!IS_POST) {
            $this->error('操作错误！');
        }
        $cfgModel = D('Syscfg');
        $cfgs     = $cfgModel->getField('varname, value');
        $vars     = array();
        if ($cfgs) {
            $vars = array_keys($cfgs);
        }
        $post = I('post.');
        unset($post['submit']);
        foreach ($post as $var => $val) {
            if (in_array($var, $vars)) {
                $cfgModel->where(array('varname' => $var))->save(array('value' => $val));
            } else {
                $cfgModel->add(array('varname' => $var, 'value' => $val));
            }
            $cfgs[$var] = $val;
        }
        F('cfgs', $cfgs);
        $this->success('保存成功');
    }
}
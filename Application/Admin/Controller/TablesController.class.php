<?php
namespace Admin\Controller;

class TablesController extends AuthController {
    public function _initialize() {
        parent::_initialize();
    }

    public function _before_index() {
        $this->assign('bar', array('curpos' => '数据表列表', 'menu' => '数据表管理', 'url' => U(CONTROLLER_NAME . '/index')));
    }

    public function _before_add() {
        if (IS_POST) {
            $tables = D('Tables')->create(I('post.'));
            if ($tables === FALSE) {
                $this->error(D('Tables')->getError());
            } else {
                $tables['model']  = strtolower($tables['model']);
                $tables['status'] = $tables['status'] == 1 ? 1 : 0;
                $tid              = D('Tables')->add();
                $tablesFields     = D('TablesFields');

                $fields        = I('post.fields', array());
                $titles        = I('post.title2', array());
                $types         = I('post.types', array());
                $len           = I('post.len', array());
                $showtypes     = I('post.showtypes', array());
                $default_value = I('post.default_value', array());
                $status2       = I('post.status2', array());
                $pk            = I('post.pk', array());
                $require       = I('post.require', array());
                $ordid         = I('post.ordid', array());
                $_table        = C('DB_PREFIX') . $tables['model'];
                $_sql          = 'CREATE TABLE IF NOT EXISTS `' . $_table . '`(';
                $_pk           = array();
                $_path         = APP_PATH . MODULE_NAME . '/' . C('DEFAULT_M_LAYER') . '/';
                $_fields       = array('fid', 'step', 'steps', 'uid');
                $_valid        = array();
                $_model        = "<?php \n";
                if (C('APP_USE_NAMESPACE')) {
                    $_model .= "namespace " . MODULE_NAME . "\\" . C("DEFAULT_M_LAYER") . ";\n\n";
                }
                $_model .= "class " . parse_name($tables['model'], 1) . C("DEFAULT_M_LAYER") . " extends BaseModel {\n";
                foreach ($fields as $key => $val) {
                    $data = array(
                        'tid'           => $tid,
                        'fields'        => $val,
                        'title'         => $titles[$key],
                        'types'         => $types[$key],
                        'len'           => $len[$key],
                        'showtypes'     => $showtypes[$key],
                        'default_value' => $default_value[$key],
                        'status'        => intval($status2[$key]) == 1 ? 1 : 0,
                        'pk'            => intval($pk[$key]) == 1 ? 1 : 0,
                        'require'       => intval($require[$key]) == 1 ? 1 : 0,
                        'ordid'         => intval($ordid[$key]),
                    );
                    if ($tablesFields->create($data) != false) {
                        $_fields[] = $data['fields'];
                        if (in_array($data['types'], array('int', 'smallint', 'tinyint', 'decimal', 'varchar'))) {
                            $_len = '(' . $data['len'] . ')';
                        } else {
                            $_len = '';
                        }
                        $_sql .= '`' . $data['fields'] . '` ' . $data['types'] . $_len;
                        list($dk, $dv) = explode(':', $data['default_value']);
                        if ($dk == 'i') {
                            $_sql .= ' DEFAULT ' . $dv;
                        }
                        if ($data['pk'] == 1) {
                            $_sql .= ' NOT NULL AUTO_INCREMENT';
                            $_pk[] = $data['fields'];
                        }
                        if ($data['title']) {
                            $_sql .= "  COMMENT '" . $data['title'] . "'";
                        }
                        $_sql .= ',';
                        $tablesFields->add();
                        if ($data['require'] == 1 && $data['pk'] == 0) {
                            $_valid = array("\n\t\tarray('{$data['fields']}', 'require', '{$data['title']}不能为空')");
                        }
                    }
                }
                $_sql .= "`fid` int(11) NOT NULL COMMENT '流程ID', `step` int(11) NOT NULL COMMENT '当前步骤', `uid` int(11) NOT NULL COMMENT '创建者', `steps` text COMMENT '经过的步骤',";
                $_sql .= ' PRIMARY KEY (`' . implode(',', $_pk) . '`)';
                $_sql .= ') ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=' . C('DB_CHARSET') . ';';
                // dump($_sql);exit;
                $_model .= "\t" . 'protected $pk = "' . implode(',', $_pk) . '";' . "\n";
                $_model .= "\t" . 'protected $fields=array(\'' . implode("','", $_fields) . '\');' . "\n";
                $_model .= "\t" . 'protected $_auto=array();' . "\n";
                $_model .= "\t" . 'protected $_validate=array(' . implode(',', $_valid) . ');' . "\n";
                $_model .= "\t" . 'protected $_checkbox=array();' . "\n";
                $_model .= '}';
                // dump($_sql);exit;
                @file_put_contents($_path . parse_name($tables['model'], 1) . C("DEFAULT_M_LAYER") . '.class.php', $_model);
                $model = new \Think\Model();
                $model->query($_sql, false, false);
                $this->success('数据表创建成功', U('Tables/index'));
            }
        }
        $this->assign('bar', array('curpos' => '新建数据表', 'menu' => '数据表管理', 'url' => U(CONTROLLER_NAME . '/index')));
    }

    public function _before_edit() {
        if (IS_POST) {
            $tid = I('post.tid', 0, 'intval');
            if (!$tid) {
                $this->error('数据错误');
            }
            D('Tables')->_save();
            $_table        = D('Tables')->where(array('tid' => $tid))->getField('model');
            $fields        = I('post.fields', array());
            $titles        = I('post.title2', array());
            $types         = I('post.types', array());
            $len           = I('post.len', array());
            $showtypes     = I('post.showtypes', array());
            $default_value = I('post.default_value', array());
            $status2       = I('post.status2', array());
            $pk            = I('post.pk', array());
            $require       = I('post.require', array());
            $ordid         = I('post.ordid', array());
            $tablesFields  = D('TablesFields');
            $tablesFields->where(array('tid' => $tid))->delete();
            $model   = new \Think\Model();
            $_fields = D($_table)->getDbFields();
            $_table  = C('DB_PREFIX') . $_table;
            $tablesFields->where(array('tid' => $tid))->delete();
            foreach ($fields as $key => $val) {
                $data = array(
                    'tid'           => $tid,
                    'fields'        => $val,
                    'title'         => $titles[$key],
                    'types'         => $types[$key],
                    'len'           => $len[$key],
                    'showtypes'     => $showtypes[$key],
                    'default_value' => $default_value[$key],
                    'status'        => intval($status2[$key]) == 1 ? 1 : 0,
                    'pk'            => intval($pk[$key]) == 1 ? 1 : 0,
                    'require'       => intval($require[$key]) == 1 ? 1 : 0,
                    'ordid'         => intval($ordid[$key]),
                );
                if (false != $tablesFields->create($data)) {
                    $tablesFields->add();
                    if (!in_array($data['fields'], $_fields)) {
                        $_len          = in_array($data['types'], array('int', 'smallint', 'tinyint', 'decimal', 'varchar')) ? '(' . $len[$key] . ')' : '';
                        $_cmt          = $data['title'] ? ' COMMENT \'' . $data['title'] . '\'' : '';
                        $_nil          = $data['require'] == 1 ? ' NOT NULL ' : ' NULL ';
                        list($dk, $dv) = explode(':', $data['default_value']);
                        $_dft          = $dk == 'i' ? ' DEFAULT ' . $dv : '';
                        $_sql          = 'ALTER TABLE `' . $_table . '` ADD COLUMN `' . $data['fields'] . '`  ' . $data['types'] . $_len . $_nil . $_dft . $_cmt . ';';
                        $model->query($_sql, false, false);
                    }
                }
            }
            $this->success('数据表更新成功', U(CONTROLLER_NAME . '/index'));
        }
        $id     = I('get.id');
        $fields = D('TablesFields')->getAll(array('tid' => $id));
        $this->assign('fields', $fields);
        $this->assign('bar', array('curpos' => '编辑数据表', 'menu' => '数据表管理', 'url' => U(CONTROLLER_NAME . '/index')));
    }
}
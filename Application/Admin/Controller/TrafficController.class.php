<?php
/**
 * 交通信息
 * 在线路团期中显示的大交通信息，如起止地，时间，班次等信息
 * 不是最终算成本的大交通产品信息
 */
namespace Admin\Controller;

class TrafficController extends AuthController {
    public function _initialize() {
        parent::_initialize();
    }

	public function index() {
        $model = D('Traffic');
        $map['deletebs'] = 0;
        $sch = array();
        if(I('get.schbs')==1){
            $sch['schbs']           = I('get.schbs');
            $sch['roundtrip']       = I('get.roundtrip', '', 'trim');
            $sch['trafficbs']       = I('get.trafficbs', 0, 'intval');
            $sch['directbs']        = I('get.directbs', 0, 'intval');
            $sch['start_district']  = I('get.start_district', '', 'trim');
            $sch['arrive_district'] = I('get.arrive_district', '', 'trim');
            $sch['classes']         = I('get.classes', '', 'trim');

            if($sch['roundtrip']){
                $map['roundtrip'] = $sch['roundtrip'];
            }
            if($sch['trafficbs']){
                $map['trafficbs'] = $sch['trafficbs'];
            }
            if($sch['directbs']){
                $map['directbs'] = $sch['directbs'];
            }
            if($sch['start_district']){
                $map['start_district'] = array("like",'%'.$sch['start_district'].'%');
            }
            if($sch['arrive_district']){
                $map['arrive_district'] = array("like",'%'.$sch['arrive_district'].'%');
            }
            if($sch['classes']){
                $map['classes'] = array("like",'%'.$sch['classes'].'%');
            }
            
        }
        
        $field  = '';
        $order  = array(
            'ordid' => 'DESC',
            'id'    => 'DESC'
        );
        $zsys_data = D('Zsysdata')->get_zsysdata(array('type' =>array('IN','traffic,directbs')),1);
        $getlist   = $model->getList($map, $field, $order,$zsys_data);
        $this->assign('bar', array('curpos' => '列表', 'menu' => '交通信息', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('list', $getlist['list']);
        $this->assign('page', $getlist['page']);
        $this->assign('sch', $sch);
        $this->assign('traffics', $zsys_data['traffic']);
        $this->assign('directs', $zsys_data['directbs']);
        $this->display();
    }

    public function add() {
        if (IS_POST) {
            $model = D('Traffic');

            $roundtrip       = I('post.roundtrip', array());
            $trafficbs       = I('post.trafficbs', array());
            $start_district  = I('post.start_district', array());
            $arrive_district = I('post.arrive_district', array());
            $classes         = I('post.classes', array());
            $start_hour      = I('post.start_hour', array());
            $start_minute    = I('post.start_minute', array());
            $arrive_hour     = I('post.arrive_hour', array());
            $arrive_minute   = I('post.arrive_minute', array());
            $directbs        = I('post.directbs', array());
            $ordid           = I('post.ordid', array());
            
            if(is_array($roundtrip)){
                foreach ($roundtrip as $key => $val) {
                    if($val == ''){
                        continue;
                    }

                    $data = array(
                        'roundtrip'       => $roundtrip[$key],
                        'trafficbs'       => $trafficbs[$key],
                        'start_district'  => $start_district[$key],
                        'arrive_district' => $arrive_district[$key],
                        'classes'         => $classes[$key],
                        'start_hour'      => $start_hour[$key],
                        'start_minute'    => $start_minute[$key],
                        'arrive_hour'     => $arrive_hour[$key],
                        'arrive_minute'   => $arrive_minute[$key],
                        'directbs'        => intval($directbs[$key]),
                        'ordid'           => intval($ordid[$key]),
                        'status'          => 1,
                        'admin_id'        => session('admin_id'),
                    );
                    if ($model->create($data)) {
                        $model->add();
                    } else {
                        echo $this->getError();
                    }
                }
            }
            $this->success('添加成功', U(CONTROLLER_NAME . '/index'));
        }

        $zsys_data = D('Zsysdata')->get_zsysdata(array('type' =>array('IN','traffic,directbs')),1);
        $this->assign('bar', array('curpos' => '添加', 'menu' => '交通信息', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('traffics', $zsys_data['traffic']);
        $this->assign('directs', $zsys_data['directbs']);
        $this->display();
    }

    public function edit() {
        $model = D('Traffic');
        if (IS_AJAX) {
            $field  = I('post.field', '', 'trim');
            $id     = I('post.id', '', 'trim');
            $pk     = I('post.pk', '', 'trim');
            $value  = I('post.value', '', 'trim');
            $status = 0;
            $msg    = '编辑失败';
            if ($field && $id & $pk && $value) {
                $status = 1;
                $msg    = '编辑成功';
                $model->where(array($pk => $id))->save(array($field => $value));
            }
            $this->ajaxReturn(array('msg' => $msg, 'status' => $status));
        } elseif (IS_POST) {
            if (!I('post.id')) {
                return array('status' => 0, 'msg' => '传参有误!');
            }
            $data = array(
                'id'              => I('post.id', 0, 'intval'),
                'roundtrip'       => I('post.roundtrip', '', 'trim'),
                'trafficbs'       => I('post.trafficbs', 0, 'intval'),
                'start_district'  => I('post.start_district', '', 'trim'),
                'arrive_district' => I('post.arrive_district', '', 'trim'),
                'classes'         => I('post.classes', '', 'trim'),
                'start_hour'      => I('post.start_hour', '', 'trim'),
                'start_minute'    => I('post.start_minute', '', 'trim'),
                'arrive_hour'     => I('post.arrive_hour', '', 'trim'),
                'arrive_minute'   => I('post.arrive_minute', '', 'trim'),
                'directbs'        => I('post.directbs', 0, 'intval'),
                'ordid'           => I('post.ordid', 0, 'intval'),
            );
            
            $result = $model->_save($data);
            if ($result['status'] == 1) {
                $this->success($result['msg'], U(CONTROLLER_NAME . '/edit?id='.I('post.id')));
            } else {
                $this->error($result['msg']);
            }
        }

        $id = I('get.id', 0, 'intval');
        if (!$id) {
            $this->error('传参有误!');
        }

        $data = array(
            'id' => $id,
            'deletebs' => 0,
        );

        $info = $model -> where($data) -> find();
        if (!$info) {
            $this->error('数据不存在');
        }
        $zsys_data = D('Zsysdata')->get_zsysdata(array('type' =>array('IN','traffic,directbs')),1);
        $this->assign('bar', array('curpos' => '编辑', 'menu' => '交通信息', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('info', $info);
        $this->assign('traffics', $zsys_data['traffic']);
        $this->assign('directs', $zsys_data['directbs']);
        $this->display();
    }
    public function delete() {
        $id    = I('request.id');
        $model = M('Traffic');
        if (!$id) {
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => '参数错误'));
            $this->error($this->error('参数错误'));
        }
        $id = explode(',', $id);
        foreach ($id as &$i) {
            $i = intval($i);
        }
        $data = array(
            'deletebs' => 1,
        );
        
        $model->where(array('id' => array('IN', implode(',', $id))))->limit(count($id))->save($data);
        IS_AJAX && $this->ajaxReturn(array('status' => 1, 'msg' => '删除成功'));
        $this->success('删除成功');
    }

    public function findbyid(){
        if(!IS_AJAX) {
            $this->error('error');
        }
        $id = I('post.traffic_id', 0, 'intval');
        if(!$id){
            $this->ajaxReturn(array('status' => 0, 'msg' => '传参有误'), 'JSON');
        }
        $data = M('Traffic')->field('le.*,z.title as t_trafficbs,z2.title as t_directbs')->alias('le')
                ->join('__ZSYSDATA__ z ON le.trafficbs = z.id', 'LEFT')
                ->join('__ZSYSDATA__ z2 ON le.directbs = z2.id', 'LEFT')
                ->where(array('le.id' => $id))->find();
        $bs = $data['roundtrip'] == '往'?'go':($data['roundtrip'] == '返'?'back':'');
        $content  = $data['roundtrip'] .'&nbsp;&nbsp;'. $data['t_trafficbs'];
        $content .= ($data['start_district']?'&nbsp;&nbsp;' . $data['start_district']:'');
        $content .= $data['arrive_district']?'/' . $data['arrive_district']:'';
        $content .= $data['classes']?'&nbsp;&nbsp;'. $data['classes']:'';
        $content .= ($data['start_hour']?'&nbsp;&nbsp;':'') . $data['start_hour'] .($data['start_minute']?':':'') . $data['start_minute'];
        $content .= ($data['arrive_hour']?'/':'') . $data['arrive_hour'] .($data['arrive_minute']?':':'') . $data['arrive_minute'];
        $content .= $data['t_directbs']?'&nbsp;&nbsp;'.$data['t_directbs']:'';
        $msg = (!$bs || $data['id'] != $id)?'数据不存在':'success';
        $this->ajaxReturn(array('status' => 1, 'msg' => $msg, 'bs' => $bs, 'content' => $content), 'JSON');
    }
}
<?php
/**
 * 团期管理(能被预定的组合后的团期，而非地接线路价格表)
 */
namespace Admin\Controller;

class TripdateController extends AuthController {
    public function _initialize() {
        parent::_initialize();
    }
    public function index() {
        $model  = D('Tripdate');
        $map['l.deletebs'] = 0;
        $field  = 'l.id,l.tripdate,l.tickettime,l.warningtime,l.days,l.seatnum,l.seatused,l.overplus,l.sharesetnum,l.assign_nums,l.assign_partners,l.child';
        $field .= ',e.title,e.departure,e.arrive,e.traffic_goid,e.traffic_backid,e.traffic_goshorttxt,e.traffic_backshorttxt,e.traffic_gotxt,e.traffic_backtxt,e.line_partner_title';
        $order  = 'l.tripdate asc';
        $getlist    = $model->getList($map, $field, $order);
        $this->assign('bar', array('curpos' => '列表', 'menu' => '团期管理', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('lists', $getlist['lists']);
        $this->assign('page', $getlist['page']);
        $this->display();
    }
    public function add() {
        if (IS_AJAX && IS_POST) {
            $lineid           = I('post.lineid', 0, 'intval');//线路ID
            $days             = I('post.days', 0, 'intval');//线路ID
            $title            = I('post.title', '', 'trim');//团期线路名称
            $_grateprices     = I('post.grateprices', array());//线路标准等级
            $viewbs           = I('post.viewbs', array());
            $departure        = I('post.departure', '', 'trim');//出发地区
            $arrive           = I('post.arrive', '', 'trim');//到达地区
            $shuttleids       = I('post.shuttleid', array());//接团地区
            $_tripdates       = I('post.tripdates', array());//出行团期
            $startdaytxt      = I('post.startdaytxt', '', 'trim');//行程第一天前追加
            $enddaytxt        = I('post.enddaytxt', '', 'trim');//行程最后一天后追加
            $roundtrip_goid   = I('post.roundtrip_goid', 0, 'intval');//交通信息 往id
            $roundtrip_backid = I('post.roundtrip_backid', 0, 'intval');//交通信息 返id
            $_assignseats     = I('post.assignseats', array());//座位分配
            $_promotion       = I('post.promotion', array());//返利促销
            $errmsg  = '';
            $errmsg .= !$lineid ?"x 请选择地接线路\r\n <br/>":'';
            $errmsg .= !$title ?"x 请填写团期线路名称\r\n <br/>":'';
            $errmsg .= !$days ?"x 请填写团期天数\r\n <br/>":'';
            $errmsg .= (!is_array($_grateprices) || empty($_grateprices))?"x 请选择线路标准等级/价格\r\n <br/>":'';
            $errmsg .= !$departure ?"x 请选择出发地区\r\n <br/>":'';
            $errmsg .= !$arrive ?"x 请选择到达地区\r\n <br/>":'';
            $errmsg .= (!is_array($shuttleids) || empty($shuttleids))?"x 请选择接团地区\r\n <br/>":'';
            $errmsg .= (!is_array($_tripdates) || empty($_tripdates))?"x 请填写出行团期\r\n <br/>":'';
            if($errmsg != ''){
                IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
                $this->show($errmsg);
                exit();
            }
            $lines     = $this->checkLine($lineid);//检查线路,是否在权限内
            $districts = $this->checkDepartureArrive($departure, $arrive);//检查起止地,返回值
            $this->checkRebate($_promotion);//检查返利
            $assignseats = $this->checkSeats('', $_assignseats);//第一个参数留空(更新团期时用到),检查座位数,返回值
            $this->checkAddTripdateSeat($_tripdates, $assignseats);//检查团期,座位分配是否合理
            $shuttles = $this->checkShuttles($shuttleids);//检查接团地区,返回值
            $grateprices = $this->checkGratePrices($_grateprices, $lineid);//检查线路等级/价格,返回值
            $zsys_data = D('Zsysdata')->get_zsysdata(array('type' =>array('IN','traffic,directbs')));// 系统配置数据
            $traffics = $this->formattraffics($roundtrip_goid,$roundtrip_backid,$zsys_data);// 交通信息格式化
            
            M('Tripdate')->startTrans();
            //写入团期主表
            foreach($_tripdates['tripdate'] as $key_t => $val_t){
                $data = array();
                $data['tripdate']             = strtotime($val_t);
                $data['tickettime']           = $_tripdates['tickettime'][$key_t]?strtotime($_tripdates['tickettime'][$key_t]):0;
                $data['warningtime']          = $_tripdates['warningtime'][$key_t]?strtotime($_tripdates['warningtime'][$key_t]):0;
                $data['departure_id']         = $districts['departure_id'];
                $data['arrive_id']            = $districts['arrive_id'];
                $data['adult_traffic_price']  = floatval($_tripdates['adult_traffic_price'][$key_t]);//成人大交通费
                $data['child_traffic_price']  = floatval($_tripdates['child_traffic_price'][$key_t]);//儿童大交通费
                $data['seatnum']              = intval($_tripdates['seatnum'][$key_t]);//共有座位数
                $data['overplus']             = intval($_tripdates['seatnum'][$key_t]);//余位数,因新开团期，初始值就是共座位数
                $data['sharesetnum']          = intval($_tripdates['seatnum'][$key_t]);//共享单位座位数,初始值为共座位数，如查存在位子分配，该值会在分配管理中自动更新
                $data['adult_adjustprice']    = floatval($_tripdates['adult_adjustprice'][$key_t]);
                $data['child_adjustprice']    = floatval($_tripdates['child_adjustprice'][$key_t]);
                $data['baby_adjustprice']     = floatval($_tripdates['baby_adjustprice'][$key_t]);
                $data['lineid']               = $lineid;
                $data['days']                 = $days;
                $data['child']                = intval($viewbs['child']) == 1 ? 1 : 0;
                $data['baby']                 = intval($viewbs['child']) == 1 && intval($viewbs['baby']) == 1 ? 1 : 0;
                $data['line_partner_id']      = $lines['partners_id'];//地接线路合作单位id
                $data['line_cate_id']         = $lines['cate_id'];//地接线路分类id,就是专线分类id

                $result = D('Tripdate')->_add($data);
                $tripdateid = intval($result['id']);
                // 2 写入团期扩展数据 ****************************
                if($result['status'] ==1 && $tripdateid > 0){
                    $data_ext['tripdateid']           = $tripdateid;
                    $data_ext['title']                = $title;
                    $data_ext['departure']            = $departure;
                    $data_ext['arrive']               = $arrive;
                    $data_ext['startdaytxt']          = $startdaytxt;
                    $data_ext['enddaytxt']            = $enddaytxt;
                    $data_ext['traffic_goid']         = $roundtrip_goid;
                    $data_ext['traffic_backid']       = $roundtrip_backid;
                    $data_ext['traffic_gotxt']        = $traffics['go'];
                    $data_ext['traffic_backtxt']      = $traffics['back'];
                    $data_ext['traffic_goshorttxt']   = $traffics['goshort'];
                    $data_ext['traffic_backshorttxt'] = $traffics['backshort'];
                    $data_ext['line_partner_title']   = $lines['partner_title'];//地接线路合作单位名称
                    
                    $result_e = D('TripdateExpand')->_add($data_ext);
                    if($result_e['status'] != 1){
                        M('Tripdate')->rollback();
                        $errmsg = '保存扩展团期数据失败';
                        IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
                        $this->show($errmsg);
                        exit();
                    }
                } else {
                    M('Tripdate')->rollback();
                    $errmsg = '保存团期数据失败';
                    IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
                    $this->show($errmsg);
                    exit();
                }
                // 3 写入接团地区/线路等级价格数据
                $tmp_startdistrict_ids = array();
                foreach($shuttles as $skey => $sval){
                    $tmp_startdistrict_ids[$sval['districtid']] = $sval['districtid'];
                    $data_s['tripdateid']        = $tripdateid;//主团期id
                    $data_s['start_district_id'] = $sval['id'];//接团信息表id
                    $data_s['district_id']       = $sval['districtid'];//接团地区id
                    $data_s['district_title']    = $sval['district_title'];//接团地名称
                    $data_s['shuttle_adult_price']  = floatval($sval['adult_price']);//接送费 成人
                    $data_s['shuttle_child_price']  = floatval($sval['child_price']);//接送费 儿童
                    //地接线标准等级/价格
                    foreach($grateprices as $gkey => $gval){
                        $data_s['lineprice_id']     = $gval['id'];//价格表id
                        $data_s['linegrade_id']     = $gval['gradeid'];//标准等级id
                        $data_s['linegrade_txt']    = $gval['grate_title'];//标准等级名称
                        $data_s['adult_price']      = $gval['adult_price'];//成人成本价
                        $data_s['adult_profit']     = $gval['adult_profit'];//成人利润
                        $data_s['child_price']      = $gval['child_price'];//儿童成本价
                        $data_s['child_profit']     = $gval['child_profit'];//儿童利润 
                        $data_s['baby_price']       = $gval['baby_price'];//婴儿成本价
                        $data_s['baby_profit']      = $gval['baby_profit'];//婴儿利润
                        $data_s['singleroom_price'] = $gval['singleroom_price'];//单房差
                        
                        //团期成人总卖价 (线路成人成本+利润) + 接团地成人接送费 + 团期成人大交通费 + 团期成人调节利润价格
                        $data_s['adult_saleprice']  = floatval($gval['adult_price']) + 
                                                      floatval($gval['adult_profit']) + 
                                                      floatval($sval['adult_price']) + 
                                                      floatval($_tripdates['adult_traffic_price'][$key_t]) +
                                                      floatval($_tripdates['adult_adjustprice'][$key_t]);
                        //团期儿童总卖价 (线路儿童成本+利润) + 接团地儿童接送费 + 团期儿童大交通费 + 团期儿童调节利润价格
                        $data_s['child_saleprice']  = floatval($gval['child_price']) + 
                                                      floatval($gval['child_profit']) + 
                                                      floatval($sval['child_price']) + 
                                                      floatval($_tripdates['child_traffic_price'][$key_t]) +
                                                      floatval($_tripdates['child_adjustprice'][$key_t]);
                        //团期婴儿总卖价 (线路婴儿成本+利润) + 无接送费 + 无大交通费 + 团期婴儿调节利润价格
                        if(intval($viewbs['child']) == 1 && intval($viewbs['baby']) == 1){//当婴儿选中
                            $data_s['baby_saleprice'] = floatval($gval['baby_price']) + 
                                                        floatval($gval['baby_profit']) + 
                                                        floatval($_tripdates['baby_adjustprice'][$key_t]);
                        } else {
                            $data_s['baby_saleprice'] = 0;
                        }
                        $data_s['status'] = 1;//状态
                        $result_s = D('TripdateTopics')->_add($data_s);
                        if($result_s['status'] != 1){
                            M('Tripdate')->rollback();
                            $errmsg = '保存团期接团地区数据失败';
                            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
                            $this->show($errmsg);
                            exit();
                        }
                    }   
                }
                // 写入座位分配数据
                if($assignseats){
                    $assign_nums = 0;
                    $tmp_arr = array();
                    foreach($assignseats as $aval){
                        if(intval($aval['seatnum']) <= 0){
                            continue;
                        }
                        $data_a = array();
                        $data_a['tripdateid']    = $tripdateid;
                        $data_a['partners_id']   = $aval['id'];
                        $data_a['seatnum']       = intval($aval['seatnum']);
                        $data_a['viewshareseat'] = intval($aval['shareseatbs']) == 1?1:0;
                        $result_a = D('TripdateSeat')->_add($data_a);
                        if($result_a['status'] != 1){
                            M('Tripdate')->rollback();
                            $errmsg = '保存团期座位分配数据失败';
                            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
                            $this->show($errmsg);
                            exit();
                        }
                        $tmp_arr[] = $aval;
                        $assign_nums += intval($aval['seatnum']);
                    }
                    //更新主团期表中的：是否有单独位子分配标识，共享单位座位是多少
                    $max_price = M('TripdateTopics')->where(array('tripdateid' => $tripdateid, 'deletebs' => 0))->max('adult_saleprice');
                    $min_price = M('TripdateTopics')->where(array('tripdateid' => $tripdateid, 'deletebs' => 0, 'adult_saleprice' => array('GT', 0)))->min('adult_saleprice');
                    $assign_partners = $tmp_arr ? implode(',', $tmp_arr) : '';
                    $datae_t = array();
                    $datae_t['id']              = $tripdateid;
                    $datae_t['assign_nums']     = $assign_nums;//单独分配了多少座位
                    $datae_t['assign_partners'] = $assign_partners;//单独分配座位给了哪些合作单位
                    $datae_t['sharesetnum']     = intval($_tripdates['seatnum'][$key_t]) - $assign_nums;//共享单位位子数 公式：团期中所有座位数 - 单独分配出去的总数
                    $datae_t['startdistricts']  = $tmp_startdistrict_ids?implode(',',$tmp_startdistrict_ids):'';
                    $datae_t['min_price']       = $min_price;
                    $datae_t['max_price']       = $max_price;
                    $result_et = D('Tripdate')->_save($datae_t);
                    if($result_et['status'] != 1){
                        M('Tripdate')->rollback();
                        $errmsg = '保存团期座位分配记录失败';
                        IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
                        $this->show($errmsg);
                        exit();
                    }
                }
                // 5 写入返利促销数据 ****************************
                if($_promotion['id']){
                    foreach($_promotion['id'] as $pkey => $pval){
                        if($pval != 1 || ($pval > 0 && intval($_promotion['price'][$pkey]) == 0)){
                            continue;
                        }
                        $data_p = array();
                        $data_p['tripdateid'] = $tripdateid;
                        $data_p['typeid']     = intval($pkey);
                        $data_p['price']      = intval($_promotion['price'][$pkey]);
                        $data_p['adult_bs']   = 1;
                        $data_p['child_bs']   = intval($_promotion['child'][$pkey])==1?1:0;
                        $data_p['baby_bs']    = intval($_promotion['child'][$pkey])==1 && intval($_promotion['baby'][$pkey])==1?1:0;
                        $result_p = D('TripdateRebate')->_add($data_p);
                        if($result_p['status'] != 1){
                            M('Tripdate')->rollback();
                            $errmsg = '保存团期返利数据失败';
                            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
                            $this->show($errmsg);
                            exit();
                        }
                    }
                }
            }
            M('Tripdate')->commit();
            $msg = '新增团期成功';
            $url = U(CONTROLLER_NAME . '/index?cate_id='.$lines['cate_id']);
            IS_AJAX && $this->ajaxReturn(array('status' => 1, 'msg' => $msg, 'url' => $url), 'JSON');
            $this->success($msg,$url);
            exit();
        }
        //所有出发地
        $startdistricts = D('StartDistrict')->getListAll(array('le.deletebs' => 0, 'le.status' => 1),'le.id,le.start_districtid,le.message,le.adult_price,le.child_price,d.title as t_start_districtid','le.ordid desc,le.id');
        if(I('get.submethod') == 'selectline'){//线路产品选择
            $this->selectline();
            exit();
        }
        if(I('get.submethod') == 'selecttraffic'){//交通信息选择
            $this->selecttraffic();
            exit();
        }
        $zsys_data = D('Zsysdata')->get_zsysdata(array('type' => 'promotion', 'status' => 1),1);//促销定制类型
        //可单独分配座位的合作伙伴
        $partners_assignseat = D('Partners')->get_partners(array('type' => 2, 'seatassign' => 1, 'status' => 1, 'deletebs' => 0));
        $this->assign('bar', array('curpos' => '添加', 'menu' => '团期管理', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('startdistricts', $startdistricts);
        $this->assign('partners_assignseats', $partners_assignseat);
        $this->assign('promotions', $zsys_data['promotion']);
        $this->display();
    }
    public function edit() {
        $model = D('tripdate');
        if (IS_AJAX || IS_POST) {
            $tripdateid        = I('post.tripdateid', 0, 'intval');
            $lineid            = I('post.lineid', 0, 'intval');
            $days              = I('post.days', 0, 'intval');//线路ID
            $title             = I('post.title', '', 'trim');//团期线路名称
            $viewbs            = I('post.viewbs', array());//儿童是否显示等
            $departure         = I('post.departure', '', 'trim');//出发地区
            $arrive            = I('post.arrive', '', 'trim');//到达地区
            $_tripdates        = I('post.tripdates', array());//接团地区
            $shuttleids        = I('post.shuttleid', array());//接团地区
            $_assignseats_data = I('post.assignseats_data', array());//已有的座位分配
            $_assignseats      = I('post.assignseats', array());//新座位分配
            $_promotions       = I('post.promotion', array());//返利
            $_gradeprice       = I('post.gradeprice', array());//线路等级/价格
            $roundtrip_goid    = I('post.roundtrip_goid', 0, 'intval');//交通信息 往id
            $roundtrip_backid  = I('post.roundtrip_backid', 0, 'intval');//交通信息 返id
            $startdaytxt       = I('post.startdaytxt', '', 'trim');
            $enddaytxt         = I('post.enddaytxt', '', 'trim');

            $info = $this->TripdateCheck($tripdateid);//检查团期id

            if($title == '' || $days <= 0){
                $errmsg = '团期线路名称,天数不能为空';
                IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
                $this->show($errmsg);
                exit();
            }

            $districts   = $this->checkDepartureArrive($departure, $arrive);//检查起止地,返回值
            $shuttles    = $this->checkShuttles($shuttleids);//检查接团地区,返回值
            $grateprices = $this->checkGratePrices($_gradeprice, $lineid);//检查线路等级/价格,返回值
            $assignseats = $this->checkSeats($_assignseats_data, $_assignseats);//检查座位数，返回值
            $this->checkTripdateSeat($_tripdates, $assignseats, $info);//检查团期,座位分配是否合理
            $this->checkRebate($_promotions);//检查返利
            $zsys_data = D('Zsysdata')->get_zsysdata(array('type' =>array('IN','traffic,directbs')));//系统配置数据
            $traffics = $this->formattraffics($roundtrip_goid,$roundtrip_backid,$zsys_data);//交通信息格式化

            //更新 
            M('Tripdate')->startTrans();
            //写入团期主表数据
            $data_z = array();
            $data_z['id']                   = $tripdateid;
            $data_z['days']                 = $days;
            $data_z['tickettime']           = $_tripdates['tickettime'] ? strtotime($_tripdates['tickettime']) : 0;
            $data_z['warningtime']          = $_tripdates['warningtime'] ? strtotime($_tripdates['warningtime']) : 0;
            $data_z['departure_id']         = $districts['departure_id'];
            $data_z['arrive_id']            = $districts['arrive_id'];
            $data_z['child']                = intval($viewbs['child']) == 1 ? 1 : 0;
            $data_z['baby']                 = intval($viewbs['child']) == 1 && intval($viewbs['baby']) == 1 ? 1 : 0;
            $data_z['adult_traffic_price']  = floatval($_tripdates['adult_traffic_price']);//成人大交通费
            $data_z['child_traffic_price']  = floatval($_tripdates['child_traffic_price']);//儿童大交通费
            $data_z['seatnum']              = intval($_tripdates['seatnum']);//共有座位数
            $data_z['adult_adjustprice']    = floatval($_tripdates['adult_adjustprice']);
            $data_z['child_adjustprice']    = floatval($_tripdates['child_adjustprice']);
            $data_z['baby_adjustprice']     = floatval($_tripdates['baby_adjustprice']);
            $result_z = D('Tripdate')->_save($data_z);
            if($result_z['status'] != 1){
                M('Tripdate')->rollback();
                $errmsg = '更新团期数据失败';
                IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
                $this->show($errmsg);
                exit();
            }
            //写入团期扩展表数据
            $data_e = array();
            $data_e['title']                = $title;
            $data_e['departure']            = $departure;
            $data_e['arrive']               = $arrive;
            $data_e['startdaytxt']          = $startdaytxt;
            $data_e['enddaytxt']            = $enddaytxt;
            $data_e['traffic_goid']         = $roundtrip_goid;
            $data_e['traffic_backid']       = $roundtrip_backid;
            $data_e['traffic_gotxt']        = $traffics['go'];
            $data_e['traffic_backtxt']      = $traffics['back'];
            $data_e['traffic_goshorttxt']   = $traffics['goshort'];
            $data_e['traffic_backshorttxt'] = $traffics['backshort'];
            $map_e['tripdateid'] = $tripdateid;
            $result_e = D('TripdateExpand')->_save($data_e, $map_e);
            if($result_e['status'] != 1){
                M('Tripdate')->rollback();
                $errmsg = '更新团期扩展数据失败';
                IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
                $this->show($errmsg);
                exit();
            }

            // 写入接团地区/线路等级价格数据 ****************************
            M('TripdateTopics')->where(array('tripdateid' => $tripdateid))->save(array('deletebs' => 1));
            $tmp_startdistrict_ids = array();
            foreach($shuttles as $skey => $sval){
                $tmp_startdistrict_ids[$sval['districtid']] = $sval['districtid'];
                $data_s = array();
                $data_s['start_district_id'] = $sval['id'];//接团信息表id
                $data_s['district_id']       = $sval['districtid'];//接团地区id
                $data_s['district_title']    = $sval['district_title'];//接团地名称
                $data_s['shuttle_adult_price']  = floatval($sval['adult_price']);//接送费 成人
                $data_s['shuttle_child_price']  = floatval($sval['child_price']);//接送费 儿童
                //地接线标准等级/价格
                foreach($grateprices as $gkey => $gval){
                    $data_s['lineprice_id']     = $gval['id'];//价格表id
                    $data_s['linegrade_id']     = $gval['gradeid'];//标准等级id
                    $data_s['linegrade_txt']    = $gval['grate_title'];//标准等级名称
                    $data_s['adult_price']      = $gval['adult_price'];//成人成本价
                    $data_s['adult_profit']     = $gval['adult_profit'];//成人利润
                    $data_s['child_price']      = $gval['child_price'];//儿童成本价
                    $data_s['child_profit']     = $gval['child_profit'];//儿童利润 
                    $data_s['baby_price']       = $gval['baby_price'];//婴儿成本价
                    $data_s['baby_profit']      = $gval['baby_profit'];//婴儿利润
                    $data_s['singleroom_price'] = $gval['singleroom_price'];//单房差
                    //团期成人总卖价 (线路成人成本+利润) + 接团地成人接送费 + 团期成人大交通费 + 团期成人调节利润价格
                    $data_s['adult_saleprice']  = floatval($gval['adult_price']) + 
                                                  floatval($gval['adult_profit']) + 
                                                  floatval($sval['adult_price']) + 
                                                  floatval($_tripdates['adult_traffic_price']) +
                                                  floatval($_tripdates['adult_adjustprice']);
                    //团期儿童总卖价 (线路儿童成本+利润) + 接团地儿童接送费 + 团期儿童大交通费 + 团期儿童调节利润价格
                    $data_s['child_saleprice']  = floatval($gval['child_price']) + 
                                                  floatval($gval['child_profit']) + 
                                                  floatval($sval['child_price']) + 
                                                  floatval($_tripdates['child_traffic_price']) +
                                                  floatval($_tripdates['child_adjustprice']);
                    //团期婴儿总卖价 (线路婴儿成本+利润) + 无接送费 + 无大交通费 + 团期婴儿调节利润价格
                    if(intval($viewbs['child']) == 1 && intval($viewbs['baby']) == 1){//当婴儿选中
                        $data_s['baby_saleprice'] = floatval($gval['baby_price']) + 
                                                    floatval($gval['baby_profit']) + 
                                                    floatval($_tripdates['baby_adjustprice']);
                    } else {
                        $data_s['baby_saleprice'] = 0;
                    }
                    $data_s['status'] = 1;
                    $map_s = array();
                    $map_s['tripdateid']        = $tripdateid;
                    $map_s['start_district_id'] = $sval['id'];
                    $map_s['district_id']       = $sval['districtid'];
                    $map_s['linegrade_id']      = $gval['gradeid'];
                    if(M('TripdateTopics')->where($map_s)->count() == 1){
                        $data_s['deletebs'] = 0;
                        $result_s = D('TripdateTopics')->_save($data_s,$map_s);
                    }else{
                        $data_s['tripdateid'] = $tripdateid;
                        $result_s = D('TripdateTopics')->_add($data_s);
                    }
                    if($result_s['status'] != 1){
                        M('Tripdate')->rollback();
                        $errmsg = '保存团期数据失败';
                        IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
                        $this->show($errmsg);
                        exit();
                    }
                }   
            }
            // 写入座位分配数据
            $data_t   = array('deletebs' => 1);
            $map_t    = array('tripdateid' => $tripdateid);
            $result_t = D('TripdateSeat')->_save($data_t, $map_t);
            if($assignseats){
                $assign_nums = 0;
                $tmp_arr = array();
                foreach($assignseats as $aval){
                    if(!$aval['seatnum'] || intval($aval['seatnum']) <= 0){
                        continue;
                    }
                    $data_a = array();
                    $data_a['seatnum']       = intval($aval['seatnum']);
                    $data_a['viewshareseat'] = intval($aval['viewshareseat']) == 1?1:0;
                    if(M('TripdateSeat')->where(array('tripdateid' => $tripdateid, 'partners_id' => $aval['id']))->count() == 1){
                        $data_a['deletebs']  = 0;
                        $map_a = array('tripdateid' => $tripdateid, 'partners_id' => $aval['id']);
                        $result_a = D('TripdateSeat')->_save($data_a, $map_a);
                    }else{
                        $data_a['tripdateid']    = $tripdateid;
                        $data_a['partners_id']   = $aval['id'];
                        $result_a = D('TripdateSeat')->_add($data_a);
                    }
                    if($result_a['status'] != 1){
                        M('Tripdate')->rollback();
                        $errmsg = '保存座位分配数据失败';
                        IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
                        $this->show($errmsg);
                        exit();
                    }
                    $tmp_arr[] = $aval['id'];
                    $assign_nums += intval($aval['seatnum']);
                }
                $max_price = M('TripdateTopics')->where(array('tripdateid' => $tripdateid, 'deletebs' => 0))->max('adult_saleprice');
                $min_price = M('TripdateTopics')->where(array('tripdateid' => $tripdateid, 'deletebs' => 0, 'adult_saleprice' => array('GT', 0)))->min('adult_saleprice');
                //更新主团期表中的：是否有单独位子分配标识，共享单位座位是多少
                $assign_partners = $tmp_arr ? implode(',', $tmp_arr) : '';
                $datae_t = array();
                $datae_t['id']              = $tripdateid;
                $datae_t['assign_nums']     = $assign_nums;//单独分配了多少座位
                $datae_t['assign_partners'] = $assign_partners;//单独分配座位给了哪些合作单位
                $datae_t['sharesetnum']     = intval($_tripdates['seatnum']) - $assign_nums;//共享单位位子数 公式：团期中所有座位数 - 单独分配出去的总数
                $datae_t['overplus']        = intval($_tripdates['seatnum']) - intval($info['seatused']);
                $datae_t['startdistricts']  = $tmp_startdistrict_ids?implode(',',$tmp_startdistrict_ids):'';
                $datae_t['min_price']       = $min_price;
                $datae_t['max_price']       = $max_price;
                $result_et = D('Tripdate')->_save($datae_t);
                if($result_et['status'] != 1){
                    M('Tripdate')->rollback();
                    $errmsg = '更新主团期座位分配数据失败';
                    IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
                    $this->show($errmsg);
                    exit();
                }
            }
            $this->updateRebate($_promotions, $tripdateid, $rollback=1);//更新返利
            M('Tripdate')->commit();
            $msg = '更新团期成功';
            $url = U(CONTROLLER_NAME . '/edit?tripdateid=' . $tripdateid);
            IS_AJAX && $this->ajaxReturn(array('status' => 1, 'msg' => $msg, 'url' => $url), 'JSON');
            $this->success($msg,$url);
            exit();
        }
        $tripdateid = I('get.tripdateid', 0, 'intval');
        $info   = M('Tripdate')->where(array('id' => $tripdateid))->find();
        if(!$info || $info['id'] != $tripdateid){
            $this->show('不存在的团期!');
            exit();
        }
        //返利按时间段管理页
        if(I('get.submethod') == 'timerebate'){
            $this->timerebate();
            exit();
        }
        //检查是否拥有该专线的权限 未处理
        $gradeprices = $this->getlineprices($info['lineid']);//地接线路标准/价格
        $startdistricts = D('StartDistrict')->getListAll(array('le.deletebs' => 0, 'le.status' => 1),'le.id,le.start_districtid,le.message,le.adult_price,le.child_price,d.title as t_start_districtid','le.ordid desc,le.id');
        $zsys_data = D('Zsysdata')->get_zsysdata(array('type' => 'promotion', 'status' => 1),1);//促销定制类型
        //可单独分配座位的合作伙伴
        $partners_assignseat = D('Partners')->get_partners(array('type' => 2, 'seatassign' => 1, 'status' => 1, 'deletebs' => 0),1);
        $expand = M('TripdateExpand')->where(array('tripdateid' => $tripdateid))->find();// 团期扩展数据
        $gradeprices_data = D('TripdateTopics')->gettopics($tripdateid);// 接团地/标准/价格数据
        $seats_data = D('TripdateSeat')->gettopics($tripdateid);//座位分配数据
        $rebates_data = D('TripdateRebate')->gettopics($tripdateid);// 返利促销数据
        $this->assign('bar', array('curpos' => '编辑', 'menu' => '团期管理', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('tripdateid', $tripdateid);
        $this->assign('info', $info);
        $this->assign('expand', $expand);
        $this->assign('startdistricts', $startdistricts);
        $this->assign('partners_assignseats', $partners_assignseat);
        $this->assign('promotions', $zsys_data['promotion']);
        $this->assign('gradeprices_data', $gradeprices_data);
        $this->assign('gradeprices', $gradeprices);
        $this->assign('seats_data', $seats_data);
        $this->assign('rebates_data', $rebates_data);
        $this->display();
    }
    //团期编辑中按时间段的返利页
    public function timerebate(){
        if (IS_AJAX || IS_POST) {
            $tripdateid       = I('post.tripdateid', 0, 'intval');//线路ID
            $rebatebsid       = I('post.rebatebsid', 0, 'intval');//线路ID
            $_edits           = I('post.edits', array());//编辑的主题
            $_timerebates     = I('post.timerebates', array());//新增的数据
            $errmsg  = '';
            $errmsg .= !$tripdateid ?"x 团期不存在\r\n <br/>":'';
            $errmsg .= !$rebatebsid ?"x 返利政策类型不存在\r\n <br/>":'';
            $errmsg .= !$_edits && !$_timerebates ?"x 没有数据被提交\r\n <br/>":'';

            if($errmsg != ''){
                IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
                $this->show($errmsg);
                exit();
            }
            if($_edits['starttime']){
                foreach($_edits['starttime'] as $skey => $sval){
                    if($sval == '' || $_edits['endtime'][$skey] == '' || intval($_edits['price'][$skey]) <= 0 || $_edits['adult'][$skey] !=1){
                        continue;
                    }
                    $data = array(
                        'tripdateid' => $tripdateid,
                        'typeid'     => $rebatebsid,
                        'starttime'  => strtotime($_edits['starttime'][$skey]),
                        'endtime'    => strtotime($_edits['endtime'][$skey]),
                        'price'      => intval($_edits['price'][$skey]),
                        'adult_bs'   => intval($_edits['adult'][$skey])==1?1:0,
                        'child_bs'   => $_edits['adult'][$skey]==1? (intval($_edits['child'][$skey])==1?1:0):0,
                        'baby_bs'    => ($_edits['adult'][$skey]==1 && $_edits['child'][$skey]==1)?(intval($_edits['baby'][$skey])==1?1:0):0,
                        'addtime'    => NOW_TIME,
                        'admin_id'   => session('admin_id'),
                    );
                    $map['id'] = $skey;
                    $result = D('TripdateRebate')->_save($data,$map);
                    if ($result['status'] != 1) {
                        $errmsg = '编辑数据失败';
                        IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
                        $this->show($errmsg);
                        exit();
                    }
                }
            }
            //新增
            if($_timerebates['starttime']){
                foreach($_timerebates['starttime'] as $key => $val){
                    if($val == '' || $_timerebates['endtime'][$key] == '' || intval($_timerebates['price'][$key]) <= 0 || intval($_timerebates['adult'][$key])!=1){
                        continue;
                    }
                    $data = array(
                        'tripdateid' => $tripdateid,
                        'typeid'     => $rebatebsid,
                        'starttime'  => $_timerebates['starttime'][$key]?strtotime($_timerebates['starttime'][$key]):0,
                        'endtime'    => $_timerebates['endtime'][$key]?strtotime($_timerebates['endtime'][$key]):0,
                        'price'      => intval($_timerebates['price'][$key]),
                        'timesbs'    => 1,
                        'adult_bs'   => intval($_timerebates['adult'][$key])==1?1:0,
                        'child_bs'   => $_timerebates['adult'][$key]==1? (intval($_timerebates['child'][$key])==1?1:0):0,
                        'baby_bs'    => ($_timerebates['adult'][$key]==1 && $_timerebates['child'][$key]==1)?(intval($_timerebates['baby'][$key])==1?1:0):0,
                        'addtime'    => NOW_TIME,
                        'admin_id'   => session('admin_id'),
                    );

                    $result = D('TripdateRebate')->_add($data);
                    if ($result['status'] != 1) {
                        $errmsg = '对不起，添加数据失败';
                        IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
                        $this->show($errmsg);
                        exit();
                    }
                }
            }
            $timesbsnums = M('TripdateRebate')->where(array('tripdateid' => $tripdateid, 'typeid' => $rebatebsid,'timesbs' => 1, 'deletebs' => 0))->count();
            $timesbsnums = $timesbsnums ? $timesbsnums.' 条' : '无';
            $msg = '操作成功';
            $url = U(CONTROLLER_NAME . "/edit?submethod=timerebate&tripdateid=$tripdateid&rebatebsid=$rebatebsid");
            IS_AJAX && $this->ajaxReturn(array('status' => 1, 'msg' => $msg, 'timesbsnums' => $timesbsnums, 'url' => $url), 'JSON');
            $this->success($msg, $url);
            exit();
        }
        $tripdateid = I('get.tripdateid', 0, 'intval');
        $rebatebsid = I('get.rebatebsid', 0, 'intval');

        if (!$tripdateid || !$rebatebsid) {
            $this->show('传参有误2!');
            exit();
        }
        $rebates   = M('Zsysdata')->where(array('id' => $rebatebsid))->find();
        if(!$rebates || $rebates['id'] != $rebatebsid){
            $this->show('不存在的返利政策!');
            exit();
        }
        $map['le.status'] = 1;
        $field  = 'le.id,le.title,le.subtitle,le.days,p.title as t_partners_id';
        $order  = 'le.days,le.ordid desc';
        $lists   = M('TripdateRebate')->where(array('typeid' => $rebatebsid, 'timesbs' => 1, 'deletebs' => 0))->order('id')->select();
        $this->assign('lists', $lists);
        $this->assign('editnums', count($lists));
        $this->assign('tripdateid', $tripdateid);
        $this->assign('rebatebsid', $rebatebsid);
        $this->assign('rebates', $rebates);
        $this->display('timerebate');
    }
    //团期生成中选择线路产品
    public function selectline(){
        $sch = $cates = $linecates = array();
        $auth_cate = $this->getAuthCategory();
        $cates     = D('Category')->toTree(array('type_id' => 1, 'cate_id' => array('IN', $auth_cate)));
        $linecates     = $this->getcates($cates);
        unset($cates);
        if(I('get.schbs') == 1){
            $sch['schbs']   = I('get.schbs');
            $sch['title']   = I('get.title', '', 'trim');
            $sch['days']    = I('get.days', '', 'intval');
            $sch['cate_id'] = I('get.cate_id', '', 'intval');

            if($sch['title']){
                $where['le.title']    = array('like','%'.$sch['title'].'%');
                $where['le.subtitle'] = array('like', '%'.$sch['title'].'%');
                $where['_logic']   = 'or';
                $map['_complex']   = $where;
            }
            if($sch['days']){
                if($sch['days'] == 15){
                    $map['le.days'] >= $sch['days'];
                } else {
                    $map['le.days'] = $sch['days'];
                }
            }
            if($sch['cate_id'] >0){
                $map['le.cate_id'] = $sch['cate_id'];
            }
        }
        $map['le.status'] = 1;
        $field  = 'le.id,le.title,le.subtitle,le.days,p.title as t_partners_id';
        $order  = 'le.days,le.ordid desc';
        $getlist   = $this->getLineList($map, $field, $order);
        $this->assign('list', $getlist['list']);
        $this->assign('page', $getlist['page']);
        $this->assign('linecates', $linecates);
        $this->assign('sch', $sch);
        $this->display('selectline');
    }
    public function getLineList($map, $field='*', $order=''){
        $model = M('Line');
        $psize  = I('request.psize', 0, 'intval') ?: C('DEFAULT_PAGE_SIZE', NULL);//实际按多少条记录分页
        $total  = $model->alias('le')->where($map)->count();
        $pager  = new \Think\Page($total, $psize);
        if ($total > 0) {
            $list = $model->alias('le')->join('__PARTNERS__ p ON le.partners_id = p.id', 'LEFT') -> where($map) -> field($field) -> order($order) -> limit($pager->firstRow . ',' . $pager -> listRows) -> select();
        }

        $obj = array(
            'list'  => $list,
            'total' => $total,
            'page'  => $pager->show(),
        );
        
        return $obj;
    }
    public function getcates($data){
        $tmparr = $arr = array();
        $tmparr = $this->getspecialcate($data);

        if($tmparr){
            foreach($tmparr as $key => $val){
                foreach($val as $val1){
                    $arr[] = $val1; 
                }
            }
        }
        return $arr;
    }
    //只取出专线 不需要国内游出境游那级
    public function getspecialcate($data){
        $arr = array();
        if(is_array($data)){
            foreach($data as $key => $val){
                if($val['pid'] > 0){
                    $arr[] =  $val;
                }
                if($val['child']){
                    $arr[] = $this->getspecialcate($val['child']);
                }
            }
        }
        return $arr;
    }
    //根据线路id获取线路等级价格表
    public function getlineprices($lineid){
        $lineid = $lineid?:I('post.lineid', 0, 'intval');
        if(!$lineid){
            $errmsg = '传参有误';
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
            $this->show($errmsg);
            exit();
        }
        $lines = M('Line')->field('le.title,le.days,le.departure,le.arrive,p.title as partner_title')->alias('le')
                ->join('__PARTNERS__ p ON le.partners_id = p.id', 'LEFT')
                ->where(array('le.id' => $lineid, 'le.status' => 1))->find();
        if(!$lines){
            $errmsg = '地接线路不存在';
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
            $this->show($errmsg);
            exit();
        }
        //获取线路等级
        $linegrades = $this->getlinegrades($lineid);
        $field = 'id,lineid,gradeid,starttime,endtime,adult_price,child_price,baby_price,adult_profit,child_profit,baby_profit,singleroom_price';
        $map_price['lineid']   = $lineid;
        $map_price['deletebs'] = 0;
        $map_price['endtime']  = array('gt',strtotime(date('Y-m-d')));
        $lineprices = M('LinePrices')->field($field)->where($map_price)->order('starttime')->select();
        if(!$linegrades || !$lineprices){
            $errmsg = '对不起,该线路没有标准/价格数据';
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
            $this->show($errmsg);
            exit();
        }
        if(IS_AJAX){
            $content  = '<div>合作单位：'.$lines['partner_title'].'</div>';
            $content .= '<table class="table table-striped table-bordered table-hover">
                            <tr align="center">
                                <td></td>
                                <td colspan="3">线路成本价</td>
                                <td colspan="3">线路利润</td>
                                <td></td>
                            </tr>
                            <tr bgcolor="#f9f9f9" align="center">
                                <td>出行团期</td>
                                <td>成人</td>
                                <td>儿童</td>
                                <td>婴儿</td>
                                <td>成人</td>
                                <td>儿童</td>
                                <td>婴儿</td>
                                <td>单房差</td>
                            </tr>
                            ';//<tbody>
            foreach($linegrades as $gkey => $gval){
                $content .= '<tr><td colspan="8" bgcolor="#FFFFFF"><span class="bolder">'. $gval['title'] .'</span> </td></tr>';
                foreach($lineprices as $pkey => $pval){
                    if($pval['gradeid'] == $gval['id']){
                        $content .= '<tr>
                                        <td><div class="radio"><label><input class="ace lineprice" type="radio" name="grateprices['.$gval['id'].']" value="'.$pval['id'].'" onclick="selectlineprice()">
                                            <span class="lbl">'.date('Y-m-d',$pval['starttime']).'/'.(date('Y',$pval['endtime'])==date('Y',$pval['starttime'])?date('m-d',$pval['endtime']):date('Y-m-d',$pval['endtime'])).'</span></label></div></td>
                                        <td align="center">'.floatval($pval['adult_price']).'</td>
                                        <td align="center">'.floatval($pval['child_price']).'</td>
                                        <td align="center">'.floatval($pval['baby_price']).'</td>
                                        <td align="center">'.floatval($pval['adult_profit']).'</td>
                                        <td align="center">'.floatval($pval['child_profit']).'</td>
                                        <td align="center">'.floatval($pval['baby_profit']).'</td>
                                        <td align="center">'.floatval($pval['singleroom_price']).'</td>
                                    </tr>';
                    }    
                }
            }
            $content .= '</table>';//</tbody>
            $content .= '<div id="reselect" style="display:none"><a href="javascript:;" onclick="cancelselect(this)">取消重选</div>';
            $this->ajaxReturn(array('status' => 1, 'msg' => 'success', 'title' => $lines['title'], 'departure' => $lines['departure'], 'arrive' => $lines['arrive'], 'days' => ($lines['days']+2), 'content' => $content), 'JSON');
        }
        if($lineprices){
            foreach ($lineprices as &$val) {
                $val['t_time'] = date('Y-m-d',$val['starttime']).'/'.(date('Y',$val['endtime'])==date('Y',$val['starttime'])?date('m-d',$val['endtime']):date('Y-m-d',$val['endtime']));
            }
        }
        return array('grades' => $linegrades, 'prices' => $lineprices);
    }
    //交通信息选择页
    public function selecttraffic(){
        $model = D('Traffic');
        $sch = $cates = $linecates = array();
        $sch = array();
        if(I('get.schbs')==1){
            $sch['schbs']           = I('get.schbs');
            $sch['roundtrip']       = I('get.roundtrip', '', 'trim');
            $sch['trafficbs']       = I('get.trafficbs', 0, 'intval');
            $sch['directbs']        = I('get.directbs', 0, 'intval');
            $sch['start_district']  = I('get.start_district', '', 'trim');
            $sch['arrive_district'] = I('get.arrive_district', '', 'trim');
            $sch['classes']         = I('get.classes', '', 'trim');
            if($sch['roundtrip']){
                $map['roundtrip'] = $sch['roundtrip'];
            }
            if($sch['trafficbs']){
                $map['trafficbs'] = $sch['trafficbs'];
            }
            if($sch['directbs']){
                $map['directbs'] = $sch['directbs'];
            }
            if($sch['start_district']){
                $map['start_district'] = array("like",'%'.$sch['start_district'].'%');
            }
            if($sch['arrive_district']){
                $map['arrive_district'] = array("like",'%'.$sch['arrive_district'].'%');
            }
            if($sch['classes']){
                $map['classes'] = array("like",'%'.$sch['classes'].'%');
            }
        }
        $map['status']   = 1;
        $map['deletebs'] = 0;
        $field  = '';
        $order  = 'ordid desc,id';
        $zsys_data = D('Zsysdata')->get_zsysdata(array('type' =>array('IN','traffic,directbs')),1);
        $getlist   = $model->getList($map, $field, $order,$zsys_data);
        $this->assign('list', $getlist['list']);
        $this->assign('page', $getlist['page']);
        $this->assign('sch', $sch);
        $this->assign('traffics', $zsys_data['traffic']);
        $this->assign('directs', $zsys_data['directbs']);
        $this->display('selecttraffic');
    }
    public function getlinegrades($lineid){
        $arr = array();
        if(!$lineid){
            return $arr;
        }
        $arr = M('Grade')->field('id,title')->where(array('lid' => $lineid, 'status' => 1))->order('ordid desc,id')->select();
        return $arr;
    }
    /**
     * 格式化 团期交通信息
     * $traffic_goid    Integer 往交通信息id
     * $traffic_backid  Integer 返交通信息id
     * $zsysdata        array   系统定制数据
     */
    public function formattraffics($traffic_goid,$traffic_backid,$zsysdata){
        $arr = array();
        $go = $back = $goshort = $backshort = '';
        if($traffic_goid > 0){
            $tmp_go = M('Traffic')->where(array('id' => $traffic_goid, 'status' => 1))->find();
            if($tmp_go['id']){
                $goshort = $tmp_go['roundtrip'].($tmp_go['trafficbs']?'&nbsp;'.$zsysdata['traffic'][$tmp_go['trafficbs']]:'').($tmp_go['classes']?'&nbsp;'. $tmp_go['classes']:'');
                $go .= $tmp_go['start_district']?$tmp_go['roundtrip']:'';
                $go .= $tmp_go['start_district']?'&nbsp;' . $tmp_go['start_district']:'';
                $go .= $tmp_go['arrive_district']?'/'. $tmp_go['arrive_district']:'';
                $go .= $tmp_go['classes']?'&nbsp;'. $tmp_go['classes']:'';
                $go .= ($tmp_go['start_hour']?'&nbsp;':'') . $tmp_go['start_hour'] .($tmp_go['start_minute']?':':'') . $tmp_go['start_minute'];
                $go .= ($tmp_go['arrive_hour']?'/':'') . $tmp_go['arrive_hour'] .($tmp_go['arrive_minute']?':':'') . $tmp_go['arrive_minute'];
                $go .= $tmp_go['directbs']?'&nbsp;'.$zsysdata['directbs'][$tmp_go['directbs']]:'';
            }
        }
        if($traffic_backid > 0){
            $tmp_back = M('Traffic')->where(array('id' => $traffic_backid, 'status' => 1))->find();
            if($tmp_back['id']){
                $backshort = $tmp_back['roundtrip'].($tmp_back['trafficbs']?'&nbsp;'.$zsysdata['traffic'][$tmp_back['trafficbs']]:'').($tmp_back['classes']?'&nbsp;'. $tmp_back['classes']:'');
                $back .= $tmp_back['start_district']?$tmp_back['roundtrip']:'';
                $back .= $tmp_back['start_district']?'&nbsp;' . $tmp_back['start_district']:'';
                $back .= $tmp_back['arrive_district']?'/'. $tmp_back['arrive_district']:'';
                $back .= $tmp_back['classes']?'&nbsp;'. $tmp_back['classes']:'';
                $back .= ($tmp_back['start_hour']?'&nbsp;':'') . $tmp_back['start_hour'] .($tmp_back['start_minute']?':':'') . $tmp_back['start_minute'];
                $back .= ($tmp_back['arrive_hour']?'/':'') . $tmp_back['arrive_hour'] .($tmp_back['arrive_minute']?':':'') . $tmp_back['arrive_minute'];
                $back .= $tmp_back['directbs']?'&nbsp;'.$zsysdata['directbs'][$tmp_back['directbs']]:'';
            }
        }
        $arr['go'] = $go;
        $arr['back'] = $back;
        $arr['goshort'] = $goshort?:'往 待定';
        $arr['backshort'] = $backshort?:'返 待定';
        return $arr;
    }
    /**
     * 获取接团地区数据
     * $shuttles    array 接团地区id
     * return  array
     */
    public function getshuttles($shuttles){
        $arr = array();
        if(!is_array($shuttles) || empty($shuttles)){
            return;
        }
        $tmpshuttles = implode(',', $shuttles);
        $arr = M('StartDistrict')->field('le.id,le.start_districtid as districtid,le.adult_price,le.child_price,d.title as district_title')->alias('le')
               ->join('__DISTRICT__ d ON le.start_districtid = d.id', 'LEFT')
               ->where(array('le.id' => array('in', $tmpshuttles)))->select();
        return $arr;
    }
    /**
     * 获取地接线路标准等级/价格
     * $priceids  array 线路价格表id
     * $lineid Integer  线路id
     * return  array
     */
    public function getgrateprices($priceids, $lineid){
        $arr = array();
        if(!is_array($priceids) || empty($priceids)){
            return;
        }
        $tmppriceids = implode(',', $priceids);
        $arr = M('LinePrices')->field('le.id,le.gradeid,le.adult_price,le.child_price,le.baby_price,le.adult_profit,le.child_profit,le.baby_profit,le.singleroom_price,g.title as grate_title')->alias('le')
               ->join('__GRADE__ g ON le.gradeid = g.id', 'LEFT')
               ->where(array('le.id' => array('in', $tmppriceids),'le.lineid' => $lineid))->select();
        return $arr;
    }
    /**
     * 删除团期中的座位分配
     * 只需要把分配的座位数全部追加到主团期中的共享座位中
     */
    public function seatdel(){
        if(!IS_AJAX){
            $this->show('操作有误!');
            exit();
        }
        $tripdateid = I('post.tripdateid', 0, 'intval');
        $seatid  = I('post.seatid', 0, 'intval');//已分配的座数记录 tripdate_seat.id
        if($tripdateid == 0 || $seatid == 0){
            $errmsg = '操作有误';
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
            $this->show($errmsg);
            exit();
        }
        $tripdates = $this->TripdateCheck($tripdateid);//团期有效性检查
        $seats = M('TripdateSeat')->field('id,tripdateid,partners_id,seatnum,seatused')->where(array('id' => $seatid, 'tripdateid' => $tripdateid))->find();
        if($seats['id'] != $seatid){
            $errmsg = '不存在的座位分配';
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
            $this->show($errmsg);
            exit();
        }
        if($seats['seatused'] > 0 ){
            $errmsg = '对不起，已使用了座位数的不能被取消';
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
            $this->show($errmsg);
            exit();
        }
        //$seatnum = $seats['seatnum'];
        
        M('Tripdate')->startTrans();
        $result_s1 = M('Tripdate')->where(array('id' => $tripdateid))->setInc('sharesetnum',$seats['seatnum']);//座位数返还到主团期表 共享座位数中
        $result_s2 = M('Tripdate')->where(array('id' => $tripdateid))->setDec('assign_nums',$seats['seatnum']);//分配的座位数减掉本次的
        if(!$result_s1 || !$result_s2){
            M('Tripdate')->rollback();
            $errmsg = '删除失败';
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
            $this->show($errmsg);
            exit();
        }
        if($tripdates['assign_partners']){
            $tmp_arr = explode(',',$tripdates['assign_partners']);
            foreach($tmp_arr as $k => $v){
                if($v == $seats['partners_id']){
                    unset($tmp_arr[$k]);
                }
            }
            $tmp_partnerids = $tmp_arr ? implode(',', $tmp_arr) : '';
            $result_s3 = M('Tripdate')->where(array('id' => $tripdateid))->setField('assign_partners', $tmp_partnerids);
            if(!$result_s3){
                M('Tripdate')->rollback();
                $errmsg = '删除失败';
                IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
                $this->show($errmsg);
                exit();
            }
        }
        $map = array();
        $map['tripdateid'] = $tripdateid;
        $map['id'] = $seatid;
        $result = D('TripdateSeat')->_save(array('deletebs' => 1),$map);
        if($result['status'] == 1){
            M('Tripdate')->commit();
            $errmsg = '删除成功';
            IS_AJAX && $this->ajaxReturn(array('status' => 1, 'msg' => $errmsg), 'JSON');
            $this->show($errmsg);
            exit();
        }else{
            M('Tripdate')->rollback();
            $errmsg = '删除失败';
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
            $this->show($errmsg);
            exit();
        }
    }
    public function TripdateCheck($tripdateid){
        $info   = M('Tripdate')->field('id,tripdate,status,seatnum,seatused,overplus,sharesetnum,assign_nums,assign_partners')->where(array('id' => $tripdateid))->find();
        if(!$info || $info['id'] != $tripdateid){
            $errmsg = '不存在的团期';
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
            $this->show($errmsg);
            exit();
        }
        if($info['tripdate'] <= strtotime(date())){
            $errmsg = '不能操作过期的团期';
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
            $this->show($errmsg);
            exit();
        }
        return $info;
    }
    /**
     * 检查返利数据
     */
    public function checkRebate($data){
        if(!$data || !is_array($data)){
            return;
        }
        foreach($data['id'] as $key => $val){
            if($val > 0 && intval($data['price'][$key]) <= 0){
                $errmsg = '选中的返利类型规则值不能为空';
                IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
                $this->show($errmsg);
                exit();
            }
        }
    }
    /**
     * 检查已有团期数据 座位数分配是否正确
     * $data         array 本次更新 成人大交通，总座位数等数据
     * $seats        array 本次更新 座位分配数据
     * $tripdatedata array 主团期数据
     */
    public function checkTripdateSeat($data,$seats,$tripdatedata = array()){
        if($data['adult_traffic_price'] <= 0 || $data['seatnum'] <= 0){
            $errmsg = '出行团期中成人交通费与座位数不能为空';
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
            $this->show($errmsg);
            exit();
        }

        if($data['seatnum'] < $tripdatedata['seatused']){
            $errmsg = '总座位数 '.$data['seatnum'].' 个,不能小于已使用的座位数 '.$tripdatedata['seatused'].' 个';
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
            $this->show($errmsg);
            exit();
        }
        $nums = 0;
        if($seats){
            foreach($seats as $val){
                $result = D('TripdateSeat')->checkPartnersSeat($tripdatedata['id'], $val['id'], $val['seatnum']);
                if($result['status'] != 1){
                    IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $result['msg']), 'JSON');
                    $this->show($result['status']);
                    exit();
                }
                $nums += $val['seatnum'];
            }
        }
        if($data['seatnum'] < $nums){
            $errmsg = '出行团期中座位数 '.$data['seatnum'].'个,不能小于单独分配给合作单位的座位数 '.$nums.' 个';
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
            $this->show($errmsg);
            exit();
        }
    }
    /**
     * 检查起止地，并数组返回对应的地区id
     */
    public function checkDepartureArrive($departure,$arrive){
        if(!$departure || !$arrive){
            $errmsg = '出发/到达地区不能为空！';
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
            $this->show($errmsg);
            exit();
        }
        $departure_id = D('District')->getDistrictId(array('title' => $departure));
        if(!$departure_id){
            $errmsg = '出发地区不存在！';
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
            $this->show($errmsg);
            exit();
        }
        $arrive_id = D('District')->getDistrictId(array('title' => $arrive));
        if(!$arrive_id){
            $errmsg = '到达地区不存在';
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
            $this->show($errmsg);
            exit();
        }
        return array('departure_id' => $departure_id, 'arrive_id' => $arrive_id);
    }
    /**
     * 更新团期返利数据
     * rollback = 1 时遇到错误进行回滚
     */
    public function updateRebate($arr,$tripdateid,$rollback = 0){
        if(!$tripdateid){
            return;
        }
        M('TripdateRebate')->where(array('tripdateid' => $tripdateid, 'timesbs' => 0))->save(array('deletebs' => 1));
        if($arr){
            foreach($arr['id'] as $key => $val){
                if($val == 1 && intval($arr['price'][$key]) > 0){
                    $data = $map = array();
                    $data['deletebs']  = 0;
                    $data['price']     = intval($arr['price'][$key]);
                    $data['adult_bs']  = 1;
                    $data['child_bs']  = intval($arr['child'][$key]) == 1 ? 1 : 0;
                    $data['baby_bs']   = (intval($arr['child'][$key])== 1 && intval($arr['baby'][$key]) == 1) ? 1 : 0;
                    
                    $map['tripdateid'] = $tripdateid;
                    $map['typeid']     = $key;
                    $map['timesbs']    = 0;

                    if(M('TripdateRebate')->where(array('tripdateid' => $tripdateid, 'typeid' => $key, 'timesbs' => 0))->count() > 0){
                        $result = D('TripdateRebate')->_save($data,$map);
                    } else {
                        $data['tripdateid'] = $tripdateid;
                        $data['typeid']     = $key;
                        $data['timesbs']    = 0;
                        $result = D('TripdateRebate')->_add($data);
                    }
                    if($result['status'] != 1){
                        if($rollback == 1){
                            M('Tripdate')->rollback();
                        }
                        $errmsg = '更新返利数据失败';
                        IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
                        $this->show($errmsg);
                        exit();
                    }
                }
            }
        }
    }
    /**
     * 检查接团地区 返回数据
     */
    //接团地区
    public function checkShuttles($data){
        $tmparr = $arr = array();
        if($data){
            foreach($data as $val){
                $tmparr[$val] = $val;
            }
        }
        if(!$data || !$tmparr){
            $errmsg = '请选择至少一个接团地区';
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
            $this->show($errmsg);
            exit();
        }
        $arr = $this->getshuttles($tmparr);
        return $arr;
    }
    /**
     * 检查线路等级/价格 返回数据
     */
    public function checkGratePrices($data, $lineid){
        if(!$lineid || !$data){
            $errmsg = '请选择至少一个线路等级/价格';
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
            $this->show($errmsg);
            exit();
        }
        $arr = $this->getgrateprices($data, $lineid);
        if(!$arr){
            $errmsg = '选择的线路等级/价格不存在';
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
            $this->show($errmsg);
            exit();
        }
        return $arr;
    }
    /**
     * 检查线路是否存在 返回数据
     */
    public function checkLine($lineid){
        $arr = M('Line')->field('le.id,le.partners_id,le.cate_id,p.title as partner_title')->alias('le')
               ->join('__PARTNERS__ p ON le.partners_id = p.id', 'LEFT')
               ->where(array('le.id' => $lineid, 'le.status' => 1))->find();
        if(!$arr || $arr['id'] != $lineid){
            $errmsg = '地接线路不存在';
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
            $this->show($errmsg);
            exit();
        }
        return $arr;
    }
    /**
     * 检查座位分配数据
     * $data array 已有数据
     * $newdata array 新分配数据
     */
    public function checkSeats($data,$newdata){
        $arr = array();
        if($data){
            foreach($data['seatnum'] as $key => $val){
                if($val <= 0){
                    $errmsg = '座位数不能为空或0！';
                    IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
                    $this->show($errmsg);
                    exit();
                }
                $arr[] = array('id' => $data['id'][$key], 'seatnum' => $val, 'viewshareseat' => $data['shareseatbs'][$key]);

            }
        }
        if($newdata){
            foreach($newdata['seatnum'] as $key => $val){
                if($val <= 0 || intval($newdata['id'][$key]) <= 0){
                    $errmsg = '座位数只能为大于0的数字且合作单位不能为空';
                    IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
                    $this->show($errmsg);
                    exit();
                }
                $arr[] = array('id' => $newdata['id'][$key], 'seatnum' => $val, 'viewshareseat' => $newdata['shareseatbs'][$key]);
            }
        }
        if($arr){
            $tmp_arr = array();
            foreach($arr as $key => $val){
                if(in_array($val['id'],$tmp_arr)){
                    $j = 1;
                }
                $tmp_arr[] = $val['id'];
            }
            
            if($j > 0){
                $errmsg = '座位分配有误，不能重复分配座位给同一合作单位';
                IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
                $this->show($errmsg);
                exit();
            }
        }
        return $arr; 
    }
    /**
     * 新增团期时，检查团期总座位与分配座位的合理性
     */
    public function checkAddTripdateSeat($data, $seats){
        $i = 0;
        $tmp_tripdate = array();
        foreach($data['tripdate'] as $key => $val){
            if(!$val || empty($val) || floatval($data['adult_traffic_price'][$key]) == 0 || intval($data['seatnum'][$key]) == 0){
                continue;
            }
            $i++;
            if($seats){
                $assign_nums = 0;
                foreach($seats as $aval){
                    $assign_nums += intval($aval['seatnum']);
                }
                if(intval($data['seatnum'][$key]) < $assign_nums){
                    $errmsg = '座位分配有误，团期 '.$val.' 共有座位数 '.$data['seatnum'][$key].' 不能小于要分配的座位总数 '.$assign_nums.' 个';
                    IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
                    $this->show($errmsg);
                    exit();
                }
            }
            if(in_array($val, $tmp_tripdate)){
                $errmsg = '对不起，出行团期不能重复添加';
                IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
                $this->show($errmsg);
                exit();
            }
            $tmp_tripdate[] = $val;
        }
        if($i == 0 || $i != count($data['tripdate'])){
            $errmsg = '团期有误，符合条件的团期数 '.$i.' 个不等于添加的团期数 '.count($data['tripdate']).'个 (出行团期,大交通费,座位数不能为空)';
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => $errmsg), 'JSON');
            $this->show($errmsg);
            exit();
        }
    }
}
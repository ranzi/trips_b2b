<?php
/**
 * 团期 返利管理
 */
namespace Admin\Controller;

class TripdateRebateController extends AuthController {
    public function _initialize() {
        parent::_initialize();
    }

    /**
     * 伪删除按日期段的返利记录
     */
	public function rebatetimedel(){
        if(!IS_AJAX){
            $this->show('操作有误!');
            exit();
        }
        $tripdateid = I('post.tripdateid', 0, 'intval');//团期id
        $rebatebsid = I('post.rebatebsid', 0, 'intval');//返利政策id 表 zsysdata.id(type.promotion)
        $rebateid  = I('post.rebateid', 0, 'intval');//返利记录id 表 tripdate_rebate.id
        if($tripdateid == 0 || $rebatebsid == 0 || $rebateid==0){
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => '对不起，操作有误!'), 'JSON');
            $this->show('对不起，操作有误');
            exit();
        }

        $result = M('TripdateRebate')->where(array('tripdateid' => $tripdateid, 'id' => $rebateid, 'timesbs' => 1))->save(array('deletebs' => 1, 'addtime' => NOW_TIME, 'admin_id' => session('admin_id')));
        if(!$result){
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => '删除失败!'), 'JSON');
        }

        $nums = M('TripdateRebate')->where(array('tripdateid' => $tripdateid, 'typeid' => $rebatebsid, 'timesbs' => 1, 'deletebs' => 0))->count();
        $nums = $nums ? $nums.' 条' : '无';
        IS_AJAX && $this->ajaxReturn(array('status' => 1, 'msg' => '删除成功!', 'timesbsnums' => $nums), 'JSON');
    }
}
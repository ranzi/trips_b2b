<?php
namespace Admin\Controller;

class TypesController extends AuthController {
    public function _initialize() {
        parent::_initialize();
    }

    public function _before_index() {
        $this->assign('bar', array('curpos' => '类型列表', 'menu' => '产品类型', 'url' => U('Types/index')));
    }

    public function _before_add() {
        $this->assign('bar', array('curpos' => '添加类型', 'menu' => '产品类型', 'url' => U('Types/index')));
    }

    public function _before_edit() {
        $this->assign('bar', array('curpos' => '添加类型', 'menu' => '产品类型', 'url' => U('Types/index')));
    }

    public function item() {
        if (IS_POST) {
            $attrs   = I('post.attr', array());
            $title   = I('post.title', array());
            $ordid   = I('post.ordid', array());
            $status  = I('post.status', array());
            $ids     = I('post.ids', array());
            $type_id = I('post.type_id', 0, 'intval');
            if (!$type_id) {
                $this->error('参数错误');
            }
            if (!is_array($title) || !is_array($attrs)) {
                $this->error('参数错误');
            }
            #处理属性
            $temp = array();
            foreach ($attrs['ordid'] as $key => $val) {
                foreach ($val as $k => $v) {
                    if ($attrs['title'][$key][$k]) {
                        $temp[$key][] = array('ordid' => intval($v), 'title' => $attrs['title'][$key][$k]);
                    }
                }
            }
            $attrs = $temp;
            // dump($attrs);
            // dump($title);
            // exit;
            $model = D('Types_item');

            $map['type_id'] = $type_id;
            if (!empty($ids)) {
                $map['id'] = array('NOT IN', $ids);
            }
            //print_r($map);
            //exit;
            $model->where($map)->delete();
            foreach ($title as $key => $item) {
                $item = trim($item);
                if ($item == '') {
                    continue;
                }
                if (empty($attrs[$key])) {
                    continue;
                }
                $attr = $attrs[$key];
                //print_r($attr);
                usort($attr, array($this, 'attrSort'));
                //print_r($attr);exit;
                $data = array(
                    'type_id' => $type_id,
                    'title'   => $item,
                    'ordid'   => intval($ordid[$key]),
                    'status'  => $status[$key] == 1 ?: 0,
                    'attr'    => serialize($attr),
                );
                if (isset($ids[$key]) && !empty($ids[$key])) {
                    $model->where(array('id' => intval($ids[$key])))->save($data);
                } else {
                    $model->add($data);
                }
            }
            $this->success('保存成功');
        }
        $this->assign('bar', array('curpos' => '筛选项', 'menu' => '类型列表', 'url' => U('Types/index')));
        $id = I('get.id', 0, 'intval');
        if (!$id) {
            $this->error('参数错误');
        }
        $types = D('Types')->where(array('id' => $id))->find();
        if (!$types) {
            $this->error('类型不存在');
        }
        $list = D('Types_item')->parseAll(array('type_id' => $id));
        $this->assign('list', $list);
        $this->assign('types', $types);
        $this->display();
    }

    private function attrSort($a, $b) {
        if ($a['ordid'] == $b['ordid']) {
            return 0;
        }
        return $a['ordid'] > $b['ordid'] ? -1 : 1;
    }
}
<?php
/**
 * 签证产品-资源管理
 */
namespace Admin\Controller;

class VisaController extends AuthController {
    public function _initialize() {
        parent::_initialize();
    }

	public function index() {
        $model  = D('Visa');
        
        $map ['le.deletebs'] = 0;
        $field  = 'le.id,le.countryid,le.visatype_id,le.district_id,le.acceptancedays,le.entrynumber,le.interviewed,le.advanceday,le.bonded,le.validterm,le.periodofstay,le.intro,le.partners_id,le.price,le.ordid,le.status,d.title as t_countryid,p.title as t_partners_id';
        $order  = 'le.ordid DESC,le.id';
        
        $sch = array();
        if(I('get.schbs')==1){
            $sch['schbs']       = I('get.schbs');
            $sch['countryid']   = I('get.countryid', '', 'trim');
            $sch['partners']    = I('get.partners', '', 'trim');
            $sch['visatypeid']  = I('get.visatypeid', '', 'intval');
            $sch['start_price'] = I('get.start_price', '', 'floatval');
            $sch['end_price']   = I('get.end_price', '', 'floatval');
            
            //签证国家
            if($sch['countryid']){
                $countryids=D('District')->get_countryids_liketitle($sch['countryid']);
                if($countryids){
                    $map['le.countryid'] = array('in',$countryids);
                } else {
                    $map['le.countryid'] = -1;
                }
            }
            //合作单位
            if($sch['partners']){
                $partnerids = D('Partners')->get_partnerids_liketitle($sch['partners']);
                if($partnerids){
                    $map['le.partners_id'] = array('in',$partnerids);
                } else {
                    $map['le.partners_id'] = -1;
                }
            }
            //签证类型
            if($sch['visatypeid']){
                $map['visatype_id'] = $sch['visatypeid'];
            }
            if($sch['start_price']){
                if($sch['end_price']){
                    $map['price']  = array('between',$sch['start_price'].','.$sch['end_price']);
                }else{
                    $map['price']  = $sch['start_price'];
                    $sch['end_price'] = '';
                }
            }else if($sch['end_price']){
                $map['price']  = array('between','0,'.$sch['end_price']);
                $sch['start_price'] = 0;
            } else {
                $sch['start_price'] = '';
                $sch['end_price'] = '';
            }
        }

        $types_data = D('Types')->get_typesdata(array('type' => 'visatype'),1);//签证类型
        $getlist    = $model->getList($map, $field, $order, $types_data);
        
        $this->assign('bar', array('curpos' => '列表', 'menu' => '签证产品', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('list', $getlist['list']);
        $this->assign('page', $getlist['page']);
        $this->assign('sch', $sch);
        $this->assign('visatypes', $types_data['visatype']);
        $this->display();
    }

    public function add() {
        if (IS_POST) {
            $model = D('Visa');
            $data = array(
                'countryid'      => I('post.countryid', 0, 'intval'),
                'visatype_id'    => I('post.visatype_id', 0, 'intval'),
                'district_id'    => I('post.district_id', 0, 'intval'),
                'acceptancedays' => I('post.acceptancedays', '', 'trim'),
                'entrynumber'    => I('post.entrynumber', '', 'trim'),
                'interviewed'    => I('post.interviewed', 0, 'intval'),
                'advanceday'     => I('post.advanceday', '', 'trim'),
                'bonded'         => I('post.bonded', '', 'trim'),
                'validterm'      => I('post.validterm', '', 'trim'),
                'periodofstay'   => I('post.periodofstay', '', 'trim'),
                'intro'          => I('post.intro', '', 'trim'),
                'message'        => I('post.message', '', 'trim'),
                'partners_id'    => I('post.partners_id', 0, 'intval'),
                'price'          => I('post.price', 0, 'floatval'),
                'remark'         => I('post.remark', '', 'trim'),
                'ordid'          => I('post.ordid', 0, 'intval'),
                'admin_id'       => session('admin_id'),
                'status'         => I('post.status')==1?:0,
            );

            $result = $model->_add($data);

            //如果添加成功
            if ($result['status'] == 1 && $result['id']) {

                //新增成本价格行
                $starttime  = I('post.starttime', array());
                $endtime    = I('post.endtime', array());
                $cost_price = I('post.cost_price', array());
                
                if(is_array($starttime)){
                    foreach ($starttime as $key => $val) {
                        if($val == ''){
                            continue;
                        }

                        $data = array(
                            'visa_id'    => $result['id'],
                            'starttime'  => strtotime($starttime[$key]),
                            'endtime'    => strtotime($endtime[$key]),
                            'cost_price' => floatval($cost_price[$key]),
                        );
                        if (M('visa_price')->create($data)) {
                            M('visa_price')->add();
                        } else {
                            echo $this->getError();
                        }
                    }
                }

                $this->success($result['msg'], U(CONTROLLER_NAME . '/index'));
            } else {
                $this->error($result['msg']);
            }
        }
        
        $zsys_data  = D('Zsysdata')->get_zsysdata(array('type' => 'interview'));//面签方式
        $types_data = D('Types')->get_typesdata(array('type' => 'visatype'));//签证分类
        $this->assign('interviews',$zsys_data['interview']);
        $this->assign('visatypes',$types_data['visatype']);
        $this->assign('bar', array('curpos' => '添加', 'menu' => '签证产品', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->display();
    }

    public function edit() {
        $model = D('Visa');
        if (IS_AJAX) {
            $field  = I('post.field', '', 'trim');
            $id     = I('post.id', '', 'trim');
            $pk     = I('post.pk', '', 'trim');
            $value  = I('post.value', '', 'trim');
            $status = 0;
            $msg    = '编辑失败';
            if ($field && $id & $pk && $value) {
                $status = 1;
                $msg    = '编辑成功';
                $model->where(array($pk => $id))->save(array($field => $value));
            }
            $this->ajaxReturn(array('msg' => $msg, 'status' => $status));
        } elseif (IS_POST) {
            if (!I('post.id')) {
                return array('status' => 0, 'msg' => '传参有误!');
            }
            $data = array(
                'id'             => I('post.id', 0, 'intval'),
                'countryid'      => I('post.countryid', 0, 'intval'),
                'visatype_id'    => I('post.visatype_id', 0, 'intval'),
                'district_id'    => I('post.district_id', 0, 'intval'),
                'acceptancedays' => I('post.acceptancedays'),
                'entrynumber'    => I('post.entrynumber'),
                'interviewed'    => I('post.interviewed', 0, 'intval'),
                'advanceday'     => I('post.advanceday'),
                'bonded'         => I('post.bonded'),
                'validterm'      => I('post.validterm'),
                'periodofstay'   => I('post.periodofstay'),
                'intro'          => I('post.intro'),
                'message'        => I('post.message'),
                'partners_id'    => I('post.partners_id', 0, 'intval'),
                'price'          => I('post.price', 0, 'floatval'),
                'remark'         => I('post.remark'),
                'ordid'          => I('post.ordid', 0, 'intval'),
                'status'         => I('post.status')==1?:0,
            );
            
            $result = $model->_save($data);
            if ($result['status'] == 1) {
                $this->success($result['msg'], U(CONTROLLER_NAME . '/edit?id='.I('post.id')));
            } else {
                $this->error($result['msg']);
            }
        }

        $id = I('get.id', 0, 'intval');
        if (!$id) {
            $this->error('参数错误');
        }

        $data = array(
            'id' => $id,
            'deletebs' => 0,
        );

        $info = $model -> where($data) -> find();
        if (!$info) {
            $this->error('数据不存在');
        }

        $zsys_data  = D('Zsysdata')->get_zsysdata(array('type' => 'interview'));//面签方式
        $types_data = D('Types')->get_typesdata(array('type' => 'visatype'));//签证分类
        $this->assign('bar', array('curpos' => '编辑', 'menu' => '签证产品', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('interviews',$zsys_data['interview']);
        $this->assign('visatypes',$types_data['visatype']);
        $this->assign('info', $info);

        $this->display();
    }
    public function delete() {
        $id    = I('request.id');
        $model = M('Visa');
        if (!$id) {
            IS_AJAX && $this->ajaxReturn(array('status' => 0, 'msg' => '参数错误'));
            $this->error($this->error('参数错误'));
        }
        $id = explode(',', $id);
        foreach ($id as &$i) {
            $i = intval($i);
        }
        $data = array(
            'deletebs' => 1,
        );
        
        $model->where(array('id' => array('IN', implode(',', $id))))->limit(count($id))->save($data);
        IS_AJAX && $this->ajaxReturn(array('status' => 1, 'msg' => '删除成功'));
        $this->success('删除成功');
    }

    //签证类型
    public function types(){
        if (IS_POST) {
            //删除价格行
            if(I('post.deleteprice')==1){
                $flag = M('Types')->where(array('id' => I('post.id'), 'type' => 'visatype'))->save(array('deletebs'=>1));
                if($flag){
                    IS_AJAX && $this->ajaxReturn(array('status' => 1, 'msg' => '删除成功'));
                    $this->success('删除成功');
                }else{
                    IS_AJAX && $this->ajaxReturn(array('status' => -1, 'msg' => '删除失败'));
                    $this->success('删除失败');
                }
                
            }
            
            $model = M('types');
            
            //修改价格行
            $edit_title  = I('post.edit_title', array());
            $edit_ordid  = I('post.edit_ordid', array());
            $edit_status = I('post.edit_status', array());

            if(is_array($edit_title)){
                foreach($edit_title as $key => $val){
                    if($edit_title[$key]==''){
                        continue;
                    }
                    $data = array(
                        'id'     => $key,
                        'title'  => $edit_title[$key],
                        'ordid'  => intval($edit_ordid[$key]),
                        'status' => $edit_status[$key]==1?1:0,
                    );
                    if ($model -> create($data)) {
                        $model -> save();
                    }
                }
            }

            //新增
            $title  = I('post.title', array());
            $ordid  = I('post.ordid', array());
            $status = I('post.status', array());
            
            if(is_array($title)){
                foreach ($title as $key => $val) {
                    if($val==''){
                        continue;
                    }

                    $data = array(
                        'title'  => $title[$key],
                        'ordid'  => intval($ordid[$key]),
                        'status' => 1,
                        'type'   => 'visatype',
                    );
                    if ($model->create($data)) {
                        $model->add();
                    }
                }
            }
            $this->success('保存成功');
        }

        $map['type']     = 'visatype';
        $map['deletebs'] = 0;
        $field = 'id,title,ordid,status';
        $order = ' ordid desc,id';
        $getlist = D('Types')->getList($map, $field, $order);
        $this->assign('bar', array('curpos' => '签证类型', 'menu' => '签证产品', 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('list', $getlist['list']);
        $this->assign('page', $getlist['page']);
        $this->display();
    }

    //签证结算价格行
    public function price() {
        if (IS_POST) {
            //删除价格行
            if(I('post.deleteprice')==1){
                $flag = M('visa_price')->where(array('id' => I('post.id'), 'visa_id' => I('post.visa_id')))->save(array('deletebs'=>1));
                if($flag){
                    IS_AJAX && $this->ajaxReturn(array('status' => 1, 'msg' => '删除成功'));
                    $this->success('删除成功');
                }else{
                    IS_AJAX && $this->ajaxReturn(array('status' => -1, 'msg' => '删除失败'));
                    $this->success('删除失败');
                }
            }

            //新增修改价格行
            $model = D('VisaPrice');

            $starttime       = I('post.starttime', array());
            $endtime         = I('post.endtime', array());
            //$market_price    = I('post.market_price', array());
            //$price           = I('post.price', array());
            $cost_price      = I('post.cost_price', array());
            $visa_id         = I('post.visa_id', 0, 'intval');
            $edit_starttime  = I('post.edit_starttime', array());
            $edit_endtime    = I('post.edit_endtime', array());
            //$edit_market_price   = I('post.edit_market_price', array());
            //$edit_price          = I('post.edit_price', array());
            $edit_cost_price = I('post.edit_cost_price', array());

            if (!$visa_id) {
                $this->error('传参错误');
            }
            
            //修改价格行
            if(is_array($edit_starttime)){
                foreach($edit_starttime as $key => $val){
                    if(strtotime($edit_starttime[$key]) >= strtotime($edit_endtime[$key]) || $edit_starttime[$key]==''){
                        continue;
                    }
                    $data = array(
                        'id'            => $key,
                        'starttime'     => strtotime($edit_starttime[$key]),
                        'endtime'       => strtotime($edit_endtime[$key]),
                        'cost_price'    => floatval($edit_cost_price[$key]),
                        //'market_price'  => floatval($edit_market_price[$key]),
                        //'price'         => floatval($edit_price[$key]),
                    );
                    $model -> _save($data);
                }
            }

            //新增
            if(is_array($starttime)){
                foreach ($starttime as $key => $val) {
                    if(strtotime($starttime[$key]) >= strtotime($endtime[$key]) || $starttime[$key]==''){
                        continue;
                    }
                    
                    //新增开始日期需与已有最大的结束日期相接 如最大的结束日期为 2015-09-30 则 新增开始日期只能为 2015-10-01
                    //当前未做完整性处理

                    $data = array(
                        'visa_id'    => $visa_id,
                        'starttime'  => strtotime($starttime[$key]),
                        'endtime'    => strtotime($endtime[$key]),
                        'cost_price' => floatval($cost_price[$key]),
                        //'market_price'  => floatval($market_price[$key]),
                        //'price'         => floatval($price[$key]),
                    );
                    $model->_add($data);
                }
            }
            $this->success('保存成功', U(CONTROLLER_NAME . '/price?id='.$visa_id));
        }

        $this->assign('bar', array('curpos' => '签证价格', 'menu' => '签证列表', 'url' => U(CONTROLLER_NAME . '/index')));
        $id = I('get.id', 0, 'intval');
        if (!$id) {
            $this->error('传参错误');
        }
        $info = M('Visa') -> where(array('id' => $id)) -> find();
        if (!$info) {
            $this->error('主题不存在');
        }

        $visatypetitle = getTypes($info['visatype_id']);
        $countryname   = getTitleById(M('district'),$info['countryid']);
        $list = D('Visa') -> getpricelist(array('visa_id' => $id, 'deletebs' => 0));
        $this->assign('list', $list);
        $this->assign('info', $info);
        $this->assign('visatypetitle', $visatypetitle);
        $this->assign('countryname', $countryname);
        $this->display();
    }
}
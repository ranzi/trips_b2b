<?php
/**
 *@author qiisyhx<sihangshi2011@gmail.com>
 *@version $Id 2015-09-22
 *无言独上西楼
 *月如钩
 *寂寞梧桐深院锁千秋
 *剪不断、理还乱
 *是离愁
 *别是一般滋味在心头
 *
 *约定：数据表别名为b，配置附加SQL时可用
 **/
namespace Admin\Controller;

class WorkflowController extends AuthController {
    private $model; //模型
    private $flow; //流程
    private $fid; //流程id
    private $sid; //当前步骤ID
    private $tid; //数据表id
    private $bid; //业务ID
    private $steps; //流程步骤
    private $min_stepid; //起始步骤
    private $cur_step; //当前步骤
    private $bdata; //业务数据
    private $isArchived; //是否归档
    private $_uuid; //当前用户id
    private $_ugroups; //当前用户所有用户组
    private $_udepts; //当前用户说要部门
    private $_upriv; //根据步骤，计算出的权限关系

    public function _initialize() {
        header('Content-Type:text/html;charset=utf-8');
        parent::_initialize();
        $fid       = I('get.fid', 0, 'intval');
        $this->fid = $fid;
        if (!$fid) {
            $this->error('Parameter Error[10001]:None fid.');
        }
        $this->flow = D('Flow')->get($fid);
        if (!$this->flow) {
            $this->error('Can not found error:workflow ' . $fid . ' not exists.');
        }
        if (!$this->flow['min_stepid']) {
            $this->error('Flow Error[10013]:Can not find the fisrt step');
        }
        $this->min_stepid = $this->flow['min_stepid'];
        $this->tid        = $this->flow['tid'];
        if (!$this->flow['tid']) {
            $this->error('Workflow Error[10010]: Workflow has not bind data model');
        }
        $this->table = D('Tables')->get($this->tid);
        if (!$this->table || !$this->table['model']) {
            $this->error('Workflow Error[10011]:Data model not exists');
        }
        $this->model = D($this->table['model']);
        $steps       = D('FlowStep')->getAll();
        foreach ($steps as $step) {
            $this->steps[$step['sid']] = $step;
        }
        #业务参数
        $this->bid = I('request.bid', 0, 'intval');
        if ($this->bid) {
            $this->bdata = $this->model->get($this->bid);
        }
        $this->isArchived = I('get.op', '', 'trim') == 'view' ? true : false;
        // dump($this->model);exit;
        #初始化用户信息
        $this->_uuid    = session('admin_id');
        $this->_ugroups = session('groupids');
        $this->_udepts  = $this->getUDepts();
        $this->_upriv   = $this->privelege();
        // dump(session());exit;
    }

    public function index() {
        $condition = $this->getListSql();
        // dump($condition);exit;
        $total = $this->model->alias('b')->where($condition)->count();
        $psize = 20;
        $pager = new \Think\Page($total, $psize);
        //可操作的数据列表
        $list = $this->model->alias('b')->where($condition)->limit($pager->firstRow . ',' . $pager->listRows)->order('id DESC')->select();
        // dump($this->model->_sql());exit;
        $fields = D('TablesFields')->alias('tf')->field('tf.*')->where(array('tf.tid' => $this->tid))->order('ordid desc')->select();
        $this->assign('fields', $fields);
        $this->assign('list', $list);
        // dump($list);exit;
        $this->assign('page', $pager->show());
        $this->assign('total', $total);
        $this->assign('start', $this->steps[$this->min_stepid]);
        $this->assign('steps', $this->steps);
        $this->assign('fid', $this->fid);
        $this->assign('bar', array('curpos' => $this->flow['title'] . '列表', 'menu' => $this->flow['title'], 'url' => U(CONTROLLER_NAME . '/index')));
        $this->display();
    }

    /**
     *创建数据操作权限为第一步权限
     *第一步权限中，由于附加SQL无法使用，因此，不作为判断是否有创建流程的权限的条件
     *若用户满足前三个条件，即：用户组，部门，指定用户，则视为有创建流程权限，
     *附加条件中，配置b.uid=${admin_id}则表示在数据列表中，自己只能看到自己填写的内容
     **/
    public function add() {
        if ($this->bid) {
            $this->cur_step = $this->bdata['step'];
            if ($this->cur_step == 0) {
                $this->isArchived = true;
            } else {
                $this->updateWorkflowWaitRead();
            }
        } else {
            $this->cur_step = $this->min_stepid;
        }
        if ((!$this->bid && !$this->hasAddPriv()) || !$this->hasEditPriv()) {
            $this->error('您没有权限');
        }
        if (IS_POST) {
            //提交数据
            $data = $this->model->create(I('post.'));
            if (FALSE !== $data) {
                //添加默认数据
                if (!$this->bid) {
                    $data['uid'] = $this->_uuid;
                    $data['fid'] = $this->fid;
                }
                $btnId = I('post.button', 0, 'intval');
                if (!$btnId) {
                    $this->error('请选择一种操作');
                }
                $button        = D('FlowStepButton')->get($btnId);
                $data['step']  = $button['next_step'];
                $data['steps'] = $this->bdata['steps'] ? $this->bdata['steps'] . ',' . $this->cur_step : $this->cur_step;
                #数据检查
                $data = $this->dataFilter($data);
                #写入数据
                if (!$this->bid) {
                    $this->bid = $this->model->add($data);
                } else {
                    $res = $this->model->where(array('id' => $this->bid))->save($data);
                }

                if (FALSE == $this->bid) {
                    $this->error('添加数据失败');
                }
                //log
                $opType = $this->bid ? 2 : 1;
                $this->operateLog($opType);
                // dump($button);exit;
                //sendmsg
                if ($this->min_stepid == $button['next_step']) {
                    //创建流程且为保存数据，则不发送消息
                    //默认之后的流程步骤不在有保存数据的情况，因此，不在判断当前步骤==下一步的情况
                    //所有流程步骤，不管后退还是前进,直接进入下一步
                } else {
                    $this->updateWorkflowWaitEnd();
                    //获得有权限的人,并发送消息
                    $users = $this->getStepPrivUsers($button['next_step']);
                    // dump($users);exit;
                    $this->sendWorkflowMsg($button['next_step'], $users);
                }
                $this->success('数据保存成功', U('Workflow/index', array('fid' => $this->fid)));
            } else {
                $this->error('数据保存失败，请重试');
            }
        }
        #进入表单创建流程
        $this->assign('html', $this->createForm());
        $this->assign('bar', array('curpos' => $this->steps[$this->cur_step]['title'], 'menu' => $this->flow['title'], 'url' => U(CONTROLLER_NAME . '/index')));
        $this->assign('buttons', $this->createButtons());
        $this->assign('isArchived', $this->isArchived);
        $this->display('action');
    }

    protected function hasAddPriv() {
        #默认无权限
        $has = false;
        if ($this->steps) {
            foreach ($this->steps as $row) {
                if ($row['sid'] == $this->min_stepid) {
                    if ($row['users']) {
                        $_tmp = explode(',', $row['users']);
                        if (in_array($this->_uuid, $_tmp)) {
                            $has = true;
                            break;
                        }
                    }
                    $__flag = false;
                    if ($row['groups']) {
                        $_tmp = explode(',', $row['groups']);
                        if (count(array_intersect($_tmp, $this->_ugroups))) {
                            $__flag = true;
                        }
                    }
                    if ($__flag) {
                        if ($row['depts']) {
                            $_tmp = explode(',', $row['depts']);
                            if (count(array_intersect($_tmp, $this->_udepts))) {
                                $has = true;
                            } else {
                                $has = false;
                            }
                        } else {
                            $has = true;
                        }
                    }
                    break;
                }
            }
        }
        return $has;
    }

    public function hasEditPriv() {
        #默认无权限
        $has         = false;
        $condition   = $this->_upriv[$this->cur_step];
        $condition   = $condition ? ' AND ' . $condition : '';
        $this->bdata = $this->model->alias('b')->where('id=' . $this->bid . $condition)->find();
        if ($this->bdata) {
            $has = true;
        } else {
            $this->bdata = $this->model->alias('b')->where('id=' . $this->bid . ' and b.uid=' . $this->_uuid)->find();
            if ($this->bdata) {
                $has              = true;
                $this->isArchived = true;
            }
        }
        return $has;
    }

    public function edit() {

    }

    /**
     *说明:组合权限
     *获取每一个步骤的权限
     *若为列表，则各个步骤权限组合，得到各自列表，在组合
     *USER为or条件，groups及depts为AND条件
     *即：若指定用户组，则用户组里所有人均有权限，
     *若追加部门，则限制为用户组x中，属于y部门的人有权限
     *users则表示，在上一个条件之外，用户u也应当具有相应的权限
     **/
    public function privelege() {
        if ($this->_upriv) {
            return $this->_upriv;
        }
        $steps  = $this->steps;
        $result = array();
        if ($steps) {
            foreach ($steps as $row) {
                $and = array();
                $or  = array();
                #用户组权限判定--start
                if ($row['groups']) {
                    #只要有一个用户组在里面，就判定为有权限
                    $_temp = explode(',', $row['groups']);
                    if ($this->_ugroups) {
                        $__flag = false;
                        // foreach ($this->_ugroups as $__g) {
                        //     if (in_array($__g, $_temp)) {
                        //         $and[]  = ' 1=1 ';
                        //         $__flag = true;
                        //         break;
                        //     }
                        // }
                        if (count(array_intersect($this->_ugroups, $_temp))) {
                            $and[]  = ' 1=1 ';
                            $__flag = true;
                        }
                        if (!$__flag) {
                            $and[] = ' 1=2 ';
                        }
                    } else {
                        $and[] = ' 1=2 ';
                    }
                }
                #用户组权限判定--end
                if ($row['depts']) {
                    if ($this->_udepts) {
                        $_temp  = explode($row['depts']);
                        $__flag = false;
                        // foreach ($this->_udepts as $__d) {
                        //     if (in_array($__d, $_temp)) {
                        //         $and[]  = ' 1=1 ';
                        //         $__flag = true;
                        //         break;
                        //     }
                        // }
                        if (count(array_intersect($_temp, $this->_udepts))) {
                            $and[]  = ' 1=1 ';
                            $__flag = true;
                        }
                        if (!$__flag) {
                            $and[] = ' 1=2 ';
                        }
                    } else {
                        $and[] = ' 1=2 ';
                    }
                }
                #部门权限判定--start
                #部门权限判定--end
                #追加单个用户操作
                if ($row['users']) {
                    $or = ' OR (' . $this->_uuid . ' IN (' . $row['users'] . '))';
                }
                $_sql = '';
                if ($and) {
                    $_sql .= '(' . implode(' AND ', $and) . ')';
                }
                if ($or) {
                    $_sql .= $or;
                }
                #拼接附加SQL,附加SQL为and和or操作之后，附加的and条件，即追加条件
                if ($row['sql']) {
                    $_sql = '(' . $_sql . ') AND ' . $row['sql'];
                }
                #替换SQL中的变量
                $_sql = preg_replace_callback("/\\\$\{([^}]+)\}/", function ($match) {return session($match[1]);}, $_sql);
                $result[$row['sid']] = '(' . $_sql . ')';
            }
        }
        return $result;
    }

    #获取当前用户的部门信息
    public function getUDepts() {
        $res  = D('AdminDept')->field('dept_id')->where(array('uid' => $this->_uuid))->select();
        $temp = array();
        foreach ($res as $row) {
            $temp[] = $row['dept_id'];
        }
        return $temp;
    }

    #获取列表页的SQL条件
    /**
     *不关联具体业务
     *数据列表按步骤划分
     *1、得到用户u每个步骤s的数据列表
     *2、合并数据列表(独立数据集之间为or关系)，提取top x条数据
     *3、每条数据对应的操作权限判定，view/edit
     *如，员工请假流程，员工a填写完成，提交到部门领导审批，此时，a仅有查看权限，领导L则有操作权限
     **/
    public function getListSql() {
        $_lastSQL = array();
        if ($this->_upriv) {
            foreach ($this->_upriv as $stepid => $sql) {
                #业务表当前步骤为$stepid且满足$sql的数据为当前步骤有效数据
                $_lastSQL[] = ' (b.`step`=' . $stepid . ' AND ' . $sql . ')';
            }
        }
        $_lastSQL[] = ' b.uid=' . $this->_uuid . ' ';
        return !empty($_lastSQL) ? implode(' OR ', $_lastSQL) : '';
    }

    /**
     *获取指定步骤的权限
     **/
    public function getStepSql($stepid) {
        return $this->_upriv[$stepid] ?: '';
    }

    /**
     *获取数据列表
     **/
    public function getViewList() {
    }

    #创建表单
    protected function createForm() {
        #数据归档后，取第一步的字段
        $step   = $this->isArchived ? $this->min_stepid : $this->cur_step;
        $fields = D('TablesFields')->alias('tf')->join(C('DB_PREFIX') . 'flow_step_fields fsf ON tf.tid=fsf.tid AND tf.fields=fsf.fields', 'LEFT')->field('tf.*,fsf.isshow,fsf.status as status2')->where(array('tf.tid' => $this->tid, 'fsf.sid' => $step))->order('ordid desc')->select();
        $datas  = $this->bdata ?: array();
        $html   = '';
        foreach ($fields as $key => $row) {
            $html .= $this->createElement($row, $datas[$row['fields']]);
        }
        return $html;
    }

    protected function createElement($field, $value) {
        // dump($field);exit;
        if ($field['pk'] == 1) {
            return '';
        }
        $parseStr = '';
        $required = $field['require'] == 1 ? '<em class="red">*</em>' : '';
        if ($this->isArchived) {
            #如果已归档，则强制设为只读、显示，即只能查看
            $field['status2'] = 1;
            $field['isshow']  = 1;
        }
        $default_value = strpos($field['default_value'], ':') !== false ? '' : $field['default_value'];
        $value         = $value ?: $default_value;
        $name          = $field['fields'];
        $id            = $name;
        $parseStr .= '<div class="form-group">';
        $parseStr .= '<label class="col-xs-12 col-sm-3 col-md-2 control-label">' . $required . $field['title'] . '</label>';
        $parseStr .= '<div class="col-xs-12 col-sm-9">';
        if ($field['isshow'] == 1) {
            #日期类型
            if ('date' == $field['showtypes']) {
                $value = $value ? date('Y-m-d', $value) : '';
                // $str         = $this->form->_datetime($tag);
                if ($field['status2'] == 2) {
                    if (!defined('TPL_INIT_DATE')) {
                        $parseStr .= '<script type="text/javascript">
			                require(["datetimepicker"], function($){
			                    $(function(){
			                        $(".datetimepicker.date").each(function(){
			                            var opt = {
			                                language: "zh-CN",
			                                minView: 2,
			                                format: "yyyy-mm-dd",
			                                autoclose: true,
			                                todayBtn: true
			                            };
			                            $(this).datetimepicker(opt);
			                        });
			                    });
			                });
			            </script>';
                        defined('TPL_INIT_DATE', TRUE);
                    }
                    $parseStr .= '<div class="input-group"><input type="text" name="' . $name . '" value="' . $value . '" data-tips="' . $field['title'] . '" readonly="readonly" class="datetimepicker date form-control" id="' . $id . '"' . ($field['require'] ? ' required="required"' : '') . '><span class="input-group-addon"><i class="icon-calendar"></i></span></div>';
                } else {
                    $parseStr .= '<span class="form-control-static">' . $value . '</span>';
                }

            } elseif ('input' == $field['showtypes']) {
                $parseStr .= '<input type="text" class="form-control" name="' . $name . '" id="' . $id . '" value="' . $value . '" data-tips="' . $field['title'] . '"' . ($field['require'] ? ' required="required"' : '');
                if ($field['status2'] == 1) {
                    $parseStr .= ' readonly="true"';
                }
                $parseStr .= '>';
            }
        }

        $parseStr .= '</div>';
        $parseStr .= '</div>';
        // dump($parseStr);exit;
        return $parseStr;
    }

    #创建OPTIONS BUTTON
    public function createButtons() {
        $buttons = D('FlowStepButton')->order('ordid desc')->getAll(array('fid' => $this->fid, 'sid' => $this->cur_step));
        $bhtml   = '<div class="col-xs-12 workflow-action-button" style="background-color: #fff; padding: 5px 10px; margin-bottom: 5px; border-radius: 8px;">';
        foreach ($buttons as $key => $item) {
            $bhtml .= '<label class="radio-inline"><input type="radio" name="button" id="button_' . $item['id'] . '" value="' . $item['id'] . '">' . $item['title'] . '</label>';
        }
        $bhtml .= '</div>';
        return $bhtml;
    }

    /**
     *工作流操作日志
     **/
    public function operateLog($type) {
        $data['fid']     = $this->fid;
        $data['sid']     = $this->cur_step;
        $data['uid']     = $this->_uuid;
        $data['uname']   = session('admin_name');
        $data['addtime'] = NOW_TIME;
        $data['types']   = $type;
        $data['title']   = $this->steps[$this->cur_step]['title'];
        $data['content'] = '';
        $data['bid']     = $this->bid;
        D('FlowLog')->add($data);
    }

    /**
     *获取有权限操作的人
     **/
    public function getStepPrivUsers($step) {
        $rows = $this->steps[$step];
        $and  = array(" 1=1 ");
        $or   = '';
        if ($rows['groups']) {
            $and[] = ' ag.group_id IN(' . $rows['groups'] . ') ';
        }
        if ($rows['depts']) {
            $and[] = ' ad.dept_id IN(' . $rows['depts'] . ') ';
        }
        if ($rows['users']) {
            $or = ' OR a.id IN (' . $rows['users'] . ') ';
        }
        $sql = "SELECT DISTINCT a.id,a.username FROM " . C('DB_PREFIX') . "admin a, " . C('DB_PREFIX') . "admingroup ag, " . C('DB_PREFIX') . "admin_dept ad WHERE a.id=ag.uid AND a.id=ad.uid AND (" . implode(' AND ', $and) . ") " . $or;
        // echo $sql;exit;
        $mod   = new \Think\Model();
        $users = $mod->query($sql);
        // dump($users);exit;
        return $users;
    }

    #发送待办消息
    public function sendWorkflowMsg($step, $users = array()) {
        if ($users && is_array($users)) {
            foreach ($users as $row) {
                $data = array(
                    'fid'     => $this->fid,
                    'sid'     => $step,
                    'uid'     => $row['id'],
                    'uname'   => $row['username'],
                    'title'   => $this->steps[$step]['title'],
                    'status'  => 0,
                    'addtime' => NOW_TIME,
                    'bid'     => $this->bid,
                );
                $res = D('FlowWait')->add($data);
            }
        }
    }

    #数据过滤
    public function dataFilter($data) {
        $fields = D('TablesFields')->alias('tf')->join(C('DB_PREFIX') . 'flow_step_fields fsf ON tf.tid=fsf.tid AND tf.fields=fsf.fields', 'LEFT')->field('tf.*,fsf.isshow,fsf.status as status2')->where(array('tf.tid' => $this->tid, 'fsf.sid' => $this->cur_step))->order('ordid desc')->select();
        foreach ($fields as $key => $row) {
            if ($row['pk'] == 1) {
                continue;
            }
            if ($row['status2'] == 1) {
                #只读状态不更新数据
                unset($data[$row['fields']]);
                continue;
            }
            if ($row['require'] == 1 && $data[$row['fields']] == '') {
                $this->error('请填写 ' . $row['title']);
            }
            if ($row['showtypes'] == 'date' || $row['showtypes'] == 'time') {
                $data[$row['fields']] = strtotime($data[$row['fields']]);
            }
        }
        // ksort($data);
        return $data;
    }

    #更新待办状态，点击进入，更改为已读状态
    public function updateWorkflowWaitRead() {
        D('FlowWait')->where(array('fid' => $this->fid, 'bid' => $this->bid, 'sid' => $this->cur_step, 'uid' => $this->_uuid))->save(array('readtime' => NOW_TIME, 'status' => 1));
    }

    #更改待办状态为完成，当提交流程，进入下一步时，更改状态
    #操作人更新完成时间，其余有权限的待办提示用户仅更新状态，以便追踪操作用户
    public function updateWorkflowWaitEnd() {
        $M = D('FlowWait');
        $M->where(array('fid' => $this->fid, 'bid' => $this->bid, 'sid' => $this->cur_step, 'uid' => $this->_uuid))->save(array('endtime' => NOW_TIME, 'status' => 2));
        $M->where(array('fid' => $this->fid, 'bid' => $this->bid, 'sid' => $this->cur_step, 'uid' => array('neq', $this->_uuid)))->save(array('status' => 2));
    }
}
<?php
namespace Admin\Model;
use Think\Model;

class AdminDeptModel extends Model {
    protected $pk     = 'uid';
    protected $fields = array('uid', 'dept_id');
    protected $_auto  = array();

    public function saveDepts($uid, $depts) {
        //delege all
        $this->where(array('uid' => $uid))->delete();
        foreach ($depts as $dept) {
            $this->add(array('uid' => $uid, 'dept_id' => $dept));
        }
        return true;
    }

    public function getAllDepts($uid) {
        $hasDepts = $this->field('dept_id')->where(array('uid' => I('get.id')))->select();
        $temp     = array();
        if ($hasDepts && is_array($hasDepts)) {
            foreach ($hasDepts as $dept) {
                $temp[] = $dept['dept_id'];
            }
        }
        return $temp;
    }
}
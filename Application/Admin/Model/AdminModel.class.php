<?php
namespace Admin\Model;
use Think\Model;

class AdminModel extends BaseModel {
    protected $tablePrefix = 'b2c_';
    protected $pk          = 'id';
    protected $fields      = array('id', 'username', 'pwd', 'mobile', 'realname', 'salt', 'status', 'avatar', 'client', 'last_time', 'last_ip', 'addtime', 'dept_id', 'views', 'job', 'pos');
    protected $_auto       = array();
    protected $_validate   = array();

    public function _save() {
        $data             = array();
        $data['realname'] = I('post.realname', '', 'trim');
        $data['status']   = I('post.status', 0, 'intval') == 1 ? 1 : 0;
        $password         = I('post.password', '', 'trim');
        $repassword       = I('post.repassword', '', 'trim');
        $data['avatar']   = I('post.avatar', '', 'trim');
        $data['dept_id']  = I('post.dept_id', 0, 'intval');
        $data['views']    = I('post.views', 2, 'intval');
        $data['job']      = I('post.job', 0, 'intval');
        $data['pos']      = I('post.pos', 0, 'intval');
        if (!$data['realname']) {
            return array('status' => 0, 'msg' => '真实姓名不能为空');
        }
        if ($password) {
            if (strlen($password) < 8 || strlen($password) > 20) {
                return array('status' => 0, 'msg' => '密码长度为8-20位');
            }
            if ($password != $repassword) {
                return array('status' => 0, 'msg' => '两次输入密码不一致');
            }
            $data['salt'] = random(4);
            $data['pwd']  = sha1(C('B2C_ENCRY') . $password . $data['salt']);
        }
        $id = I('post.id', 0, 'intval');
        if (false !== $this->where(array('id' => $id))->save($data)) {
            //update group
            D('Admingroup')->saveGroup($id, I('post.group_id', array()));
            D('AdminDept')->saveDepts($id, I('post.depts', array()));
            return array('status' => 1, 'msg' => '编辑管理员成功');
        } else {
            return array('status' => 0, 'msg' => '编辑管理员失败');
        }
    }

    public function _add() {
        $data             = array();
        $data['username'] = I('post.username', '', 'trim');
        $data['realname'] = I('post.realname', '', 'trim');
        $data['avatar']   = I('post.avatar', '', 'trim');
        $data['mobile']   = I('post.mobile', '', 'trim');
        $data['status']   = I('post.status', 0, 'intval') == 1 ? 1 : 0;
        $data['addtime']  = NOW_TIME;
        $data['salt']     = random(4);
        $data['views']    = I('post.views', 2, 'intval');
        $data['dept_id']  = I('post.dept_id', 0, 'intval');
        $data['job']      = I('post.job', 0, 'intval');
        $data['pos']      = I('post.pos', 0, 'intval');
        $password         = I('post.password', '', 'trim');
        $repassword       = I('post.repassword', '', 'trim');
        if (!$data['username']) {
            return array('status' => 0, 'msg' => '登录名不能为空');
        }
        if (!$data['realname']) {
            return array('status' => 0, 'msg' => '真实姓名不能为空');
        }
        if (!$data['mobile']) {
            return array('status' => 0, 'msg' => '手机号码不能为空');
        }
        if (!$password) {
            return array('status' => 0, 'msg' => '密码不能为空');
        }
        if (strlen($password) < 8 || strlen($password) > 20) {
            return array('status' => 0, 'msg' => '密码长度为8-20位');
        }
        if ($password != $repassword) {
            return array('status' => 0, 'msg' => '两次输入密码不一致');
        }
        if ($this->mobileExists($data['mobile'])) {
            return array('status' => 0, 'msg' => '手机号码已存在');
        }
        if ($this->usernameExists($data['username'])) {
            return array('status' => 0, 'msg' => '登录名已存在');
        }

        $data['pwd'] = sha1(C('B2C_ENCRY') . $password . $data['salt']);
        if (($uid = $this->add($data))) {
            D('Admingroup')->saveGroup($uid, I('post.group_id', array()));
            D('AdminDept')->saveDepts($uid, I('post.depts', array()));
            return array('status' => 1, 'msg' => '添加管理员成功');
        } else {
            return array('status' => 0, 'msg' => '添加管理员失败');
        }
    }

    private function mobileExists($mobile) {
        $info = $this->where(array('mobile' => $mobile))->getField('mobile');
        if (empty($info)) {
            return false;
        }
        return true;
    }

    private function usernameExists($username) {
        $info = $this->where(array('username' => $username))->getField('username');
        if (empty($info)) {
            return false;
        }
        return true;
    }
}
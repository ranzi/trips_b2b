<?php
namespace Admin\Model;
use Think\Model;

class AdmingroupModel extends Model {
    protected $pk     = 'uid';
    protected $fields = array('uid', 'group_id');
    protected $_auto  = array();

    public function saveGroup($uid, $groups) {
        //delege all
        $this->where(array('uid' => $uid))->delete();
        foreach ($groups as $group_id) {
            $this->add(array('uid' => $uid, 'group_id' => $group_id));
        }
        return true;
    }
}
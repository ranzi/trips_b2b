<?php
namespace Admin\Model;

class AttachmentModel extends BaseModel {
    protected $pk        = 'id';
    protected $fields    = array('id', 'addtime', 'counts', 'downloads');
    protected $_auto     = array();
    protected $_validate = array(
    );
}
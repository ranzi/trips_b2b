<?php
namespace Admin\Model;

class AttachmentUrlModel extends BaseModel {
    protected $pk        = 'id';
    protected $fields    = array('id', 'title', 'url', 'downloads', 'addtime', 'admin_id', 'type', 'pid', 'size', 'sizeShow', 'status');
    protected $_auto     = array();
    protected $_validate = array(
    );
}
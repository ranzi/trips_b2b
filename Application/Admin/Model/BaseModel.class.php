<?php
namespace Admin\Model;
use Think\Model;

class BaseModel extends Model {
    public function __construct() {
        parent::__construct();
    }

    public function _add() {
        if ($this->create($_POST) != false) {
            // if (in_array('status', $this->fields)) {
            //     $this->status = $this->status == 1 ? 1 : 0;
            // }
            if (is_array($this->_checkbox) && !empty($this->_checkbox)) {
                foreach ($this->_checkbox as $val) {
                    $this->$val = $this->$val == 1 ? $this->$val : 0;
                }
            }
            if (method_exists($this, '_handler')) {
                $this->_handler();
            }
            $this->add();
            return array('status' => 1, 'msg' => '添加成功');
        } else {
            return array('status' => 0, 'msg' => $this->getError());
        }
    }

    public function _save() {
        if ($this->create($_POST) != false) {
            if (is_array($this->_checkbox) && !empty($this->_checkbox)) {
                foreach ($this->_checkbox as $val) {
                    $this->$val = $this->$val == 1 ? $this->$val : 0;
                }
            }
            if (method_exists($this, '_handler')) {
                $this->_handler();
            }
            $this->where(array($this->getPk() => I('post.' . $this->getPk(), '', 'trim')))->save();
            return array('status' => 1, 'msg' => '编辑成功');
        } else {
            return array('status' => 0, 'msg' => $this->getError());
        }
    }

    public function toSelect($map = array()) {
        $map['status'] = 1;
        return $this->where($map)->getField('id, title');
    }

    public function toTree($map = array()) {
        return toTree($this->where($map)->order('ordid desc')->select());
    }

    public function get($id) {
        return $this->where(array($this->pk => $id))->find();
    }

    public function getAll($map = array()) {
        return $this->where($map)->select();
    }
}
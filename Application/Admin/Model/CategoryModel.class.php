<?php
namespace Admin\Model;

class CategoryModel extends BaseModel {
    protected $pk        = 'id';
    protected $fields    = array('id', 'pid', 'title', 'ordid', 'status', 'path', 'type_id');
    protected $_auto     = array();
    protected $_validate = array(
        array('title', 'require', '分类名称不能为空'),
        array('type_id', 'require', '请选择所属类型'),
    );

    public function _add() {
        if ($this->create($_POST) != false) {
            $this->status = $this->status == 1 ? 1 : 0;
            $pid          = $this->pid;
            $insertId     = $this->add();

            $path = trim($this->getPath($pid), ',');

            $path = empty($path) ? ',' . $insertId . ',' : ',' . $path . ',' . $insertId . ',';

            $this->where(array('id' => $insertId))->save(array('path' => $path));
            return array('status' => 1, 'msg' => '添加成功');
        } else {
            return array('status' => 0, 'msg' => $this->getError());
        }
    }

    public function _save() {
        if ($this->create($_POST) != false) {
            $this->status = $this->status == 1 ? 1 : 0;

            $id   = I('post.id', 0, 'intval');
            $pid  = $this->pid;
            $path = trim($this->getPath($pid), ',');
            $path = empty($path) ? ',' . $id . ',' : ',' . $path . ',' . $id . ',';

            $oldpid = $this->where(array('id' => $id))->getField('pid');

            $this->where(array('id' => $id))->save();
            if ($pid != $oldpid) {
                $tree = $this->toTree();
                if (!empty($tree)) {
                    $this->updatePath($tree, $id);
                }
            }

            return array('status' => 1, 'msg' => '编辑成功');
        } else {
            return array('status' => 0, 'msg' => $this->getError());
        }
    }

    public function getPath($id) {
        return $this->where(array('id' => $id))->getField('path');
    }

    public function updatePath($tree, $root) {
        foreach ($tree as $row) {
            if ($row['id'] == $root) {
                $this->doUpdate($row);
                break;
            }
            if (!empty($row['child'])) {
                $this->updatePath($row['child'], $root);
            }
        }
    }

    public function doUpdate($tree) {
        if (!is_array($tree)) {
            return false;
        }
        $path = trim($this->getPath($tree['pid']), ',');
        $path = empty($path) ? ',' . $tree['id'] . ',' : ',' . $path . ',' . $tree['id'] . ',';
        $this->where(array('id' => $tree['id']))->save(array('path' => $path));
        if (!empty($row['child'])) {
            foreach ($row['child'] as $row) {
                $this->doUpdate($row);
            }
        }
    }
}
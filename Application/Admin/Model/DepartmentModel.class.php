<?php
namespace Admin\Model;

class DepartmentModel extends BaseModel {
    protected $pk        = 'id';
    protected $fields    = array('id', 'title', 'status', 'ordid', 'pid');
    protected $_auto     = array();
    protected $_validate = array(
        array('title', 'require', '部门名称不能为空'),
    );

    public function toTree() {
        $list = $this->where(array('status' => 1))->select();
        return toTree($list);
    }

    public function toSelect() {
        return $this->where(array('status' => 1))->getField('id,title');
    }
}
<?php
namespace Admin\Model;

class DistrictModel extends BaseModel {
    protected $pk = 'id';

    protected $fields    = array('id', 'title', 'letter', 'pid', 'level', 'ordid', 'status', 'ishot');
    protected $_auto     = array();
    protected $_validate = array(
        array('title', 'require', '请填写地区名称'),
    );
    protected $_checkbox = array('status', 'ishot');

    public function _handler() {
        $this->level = $this->getLevel($this->pid);
        // $this->ishot = $this->ishot == 1 ?: 0;
    }

    public function getLevel($id) {
        if ($id == 0) {
            return 1;
        } else {
            $level = $this->where(array('id' => $id))->getField('level');
            return $level + 1;
        }
    }

    /**
     * 获取国家ids
     * $title string
     * return string
     */
    public function get_countryids_liketitle($title) {
        if (!$title) {
            return;
        }
        $map['title'] = array("like", '%' . $title . '%');
        $map['type']  = 2; //境外
        $map['level'] = 3; //国家那一级

        $list = $this->field('id')->where($map)->select();
        $ids  = array();
        if ($list) {
            foreach ($list as $key => $val) {
                $ids[$val['id']] = $val['id'];
            }
            if ($ids) {
                return implode(',', $ids);
            }
        }
        return;
    }
    public function getDistrictId($map){
        $id = $this->where($map)->getField('id');
        return $id;
    }
}
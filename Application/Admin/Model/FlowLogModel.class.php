<?php
/**
 *@author qiisyhx<sihangshi2011@gmail.com>
 *@version $Id 2015-09-19
 *寻寻觅觅
 *冷冷清清
 *凄凄惨惨戚戚
 *乍暖还寒时候
 *最难将息
 *三杯两盏淡酒
 *怎敌他晚来风急
 *雁过也正伤心
 *却是旧时相识
 *满地黄花堆积
 *憔悴损、如今有谁堪摘
 *守着窗儿独自
 *怎生得黑
 *梧桐更兼细雨
 *到黄昏
 *点点滴滴
 *这次第
 *怎一个愁字了得
 **/
namespace Admin\Model;

class FlowLogModel extends BaseModel {
    protected $pk        = 'id';
    protected $fields    = array('id', 'sid', 'fid', 'title', 'types', 'uname', 'uid', 'addtime', 'content');
    protected $_auto     = array();
    protected $_validate = array();
    protected $_checkbox = array();
}
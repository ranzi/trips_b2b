<?php
namespace Admin\Model;

class FlowModel extends BaseModel {
    protected $pk     = 'fid';
    protected $fields = array('fid', 'title', 'addtime', 'min_stepid', 'status', 'url', 'tid', 'params');
    protected $_auto  = array(
        array('addtime', NOW_TIME),
    );
    protected $_validate = array(
        array('title', 'require', '业务名称不能为空'),
        array('tid', 'require', '请绑定业务表'),
    );
    protected $_checkbox = array('status');
}
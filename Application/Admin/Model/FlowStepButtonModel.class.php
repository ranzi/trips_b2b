<?php
/**
 *@author qiisyhx<sihangshi2011@gmail.com>
 *@version $Id 2015-09-19
 *老夫聊发少年狂
 *左牵黄
 *右擎苍
 *锦帽貂裘
 *千骑卷平冈
 *为报倾城随太守
 *亲射虎
 *看孙郎
 *酒酣胸胆尚开张
 *鬓微霜
 *又何妨
 *持节云中
 *何日遣冯唐
 *会挽雕弓如满月
 *西北望
 *射天狼
 **/
namespace Admin\Model;

class FlowStepButtonModel extends BaseModel {
    protected $pk        = 'id';
    protected $fields    = array('id', 'sid', 'fid', 'title', 'types', 'next_step', 'ordid');
    protected $_auto     = array();
    protected $_validate = array(
        array('fid', 'require', '按钮名称不能为空'),
        array('sid', 'require', '步骤不能为空'),
        array('types', 'require', '按钮类型不能为空'),
    );
    protected $_checkbox = array();
}
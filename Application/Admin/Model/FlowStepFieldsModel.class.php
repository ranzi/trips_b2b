<?php
/**
 *@author qiisyhx<sihangshi2011@gmail.com>
 *@version $Id 2015-09-22
 *春花秋月何时了？
 *往事知多少
 *小楼昨夜又东风
 *故国不堪回首明月中
 *雕栏玉砌应犹在
 *只是朱颜改
 *问君能有几多愁
 *恰似一江春水向东流
 **/
namespace Admin\Model;

class FlowStepFieldsModel extends BaseModel {
    protected $pk        = 'id';
    protected $fields    = array('id', 'sid', 'tid', 'fid', 'fields', 'isshow', 'status');
    protected $_auto     = array();
    protected $_validate = array(
        array('fieldid', 'require', '字段ID不能为空'),
    );
    protected $_checkbox = array('status', 'isshow');
}
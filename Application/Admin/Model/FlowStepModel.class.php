<?php
namespace Admin\Model;

class FlowStepModel extends BaseModel {
    protected $pk        = 'sid';
    protected $fields    = array('sid', 'fid', 'title', 'next_step', 'users', 'groups', 'depts', 'types', 'sql');
    protected $_auto     = array();
    protected $_validate = array(
        array('title', 'require', '步骤名称不能为空'),
        array('fid', 'require', '业务流程不能为空'),
    );
    protected $_checkbox = array();
}
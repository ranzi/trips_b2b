<?php
namespace Admin\Model;

class FlowWaitModel extends BaseModel {
    protected $pk        = 'id';
    protected $fields    = array('id', 'sid', 'fid', 'title', 'status', 'uname', 'uid', 'addtime', 'readtime', 'endtime');
    protected $_auto     = array();
    protected $_validate = array();
    protected $_checkbox = array();
}
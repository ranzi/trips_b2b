<?php
namespace Admin\Model;

class GradeModel extends BaseModel {
    protected $pk = 'id';

    protected $fields    = array('id', 'lid', 'title', 'contents', 'ordid', 'status');
    protected $_auto     = array();
    protected $_validate = array(
        array('title', 'require', '请填写价格等级'),
    );
    protected $_checkbox = array('status');
}
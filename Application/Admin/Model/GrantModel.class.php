<?php
namespace Admin\Model;
use Think\Model;

class GrantModel extends Model {
    protected $pk        = 'id';
    protected $fields    = array('id', 'auth_type', 'auth_id', 'auth', 'status');
    protected $_auto     = array();
    protected $_validate = array(
        array('auth_type', 'require', '授权类型不能为空'),
        array('auth_id', 'require', '授权对象不能为空'),
        array('auth[]', 'require', '授权内容不能为空'),
    );

    public function _add() {
        if (!($data = $this->create($_POST))) {
            return array('status' => 0, 'msg' => $this->getError());
        }
        $data['auth'] = implode(',', $data['auth']);
        if ($this->add($data)) {
            return array('status' => 1, 'msg' => '授权成功');
        } else {
            return array('status' => 0, 'msg' => '授权失败');
        }
    }

    public function _save() {
        if (!($data = $this->create($_POST))) {
            return array('status' => 0, 'msg' => $this->getError());
        }
        $data['auth'] = implode(',', $data['auth']);
        if (FALSE !== $this->where(array('id' => I('post.id', 0, 'intval')))->save($data)) {
            return array('status' => 1, 'msg' => '授权成功');
        } else {
            return array('status' => 0, 'msg' => '授权失败');
        }
    }
}
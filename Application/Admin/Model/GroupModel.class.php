<?php
/**
 *@author Chunlei Wang <sihangshi2011@gmail.com>
 *
 **/
namespace Admin\Model;
use Think\Model;

class GroupModel extends Model {
    protected $pk        = 'id';
    protected $fields    = array('id', 'title', 'status', 'auth');
    protected $_auto     = array();
    protected $_validate = array(
        array('title', 'require', '用户组名称不能为空'),
    );

    public function _add() {
        if (!($data = $this->create())) {
            return array('status' => 0, 'msg' => $this->getError());
        }
        $this->status = $this->status == 1 ? 1 : 0;
        if ($this->add()) {
            return array('status' => 1, 'msg' => '添加用户组成功');
        }
        return array('status' => 0, 'msg' => '添加用户组失败');
    }

    public function _save() {
        if (!($data = $this->create())) {
            return array('status' => 0, 'msg' => $this->getError());
        }
        $this->status = $this->status == 1 ? 1 : 0;
        if ($this->where(array('id' => I('post.id', 0, 'intval')))->save() !== FALSE) {
            return array('status' => 1, 'msg' => '编辑用户组成功');
        }
        return array('status' => 0, 'msg' => '编辑用户组失败');
    }

    public function toSelect() {
        return $this->where(array('status' => 1))->getField('id, title');
    }
}
<?php
namespace Admin\Model;

class ImagesModel extends BaseModel {
    protected $pk = 'id';

    protected $fields = array('id', 'type_id', 'title', 'addtime', 'admin_id', 'status', 'dist', 'cate_id');
    protected $_auto  = array(
        array('addtime', NOW_TIME, self::MODEL_INSERT, 'string'),
        array('admin_id', 'curAdmin', self::MODEL_INSERT, 'callback'),
    );
    protected $_validate = array(
        array('title', 'require', '请填图集标题'),
        array('type_id', 'require', '请选择分类'),
    );
    protected $_checkbox = array('status');

    public function curAdmin() {
        return session('admin_id');
    }
}
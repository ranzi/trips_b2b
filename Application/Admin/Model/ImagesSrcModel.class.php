<?php
namespace Admin\Model;

class ImagesSrcModel extends BaseModel {
    protected $pk = 'id';

    protected $fields = array('id', 'pid', 'title', 'src', 'thumb', 'status', 'addtime', 'admin_id');
    protected $_auto  = array(
    );
    protected $_validate = array(
    );
    protected $_checkbox = array('status');
}
<?php
namespace Admin\Model;
use Think\Model;

class InsuranceModel extends Model {
    protected $pk        = 'id';
    protected $fields    = array('id','title','intro','price','explain','timelimit','industry_id','partners_id','remark','ordid','status','addtime','deletebs');
    protected $_auto     = array(
        array('addtime', 'time', self::MODEL_BOTH, 'function'),
    );
    protected $_validate = array(
        array('title', 'require', '名称不能为空'),
    );

    public function getList($map, $field='*', $order=''){
        if(!$order){
            $order=array(
                'le.ordid' => 'DESC',
                'le.id'    => 'ASC'
            );
        }
        $psize  = I('request.psize', 0, 'intval') ?: C('DEFAULT_PAGE_SIZE', NULL);//实际按多少条记录分页
        $total  = $this->alias('le')->where($map)->count();
        $pager  = new \Think\Page($total, $psize);
        if ($total > 0) {
            $list = $this ->alias('le')
                    ->join('__PARTNERS__ p ON le.partners_id = p.id', 'LEFT')
                    ->where($map) -> field($field) -> order($order) -> limit($pager->firstRow . ',' . $pager -> listRows) -> select();
        }
        if($list){
            foreach($list as $key => $val){
                $list[$key]['price']=$val['price']+0;
            }

        }
        $obj = array(
            'list'  => $list,
            'total' => $total,
            'page'  => $pager->show(),
        );
        
        return $obj;
    }

    public function _add($data){
        if ($this->create($data)) {
            $id = $this->add();
            return array('status' => 1, 'msg' => '添加成功', 'id' => $id);
        } else {
            return array('status' => 0, 'msg' => '添加失败');//$this->getError()
        }
    }

    public function _save($data){

        if ($this->create($data)) {
            $this->save();
            return array('status' => 1, 'msg' => '编辑成功');
        } else {
            return array('status' => 0, 'msg' => '编辑失败');//$this->getError()
        }
    }
}
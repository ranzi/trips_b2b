<?php
namespace Admin\Model;

class JobModel extends BaseModel {
    protected $pk        = 'id';
    protected $fields    = array('id', 'title', 'ordid', 'status', 'belong');
    protected $_auto     = array();
    protected $_validate = array(
        array('title', 'require', '职务名称不能为空'),
    );
    protected $_checkbox = array('status');
}
<?php 
namespace Admin\Model;

class LeaveModel extends BaseModel {
	protected $pk = "id";
	protected $fields=array('fid','step','steps','uid','id','username','starttime','endtime','total','reason');
	protected $_auto=array();
	protected $_validate=array(
		array('reason', 'require', '请假原因不能为空'));
	protected $_checkbox=array();
}
<?php
namespace Admin\Model;

class LineAttrModel extends BaseModel {
    protected $pk        = 'lid';
    protected $fields    = array('lid', 'type_id', 'item_id', 'attr');
    protected $_auto     = array();
    protected $_validate = array();
}
<?php
namespace Admin\Model;

class LineExpandModel extends BaseModel {
    protected $pk        = 'lid';
    protected $fields    = array('lid', 'contents', 'imgs', 'feature', 'feeinclude', 'feeexclude', 'notice', 'tips', 'children', 'shopping', 'optional', 'gift', 'remark', 'together', 'place', 'local_man', 'open_man', 'open_mobile', 'dateline');
    protected $_auto     = array();
    protected $_validate = array();
}
<?php
namespace Admin\Model;

class LineModel extends BaseModel {
    protected $pk        = 'id';
    protected $fields    = array('id', 'title', 'subtitle', 'ordid', 'status', 'isnew', 'ishot', 'ispromotion', 'iscoupon', 'begintime', 'endtime', 'img', 'costprice', 'sellprice', 'marketprice', 'type_id', 'cate_id', 'badminid', 'bdeptid', 'hits', 'cmts', 'buycounts', 'tags', 'allowpay', 'departure', 'arrive', 'days', 'attachment');
    protected $_auto     = array();
    protected $_validate = array(
        array('title', 'require', '线路名称不能为空'),
    );
    protected $_checkbox = array('status', 'allowpay');
}
<?php
namespace Admin\Model;

class LinePriceModel extends BaseModel {
    protected $pk        = 'lid';
    protected $fields    = array('lid', 'days', 'booking', 'booking_child', 'dijie', 'dijie_child', 'stock');
    protected $_auto     = array();
    protected $_validate = array();
}
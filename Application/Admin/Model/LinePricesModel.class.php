<?php
namespace Admin\Model;

class LinePriceModel extends BaseModel {
    protected $pk        = 'lineid';
    protected $fields    = array('lineid', 'gradeid', 'starttime', 'endtime', 'adult_price', 'child_price', 'baby_price', 'adult_profit', 'child_profit', 'baby_profit', 'singleroom_price');
    protected $_auto     = array();
    protected $_validate = array();
}
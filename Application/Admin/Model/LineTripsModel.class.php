<?php
namespace Admin\Model;

class LineTripsModel extends BaseModel {
    protected $pk        = 'lid';
    protected $fields    = array('lid', 'days', 'title', 'breakfast', 'lunch', 'dinner', 'contents', 'hotel', 'hotel_img', 'trips_img', 'eat_img');
    protected $_auto     = array();
    protected $_validate = array();
}
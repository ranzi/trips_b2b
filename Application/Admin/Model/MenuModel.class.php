<?php
/**
 *@author Chunlei Wang <sihangshi2011@gmail.com>
 *
 **/
namespace Admin\Model;
use \Think\Model;

class MenuModel extends Model {
    protected $pk        = 'id';
    protected $fields    = array('id', 'title', 'pid', 'ordid', 'status', 'is_dev', 'url', 'icon');
    protected $_auto     = array();
    protected $_validate = array(
        array('title', 'require', '菜单名称不能为空'),
    );

    public function getAll() {
        return $this->order('ordid DESC, id DESC')->select();
    }

    public function getEffectAll() {
        return $this->where(array('status' => 1))->order('ordid DESC, id DESC')->select();
    }

    public function toSelect() {
        return $this->where(array('status' => 1))->order('ordid DESC, id DESC')->getField('id, title');
    }

    public function toTree() {
        $list = $this->where(array('status' => 1))->order('ordid DESC, id DESC')->select();
        return toTree($list);
    }

    public function _add() {
        if (!($data = $this->create($_POST))) {
            return array('status' => 0, 'msg' => $this->getError());
        }
        $data['pid']    = intval($data['pid']);
        $data['status'] = $data['status'] == 1 ? 1 : 0;
        $dta['is_dev']  = $data['is_dev'] == 1 ? 1 : 0;
        if ($this->add($data)) {
            return array('status' => 1, 'msg' => '添加菜单成功');
        }
        return array('status' => 0, 'msg' => '添加菜单失败');
    }

    public function _save() {
        if (!($data = $this->create($_POST))) {
            return array('status' => 0, 'msg' => $this->getError());
        }
        $data['pid']    = intval($data['pid']);
        $data['status'] = $data['status'] == 1 ? 1 : 0;
        $dta['is_dev']  = $data['is_dev'] == 1 ? 1 : 0;

        $id = intval(I('post.id', 0, 'intval'));
        if ($this->where(array('id' => $id))->save($data)) {
            return array('status' => 1, 'msg' => '编辑菜单成功');
        }
        return array('status' => 0, 'msg' => '编辑菜单失败');
    }
}
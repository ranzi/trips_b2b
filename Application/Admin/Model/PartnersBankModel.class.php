<?php
/**
 * 结算账号(合作单位)
 */
namespace Admin\Model;
use Think\Model;

class PartnersBankModel extends Model {
    protected $pk        = 'id';
    protected $fields    = array('id','partners_id','title','bankaccount','accountname','remark','ordid','status','addtime','deletebs');
    protected $_auto     = array(
        array('addtime', 'time', self::MODEL_BOTH, 'function'),
    );
    protected $_validate = array(
        array('title', 'require', '名称不能为空'),
    );

    public function getbanklist($map, $field = '*', $order = ''){
        if(!$order){
            $order=array(
                    'ordid' => 'DESC',
                    'id'    => 'ASC'
                );
        }
        $list = $this->field($field)->where($map)->order($order)->select();
        return $list;
    }
    public function _add($data){
        if ($this -> create($data)) {
            $this -> add();
        }
    }
    public function _save($data, $map = ''){
        if ($this -> where($map)->create($data)) {
            $flag = $this->where($map)->save();
            return $flag;
        }
    }
}
<?php
namespace Admin\Model;
use Think\Model;

class PartnersContactModel extends Model {
    protected $pk        = 'id';
    protected $fields    = array('id','partners_id','realname','mobile','mobile2','tel','fax','qq','weixin','email','remark','ordid','status','addtime','deletebs');
    protected $_auto     = array(
        array('addtime', 'time', self::MODEL_BOTH, 'function'),
    );
    protected $_validate = array(
        array('realname', 'require', '姓名不能为空'),
    );

    public function getlist($map, $field = '*', $order = ''){
        if(!$order){
            $order=array(
                    'ordid' => 'DESC',
                    'id'    => 'ASC'
                );
        }
        $list = $this->field($field)->where($map)->order($order)->select();
        return $list;
    }
    public function _add($data){
        if ($this -> create($data)) {
            $this -> add();
        }
    }
    public function _save($data, $map = ''){
        if ($this -> where($map)->create($data)) {
            $flag = $this->where($map)->save();
            return $flag;
        }
    }
}
<?php
namespace Admin\Model;
use Think\Model;

class PartnersModel extends Model {
    protected $pk        = 'id';
    protected $fields    = array('id', 'title', 'type','type_id','district_id','tel','settlement_id','creditquota','seatassign','remark','ordid','admin_id','status','addtime','deletebs');
    protected $_auto     = array(
        array('addtime', 'time', self::MODEL_BOTH, 'function'),
    );
    protected $_validate = array(
        array('title','require','单位名称不能为空!'), 
    );

    public function getList($map, $field='*', $order='', $types_data){
        $psize  = I('request.psize', 0, 'intval') ?: C('DEFAULT_PAGE_SIZE', NULL);//实际按多少条记录分页
        $total  = $this->where($map)->count();
        $pager  = new \Think\Page($total, $psize);
        if ($total > 0) {
            $list = $this -> where($map) -> field($field) -> order($order) -> limit($pager->firstRow . ',' . $pager -> listRows) -> select();
        }
        if($list){
            foreach($list as $key => $val){
                $list[$key]['t_type_id'] = $val['type_id']?$types_data['industry'][$val['type_id']]['title']:'';
                $list[$key]['t_settlement_id'] = $val['settlement_id']?$types_data['settlement'][$val['settlement_id']]['title']:'';
                $list[$key]['creditquota'] = $val['creditquota']+0;
            }
        }
        $obj = array(
            'list'  => $list,
            'total' => $total,
            'page'  => $pager->show(),
        );
        
        return $obj;
    }

    public function _add() {
        $data = array(
            'title'          => I('post.title', '', 'trim'),
            'type'           => 1,//资源型合作单位
            'type_id'        => I('post.type_id', 0, 'intval'),
            'district_id'    => I('post.district_id', 0, 'intval'),
            'tel'            => I('post.tel', '', 'trim'),
            'settlement_id'  => I('post.settlement_id', 0, 'intval'),
            'creditquota'    => I('post.creditquota', 0, 'floatval'),
            'ordid'          => I('post.ordid', 0, 'intval'),
            'status'         => I('post.status')==1?:0,
            'admin_id'       => session('admin_id'),
            'remark'         => I('post.remark', '', 'trim'),
        );
        if ($this->titleExists(array('title' => $data['title'],'type_id' => $data['type_id']))) {
            return array('status' => 0, 'msg' => '单位名称已存在');
        }

        if ($this->create($data)) {
            $id = $this->add();
            return array('status' => 1, 'msg' => '添加成功','id' => $id);
        } else {
            return array('status' => 0, 'msg' => '添加失败');//$this->getError()
        }
    }

    public function _save() {
        if (!I('post.id')) {
            return array('status' => 0, 'msg' => '传参有误!');
        }
        $data = array(
            'id'             => I('post.id', 0, 'intval'),
            'title'          => I('post.title', '', 'trim'),
            'type_id'        => I('post.type_id', 0, 'intval'),
            'district_id'    => I('post.district_id', 0, 'intval'),
            'tel'            => I('post.tel', '', 'trim'),
            'settlement_id'  => I('post.settlement_id', 0, 'intval'),
            'creditquota'    => I('post.creditquota', 0, 'floatval'),
            'ordid'          => I('post.ordid', 0, 'intval'),
            'status'         => I('post.status')==1?:0,
            'admin_id'       => session('admin_id'),
            'remark'         => I('post.remark', '', 'trim'),
        );
        if ($this->titleExists(array('title' => $data['title'],'type_id' => $data['type_id'],'id' => array('NEQ',$data['id'])))) {
            return array('status' => 0, 'msg' => '单位名称已存在');
        }
        if ($this->create($data)) {
            $flag = $this->save();
            return array('status' => 1, 'msg' => '编辑成功');
        } else {
            return array('status' => 0, 'msg' => '编辑失败');//$this->getError()
        }
    }

    /**
     * 获取合作单位ids
     * $title string
     * return string
     */
    public function get_partnerids_liketitle($title){
        if(!$title){
            return;
        }
        $map['title']    = array("like",'%'.$title.'%');
        $map['deletebs'] = 0;

        $list = $this->field('id')->where($map)->select();
        $ids=array();
        if($list){
            foreach($list as $key => $val){
                $ids[$val['id']]=$val['id'];
            }
            if($ids){
                return implode(',',$ids);
            }
        }
        return;
    }
    /**
     * 获取合作单位
     * $bs==1 返回数组 单位ids 与 id=>title
     */
    public function get_partners($map,$bs=0){
        if(!$map){
            return;
        }
        $arr = array();
        $list = $this->field('id,title')->where($map)->order('ordid desc,id')->select();
        if($bs == 1 && $list){
            foreach($list as $val){
                $arr['ids'][] = $val['id'];
                $arr['idtxt'][$val['id']] = $val['title'];
            }
            $arr['list'] = $list;
            return $arr;
        }
        return $list;
    }

    private function titleExists($map) {
        $map['deletebs'] = 0;
        $info = $this->where($map)->count();
        if ($info > 0) {
            return true;
        }
        return false;
    }
}
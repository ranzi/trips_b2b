<?php
namespace Admin\Model;

class PositionModel extends BaseModel {
    protected $pk        = 'id';
    protected $fields    = array('id', 'title', 'ordid', 'status', 'belong', 'pos_num');
    protected $_auto     = array();
    protected $_validate = array(
        array('title', 'require', '职位名称不能为空'),
        array('belong', 'require', '所属职务不能为空'),
    );
    protected $_checkbox = array('status');
}
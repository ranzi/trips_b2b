<?php
namespace Admin\Model;

class PrivelegeModel extends BaseModel {
    protected $pk        = 'id';
    protected $fields    = array('id', 'title', 'model', 'ctrl', 'action', 'op', 'extra', 'menu_id', 'sql', 'status', 'type');
    protected $_auto     = array();
    protected $_validate = array(
        array('title', 'require', '权限名称不能为空'),
        array('model', 'require', '模块不能为空'),
        array('ctrl', 'require', '模型不能为空'),
        array('action', 'require', '操作不能为空'),
        array('type', 'require', '权限分组不能为空'),
    );
    protected $_checkbox = array('status');

    public function getAll() {
        return $this->where(array('status' => 1))->select();
    }
}
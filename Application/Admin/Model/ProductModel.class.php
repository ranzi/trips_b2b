<?php
namespace Admin\Model;

class ProductModel extends BaseModel {
    protected $pk        = 'id';
    protected $fields    = array('id', 'title', 'subtitle', 'buid', 'bdid', 'content', 'status', 'sell_price', 'market_price', 'cost_price', 'type_id', 'cate_id', 'ordid', 'addtime', 'scores', 'is_coupon', 'begintime', 'endtime', 'buy_count', 'cmt_count', 'allow_pay', 'tags', 'hits', 'img', 'imgs');
    protected $_auto     = array();
    protected $_validate = array(
        array('title', 'required', '产品标题不能为空'),
        array('subtitle', 'required', '产品短标题不能为空'),
        array('content', 'required', '产品内容不能为空'),
        array('sell_price', 'required', '产品售价不能为空'),
        array('type_id', 'required', '请选择产品类型'),
        array('cate_id', 'required', '请选择产品分类'),
        array('img', 'required', '请上传产品图片'),
    );
}
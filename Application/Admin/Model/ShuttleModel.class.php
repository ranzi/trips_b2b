<?php
namespace Admin\Model;
use Think\Model;

class ShuttleModel extends Model {
    protected $pk        = 'id';
    protected $fields    = array('id','start_district','end_district','message','price','remark','ordid','status','admin_id','addtime','deletebs');
    protected $_auto     = array();
    protected $_validate = array(
        //array('start_district', 'require', '单位名称不能为空'),
    );

    public function getList($map, $field='*', $order=''){
        $psize  = I('request.psize', 0, 'intval') ?: C('DEFAULT_PAGE_SIZE', NULL);//实际按多少条记录分页
        $total  = $this->where($map)->count();
        $pager  = new \Think\Page($total, $psize);
        if ($total > 0) {
            $list = $this -> where($map) -> field($field) -> order($order) -> limit($pager->firstRow . ',' . $pager -> listRows) -> select();
        }
        if($list){
            foreach($list as $key => $val){
                $list[$key]['departuretime']=date("Y")==date("Y",$val['departuretime'])?date("m-d H:i",$val['departuretime']):date("Y-m-d H:i",$val['departuretime']);
                $list[$key]['arrivaltime']=date("Y")==date("Y",$val['arrivaltime'])?date("m-d H:i",$val['arrivaltime']):date("Y-m-d H:i",$val['arrivaltime']);
            }

        }
        $obj = array(
            'list'  => $list,
            'total' => $total,
            'page'  => $pager->show(),
        );
        
        return $obj;
    }

    public function _add($data){
        if ($this->create($data)) {
            $id = $this->add();
            return array('status' => 1, 'msg' => '添加成功', 'id' => $id);
        } else {
            //return $this->getError();
            return array('status' => 0, 'msg' => '添加失败');
        }
    }

    public function _save($data){
        if ($this->create($data)) {
            $this->where(array($this->getPk() => I('post.' . $this->getPk(), '', 'trim')))->save();
            return array('status' => 1, 'msg' => '更新成功');
        } else {
            return array('status' => 0, 'msg' => $this->getError());
        }
    }
}
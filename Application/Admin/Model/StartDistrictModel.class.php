<?php
namespace Admin\Model;
use Think\Model;

class StartDistrictModel extends Model {
    protected $pk        = 'id';
    protected $fields    = array('id','start_districtid','start_district','message','adult_price','child_price','remark','ordid','status','admin_id','addtime','deletebs');
    protected $_auto     = array(
        array('addtime', 'time', self::MODEL_BOTH, 'function'),
    );
    protected $_validate = array();

    public function getList($map, $field='*', $order=''){
        $psize  = I('request.psize', 0, 'intval') ?: C('DEFAULT_PAGE_SIZE', NULL);//实际按多少条记录分页
        $total  = $this->where($map)->count();
        $pager  = new \Think\Page($total, $psize);
        if ($total > 0) {
            $list = $this ->alias('le')->join('__DISTRICT__ d ON le.start_districtid=d.id', 'LEFT') -> where($map) -> field($field) -> order($order) -> limit($pager->firstRow . ',' . $pager -> listRows) -> select();
        }
        if($list){
            foreach($list as $key => $val){
                $list[$key]['adult_price'] = $val['adult_price']+0;
                $list[$key]['child_price'] = $val['child_price']+0;
            }
        }
        $obj = array(
            'list'  => $list,
            'total' => $total,
            'page'  => $pager->show(),
        );
        
        return $obj;
    }

    public function _add($data){
        if ($this->create($data)) {
            $id = $this->add();
            return array('status' => 1, 'msg' => '添加成功', 'id' => $id);
        } else {
            return array('status' => 0, 'msg' => '添加失败');//$this->getError()
        }
    }

    public function _save($data){
        if ($this->create($data)) {
            $this->save();
            return array('status' => 1, 'msg' => '编辑成功');
        } else {
            return array('status' => 0, 'msg' => '编辑失败');//$this->getError()
        }
    }

    public function getListAll($map, $field='*', $order=''){
        $list = $this ->alias('le')->join('__DISTRICT__ d ON le.start_districtid=d.id', 'LEFT') -> where($map) -> field($field) -> order($order) -> select();
        return $list;
    }
}
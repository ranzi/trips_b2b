<?php
namespace Admin\Model;

class SyscfgModel extends BaseModel {
    protected $pk        = 'varname';
    protected $fields    = array('varname', 'value', 'description', 'status');
    protected $_auto     = array();
    protected $_validate = array(
        array('varname', '', '选项已存在', 0, 'unique', self::MODEL_BOTH),
        array('varname', 'require', '选项不能为空'),
        array('value', 'require', '值不能为空'),
    );

    protected $_checkbox = array('');

    public function getAll() {
        return $this->where(array('status' => 1))->select();
    }
}
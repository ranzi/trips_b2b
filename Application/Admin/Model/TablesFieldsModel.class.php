<?php
namespace Admin\Model;

class TablesFieldsModel extends BaseModel {
    protected $pk        = 'tid,fields';
    protected $fields    = array('tid', 'types', 'title', 'status', 'fields', 'require', 'default_value', 'pk', 'len', 'showtypes', 'ordid');
    protected $_auto     = array();
    protected $_validate = array(
        array('title', 'require', '字段描述不能为空'),
        array('fields', 'require', '字段名不能为空'),
        array('types', 'require', '字段类型不能为空'),
    );
    protected $_checkbox = array('status', 'require');
}
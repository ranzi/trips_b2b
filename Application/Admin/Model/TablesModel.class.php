<?php
/**
 *@author qiisyhx<sihangshi2011@gmail.com>
 *@version $Id 2015-09-18
 *花褪红残青杏小
 *燕子飞时、绿水人家绕
 *枝上柳绵吹多少
 *天涯何处无芳草
 *墙里秋千墙外道
 *墙外行人、墙里佳人笑
 *笑渐不闻声渐悄
 *多情却被无情恼
 */
namespace Admin\Model;

class TablesModel extends BaseModel {
    protected $pk        = 'tid';
    protected $fields    = array('tid', 'model', 'title', 'status');
    protected $_auto     = array();
    protected $_validate = array(
        array('title', 'require', '表描述不能为空'),
        array('model', 'require', '表名不能为空'),
    );
    protected $_checkbox = array('status');
}
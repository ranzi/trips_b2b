<?php
namespace Admin\Model;
use Think\Model;

class TrafficModel extends Model {
    protected $pk        = 'id';
    protected $fields    = array('id','roundtrip','trafficbs','start_district','arrive_district','classes','start_hour','start_minute','arrive_hour','arrive_minute','directbs','remark','ordid','status','admin_id','addtime','deletebs');
    protected $_auto     = array(
        array('addtime', 'time', self::MODEL_BOTH, 'function'),
    );
    protected $_validate = array();

    public function getList($map, $field='*', $order='',$zsys_data){
        $psize  = I('request.psize', 0, 'intval') ?: C('DEFAULT_PAGE_SIZE', NULL);//实际按多少条记录分页
        $total  = $this->where($map)->count();
        $pager  = new \Think\Page($total, $psize);
        if ($total > 0) {
            $list = $this -> where($map) -> field($field) -> order($order) -> limit($pager->firstRow . ',' . $pager -> listRows) -> select();
        }
        if($list){
            foreach($list as $key => $val){
                $list[$key]['t_trafficbs'] = $val['trafficbs']?$zsys_data['traffic'][$val['trafficbs']]['title']:'';
                $list[$key]['t_district'] = $val['start_district']?$val['start_district'] . '/' . $val['arrive_district']:'';
                $list[$key]['t_directbs'] = $val['directbs']?$zsys_data['directbs'][$val['directbs']]['title']:'';
                $list[$key]['t_time'] = $val['start_hour']?($val['start_hour'] . ':' . $val['start_minute']) . '/' . ($val['arrive_hour']?$val['arrive_hour'] . ':' . $val['arrive_minute']:''):'';
            }
        }
        $obj = array(
            'list'  => $list,
            'total' => $total,
            'page'  => $pager->show(),
        );
        
        return $obj;
    }

    public function _add($data){
        if ($this->create($data)) {
            $id = $this->add();
            return array('status' => 1, 'msg' => '添加成功', 'id' => $id);
        } else {
            return array('status' => 0, 'msg' => '添加失败');//$this->getError()
        }
    }

    public function _save($data){
        if ($this->create($data)) {
            $this->save();
            return array('status' => 1, 'msg' => '编辑成功');
        } else {
            return array('status' => 0, 'msg' => '编辑失败');//$this->getError()
        }
    }

    public function get_zsys_data_title(){
        $map['type']     = array('in',array(3,4));
        $list = M('zsys_data')->field('id,title')->where($map)->select();
        $arr = array();
        if($list){
            foreach($list as $key => $val){
                $arr[$val['id']] = $val['title'];
            }
        }
        return $arr;
    }
}
<?php
namespace Admin\Model;

class TripdateExpandModel extends BaseModel {
    protected $pk        = 'tripdateid';
    protected $fields    = array('tripdateid','title','departure','arrive','startdaytxt','enddaytxt','traffic_goid','traffic_backid','traffic_goshorttxt','traffic_backshorttxt','traffic_gotxt','traffic_backtxt','line_partner_title');
    protected $_auto     = array();
    protected $_validate = array();

    public function _add($data){
        if ($this->create($data)) {
            $this->add();
            return array('status' => 1, 'msg' => '添加成功', 'id' => $id);
        } else {
            return array('status' => 0, 'msg' => '添加失败');//$this->getError()
        }
    }
    public function _save($data,$map=array()){
        if($map){
            if ($this->where($map)->create($data)) {
                $this->where($map)->save();
                return array('status' => 1, 'msg' => '编辑成功');
            } else {
                return array('status' => 0, 'msg' => '编辑失败');//$this->getError()
            }
        }else{
            if ($this->create($data)) {
                $this->save();
                return array('status' => 1, 'msg' => '编辑成功');
            } else {
                return array('status' => 0, 'msg' => '编辑失败');//$this->getError()
            }
        }
    }
}
<?php
namespace Admin\Model;

class TripdateModel extends BaseModel {
    protected $pk        = 'id';
    protected $fields    = array('id','tripdatecode','tripdate','tickettime','warningtime','departure_id','arrive_id','bigtraffic_bs','adult_traffic_price','child_traffic_price','lineid','days','line_partner_id','line_cate_id','adult_price','adult_profitprice','adult_adjustprice','child_price','child_profitprice','child_adjustprice','baby_price','baby_profitprice','baby_adjustprice','seatnum','seatused','overplus','sharesetnum','assign_nums','assign_partners','startdistricts','min_price','max_price','adult','child','baby','ordid','status','remark','admin_id','addtime','deletebs');
    protected $_auto     = array();
    protected $_validate = array();

    public function getList($map, $field='*', $order=''){
        $psize  = I('request.psize', 0, 'intval') ?: C('DEFAULT_PAGE_SIZE', NULL);//实际按多少条记录分页
        $total  = $this->alias('l')->where($map)->count();
        $pager  = new \Think\Page($total, $psize);
        if ($total > 0) {
            $list = $this->alias('l')->field($field)
                    ->join('__TRIPDATE_EXPAND__ e ON l.id = e.tripdateid', 'LEFT')
                    ->where($map)->order($order)->limit($pager->firstRow . ',' . $pager -> listRows) -> select();
        }
        $tripdate_ids = $tripdatetopics = array();
        $weekarray = array("日","一","二","三","四","五","六");
        if($list){
            foreach($list as $key => $val){
                $tripdate_ids[] = $val['id'];
                $list[$key]['t_tripdate']   = date('Y-m-d', $val['tripdate']) .' '. $weekarray[date('w',$val['tripdate'])];
                if($val['status'] == 1){
                    $list[$key]['t_status'] = '正常';
                } elseif ($val['status'] == 0){
                    $list[$key]['t_status'] = '已暂停,不能预定';
                }
                
                //$list[$key]['departuretime']=date("Y")==date("Y",$val['departuretime'])?date("m-d H:i",$val['departuretime']):date("Y-m-d H:i",$val['departuretime']);
                //$list[$key]['arrivaltime']=date("Y")==date("Y",$val['arrivaltime'])?date("m-d H:i",$val['arrivaltime']):date("Y-m-d H:i",$val['arrivaltime']);
            }
            $field = 'tripdateid,district_title,linegrade_txt,adult_saleprice,child_saleprice,baby_saleprice,status';
            $map   = array();
            $map['tripdateid'] = array('in',implode(',', $tripdate_ids));
            $map['deletebs'] = 0;
            $tripdatetopics = M('TripdateTopics')->field($field)->where($map)->order('start_district_id')->select();
            foreach($list as $key => $val){
                foreach($tripdatetopics as $tval){
                    if($val['id'] == $tval['tripdateid']){
                        $list[$key]['topics'][] = $tval;
                    }
                }
            }
            unset($tripdatetopics);
        }
        $obj = array(
            'lists'  => $list,
            'total' => $total,
            'page'  => $pager->show(),
        );
        
        return $obj;
    }

    public function _add($data){
        if ($this->create($data)) {
            $id = $this->add();
            return array('status' => 1, 'msg' => '添加成功', 'id' => $id);
        } else {
            return array('status' => 0, 'msg' => '添加失败');//$this->getError()
        }
    }

    public function _save($data,$map=array()){
        if($map){
            if ($this->where($map)->create($data)) {
                $this->where($map)->save();
                return array('status' => 1, 'msg' => '编辑成功');
            } else {
                return array('status' => 0, 'msg' => '编辑失败');//$this->getError()
            }
        }else{
            if ($this->create($data)) {
                $this->save();
                return array('status' => 1, 'msg' => '编辑成功');
            } else {
                return array('status' => 0, 'msg' => '编辑失败');//$this->getError()
            }
        }
    }
}
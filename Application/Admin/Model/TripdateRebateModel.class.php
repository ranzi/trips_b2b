<?php
namespace Admin\Model;

class TripdateRebateModel extends BaseModel {
    protected $pk        = 'id';
    protected $fields    = array('id','tripdateid','typeid','price','starttime','endtime','adult_bs','child_bs','baby_bs','addtime','admin_id','timesbs','deletebs');
    protected $_auto     = array();
    protected $_validate = array();

    public function _add($data){
        if ($this->create($data)) {
            $this->add();
            return array('status' => 1, 'msg' => '添加成功', 'id' => $id);
        } else {
            return array('status' => 0, 'msg' => '添加失败');//$this->getError()
        }
    }

    public function _save($data,$map=array()){
        if($map){
            if ($this->where($map)->create($data)) {
                $this->where($map)->save();
                return array('status' => 1, 'msg' => '编辑成功');
            } else {
                return array('status' => 0, 'msg' => '编辑失败');//$this->getError()
            }
        }else{
            if ($this->create($data)) {
                $this->save();
                return array('status' => 1, 'msg' => '编辑成功');
            } else {
                return array('status' => 0, 'msg' => '编辑失败');//$this->getError()
            }
        }
    }

    public function gettopics($tripdateid){
        if(!$tripdateid){
            return;
        }
        $tmparr = $arr = array();
        $map['tripdateid'] = $tripdateid;
        $map['deletebs'] = 0;
        $tmparr = $this->where($map)->select();
        if($tmparr){
            foreach($tmparr as $v){
                if($v['timesbs'] == 0){
                    $v['adult_checked'] = $v['adult_bs'] == 1 ?'checked="checked"':'';
                    $v['child_checked'] = $v['child_bs'] == 1 ?'checked="checked"':'';
                    $v['baby_checked'] = $v['baby_bs'] == 1 ?'checked="checked"':'';
                    $arr['main'][$v['typeid']] = $v;//每个主返利只会有一条记录
                }
                if($v['timesbs'] == 1){ //按日期段
                    $arr['timesarr'][$v['typeid']]['nums'] += 1;
                }
            }
        }
        return $arr;
    }
}   
<?php
/**
 * 座位数分配(团期管理)
 */
namespace Admin\Model;

class TripdateSeatModel extends BaseModel {
    protected $pk        = 'id';
    protected $fields    = array('id','tripdateid','partners_id','seatnum','seatused','viewshareseat','deletebs');
    protected $_auto     = array();
    protected $_validate = array();

    public function _add($data){
        if ($this->create($data)) {
            $this->add();
            return array('status' => 1, 'msg' => '添加成功', 'id' => $id);
        } else {
            return array('status' => 0, 'msg' => '添加失败');//$this->getError()
        }
    }

    public function _save($data,$map=array()){
        if($map){
            if ($this->where($map)->create($data)) {
                $this->where($map)->save();
                return array('status' => 1, 'msg' => '编辑成功');
            } else {
                return array('status' => 0, 'msg' => '编辑失败');//$this->getError()
            }
        }else{
            if ($this->create($data)) {
                $this->save();
                return array('status' => 1, 'msg' => '编辑成功');
            } else {
                return array('status' => 0, 'msg' => '编辑失败');//$this->getError()
            }
        }
    }

    public function gettopics($tripdateid){
        if(!$tripdateid){
            return;
        }
        $arr = $ids = array();
        $map['tripdateid'] = $tripdateid;
        $map['deletebs'] = 0;
        $arr = $this->where($map)->select();
        if($arr){
            foreach($arr as $key => $val){
                $ids[]=$val['partners_id'];
            }
        }
        return array('data' => $arr, 'ids' => $ids);
    }
    /**
     * 获取某团期某合作单位座位数更新是否合理
     * $tripdateid    团期id
     * $partners_id   合作单位 id
     * $seatnum       新分配的座位数
     * 如果分配了座数的合作单位已使用了位了，则修改后的座位数，不能小于已用的座位数，也不能被取消
     */
    public function checkPartnersSeat($tripdateid, $partners_id, $seatnum){
        if(!$tripdateid || !$partners_id){
            return array('status' => 0, 'msg' => '传入数据不完整');
        }
        $map['tripdateid']  = $tripdateid;
        $map['partners_id'] = $partners_id;
        $map['deletebs']    = 0;
        $seats = $this->where($map)->find();
        if(!$seats){
            return array('status' => 0, 'msg' => '数据不存在');
        }
        if($seats['seatused'] >0 && $seats['seatused'] > intval($seatnum)){
            return array('status' => 0, 'msg' => '合作单位已使用的座位数 '. $seats['seatused'] .' 个,不能小于新分配的座位数 '. $seatnum .' 个');
        }
        return array('status' => 1);
    }
}
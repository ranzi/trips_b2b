<?php
/**
 * 团期主题数据(接团地区/地接线路标准等级/地接线路价格)
 */
namespace Admin\Model;

class TripdateTopicsModel extends BaseModel {
    protected $pk        = 'id';
    protected $fields    = array('id','tripdateid','start_district_id','district_id','district_title','shuttle_adult_price','shuttle_child_price','lineprice_id','linegrade_id','linegrade_txt','adult_price','adult_profit','adult_saleprice','child_price','child_profit','child_saleprice','baby_price','baby_profit','baby_saleprice','singleroom_price','status','deletebs');
    protected $_auto     = array();
    protected $_validate = array();

    public function _add($data){
        if ($this->create($data)) {
            $this->add();
            return array('status' => 1, 'msg' => '添加成功', 'id' => $id);
        } else {
            return array('status' => 0, 'msg' => '添加失败');//$this->getError()
        }
    }

    public function _save($data,$map=array()){
        if($map){
            if ($this->where($map)->create($data)) {
                $this->where($map)->save();
                return array('status' => 1, 'msg' => '编辑成功');
            } else {
                return array('status' => 0, 'msg' => '编辑失败');//$this->getError()
            }
        }else{
            if ($this->create($data)) {
                $this->save();
                return array('status' => 1, 'msg' => '编辑成功');
            } else {
                return array('status' => 0, 'msg' => '编辑失败');//$this->getError()
            }
        }
    }

    public function gettopics($tripdateid){
        if(!$tripdateid){
            return;
        }
        $map['tripdateid'] = $tripdateid;
        $map['deletebs'] = 0;
        $arr = $this->where($map)->select();
        
        $arrids = array();
        if($arr){
            foreach($arr as $key => $val){
                if($val['start_district_id']){
                    $arrids['startdistrictids'][$val['start_district_id']] = $val['start_district_id'];
                }
                if($val['lineprice_id']){
                    $arrids['linepriceids'][$val['lineprice_id']] = $val['lineprice_id'];
                }
                if($val['linegrade_id']){
                    $arrids['linegradeids'][$val['linegrade_id']] = $val['linegrade_id'];
                }
                $linegradedata[$val['linegrade_id']] = $val;
            }
        }
        return array('data' => $arr,'arrids' => $arrids ,'linegradedata' => $linegradedata);
    }
}

    
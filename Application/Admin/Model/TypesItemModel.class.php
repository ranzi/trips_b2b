<?php
namespace Admin\Model;

class TypesItemModel extends BaseModel {
    protected $pk        = 'id';
    protected $fields    = array('id', 'title', 'ordid', 'attr', 'status', 'type_id');
    protected $_auto     = array();
    protected $_validate = array(
        array('title', 'require', '分类名称不能为空'),
        array('type_id', 'require', '请选择所属类型'),
    );

    public function parseAll($map, $enable = false) {
        if ($enable) {
            $map['status'] = 1;
        }
        $list = $this->where($map)->order('ordid desc')->select();
        foreach ($list as &$item) {
            $item['attr'] = unserialize($item['attr']);
        }
        return $list;
    }
}
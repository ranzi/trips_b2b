<?php
namespace Admin\Model;

class TypesModel extends BaseModel {
    protected $pk        = 'id';
    protected $fields    = array('id', 'title', 'ordid', 'status', 'type', 'industryid','deletebs');
    protected $_auto     = array();
    protected $_validate = array(
        array('title', 'require', '名称不能为空'),
    );

    public function getList($map, $field='*', $order='',$zsys_data=array()){
		$psize 	= I('request.psize', 0, 'intval') ?: C('DEFAULT_PAGE_SIZE', NULL);//实际按多少条记录分页
        $total 	= $this->where($map)->count();
        $pager 	= new \Think\Page($total, $psize);
        if ($total > 0) {
            $list = $this -> where($map) -> field($field) -> order($order) -> limit($pager->firstRow . ',' . $pager -> listRows) -> select();
        }
		if($list){
            foreach($list as $key => $val){
				$list[$key]['t_industryid'] = $val['industryid']?$zsys_data['industry'][$val['industryid']]['title']:'';
			}
		}
    	$obj = array(
			'list'	=> $list,
			'total'	=> $total,
			'page'	=> $pager->show(),
		);
		return $obj;
    }

    public function insert($data){
		if ($this->titleExists(array('title' => $data['title'],'type' => 'industry'))) {
            return array('status' => 0, 'msg' => '名称已存在');
        }
        if ($this->create($data)) {
			$id = $this->add();
			return array('status' => 1, 'msg' => '添加成功', 'id' => $id);
		} else {
			return array('status' => 0, 'msg' => '添加失败');//$this->getError()
		}
    }

    public function update($data){
    	if ($this->titleExists(array('title' => $data['title'],'type' => 'industry','id' => array('NEQ',$data['id'])))) {
            return array('status' => 0, 'msg' => '名称已存在');
        }
        if ($this->create($data)) {
            $this->save();
            return array('status' => 1, 'msg' => '编辑成功');
        } else {
            return array('status' => 0, 'msg' => '编辑失败');//$this->getError()
        }
    }

    /* 
     * multibs = 1 返回 所有字段值    0 只返回title
     */
    public function get_typesdata($map, $multibs = 0){
        $data  = array();
        $map['deletebs'] = 0;
        $list  = $this -> field('id,title,type,status') -> where($map) -> order('ordid desc,id') -> select();
        foreach ($list as $key => $val) {
            if($multibs == 1){
                $data[$val['type']][$val['id']] = $val;
            } else {
                $data[$val['type']][$val['id']] = $val['title'];
            } 
        }
        unset($list);
        return $data;
    }

    private function titleExists($map) {
        $map['deletebs'] = 0;
        $info = $this->where($map)->count();
        if ($info > 0) {
            return true;
        }
        return false;
    }
}
<?php
namespace Admin\Model;
use Think\Model;

class VisaModel extends Model {
    protected $pk        = 'id';
    protected $fields    = array('id','countryid','visatype_id','district_id','acceptancedays','entrynumber','interviewed','advanceday','bonded','validterm','periodofstay','intro','message','partners_id','price','remark','ordid','status','addtime','deletebs');
    protected $_auto     = array(
        array('addtime', 'time', self::MODEL_BOTH, 'function'),
    );
    protected $_validate = array();

    public function getList($map, $field='*', $order='', $types_data){
        $psize  = I('request.psize', 0, 'intval') ?: C('DEFAULT_PAGE_SIZE', NULL);//实际按多少条记录分页
        $total  = $this->alias('le')->where($map)->count();
        $pager  = new \Think\Page($total, $psize);
        if ($total > 0) {
            $list = $this ->alias('le')
                    ->join('__DISTRICT__ d ON le.countryid = d.id', 'LEFT')
                    ->join('__PARTNERS__ p ON le.partners_id = p.id', 'LEFT')
                    ->where($map) -> field($field) -> order($order) -> limit($pager->firstRow . ',' . $pager -> listRows) -> select();
        }
        
        if($list){
            foreach($list as $key => $val){
                $list[$key]['price']=$val['price']+0;
                $list[$key]['t_visatype_id'] = $val['visatype_id']?$types_data['visatype'][$val['visatype_id']]['title']:'';
            }
        }
        
        $obj = array(
            'list'  => $list,
            'total' => $total,
            'page'  => $pager->show(),
        );
        
        return $obj;
    }

    //签证结算价格行
    public function getpricelist($map){
        $list = M('Visa_price')->where($map)->order('starttime desc')->select();
        return $list;
    }

    //签证类型
    public function getvisatypelist($map, $field='*', $order=''){
        $order  = $order?:" ordid desc,id";
        $list = M('types') -> where($map) -> field($field) -> order($order) -> select();
        return $list;
    }

    public function _add($data){
        if ($this->create($data)) {
            $id = $this->add();
            return array('status' => 1, 'msg' => '添加成功', 'id' => $id);
        } else {
            return array('status' => 0, 'msg' => '添加失败');//$this->getError()
        }
    }

    public function _save($data){
        if ($this->create($data)) {
            $this->save();
            return array('status' => 1, 'msg' => '编辑成功');
        } else {
            return array('status' => 0, 'msg' => '编辑失败');//$this->getError()
        }
    }
}
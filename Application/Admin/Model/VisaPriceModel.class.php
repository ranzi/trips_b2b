<?php
/**
 * 签证价格
 */
namespace Admin\Model;
use Think\Model;

class VisaPriceModel extends Model {
    protected $pk        = 'id';
    protected $fields    = array('id','starttime','endtime','visa_id','cost_price','deletebs');
    protected $_auto     = array(
        array('addtime', 'time', self::MODEL_BOTH, 'function'),
    );
    protected $_validate = array();

    public function getpricelist($map){
        $list = $this->where($map)->order('id desc')->select();
        return $list;
    }
    public function _add($data){
        if ($this -> create($data)) {
            $id = $this->add();
            return array('status' => 1, 'msg' => '添加成功', 'id' => $id);
        } else {
            return array('status' => 0, 'msg' => '添加失败');//$this->getError()
        }
    }
    public function _save($data, $map = ''){
        if ($this -> where($map)->create($data)) {
            $flag = $this->where($map)->save();
            return $flag;
        }
    }
}
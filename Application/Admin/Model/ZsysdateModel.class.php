<?php
/**
 * 系统定制数据,开发者管理
 */
namespace Admin\Model;

class ZsysDataModel extends BaseModel {
    protected $pk      = 'id';
    protected $fields  = array('id', 'title', 'ordid', 'status', 'type');
    protected $_auto     = array();
    protected $_validate = array();
    /* 
     * multibs = 1 返回 所有字段值    0 只返回title
     */
    public function get_zsysdata($map, $multibs = 0){
        $data = array();
        $list = $this -> field('id,title,type,status') -> where($map) -> order('ordid desc,id') -> select();
        foreach ($list as $key => $val) {
            if($multibs == 1){
                $data[$val['type']][$val['id']] = $val;
            } else {
                $data[$val['type']][$val['id']] = $val['title'];
            } 
        }
        unset($list);
        return $data;
    }
}
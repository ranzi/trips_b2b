<?php
/**
 *desc
 *create a random string
 *@author Chunlei Wang <sihangshi2011@gmail.com>
 *@param Integer $length random string length
 *@param Boolean $numeric is a numeric string
 */
function random($length, $numeric = 0) {
    if ($numeric) {
        $hash = sprintf('%0' . $length . 'd', mt_rand(0, pow(10, $length) - 1));
    } else {
        $hash  = '';
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        $max   = strlen($chars) - 1;
        for ($i = 0; $i < $length; $i++) {
            $hash .= $chars[mt_rand(0, $max)];
        }
    }
    return $hash;
}

/**
 *desc
 *date format
 *@author Chunlei Wang <sihangshi2011@gmail.com>
 *@param String $str timestamp
 */
function bdate($str) {
    return date('Y-m-d H:i:s', $str);
}

/**
 * 将列表转换为树状
 * @param  array   $list  列表
 * @param  string  $pk    主键
 * @param  string  $pid   父级id字段
 * @param  string  $child 存放子级的key
 * @param  integer $root  根节点
 * @return array
 */
function toTree($list, $pk = 'id', $pid = 'pid', $child = 'child', $root = 0) {
    // 创建Tree
    $tree = array();
    if (is_array($list)) {
        // 创建基于主键的数组引用
        $refer = array();
        foreach ($list as $key => $data) {
            $refer[$data[$pk]] = &$list[$key];
        }
        foreach ($list as $key => $data) {
            // 判断是否存在parent
            $parentId = $data[$pid];
            if ($root == $parentId) {
                $tree[] = &$list[$key];
            } else {
                if (isset($refer[$parentId])) {
                    $parent           = &$refer[$parentId];
                    $parent[$child][] = &$list[$key];
                }
            }
        }
    }
    return $tree;
}
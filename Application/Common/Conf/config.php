<?php
return array(
	//'配置项'=>'配置值'
	'DEFAULT_MODULE'   => 'admin',
	'DB_TYPE'          => 'mysql', // 类型
	'DB_HOST'          => 'localhost', // 主机
	'DB_NAME'          => 'b2c', // 名称
	'DB_USER'          => 'root', // 用户名
	'DB_PWD'           => 'root', // 密码
	'DB_PREFIX'        => 'b2c_', // 表前缀
	'DB_CHARSET'       => 'utf8', // 编码
	'IMAGE_REMOTE_URL' => '',
);
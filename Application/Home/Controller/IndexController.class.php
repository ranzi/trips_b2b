<?php
namespace Home\Controller;
use Think\Controller;

class IndexController extends Controller {
    public function index() {
        $this->display();
    }

    public function editor() {
        $this->display();
    }

    public function select() {
        $this->assign('single', array('1' => 'first', '2' => 'second', '3' => 'third'));
        $this->assign('selected', '2');
        $this->assign('nest', array(array('id' => 1, 'name' => 'name1'), array('id' => 2, 'name' => 'name2'), array('id' => 3, 'name' => 'name3')));
        $this->assign('info', array('id' => 3));
        $this->display();
    }

    public function date() {
        $this->assign('startdate', date('Y-m-d'));
        $this->display('calendar');
    }

    public function image() {
        $this->display();
    }

    public function txtbox() {
        $str = '产品特色
★ 陕北    ——  一曲高亢率真的信天游，一段人文始祖-黄帝的传奇故事，一幕黄河上最壮观的黄色瀑布相信都会让您流连忘返！​
★ 延安    ——  聆听艰苦岁月中可歌可泣的英雄故事，传递生生不息的革命火种！
★ 兵马俑  ——  1987年被列入世界文化遗产，穿越千年的秦俑军阵，只为等待您的大驾亲临！
★ 华清池  ——  一首《长恨歌》续写了大唐多少兴衰往事，传唱千年的爱情故事就在此开始！
★ 大雁塔  ——  2014年被列入世界遗产名录，因玄奘法师取经而修建，现存最早、规模最大的唐代四方楼阁式砖塔！
★ 华山    ——  感受自古华山一条路，实地品读金庸小说中的-华山论剑，独站华山之巅！
★ 高家大院 ——  这是满是400年的动人故事，丰富多彩的陕西民间绝活：皮影戏、民间剪纸、秦腔。。。！';
        $this->assign('str', $str);
        $this->display();
    }

    public function fullcalendar() {
        $this->display();
    }

    public function dateprice() {
        $this->display('dateprice2');
    }

    public function daterange() {
        $this->display();
    }

    public function datetime() {
        $this->assign('time', 1);
        $this->display('daterange');
    }

    public function dialog() {
        $this->display();
    }

    public function chart() {
        $data = array(
            'tripcms' => array(20, 16, 15, 23, 19, 32, 20, 16, 15, 23, 19, 32),
            'b2c'     => array(24, 54, 23, 21, 6, 34, 40, 54, 23, 21, 6, 34),
        );
        $data2 = array(
            '销售1'  => 25,
            '销售2'  => 24,
            '销售3'  => 36,
            '销售4'  => 22,
            '销售5'  => 16,
            '销售6'  => 47,
            '销售7'  => 23,
            '销售8'  => 12,
            '销售9'  => 23,
            '销售10' => 31,
        );
        $this->assign('data', $data);
        $this->assign('data2', $data2);
        $this->display();
    }

    public function chosen() {
        $data = array('1' => 'a', '2' => 'b', '3' => 'c');
        $this->assign('data', $data);
        $this->assign('selected', 2);
        $this->assign('selected2', array(2, 3));
        $this->display();
    }

    public function seltree() {
        $arr = D('Admin/Department')->toTree();
        $this->assign('rows', $arr);
        $this->assign('sld', 6);
        $this->display();
    }

    public function ztree() {
        $this->display();
    }

    public function jqgrid() {
        $list = D('Privelege')->select();
        $this->assign('list', $list);
        $this->display();
    }

    public function district() {
        $this->display();
    }

    public function imgTree() {
        $this->display();
    }

    public function uploadify() {
        $this->display();
    }
}
<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo ($bar["curpos"]); ?> - <?php echo ($bar["menu"]); ?></title>
		<meta name="description" content="<?php echo ($cfgs["seodescription"]); ?>" />
		<meta name="keywords" content="<?php echo ($cfgs["seokeywords"]); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<script type="text/javascript" src="/Public/js/lib/require.js"></script>
		<script type="text/javascript" src="/Public/js/lib/config.js"></script>
		<link rel="stylesheet" href="/Public/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/Public/css/font-awesome.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-fonts.css" />
		<link rel="stylesheet" href="/Public/css/ace.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-skins.min.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="css/ace-ie.min.css" />
		  <script src="/Public/js/excanvas.min.js"></script>
		<![endif]-->
		<!-- inline styles related to this page -->
		<!-- ace settings handler -->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="/Public/js/html5shiv.js"></script>
		<script src="/Public/js/respond.min.js"></script>
		<![endif]-->
		<script type="text/javascript">
		if(navigator.appName == 'Microsoft Internet Explorer'){
			if(navigator.userAgent.indexOf("MSIE 5.0")>0 || 
				navigator.userAgent.indexOf("MSIE 6.0")>0 || 
				navigator.userAgent.indexOf("MSIE 7.0")>0) {
				alert('您使用的 IE 浏览器版本过低,无法获得最好的体验且可能出现无法预期的错误, 推荐使用 Chrome 浏览器或 IE8 及以上版本浏览器.');
			}
		}
		</script>
	</head>
	
	
<body>
	<div class="navbar navbar-default" id="navbar">
			<div class="navbar-container" id="navbar-container">
				<div class="navbar-header pull-left">
					<a href="<?php echo U('Index/index');?>" class="navbar-brand" style="padding:0px">
						<img src="<?php echo ($cfgs["logo"]); ?>" class="img-rounded img-thumbnail" style="height:45px;padding:0">
						<small>
							<?php echo ($cfgs["sitename"]); ?>后台管理系统
						</small>
					</a><!-- /.brand -->
				</div><!-- /.navbar-header -->

				<div class="navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="<?php echo ($_SESSION['admin']['avatar']); ?>" alt="<?php echo (session('admin_name')); ?>" />
								<span class="user-info">
									<small>欢迎使用<?php echo ($cfgs["sitename"]); ?>后台管理系统后台管理系统后台管理系统后台管理系统</small>
									<?php echo (session('admin_name')); ?>
								</span>
								<i class="icon-caret-down"></i>
							</a>
							<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="<?php echo U('Admin/profile');?>">
										<i class="icon-user"></i>
										个人资料
									</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="<?php echo U('Login/logout');?>">
										<i class="icon-off"></i>
										退出登录
									</a>
								</li>
							</ul>
						</li>
					</ul><!-- /.ace-nav -->
				</div><!-- /.navbar-header -->
			</div><!-- /.container -->
		</div>
		<div class="main-container" id="main-container">
			<div class="main-container-inner">
				<a class="menu-toggler" id="menu-toggler" href="#">
					<span class="menu-text"></span>
				</a>

				<div class="sidebar" id="sidebar">
					<script type="text/javascript">
					require(['jquery', 'acemy'], function($){
						try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
					});
					</script>
					<ul class="nav nav-list">
						<?php if(is_array($authMenus)): $i = 0; $__LIST__ = $authMenus;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu): $mod = ($i % 2 );++$i;?><li class="<?php echo ($menu["class"]); ?>">
							<a class="dropdown-toggle" href="<?php if(empty($menu["child"])): echo U($menu['url']); endif; ?>">
								<?php if(!empty($menu["icon"])): ?><i class="<?php echo ($menu["icon"]); ?>"></i><?php endif; ?>
								<span class="menu-text"><?php echo ($menu["title"]); ?></span>
							</a>
							<?php if(!empty($menu["child"])): ?><ul class="submenu">
							<?php if(is_array($menu["child"])): $i = 0; $__LIST__ = $menu["child"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li <?php if(!empty($vo["class"])): ?>class="<?php echo ($vo["class"]); ?>"<?php endif; ?>>
									<a href="<?php echo U($vo['url']);?>">
										<i class="icon-double-angle-right"></i>
										<?php echo ($vo["title"]); ?>
									</a>
								</li><?php endforeach; endif; else: echo "" ;endif; ?>
							</ul><?php endif; ?>
						</li><?php endforeach; endif; else: echo "" ;endif; ?>
					</ul><!-- /.nav-list -->

					<div class="sidebar-collapse" id="sidebar-collapse">
						<i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right" data-toggle="tooltip" data-placement="right" title="收起面板"></i>
					</div>

					<script type="text/javascript">
					require(['jquery', 'acemy'], function($){
						try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
					});
					</script>
				</div>
				<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
						require(['jquery', 'acemy'], function($){
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						});
						</script>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<?php echo U('Index/index');?>">首页</a>
							</li>
							<?php if(!empty($bar["menu"])): ?><li>
								<a href="<?php echo ($bar["url"]); ?>"><?php echo ($bar["menu"]); ?></a>
							</li><?php endif; ?>
							<li class="active"><?php echo ($bar["curpos"]); ?></li>
						</ul><!-- .breadcrumb -->
					</div>
<div class="page-content">
	<div class="page-header" style="position:relative;">
		<h1>
			编辑图集
			<small>
				<i class="icon-double-angle-right"></i>
				<a href="javascript:history.go(-1);" class="btn btn-link">返回列表</a>
			</small>
		</h1>
	</div>
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="table-responsive panel-body">
				<form class="form form-horizontal" method="post" action="">
					<div class="form-group">
						<label class="control-label  col-xs-12 col-md-2 ">图集名称</label>
						<div class="col-sm-9">
							<input class="form-control" type="text" name="title" value="<?php echo ($info["title"]); ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-12 col-md-2 ">图集类型</label>
						<div class="col-sm-9">
							<!--default select start--><select name="type_id" id="_select" onClick="" onChange="" class="form-control" ><option value="">==请选择==</option><option value="41"  selected="selected">景点</option><option value="42" >餐饮</option><option value="43" >酒店</option></select><!--default select end-->
						</div>
					</div>
					<div class="form-group">
						<label class="col-xs-12 col-sm-3 col-md-2 control-label">地区</label>
						<div class="col-xs-12 col-sm-9">
							<select class="chosen-select  " name="dist" id="" onchange=""><option value="">请选择</option><option value="1" disabled>国内</option><option value="3" disabled>|——北京</option><option value="51">|————北京</option><option value="4" disabled>|——天津</option><option value="52">|————天津</option><option value="5" disabled>|——河北</option><option value="53">|————石家庄</option><option value="54">|————唐山</option><option value="55">|————秦皇岛</option><option value="56">|————邯郸</option><option value="57">|————邢台</option><option value="58">|————保定</option><option value="59">|————张家口</option><option value="60">|————承德</option><option value="61">|————沧州</option><option value="62">|————廊坊</option><option value="63">|————衡水</option><option value="6" disabled>|——山西</option><option value="64">|————太原</option><option value="65">|————大同</option><option value="66">|————阳泉</option><option value="67">|————长治</option><option value="68">|————晋城</option><option value="69">|————朔州</option><option value="70">|————晋中</option><option value="71">|————运城</option><option value="72">|————忻州</option><option value="73">|————临汾</option><option value="74">|————吕梁</option><option value="7" disabled>|——内蒙古</option><option value="75">|————呼和浩特</option><option value="76">|————包头</option><option value="77">|————乌海</option><option value="78">|————赤峰</option><option value="79">|————通辽</option><option value="80">|————呼伦贝尔</option><option value="81">|————兴安</option><option value="82">|————锡林郭勒</option><option value="83">|————乌兰察布</option><option value="84">|————伊克昭</option><option value="85">|————巴彦淖尔</option><option value="86">|————阿拉善</option><option value="8" disabled>|——辽宁</option><option value="87">|————沈阳</option><option value="88">|————大连</option><option value="89">|————鞍山</option><option value="90">|————抚顺</option><option value="91">|————本溪</option><option value="92">|————丹东</option><option value="93">|————锦州</option><option value="94">|————营口</option><option value="95">|————阜新</option><option value="96">|————辽阳</option><option value="97">|————盘锦</option><option value="98">|————铁岭</option><option value="99">|————朝阳</option><option value="100">|————葫芦岛</option><option value="9" disabled>|——吉林</option><option value="101">|————长春</option><option value="102">|————吉林</option><option value="103">|————四平</option><option value="104">|————辽源</option><option value="105">|————通化</option><option value="106">|————白山</option><option value="107">|————松原</option><option value="108">|————白城</option><option value="109">|————延边</option><option value="10" disabled>|——黑龙江</option><option value="110">|————哈尔滨</option><option value="111">|————齐齐哈尔</option><option value="112">|————鸡西</option><option value="113">|————鹤岗</option><option value="114">|————双鸭山</option><option value="115">|————大庆</option><option value="116">|————伊春</option><option value="117">|————佳木斯</option><option value="118">|————七台河</option><option value="119">|————牡丹江</option><option value="120">|————黑河</option><option value="121">|————绥化</option><option value="122">|————大兴安岭</option><option value="11" disabled>|——上海</option><option value="123" selected="selected">|————上海</option><option value="12" disabled>|——江苏</option><option value="124">|————南京</option><option value="125">|————无锡</option><option value="126">|————徐州</option><option value="127">|————常州</option><option value="128">|————苏州</option><option value="129">|————南通</option><option value="130">|————连云港</option><option value="131">|————淮安</option><option value="132">|————盐城</option><option value="133">|————扬州</option><option value="134">|————镇江</option><option value="135">|————泰州</option><option value="136">|————宿迁</option><option value="13" disabled>|——浙江</option><option value="137">|————杭州</option><option value="138">|————宁波</option><option value="139">|————温州</option><option value="140">|————嘉兴</option><option value="141">|————湖州</option><option value="142">|————绍兴</option><option value="143">|————金华</option><option value="144">|————衢州</option><option value="145">|————舟山</option><option value="146">|————台州</option><option value="147">|————丽水</option><option value="14" disabled>|——安徽</option><option value="148">|————合肥</option><option value="149">|————芜湖</option><option value="150">|————蚌埠</option><option value="151">|————淮南</option><option value="152">|————马鞍山</option><option value="153">|————淮北</option><option value="154">|————铜陵</option><option value="155">|————安庆</option><option value="156">|————黄山</option><option value="157">|————滁州</option><option value="158">|————阜阳</option><option value="159">|————宿州</option><option value="160">|————巢湖</option><option value="161">|————六安</option><option value="162">|————亳州</option><option value="163">|————池州</option><option value="164">|————宣城</option><option value="15" disabled>|——福建</option><option value="165">|————福州</option><option value="166">|————厦门</option><option value="167">|————莆田</option><option value="168">|————三明</option><option value="169">|————泉州</option><option value="170">|————漳州</option><option value="171">|————南平</option><option value="172">|————龙岩</option><option value="173">|————宁德</option><option value="16" disabled>|——江西</option><option value="174">|————南昌</option><option value="175">|————景德镇</option><option value="176">|————萍乡</option><option value="177">|————九江</option><option value="178">|————新余</option><option value="179">|————鹰潭</option><option value="180">|————赣州</option><option value="181">|————吉安</option><option value="182">|————宜春</option><option value="183">|————抚州</option><option value="184">|————上饶</option><option value="17" disabled>|——山东</option><option value="185">|————济南</option><option value="186">|————青岛</option><option value="187">|————淄博</option><option value="188">|————枣庄</option><option value="189">|————东营</option><option value="190">|————烟台</option><option value="191">|————潍坊</option><option value="192">|————济宁</option><option value="193">|————泰安</option><option value="194">|————威海</option><option value="195">|————日照</option><option value="196">|————莱芜</option><option value="197">|————临沂</option><option value="198">|————德州</option><option value="199">|————聊城</option><option value="200">|————滨州</option><option value="201">|————菏泽</option><option value="18" disabled>|——河南</option><option value="202">|————郑州</option><option value="203">|————开封</option><option value="204">|————洛阳</option><option value="205">|————平顶山</option><option value="206">|————安阳</option><option value="207">|————鹤壁</option><option value="208">|————新乡</option><option value="209">|————焦作</option><option value="210">|————濮阳</option><option value="211">|————许昌</option><option value="212">|————漯河</option><option value="213">|————三门峡</option><option value="214">|————南阳</option><option value="215">|————商丘</option><option value="216">|————信阳</option><option value="217">|————周口</option><option value="218">|————驻马店</option><option value="19" disabled>|——湖北</option><option value="219">|————武汉</option><option value="220">|————黄石</option><option value="221">|————十堰</option><option value="222">|————宜昌</option><option value="223">|————襄樊</option><option value="224">|————鄂州</option><option value="225">|————荆门</option><option value="226">|————孝感</option><option value="227">|————荆州</option><option value="228">|————黄冈</option><option value="229">|————咸宁</option><option value="230">|————随州</option><option value="231">|————恩施</option><option value="20" disabled>|——湖南</option><option value="232">|————长沙</option><option value="233">|————株洲</option><option value="234">|————湘潭</option><option value="235">|————衡阳</option><option value="236">|————邵阳</option><option value="237">|————岳阳</option><option value="238">|————常德</option><option value="239">|————张家界</option><option value="240">|————益阳</option><option value="241">|————郴州</option><option value="242">|————永州</option><option value="243">|————怀化</option><option value="244">|————娄底</option><option value="245">|————湘西</option><option value="21" disabled>|——广东</option><option value="246">|————广州</option><option value="247">|————韶关</option><option value="248">|————深圳</option><option value="249">|————珠海</option><option value="250">|————汕头</option><option value="251">|————佛山</option><option value="252">|————江门</option><option value="253">|————湛江</option><option value="254">|————茂名</option><option value="255">|————肇庆</option><option value="256">|————惠州</option><option value="257">|————梅州</option><option value="258">|————汕尾</option><option value="259">|————河源</option><option value="260">|————阳江</option><option value="261">|————清远</option><option value="262">|————东莞</option><option value="263">|————中山</option><option value="264">|————潮州</option><option value="265">|————揭阳</option><option value="266">|————云浮</option><option value="22" disabled>|——广西</option><option value="267">|————南宁</option><option value="268">|————柳州</option><option value="269">|————桂林</option><option value="270">|————梧州</option><option value="271">|————北海</option><option value="272">|————防城港</option><option value="273">|————钦州</option><option value="274">|————贵港</option><option value="275">|————玉林</option><option value="277">|————柳州</option><option value="278">|————贺州</option><option value="279">|————百色</option><option value="280">|————河池</option><option value="23" disabled>|——海南</option><option value="281">|————海南</option><option value="282">|————海口</option><option value="283">|————三亚</option><option value="24" disabled>|——重庆</option><option value="284">|————重庆</option><option value="25" disabled>|——四川</option><option value="285">|————成都</option><option value="286">|————自贡</option><option value="287">|————攀枝花</option><option value="288">|————泸州</option><option value="289">|————德阳</option><option value="290">|————绵阳</option><option value="291">|————广元</option><option value="292">|————遂宁</option><option value="293">|————内江</option><option value="294">|————乐山</option><option value="295">|————南充</option><option value="296">|————眉山</option><option value="297">|————宜宾</option><option value="298">|————广安</option><option value="299">|————达州</option><option value="300">|————雅安</option><option value="301">|————巴中</option><option value="302">|————资阳</option><option value="303">|————阿坝</option><option value="304">|————甘孜</option><option value="305">|————凉山</option><option value="26" disabled>|——贵州</option><option value="306">|————贵阳</option><option value="307">|————六盘水</option><option value="308">|————遵义</option><option value="309">|————安顺</option><option value="310">|————铜仁</option><option value="311">|————黔西南</option><option value="312">|————毕节</option><option value="313">|————黔东南</option><option value="314">|————黔南</option><option value="27" disabled>|——云南</option><option value="315">|————昆明</option><option value="316">|————曲靖</option><option value="317">|————玉溪</option><option value="318">|————保山</option><option value="319">|————昭通</option><option value="320">|————楚雄</option><option value="321">|————红河</option><option value="322">|————文山</option><option value="323">|————思茅</option><option value="324">|————西双版纳</option><option value="325">|————大理</option><option value="326">|————德宏</option><option value="327">|————丽江</option><option value="328">|————怒江</option><option value="329">|————迪庆</option><option value="330">|————临沧</option><option value="28" disabled>|——西藏</option><option value="331">|————拉萨</option><option value="332">|————昌都</option><option value="333">|————山南</option><option value="334">|————日喀则</option><option value="335">|————那曲</option><option value="336">|————阿里</option><option value="337">|————林芝</option><option value="29" disabled>|——陕西</option><option value="338">|————西安</option><option value="339">|————铜川</option><option value="340">|————宝鸡</option><option value="341">|————咸阳</option><option value="342">|————渭南</option><option value="343">|————延安</option><option value="345">|————汉中</option><option value="346">|————榆林</option><option value="347">|————安康</option><option value="348">|————商洛</option><option value="30" disabled>|——甘肃</option><option value="349">|————兰州</option><option value="350">|————嘉峪关</option><option value="351">|————金昌</option><option value="352">|————白银</option><option value="353">|————天水</option><option value="354">|————酒泉</option><option value="355">|————张掖</option><option value="356">|————武威</option><option value="357">|————定西</option><option value="358">|————陇南</option><option value="359">|————平凉</option><option value="360">|————庆阳</option><option value="361">|————临夏</option><option value="362">|————甘南</option><option value="31" disabled>|——青海</option><option value="363">|————西宁</option><option value="364">|————海东</option><option value="365">|————海北</option><option value="366">|————黄南</option><option value="367">|————海南</option><option value="368">|————果洛</option><option value="369">|————玉树</option><option value="370">|————海西</option><option value="32" disabled>|——宁夏</option><option value="371">|————银川</option><option value="372">|————石嘴山</option><option value="373">|————吴忠</option><option value="374">|————固原</option><option value="33" disabled>|——新疆</option><option value="375">|————乌鲁木齐</option><option value="376">|————克拉玛依</option><option value="377">|————吐鲁番</option><option value="378">|————哈密</option><option value="379">|————昌吉</option><option value="380">|————博州</option><option value="381">|————巴州</option><option value="382">|————阿克苏</option><option value="383">|————克孜勒苏柯尔克孜</option><option value="384">|————喀什</option><option value="385">|————和田</option><option value="386">|————伊犁</option><option value="387">|————塔城</option><option value="34">|——台湾</option><option value="35">|——香港</option><option value="36">|——澳门</option><option value="2" disabled>境外</option><option value="401" disabled>|——亚洲</option><option value="451">|————沙特</option><option value="452">|————韩国</option><option value="453">|————菲律宾</option><option value="454">|————日本</option><option value="455">|————文莱</option><option value="456">|————越南</option><option value="457">|————马来西亚</option><option value="458">|————印度尼西亚</option><option value="459">|————新加坡</option><option value="460">|————柬埔寨</option><option value="461">|————泰国</option><option value="462">|————缅甸</option><option value="463">|————斯里兰卡</option><option value="464">|————印度</option><option value="465">|————尼泊尔</option><option value="466">|————巴基斯坦</option><option value="467">|————马尔代夫</option><option value="468">|————约旦</option><option value="469">|————朝鲜</option><option value="470">|————老挝</option><option value="471">|————以色列</option><option value="472">|————阿联酋</option><option value="473">|————阿富汗</option><option value="474">|————不丹</option><option value="475">|————科威特</option><option value="476">|————阿曼</option><option value="477">|————巴林</option><option value="478">|————卡塔尔</option><option value="480">|————东帝汶</option><option value="481">|————叙利亚</option><option value="482">|————黎巴嫩</option><option value="483">|————阿塞拜疆</option><option value="484">|————也门</option><option value="485">|————土库曼斯坦</option><option value="486">|————伊拉克</option><option value="487">|————格鲁吉亚</option><option value="488">|————吉尔吉斯</option><option value="489">|————哈萨克斯坦</option><option value="490">|————蒙古</option><option value="491">|————孟加拉</option><option value="492">|————塔吉克斯坦</option><option value="493">|————乌兹别克斯坦</option><option value="494">|————伊朗</option><option value="495">|————中国香港</option><option value="496">|————中国澳门</option><option value="497">|————中国台湾</option><option value="402" disabled>|——欧洲</option><option value="498">|————英国</option><option value="499">|————法国</option><option value="500">|————西班牙</option><option value="501">|————葡萄牙</option><option value="502">|————爱尔兰</option><option value="503">|————冰岛</option><option value="504">|————卢森堡</option><option value="505">|————荷兰</option><option value="506">|————比利时</option><option value="507">|————瑞士</option><option value="508">|————列支敦士登</option><option value="509">|————德国</option><option value="510">|————挪威</option><option value="511">|————瑞典</option><option value="512">|————芬兰</option><option value="513">|————爱沙尼亚</option><option value="514">|————立陶宛</option><option value="515">|————拉托维亚</option><option value="516">|————波兰</option><option value="517">|————捷克</option><option value="518">|————斯洛伐克</option><option value="519">|————奥地利</option><option value="520">|————匈牙利</option><option value="521">|————克罗地亚</option><option value="522">|————斯洛文尼亚</option><option value="533">|————意大利</option><option value="534">|————马其他</option><option value="535">|————罗马尼亚</option><option value="536">|————希腊</option><option value="537">|————俄罗斯</option><option value="538">|————塞浦路斯</option><option value="539">|————丹麦</option><option value="540">|————马耳他</option><option value="541">|————土耳其</option><option value="542">|————阿尔巴尼亚</option><option value="543">|————保加利亚</option><option value="544">|————摩纳哥</option><option value="545">|————摩尔多瓦</option><option value="546">|————马其顿</option><option value="547">|————塞尔维亚</option><option value="548">|————乌克兰</option><option value="550">|————圭亚那</option><option value="551">|————白俄罗斯</option><option value="403" disabled>|——大洋洲</option><option value="552">|————澳大利亚</option><option value="553">|————瓦努阿图</option><option value="554">|————斐济</option><option value="555">|————新西兰</option><option value="556">|————巴布亚新几内亚</option><option value="404" disabled>|——北美洲</option><option value="557">|————美国</option><option value="558">|————加拿大</option><option value="559">|————墨西哥</option><option value="560">|————古巴</option><option value="561">|————哥斯达黎加</option><option value="562">|————牙买加</option><option value="563">|————多米尼克</option><option value="564">|————巴拿马</option><option value="405" disabled>|——南美洲</option><option value="565">|————秘鲁</option><option value="566">|————巴西</option><option value="567">|————智利</option><option value="568">|————阿根廷</option><option value="569">|————苏里南</option><option value="570">|————玻利维亚</option><option value="571">|————哥伦比亚</option><option value="572">|————委内瑞拉</option><option value="573">|————乌拉圭</option><option value="406" disabled>|——非洲</option><option value="574">|————摩洛哥</option><option value="575">|————突尼斯</option><option value="576">|————埃及</option><option value="577">|————埃塞俄比亚</option><option value="578">|————肯尼亚</option><option value="579">|————塞舌尔</option><option value="580">|————坦桑尼亚</option><option value="581">|————赞比亚</option><option value="582">|————津巴布韦</option><option value="583">|————毛里求斯</option><option value="584">|————南非</option><option value="585">|————尼日利亚</option><option value="586">|————苏丹</option><option value="587">|————喀麦隆</option><option value="588">|————乍得</option><option value="589">|————塞拉利昂</option><option value="590">|————贝宁</option><option value="591">|————多哥</option><option value="592">|————几内亚</option><option value="593">|————毛里塔尼亚</option><option value="594">|————马达加斯加</option><option value="595">|————乌干达</option><option value="596">|————中非</option><option value="597">|————几内亚比绍</option><option value="598">|————赤道几内亚</option><option value="599">|————科摩罗</option><option value="600">|————圣普</option><option value="601">|————吉布提</option><option value="602">|————纳米比亚</option><option value="603">|————塞内加尔</option><option value="604">|————布基纳法索</option><option value="605">|————尼日尔</option><option value="606">|————加蓬</option><option value="607">|————冈比亚</option><option value="608">|————马里</option><option value="609">|————佛得角</option><option value="610">|————阿尔及利亚</option><option value="611">|————加纳</option><option value="612">|————留尼汪</option><option value="613">|————利比亚</option><option value="614">|————卢旺达</option><option value="615">|————索马里</option><option value="616">|————布隆迪</option><option value="617">|————安哥拉</option><option value="618">|————莫桑比克</option><option value="619">|————博茨瓦纳</option><option value="620">|————利比里亚</option><option value="621">|————刚果金</option><option value="622">|————科特迪瓦</option><option value="623">|————厄立特里亚</option><option value="624">|————莱索托</option><option value="625">|————马拉维</option><option value="626">|————刚果布</option><option value="627">|————南苏丹</option><option value="628"></option></select><script type="text/javascript">
            require(["chosen"], function(){
                $(".chosen-select").chosen({width: "100%", search_contains: true});
            })
            </script>
						</div>
					</div>
					<div class="form-group">
						<label class="col-xs-12 col-sm-3 col-md-2 control-label">分类</label>
						<div class="col-xs-12 col-sm-9">
							<select class="chosen-select  " name="cate_id" id="" onchange=""><option value="">请选择</option><option value="1" disabled>国内游</option><option value="2">|——张家界</option><option value="3">出境游</option></select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-12 col-md-2 ">状态</label>
						<div class="col-sm-9">
							<input name="status" class="ace ace-switch" type="checkbox" value="1" <?php if(($info["status"]) == "1"): ?>checked="checked"<?php endif; ?>/>
							<span class="lbl"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-12 col-md-2 ">图片</label>
						<div class="col-sm-9">
							<div class="input-group"> <input type="text" class="form-control" readonly="readonly" value="" placeholder="批量上传图片" autocomplete="off"> <span class="input-group-btn"> <button class="btn btn-default btn-sm" type="button" onclick="uploadMultiImage(this, 'imgs');" data-nowater="" data-dir="attachment/images" data-size="">选择图片</button> </span> </div> <div class="input-group multi-img-details"> </div><script type="text/javascript">
            var itype=[];
            function uploadMultiImage(elm, nm) {
                require(["jquery", "bootbox", "filestyle"], function($){
                    var dialog = bootbox.dialog({
                        title: "上传图片",
                        message: '<iframe width="0" height="0" name="__image_file_uploader" style="display:none;"></iframe>'
                        +'<ul class="nav nav-tabs">'
                        +'<li class="active"><a data-toggle="tab" href="#local">本地图片</a></li>'
                        +'<li> <a data-toggle="tab" href="#remote">远程图片 </a> </li>'
                        +'<li><a data-toggle="tab" href="#uploaded">浏览附件</a></li>'
                        +'</ul>'
                        +'<div class="tab-content"> <div id="local" class="tab-pane in active"> '
                        +'<form action="/index.php/Admin/Base/frameUpload/callback/uploadLocalPic" enctype="multipart/form-data" method="post" target="__image_file_uploader">'
                        +'<input type="file" name="upfile" >'
                        +'<input type="hidden" name="dir" value="">'
                        +'<input type="hidden" name="size" value="">'
                        +'<input type="hidden" name="nowater" value="">'
                        +'</form></div> '
                        +'<div id="remote" class="tab-pane">'
                        +'<form action="/index.php/Admin/Base/frameUpload/callback/uploadLocalPic" method="post" target="__image_file_uploader">'
                        +'<input type="txt" name="url" class="form-control" placeholder="请输入图片URL" >'
                        +'<input type="hidden" name="dir" value="">'
                        +'<input type="hidden" name="size" value="">'
                        +'<input type="hidden" name="nowater" value="">'
                        +'</form></div>'
                        +'<div class="tab-pane" id="uploaded">'
                        +'<div class="row">'
                        +'<div class="col-xs-12">'
                        +'类型：<label class="radio-inline"><input type="radio" name="type" value="1" checked>按分类</label>'
                        +'<label class="radio-inline"><input type="radio" name="type" value="2">按目录</label>'
                        +'</div>'
                        +'<div class="col-xs-12">分类：'+'<label class="radio-inline"><input type="radio" name="type2" value="41" checked>景点</label>'+'<label class="radio-inline"><input type="radio" name="type2" value="42">餐饮</label>'+'<label class="radio-inline"><input type="radio" name="type2" value="43">酒店</label>'+'</div>'
                        +'<div class="col-xs-12" id="uploaded-content"></div>'
                        +'</div>'
                        +'</div>'
                        +'</div>',
                        buttons:{
                            "cancel": {
                                label: "取消",
                                className: "btn btn-default"
                            },
                            "ok": {
                                label: "确定",
                                className: "btn btn-success",
                                callback: function(){
                                    var form;
                                    var idx = dialog.find(".nav.nav-tabs li.active").index();
                                    if(idx == 0){
                                        form=dialog.find("form")[0];
                                    } else if(idx == 1) {
                                        form=dialog.find("form")[1];
                                    } else if(idx == 2){
                                        dialog.find("#uploaded-content .ccbox-img-item.selected").each(function(){
                                             $(elm).parent().parent().next().append('<div class="multi-item" style="height: 150px; position:relative; float: left; margin-right: 18px;"><img style="max-width: 150px; max-height: 150px;" onerror="this.src=\"/Public/images/nopic.jpg\"; this.title=\"图片未找到.\"" src="'+$(this).attr("data-rel")+'" class="img-responsive img-thumbnail"><input type="hidden" name="'+nm+'[]" value="'+$(this).attr("data-rel")+'"><em class="close" style="position:absolute; top: 0px; right: -14px;" title="删除这张图片" onclick="deleteMultiImage(this)">×</em></div>');
                                        });
                                        dialog.modal("hide");
                                        return false;
                                    }
                                    $(form).find('input[name="dir"]').val($(elm).attr("data-dir"));
                                    $(form).find('input[name="size"]').val($(elm).attr("data-size"));
                                    $(form).find('input[name="nowater"]').val($(elm).attr("data-nowater"));
                                    form.submit();
                                    return false;
                                }
                            }
                        }
                    });
                    dialog.find("input[name='type']").on("click",function(){
                        var val=$(this).val();
                        if(val == 1){
                            $(this).parent().parent().next().css("display","block");
                        }else{
                            $(this).parent().parent().next().css("display","none");
                            $.post(
                                "/index.php/Admin/Base/loadDirs.html",
                                { dir:"images"},
                                function(json){
                                    if(json.status == 1){
                                        var htm = '';
                                        for(var i = 0, data = json.data, l = data.length; i < l; i ++){
                                            htm += '<a href="javascript:;" class="btn btn-app btn-lg img-album-dir" data-dir="'+data[i].name+'" data-parent="'+data[i].parent+'" data-rel="'+(data[i].isdir==true?"dir":"file")+'"><i class="icon-folder-close blue"></i>'+data[i].name+'</a>';
                                        }
                                        $("#uploaded-content").html(htm);
                                    }else {
                                        alert("加载文件夹失败");
                                    }
                                },
                                "json"
                            );
                        }
                    });
                    dialog.find("input[name='type2']").click(function(){
                        var type=$(this).val();
                        if(itype[type] != null){
                            var html = '';
                            for(var i = 0, data = itype[type], l = data.length; i < l; i++){
                                html += '<div class="col-xs-12 col-sm-3"><a class="btn btn-link img-album-item" data-rel="'+data[i].id+'">'+data[i].title+'</a></div>';
                            }
                            $("#uploaded-content").html(html);
                        } else {
                            $.post(
                                "/index.php/Admin/Base/loadImages.html",
                                { id: type},
                                function(json){
                                    itype[type]=json.data;
                                    var html = '';
                                    if(json.status == 1 && json.data.length > 0){
                                        for(var i = 0, data = json.data, l = data.length; i < l; i++){
                                            html += '<div class="col-xs-12 col-sm-3"><a class="btn btn-link img-album-item" data-rel="'+data[i].id+'">'+data[i].title+'</a></div>';
                                        }
                                    }
                                    $("#uploaded-content").html(html);
                                },"JSON"
                            );
                        }
                    }).eq(0).click();
                    $("#uploaded-content").delegate(".img-album-item","click",function(){
                        var self=$(this),id=self.attr("data-rel");
                        $.post(
                            "/index.php/Admin/Base/loadImagesDetail.html",
                            { id: id},
                            function(json){
                                if(json.status == 1){
                                    var htm = '<div class="ace-thumbnails">';
                                    for(var i = 0, data = json.data, l = data.length; i < l; i ++){
                                        htm += '<li style="border:2px solid transparent;" class="ccbox-img-item" data-rel="'+data[i].thumb+'"><i style="position:absolute;top:0;right:0;" class="icon-ok red hide"></i><a href="javascript://"><img src="'+data[i].thumb+'" width="80" height="80"></a></li>';
                                    }
                                    htm += '</div>';
                                    $("#uploaded-content").html(htm);
                                } else {
                                    alert("加载失败!");
                                }
                            },"json"
                        );
                    });
                    $("#uploaded-content").delegate(".ccbox-img-item", "click", function(){
                        var self=$(this), selected=self.hasClass("selected");
                        if(selected){
                            self.removeClass("selected").css("borderColor","transparent").find("i.icon-ok").addClass("hide");
                        }else{
                            self.addClass("selected").css("borderColor","#dd5a43").find("i.icon-ok").removeClass("hide");
                        }
                    });
                    $("#uploaded-content").delegate(".img-album-dir", "click", function(){
                        var self=$(this),dir=self.attr("data-dir"),parent=self.attr("data-parent");
                        $.post(
                            "/index.php/Admin/Base/loadDirs.html",
                            { dir: dir, parent: parent},
                            function(json){
                                if(json.status == 1){
                                    var htm = '<div class="ace-thumbnails">';
                                    for(var i = 0, data = json.data, l = data.length; i < l; i ++){
                                        htm += '<li style="border:2px solid transparent;" class="ccbox-img-item" data-rel="/uploads/attachment'+data[i].parent+"/"+data[i].name+'"><i style="position:absolute;top:0;right:0;" class="icon-ok red hide"></i><a href="javascript://"><img src="/uploads/attachment'+data[i].parent+"/"+data[i].name+'" width="80" height="80"></a></li>';
                                    }
                                    htm += '</div>';
                                    $("#uploaded-content").html(htm);
                                } else {
                                    alert("加载失败!");
                                }
                            },"json"
                        );
                    });
                    dialog.find(":file").filestyle({buttonText: "选择图片", buttonName: "btn-sm"});
                    window.uploadLocalPic = function(data){
                        if(data.status == 1){
                            $(elm).parent().parent().next().append('<div class="multi-item" style="height: 150px; position:relative; float: left; margin-right: 18px;"><img style="max-width: 150px; max-height: 150px;" onerror="this.src=\"/Public/images/nopic.jpg\"; this.title=\"图片未找到.\" src="'+data.file+'" class="img-responsive img-thumbnail"><input type="hidden" name="'+nm+'[]" value="'+data.file+'"><em class="close" style="position:absolute; top: 0px; right: -14px;" title="删除这张图片" onclick="deleteMultiImage(this)">×</em></div>');
                            dialog.modal("hide");
                        } else {
                            alert(data.msg);
                        }
                    }
                });
            }
            function deleteMultiImage(elm){
                require(["jquery"], function($){
                    $(elm).parent().remove();
                });
            }
            </script>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<div class="row-fluid">
								<ul class="ace-thumbnails">
									<?php if(is_array($ilist)): $i = 0; $__LIST__ = $ilist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li>
										<a href="<?php echo ($vo["src"]); ?>" title="<?php echo ($vo["title"]); ?>" data-rel="colorbox">
											<img alt="/Public/images/nopic.jpg" onerror="this.src='/Public/images/nopic.jpg';return !1;" src="<?php echo ($vo["thumb"]); ?>" width="150" height="150" />
										</a>
										<div class="tools tools-right">
											<a href="#">
												<i class="icon-remove red" data-id="<?php echo ($vo["id"]); ?>" data-pid="<?php echo ($info["id"]); ?>"></i>
											</a>
										</div>
									</li><?php endforeach; endif; else: echo "" ;endif; ?>
								</ul>
							</div>
						</div>
					</div>
					<div class="form-actions clearfix">
						<div class="col-xs-12 col-sm-9 col-md-offset-1">
							<input type="hidden" name="id" value="<?php echo ($info["id"]); ?>">
							<button type="submit" class="btn btn-success">编辑图集</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
require(["colorbox"],function($){
	var colorbox_params = {
		reposition:true,
		scalePhotos:true,
		scrolling:false,
		previous:'<i class="icon-arrow-left"></i>',
		next:'<i class="icon-arrow-right"></i>',
		close:'&times;',
		current:'{current} / {total}',
		maxWidth:'100%',
		maxHeight:'100%',
		onOpen:function(){
			document.body.style.overflow = 'hidden';
		},
		onClosed:function(){
			document.body.style.overflow = 'auto';
		},
		onComplete:function(){
			$.colorbox.resize();
		}
	};
	$('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
	$('.tools .icon-remove').click(function(){
		var self=$(this),id=self.attr('data-id'),pid=self.attr('data-pid');
		if(id && pid){
			$.post(
				"<?php echo U('Images/delImages');?>",
				{ id: id,pid:pid},
				function(data){
					if(data.status==1){
						require(["util"],function(util){
							util.success(data.msg);
							self.parent().parent().parent().remove();
						});
					}else{
						require(["util"],function(util){
							util.error(data.msg);
						});
					}
				}
			);
		}
		return false;
	});
})
</script>
</div><!-- /.main-content -->
			</div><!-- /.main-container-inner -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->
		<script type="text/javascript">
		require(['bootstrap'],function($){
			$('[data-toggle="tooltip"]').tooltip();
		})
		</script>
		
	</body>
</html>
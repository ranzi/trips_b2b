<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo ($bar["curpos"]); ?> - <?php echo ($cfgs["sitename"]); ?></title>
		<meta name="description" content="<?php echo ($cfgs["seodescription"]); ?>" />
		<meta name="keywords" content="<?php echo ($cfgs["seokeywords"]); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!-- basic styles -->
		<script type="text/javascript" src="/Public/js/lib/require.js"></script>
		<script type="text/javascript" src="/Public/js/lib/config.js"></script>
        <script type="text/javascript" src="/Public/js/lib/my97/WdatePicker.js"></script>
		<link rel="stylesheet" href="/Public/css/bootstrap.min.css" />
        <link rel="stylesheet" href="/Public/css/custom.css" />
		<link rel="stylesheet" href="/Public/css/font-awesome.min.css" />
		
		<!-- fonts -->
		<link rel="stylesheet" href="/Public/css/ace-fonts.css" />
		<!-- ace styles -->
		<link rel="stylesheet" href="/Public/css/ace.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-skins.min.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="css/ace-ie.min.css" />
		  <script src="/Public/js/excanvas.min.js"></script>
		<![endif]-->
		
		<!-- inline styles related to this page -->
		<!-- ace settings handler -->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="/Public/js/html5shiv.js"></script>
		<script src="/Public/js/respond.min.js"></script>
		<![endif]-->
		<script type="text/javascript">
		if(navigator.appName == 'Microsoft Internet Explorer'){
			if(navigator.userAgent.indexOf("MSIE 5.0")>0 || 
				navigator.userAgent.indexOf("MSIE 6.0")>0 || 
				navigator.userAgent.indexOf("MSIE 7.0")>0) {
				alert('您使用的 IE 浏览器版本过低,无法获得最好的体验且可能出现无法预期的错误, 推荐使用 Chrome 浏览器或 IE8 及以上版本浏览器.');
			}
		}
		</script>
	</head>
	
	
<body>
<div class="navbar navbar-default" id="navbar">
			<script type="text/javascript">
			require(['jquery', 'acemy'], function($){
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			});
			</script>
			<div class="navbar-container" id="navbar-container">
				<div class="navbar-header pull-left">
					<a href="<?php echo U('Index/index');?>" class="navbar-brand" style="padding:0px">
						<img src="<?php echo ($cfgs["logo"]); ?>" class="img-rounded img-thumbnail" style="height:45px;padding:0">
						<small>
							<?php echo ($cfgs["sitename"]); ?>后台管理系统
						</small>
					</a><!-- /.brand -->
				</div><!-- /.navbar-header -->

				<div class="navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						<li class="grey">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="icon-tasks"></i>
								<span class="badge badge-grey">4</span>
							</a>

							<ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="icon-ok"></i>
									4 Tasks to complete
								</li>

								<li>
									<a href="#">
										<div class="clearfix">
											<span class="pull-left">Software Update</span>
											<span class="pull-right">65%</span>
										</div>

										<div class="progress progress-mini ">
											<div style="width:65%" class="progress-bar "></div>
										</div>
									</a>
								</li>

								<li>
									<a href="#">
										<div class="clearfix">
											<span class="pull-left">Hardware Upgrade</span>
											<span class="pull-right">35%</span>
										</div>

										<div class="progress progress-mini ">
											<div style="width:35%" class="progress-bar progress-bar-danger"></div>
										</div>
									</a>
								</li>

								<li>
									<a href="#">
										<div class="clearfix">
											<span class="pull-left">Unit Testing</span>
											<span class="pull-right">15%</span>
										</div>

										<div class="progress progress-mini ">
											<div style="width:15%" class="progress-bar progress-bar-warning"></div>
										</div>
									</a>
								</li>

								<li>
									<a href="#">
										<div class="clearfix">
											<span class="pull-left">Bug Fixes</span>
											<span class="pull-right">90%</span>
										</div>

										<div class="progress progress-mini progress-striped active">
											<div style="width:90%" class="progress-bar progress-bar-success"></div>
										</div>
									</a>
								</li>

								<li>
									<a href="#">
										See tasks with details
										<i class="icon-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>

						<li class="purple">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="icon-bell-alt icon-animated-bell"></i>
								<span class="badge badge-important">8</span>
							</a>

							<ul class="pull-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="icon-warning-sign"></i>
									8 Notifications
								</li>

								<li>
									<a href="#">
										<div class="clearfix">
											<span class="pull-left">
												<i class="btn btn-xs no-hover btn-pink icon-comment"></i>
												New Comments
											</span>
											<span class="pull-right badge badge-info">+12</span>
										</div>
									</a>
								</li>

								<li>
									<a href="#">
										<i class="btn btn-xs btn-primary icon-user"></i>
										Bob just signed up as an editor ...
									</a>
								</li>

								<li>
									<a href="#">
										<div class="clearfix">
											<span class="pull-left">
												<i class="btn btn-xs no-hover btn-success icon-shopping-cart"></i>
												New Orders
											</span>
											<span class="pull-right badge badge-success">+8</span>
										</div>
									</a>
								</li>

								<li>
									<a href="#">
										<div class="clearfix">
											<span class="pull-left">
												<i class="btn btn-xs no-hover btn-info icon-twitter"></i>
												Followers
											</span>
											<span class="pull-right badge badge-info">+11</span>
										</div>
									</a>
								</li>

								<li>
									<a href="#">
										See all notifications
										<i class="icon-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>

						<li class="green">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="icon-envelope icon-animated-vertical"></i>
								<span class="badge badge-success">5</span>
							</a>

							<ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="icon-envelope-alt"></i>
									5 Messages
								</li>

								<li>
									<a href="#">
										<img src="/Public/avatars/avatar.png" class="msg-photo" alt="Alex's Avatar" />
										<span class="msg-body">
											<span class="msg-title">
												<span class="blue">Alex:</span>
												Ciao sociis natoque penatibus et auctor ...
											</span>

											<span class="msg-time">
												<i class="icon-time"></i>
												<span>a moment ago</span>
											</span>
										</span>
									</a>
								</li>

								<li>
									<a href="#">
										<img src="/Public/avatars/avatar3.png" class="msg-photo" alt="Susan's Avatar" />
										<span class="msg-body">
											<span class="msg-title">
												<span class="blue">Susan:</span>
												Vestibulum id ligula porta felis euismod ...
											</span>

											<span class="msg-time">
												<i class="icon-time"></i>
												<span>20 minutes ago</span>
											</span>
										</span>
									</a>
								</li>

								<li>
									<a href="#">
										<img src="/Public/avatars/avatar4.png" class="msg-photo" alt="Bob's Avatar" />
										<span class="msg-body">
											<span class="msg-title">
												<span class="blue">Bob:</span>
												Nullam quis risus eget urna mollis ornare ...
											</span>

											<span class="msg-time">
												<i class="icon-time"></i>
												<span>3:15 pm</span>
											</span>
										</span>
									</a>
								</li>

								<li>
									<a href="inbox.html">
										See all messages
										<i class="icon-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>

						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="<?php echo ($_SESSION['admin']['avatar']); ?>" alt="<?php echo (session('admin_name')); ?>" />
								<span class="user-info">
									<small>欢迎使用<?php echo ($cfgs["sitename"]); ?>后台管理系统后台管理系统后台管理系统后台管理系统</small>
									<?php echo (session('admin_name')); ?>
								</span>
								<i class="icon-caret-down"></i>
							</a>
							<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="<?php echo U('Admin/profile');?>">
										<i class="icon-user"></i>
										个人资料
									</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="<?php echo U('Login/logout');?>">
										<i class="icon-off"></i>
										退出登录
									</a>
								</li>
							</ul>
						</li>
					</ul><!-- /.ace-nav -->
				</div><!-- /.navbar-header -->
			</div><!-- /.container -->
		</div>
		<div class="main-container" id="main-container">
			<script type="text/javascript">
			// require(['jquery', 'ace'], function($){
			// 	try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			// });
			</script>

			<div class="main-container-inner">
				<a class="menu-toggler" id="menu-toggler" href="#">
					<span class="menu-text"></span>
				</a>

				<div class="sidebar" id="sidebar">
					<script type="text/javascript">
					require(['jquery', 'acemy'], function($){
						try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
					});
					</script>

					<div class="sidebar-shortcuts" id="sidebar-shortcuts">
						<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
							<button class="btn btn-success">
								<i class="icon-signal"></i>
							</button>

							<button class="btn btn-info">
								<i class="icon-pencil"></i>
							</button>

							<button class="btn btn-warning">
								<i class="icon-group"></i>
							</button>

							<button class="btn btn-danger">
								<i class="icon-cogs"></i>
							</button>
						</div>

						<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
							<span class="btn btn-success"></span>

							<span class="btn btn-info"></span>

							<span class="btn btn-warning"></span>

							<span class="btn btn-danger"></span>
						</div>
					</div><!-- #sidebar-shortcuts -->

					<ul class="nav nav-list">
						<?php if(is_array($authMenus)): $i = 0; $__LIST__ = $authMenus;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu): $mod = ($i % 2 );++$i;?><li class="<?php echo ($menu["class"]); ?>">
							<a class="dropdown-toggle" href="<?php if(empty($menu["child"])): echo U($menu['url']); endif; ?>">
								<?php if(!empty($menu["icon"])): ?><i class="<?php echo ($menu["icon"]); ?>"></i><?php endif; ?>
								<span class="menu-text"><?php echo ($menu["title"]); ?></span>
							</a>
							<?php if(!empty($menu["child"])): ?><ul class="submenu">
							<?php if(is_array($menu["child"])): $i = 0; $__LIST__ = $menu["child"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li <?php if(!empty($vo["class"])): ?>class="<?php echo ($vo["class"]); ?>"<?php endif; ?>>
									<a href="<?php echo U($vo['url']);?>">
										<i class="icon-double-angle-right"></i>
										<?php echo ($vo["title"]); ?>
									</a>
								</li><?php endforeach; endif; else: echo "" ;endif; ?>
							</ul><?php endif; ?>
						</li><?php endforeach; endif; else: echo "" ;endif; ?>
					</ul><!-- /.nav-list -->

					<div class="sidebar-collapse" id="sidebar-collapse">
						<i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right" data-toggle="tooltip" data-placement="right" title="收起面板"></i>
					</div>

					<script type="text/javascript">
					require(['jquery', 'acemy'], function($){
						try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
					});
					</script>
				</div>
				<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
						require(['jquery', 'acemy'], function($){
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						});
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<?php echo U('Index/index');?>">首页</a>
							</li>
							<?php if(!empty($bar["menu"])): ?><li>
								<a href="<?php echo ($bar["url"]); ?>"><?php echo ($bar["menu"]); ?></a>
							</li><?php endif; ?>
							<li class="active"><?php echo ($bar["curpos"]); ?></li>
						</ul><!-- .breadcrumb -->

						<!-- <div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="icon-search nav-search-icon"></i>
								</span>
							</form>
						</div> --><!-- #nav-search -->
					</div>
<style type="text/css">
span.list-title{ line-height: 18px; height: 18px; padding-top: 3px; display: inline-block; vertical-align: middle;}
</style>
<div class="page-content">
	<div class="page-header" style="position:relative;">
		<h1>
			添加线路
			<small>
				<i class="icon-double-angle-right"></i>
				<a href="javascript:history.go(-1);" class="btn btn-link">返回列表</a>
			</small>
		</h1>
	</div>
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="table-responsive panel-body">
				<form class="form form-horizontal" method="post" action="">
					<div class="tabtab">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#line-base" data-toggle="tab">基础信息</a>
							</li>
							<li>
								<a href="#line-image" data-toggle="tab">图片</a>
							</li>
							<li>
								<a href="#line-advance" data-toggle="tab">线路描述</a>
							</li>
							<li>
								<a href="#line-trips" data-toggle="tab">行程描述</a>
							</li>
							<li>
								<a href="#line-extra" data-toggle="tab">附加说明</a>
							</li>
							<li>
								<a href="#line-price" data-toggle="tab">团期</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="line-base">
								<div class="form-group">
									<label class="control-label col-xs-12 col-md-2 col-sm-3">线路标题</label>
									<div class="col-sm-9 col-xs-12">
										<input class="form-control" type="text" name="title">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-md-2 col-sm-3">线路短标题</label>
									<div class="col-sm-9 col-xs-12">
										<input class="form-control" type="text" name="subtitle">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-md-2 col-sm-3">产品类型</label>
									<div class="col-sm-9 col-xs-12">
										<!--default select start--><select name="type_id" id="_select" onClick="" onChange="" class="form-control" ><option value="">==请选择==</option><?php foreach($types as $key => $val){ ?><option value="<?php echo $key;?>"><?php echo $val;?></option><?php }?></select><!--default select end-->
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-md-2 col-sm-3">筛选项</label>
									<div class="col-sm-9 col-xs-12" id="filter_container">
										<div class="list-group">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-md-2 col-sm-3">产品分类</label>
									<div class="col-sm-9 col-xs-12" id="cates_container">
										
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-md-2 col-sm-3">产品标识</label>
									<div class="col-sm-9 col-xs-12">
										<label class="checkbox-inline">
											<input type="checkbox" value="1" name="isnew">新品
										</label>
										<label class="checkbox-inline">
											<input type="checkbox" value="1" name="ishot">热卖
										</label>
										<label class="checkbox-inline">
											<input type="checkbox" value="1" name="ispromotion">促销
										</label>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-md-2 col-sm-3">允许直接购买</label>
									<div class="col-sm-9 col-xs-12">
										<input name="allowpay" class="ace ace-switch" type="checkbox" checked="checked" value="1" />
										<span class="lbl"></span>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-md-2 col-sm-3">排序</label>
									<div class="col-sm-9 col-xs-12">
										<input class="form-control" type="text" name="ordid">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-md-2 col-sm-3">状态</label>
									<div class="col-sm-9 col-xs-12">
										<input name="status" class="ace ace-switch" type="checkbox" checked="checked" value="1" />
										<span class="lbl"></span>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="line-image">
								<div class="form-group">
									<label class="control-label col-xs-12 col-md-2 col-sm-3">主图</label>
									<div class="col-sm-9 col-xs-12">
										<div class="input-group "> <input type="text" name="img" value="/Public/images/nopic.jpg" class="form-control" autocomplete="off" readonly="true"> <span class="input-group-btn"> <button class="btn btn-default btn-sm" type="button" onclick="showImageDialog(this);" data-nowater="" data-dir="line" data-size="">选择图片</button> </span> </div> <div class="input-group " style=""> <img src="/Public/images/nopic.jpg" onerror="this.src='/Public/images/nopic.jpg'; this.title='图片未找到.'" class="img-responsive img-thumbnail" style="max-width:150px;" /> </div><script type="text/javascript">
            function showImageDialog(ele){
                require(["jquery", "bootstrap", "bootbox", "filestyle"], function($){
                    var btn = $(ele);
                    var dialog = bootbox.dialog({
                        message: '<iframe width="0" height="0" name="__image_file_uploader" style="display:none;"></iframe>'
                        +'<ul class="nav nav-tabs">'
                        +'<li class="active"><a data-toggle="tab" href="#local">本地图片</a></li>'
                        +'<li> <a data-toggle="tab" href="#remote">远程图片 </a> </li>'
                        +'</ul>'
                        +'<div class="tab-content">'
                        +'<div id="local" class="tab-pane in active"> '
                        +'<form action="/index.php/Admin/Base/frameUpload/callback/uploadLocalPic" enctype="multipart/form-data" method="post" target="__image_file_uploader">'
                        +'<input type="file" name="upfile" >'
                        +'<input type="hidden" name="dir" value="">'
                        +'<input type="hidden" name="size" value="">'
                        +'<input type="hidden" name="nowater" value="">'
                        +'</form></div>'
                        +' <div id="remote" class="tab-pane">'
                        +'<form action="/index.php/Admin/Base/frameUpload/callback/uploadLocalPic" method="post" target="__image_file_uploader">'
                        +'<input type="txt" name="url" class="form-control" placeholder="请输入图片URL" >'
                        +'<input type="hidden" name="dir" value="">'
                        +'<input type="hidden" name="size" value="">'
                        +'<input type="hidden" name="nowater" value="">'
                        +'</form></div> </div>',
                        title: "上传图片",
                        buttons:{
                            "cancel": {
                                label: "取消",
                                className: "btn btn-default"
                            },
                            "ok":{
                                label: "确定",
                                className: "btn btn-success",
                                callback: function(){
                                    var form;
                                    if(dialog.find(".nav.nav-tabs li").eq(0).hasClass("active")){
                                        form=dialog.find("form")[0];
                                    } else {
                                        form=dialog.find("form")[1];
                                    }
                                    $(form).find('input[name="dir"]').val(btn.attr("data-dir"));
                                    $(form).find('input[name="size"]').val(btn.attr("data-size"));
                                    $(form).find('input[name="nowater"]').val(btn.attr("data-nowater"));
                                    form.submit();
                                    return false;
                                }
                            }
                        }
                    });
                    dialog.find(":file").filestyle({buttonText: "选择图片", buttonName: "btn-sm"});
                    window.uploadLocalPic = function(data){
                        if(data.status == 1){
                            btn.parent().parent().next().children().attr("src", data.file);
                            btn.parent().prev().val(data.file); dialog.modal("hide");
                        } else {
                            alert(data.msg);
                        }
                    }
                });
            } </script>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-md-2 col-sm-3">相册</label>
									<div class="col-sm-9 col-xs-12">
										<div class="input-group"> <input type="text" class="form-control" readonly="readonly" value="" placeholder="批量上传图片" autocomplete="off"> <span class="input-group-btn"> <button class="btn btn-default btn-sm" type="button" onclick="uploadMultiImage(this, 'imgs');" data-nowater="" data-dir="line" data-size="">选择图片</button> </span> </div> <div class="input-group multi-img-details"> </div><script type="text/javascript">
            function uploadMultiImage(elm, nm) {
                require(["jquery", "bootbox", "filestyle"], function($){
                    var dialog = bootbox.dialog({
                        title: "上传图片",
                        message: '<iframe width="0" height="0" name="__image_file_uploader" style="display:none;"></iframe>'
                        +'<ul class="nav nav-tabs">'
                        +'<li class="active"><a data-toggle="tab" href="#local">本地图片</a></li>'
                        +'<li> <a data-toggle="tab" href="#remote">远程图片 </a> </li></ul>'
                        +'<div class="tab-content"> <div id="local" class="tab-pane in active"> '
                        +'<form action="/index.php/Admin/Base/frameUpload/callback/uploadLocalPic" enctype="multipart/form-data" method="post" target="__image_file_uploader">'
                        +'<input type="file" name="upfile" >'
                        +'<input type="hidden" name="dir" value="">'
                        +'<input type="hidden" name="size" value="">'
                        +'<input type="hidden" name="nowater" value="">'
                        +'</form></div> '
                        +'<div id="remote" class="tab-pane">'
                        +'<form action="/index.php/Admin/Base/frameUpload/callback/uploadLocalPic" method="post" target="__image_file_uploader">'
                        +'<input type="txt" name="url" class="form-control" placeholder="请输入图片URL" >'
                        +'<input type="hidden" name="dir" value="">'
                        +'<input type="hidden" name="size" value="">'
                        +'<input type="hidden" name="nowater" value="">'
                        +'</form></div> </div>',
                        buttons:{
                            "cancel": {
                                label: "取消",
                                className: "btn btn-default"
                            },
                            "ok": {
                                label: "确定",
                                className: "btn btn-success",
                                callback: function(){
                                    var form;
                                    if(dialog.find(".nav.nav-tabs li").eq(0).hasClass("active")){
                                        form=dialog.find("form")[0];
                                    } else {
                                        form=dialog.find("form")[1];
                                    }
                                    $(form).find('input[name="dir"]').val($(elm).attr("data-dir"));
                                    $(form).find('input[name="size"]').val($(elm).attr("data-size"));
                                    $(form).find('input[name="nowater"]').val($(elm).attr("data-nowater"));
                                    form.submit();
                                    return false;
                                }
                            }
                        }
                    });
                    dialog.find(":file").filestyle({buttonText: "选择图片", buttonName: "btn-sm"});
                    window.uploadLocalPic = function(data){
                        if(data.status == 1){
                            $(elm).parent().parent().next().append('<div class="multi-item" style="height: 150px; position:relative; float: left; margin-right: 18px;"><img style="max-width: 150px; max-height: 150px;" onerror="this.src=\"/Public/images/nopic.jpg\"; this.title=\"图片未找到.\" src="'+data.file+'" class="img-responsive img-thumbnail"><input type="hidden" name="'+nm+'[]" value="'+data.file+'"><em class="close" style="position:absolute; top: 0px; right: -14px;" title="删除这张图片" onclick="deleteMultiImage(this)">×</em></div>');
                            dialog.modal("hide");
                        } else {
                            alert(data.msg);
                        }
                    }
                });
            }
            function deleteMultiImage(elm){
                require(["jquery"], function($){
                    $(elm).parent().remove();
                });
            }
            </script>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="line-advance">
								<div class="form-group">
									<!-- <label class="control-label col-xs-12 col-md-2 col-sm-3">详细内容</label> -->
									<div class="col-sm-10 col-xs-12 col-md-offset-1">
										<!--baidu editor start--><script  type="text/plain" id="contents" name="contents" style="height:300px;width:980">请在这里输入内容</script> <script type="text/javascript"> require(["baidueditor", "zeroclipboard", "bdlang"], function(UE, zcl){window.ZeroClipboard = zcl; var ue = UE.getEditor("contents"); }) </script><!--baidu editor end-->
									</div>
								</div>
							</div>
							<div class="tab-pane" id="line-trips">
								<div class="row">
									<div class="col-xs-12">
										<a class="btn btn-info" href="javascript:;" onClick="addTrips(this);">添加行程</a>
										<a class="btn btn-warning" href="javascript:;" onClick="delLastTrip(this);">删除最后一天行程</a>
										<a class="btn btn-danger" href="javascript:;" onClick="delAllTrips(this);">删除全部行程</a>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="line-extra">
								<div class="form-group">
									<label class="control-label col-xs-12 col-md-2 col-sm-3">线路特色</label>
									<div class="col-xs-12 col-sm-9">
										<textarea id="_txtbox" name="feature" class="autosize-transition form-control "></textarea><script type="text/javascript">
            var ta;
            require(["jquery", "at"],function($, at){
                ta=at($("textarea[class*=autosize]"));
            });
            </script>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-md-2 col-sm-3">费用包含</label>
									<div class="col-xs-12 col-sm-9">
										<textarea id="_txtbox" name="feeinclude" class="autosize-transition form-control "></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-md-2 col-sm-3">费用不包含</label>
									<div class="col-xs-12 col-sm-9">
										<textarea id="_txtbox" name="feeexclude" class="autosize-transition form-control "></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-md-2 col-sm-3">注意事项</label>
									<div class="col-xs-12 col-sm-9">
										<textarea id="_txtbox" name="notice" class="autosize-transition form-control "></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-md-2 col-sm-3">温馨提示</label>
									<div class="col-xs-12 col-sm-9">
										<textarea id="_txtbox" name="tips" class="autosize-transition form-control "></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-md-2 col-sm-3">儿童描述</label>
									<div class="col-xs-12 col-sm-9">
										<textarea id="_txtbox" name="children" class="autosize-transition form-control "></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-md-2 col-sm-3">购物描述</label>
									<div class="col-xs-12 col-sm-9">
										<textarea id="_txtbox" name="shopping" class="autosize-transition form-control "></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-md-2 col-sm-3">可选项目</label>
									<div class="col-xs-12 col-sm-9">
										<textarea id="_txtbox" name="optional" class="autosize-transition form-control "></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-md-2 col-sm-3">赠送项目</label>
									<div class="col-xs-12 col-sm-9">
										<textarea id="_txtbox" name="gift" class="autosize-transition form-control "></textarea>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="line-price">
								<div class="row">
									<div class="col-xs-12 pro_input date_price">
										<div class="row">
											<div class="col-xs-12">
												<input type="hidden" value="" id="date_price_data" name="date_price_data">
												<div class="input-group">
													<span class="input-group-addon">门市价</span>
													<input type="text" value="12" name="menshijia" class="form-control" />
												</div>
											</div>
										</div>
										<div class="space-4"></div>
										<div class="tabtab">
											<ul class="nav nav-tabs">
												<li class="active">
													<a href="#J_batch_area" data-toggle="tab">批量添加价格</a>
												</li>
												<li class="">
													<a href="#J_date_area" data-toggle="tab">批量添加价格</a>
												</li>
												<li class="pull-right">
													<a href="javascript:;" class="J_clear_all_price btn btn-link">清除所有报价</a>
												</li>
											</ul>
											<div class="tab-content">
												<div class="tab-pane active" id="J_batch_area">
													<div class="row col-md-offset-1">
														<label class="checkbox-inline">
															<input type="checkbox" class="J_week_all_select"/>		
															天天发团
														</label>
														<label class="checkbox-inline">
															<input type="checkbox" class="J_week_select" value="1"/>		
															周一
														</label>
														<label class="checkbox-inline">
															<input type="checkbox" class="J_week_select" value="2"/>		
															周二
														</label>
														<label class="checkbox-inline">
															<input type="checkbox" class="J_week_select" value="3"/>		
															周三
														</label>
														<label class="checkbox-inline">
															<input type="checkbox" class="J_week_select" value="4"/>		
															周四
														</label>
														<label class="checkbox-inline">
															<input type="checkbox" class="J_week_select" value="5"/>		
															周五
														</label>
														<label class="checkbox-inline">
															<input type="checkbox" class="J_week_select" value="6"/>		
															周六
														</label>
														<label class="checkbox-inline">
															<input type="checkbox" class="J_week_select" value="0"/>		
															周日
														</label>
													</div>
													<div class="row">
														<div class="col-xs-12">
															<div class="col-xs-3">
																<div class="input-group">
																	<span class="input-group-addon">预定价</span>
																	<input type="text" class="J_input_limit form-control" name="price_cncn"/>		
																</div>
															</div>
															<div class="col-xs-3">
																<div class="input-group">
																	<span class="input-group-addon">预定儿童价</span>
																	<input type="text" class="J_input_limit form-control" name="price_cncn_child"/>
																</div>
															</div>
															<div class="col-xs-3">
																<div class="input-group">
																	<span class="input-group-addon">地接价</span>
																	<input type="text" class="J_input_limit form-control form-control" name="price_cncn"/>
																</div>
															</div>
															<div class="col-xs-3">
																<div class="input-group">
																	<span class="input-group-addon">地接儿童价</span>
																	<input type="text" class="J_input_limit form-control" name="price_cncn_child"/>
																</div>
															</div>
														</div>
														<div class="col-xs-12">
															<a class="btn btn-link" id="J_batch_add_by_week">添加</a>
														</div>
													</div>
												</div>
												<div class="tab-pane" id="J_date_area">
													<div class="row">
														<div class="form-group">
															<label class="control-label col-xs-12 col-sm-3">指定时间段</label>
															<div class="col-xs-12 col-sm-9">
																<div class="input-gorup">
																	<input name="rangetime[start]" type="hidden" value="2015-09-03" /> <input name="rangetime[end]" type="hidden" value="2015-09-03" /> <button class="btn btn-info daterange daterange-date" type="button" style="border:0;"><span class="date-title">2015-09-03至2015-09-03</span> <i class="fa fa-calendar"></i></button><script type="text/javascript">
                require(["daterangepicker"], function($){
                    $(function(){
                        $(".daterange.daterange-date").each(function(){
                            var elm = this;
                            $(this).daterangepicker({
                                startDate: $(elm).prev().prev().val(),
                                endDate: $(elm).prev().val(),
                                format: "YYYY-MM-DD",
                            }, function(start, end){
                                $(elm).find(".date-title").html(start.toDateStr() + " 至 " + end.toDateStr());
                                $(elm).prev().prev().val(start.toDateStr());
                                $(elm).prev().val(end.toDateStr());
                            });
                        });
                    });
                });
                </script>
																</div>
															</div>
														</div>
													</div>
													<div class="row col-md-offset-1" style="">
														<label class="checkbox-inline">
															<input type="checkbox" class="J_week_select1" value="1" checked />		
															周一
														</label>
														<label class="checkbox-inline">
															<input type="checkbox" class="J_week_select1" value="2" checked />		
															周二
														</label>
														<label class="checkbox-inline">
															<input type="checkbox" class="J_week_select1" value="3" checked />		
															周三
														</label>
														<label class="checkbox-inline">
															<input type="checkbox" class="J_week_select1" value="4" checked />		
															周四
														</label>
														<label class="checkbox-inline">
															<input type="checkbox" class="J_week_select1" value="5" checked />		
															周五
														</label>
														<label class="checkbox-inline">
															<input type="checkbox" class="J_week_select1" value="6" checked />		
															周六
														</label>
														<label class="checkbox-inline">
															<input type="checkbox" class="J_week_select1" value="0" checked />		
															周日
														</label>
													</div>
													<div class="row">
														<div class="col-xs-12">
															<div class="col-xs-12 col-sm-3">
																<div class="input-group">
																	<span class="input-group-addon">预定价</span>
																	<input type="text" class="form-control J_input_limit" name="price_cncn">
																</div>
															</div>
															<div class="col-xs-12 col-sm-3">
																<div class="input-group">
																	<span class="input-group-addon">预定儿童价</span>
																	<input type="text" class="form-control J_input_limit" name="price_cncn_child">
																</div>
															</div>
															<div class="col-xs-12 col-sm-3">
																<div class="input-group">
																	<span class="input-group-addon">地接价</span>
																	<input type="text" class="form-control J_input_limit" name="price_cncn">
																</div>
															</div>
															<div class="col-xs-12 col-sm-3">
																<div class="input-group">
																	<span class="input-group-addon">地接儿童价</span>
																	<input type="text" class="form-control J_input_limit" name="price_cncn_child">
																</div>
															</div>
														</div>
														<div class="col-xs-12">
															<a class="btn btn-link" id="J_batch_add_by_dateandweek">添加</a>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12" id="J_date_table"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-actions clearfix">
						<div class="col-sm-11 col-xs-12 col-md-offset-1">
							<button type="submit" class="btn btn-success">添加线路</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
require(["jquery", "util", "dateprice"], function($, util){
	$('select[name="type_id"]').change(function(){
		var val = $(this).val();
		if(val==''){
			return false;
		}
		$.post("<?php echo U('Line/getTypesItem');?>", { type_id: val}, typeItem, 'JSON');
		$.post("<?php echo U('Line/getTypeCates');?>", { type_id: val}, typeCate, 'JSON');
	});

	function typeItem(data){
		if(data.status == 0){
			util.error(data.msg ? data.msg : '获取5A失败');
		} else {
			data = data.data;
			var html = '<div class="list-group">';
			for(var i = 0, len = data.length; i < len; i ++){
				html += '<li class="list-group-item"><span class="list-title">'+data[i].title+'：</span>';
				if($.isArray(data[i].attr)){
					for(var j = 0, k = data[i].attr.length; j < k; j ++){
						html += '<label class="radio-inline"><input type="radio" name="typeitem['+data[i].id+']" value="'+data[i].attr[j].title+'">'+data[i].attr[j].title+'</label>';
					}
				}
				html += '</li>';
			}
			html += '</div>';
			$("#filter_container").html(html);
		}
	}

	function typeCate(data){
		if(data.status == 0){
			util.error(data.msg ? data.msg : '获取线路分类失败');
		} else {
			data = data.data;
			var opts = '<select name="cate_id" class="form-control">';
			opts += createOpts(data, 0);
			opts += '</select>';
			$('#cates_container').html(opts);
		}
	}

	function createOpts(options, lv){
		if(!$.isArray(options)){
			console.log(options)
			return '';
		}
		var opts = '';
		for(var i = 0, l = options.length; i < l; i ++){
			opts += '<option value="'+options[i].id+'">'+prefix(lv)+options[i].title+'</option>';
			if(options[i].child){
				opts += createOpts(options[i].child, lv + 1);
			}
		}
		return opts;
	}

	function prefix(lv){
		if(lv == 0){
			return '';
		} else {
			var str = '|';
			for(;lv > 0; lv --){
				str += '——';
			}
			return str;
		}
	}
	$('form.form').submit(function(){
		var prices = [];
		$('#J_date_table .month_row .bd li:not(.disable)').each(function(){
			var self = $(this);
			var youhui_price = self.data('youhui_price') || 0;
            var child_price = self.data('child_price') || 0;
            var dijie = self.data('dijie') || 0;
            var dijie_child = self.data('dijie_child') || 0;
            var kucun = self.data('kucun') || 0;
            var date = self.data('date');
            if(youhui_price <= 0){
            	return;
            }
            if(kucun == 0){
            	kucun = 9999;
            }
            prices.push({youhui_price: youhui_price, child_price: child_price, dijie: dijie, dijie_child: dijie_child, kucun: kucun, date: date});
		});
		$("input#date_price_data").val(JSON.stringify(prices));
	});
});
var days = 1, uet=[];
function addTrips(obj){
	require(["jquery"], function($){
		var tpl = '<div class="col-xs-12"><div class=" well">'
			+'<div class="form-group">'
				+'<label class="col-xs-12 col-sm-3 col-md-2 control-label">行程天数</label>'
				+'<div class="col-xs-12 col-sm-9">'
					+'<input type="text" name="trips[days]['+days+']" class="form-control" value="'+days+'" readonly="true">'
				+'</div>'
			+'</div>'
			+'<div class="form-group">'
				+'<label class="col-xs-12 col-sm-3 col-md-2 control-label">行程标题</label>'
				+'<div class="col-xs-12 col-sm-9">'
					+'<input type="text" name="trips[title]['+days+']" class="form-control">'
				+'</div>'
			+'</div>'
			+'<div class="form-group">'
				+'<label class="col-xs-12 col-sm-3 col-md-2 control-label">早餐</label>'
				+'<div class="col-xs-12 col-sm-9">'
					+'<input type="checkbox" name="trips[breakfast]['+days+']" class="ace ace-switch" value="1">'
					+'<span class="lbl"></span>'
				+'</div>'
			+'</div>'
			+'<div class="form-group">'
				+'<label class="col-xs-12 col-sm-3 col-md-2 control-label">中餐</label>'
				+'<div class="col-xs-12 col-sm-9">'
					+'<input type="checkbox" name="trips[lunch]['+days+']" class="ace ace-switch" value="1">'
					+'<span class="lbl"></span>'
				+'</div>'
			+'</div>'
			+'<div class="form-group">'
				+'<label class="col-xs-12 col-sm-3 col-md-2 control-label">晚餐</label>'
				+'<div class="col-xs-12 col-sm-9">'
					+'<input type="checkbox" name="trips[dinner]['+days+']" class="ace ace-switch" value="1">'
					+'<span class="lbl"></span>'
				+'</div>'
			+'</div>'
			+'<div class="form-group">'
				+'<label class="col-xs-12 col-sm-3 col-md-2 control-label">图片</label>'
				+'<div class="col-xs-12 col-sm-9">'
					+'<div class="input-group ">'
						+'<input type="text" class="form-control" readonly="readonly" value="" placeholder="批量上传图片" autocomplete="off">'
						+'<span class="input-group-btn">'
							+'<button class="btn btn-default btn-sm" type="button" onclick="uploadMultiImage(this, \'trips[imgs]['+days+']\');"  data-dir="line">选择图片</button> '
						+'</span>'
					+'</div> '
					+'<div class="input-group multi-img-details">'
					+'</div>'
				+'</div>'
			+'</div>'
			+'<div class="form-group">'
				+'<label class="col-xs-12 col-sm-3 col-md-2 control-label">行程详情</label>'
				+'<div class="col-xs-12 col-sm-9">'
				+'<script  type="text\/plain" id="trips_'+days+'" name="trips[contents]['+days+']" style="height:300px;width:980px"><\/script>'
				+' <script type="text\/javascript"> require(["baidueditor", "zeroclipboard", "bdlang"], function(UE, zcl){window.ZeroClipboard = zcl; uet['+days+'] = UE.getEditor("trips_'+days+'");});<\/script>'
				+'</div>'
			+'</div>'
		+'</div></div>';
		days ++; 
		$(tpl).insertBefore($(obj).parent());
	});
}

function delAllTrips(obj){
	require(["jquery"], function($){
		for(var i=1; i<days; i++){
			uet[i].destroy();
		}
		$(obj).parent().siblings('div').remove();
		days = 1;
	});
}

function delLastTrip(obj){
	require(['jquery'], function($){
		days --;
		uet[days].destroy();
		$(obj).parent().prev('div').remove();
	});
}
</script>
</div><!-- /.main-content -->
				<div class="ace-settings-container" id="ace-settings-container">
					<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
						<i class="icon-cog bigger-150"></i>
					</div>

					<div class="ace-settings-box" id="ace-settings-box">
						<div>
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="default" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; Choose Skin</span>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-navbar" />
							<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-sidebar" />
							<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-breadcrumbs" />
							<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" />
							<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-add-container" />
							<label class="lbl" for="ace-settings-add-container">
								Inside
								<b>.container</b>
							</label>
						</div>
					</div>
				</div><!-- /#ace-settings-container -->
			</div><!-- /.main-container-inner -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->
		<script type="text/javascript">
		require(['bootstrap'],function($){
			$('[data-toggle="tooltip"]').tooltip();
		})
		</script>
		
	</body>
</html>
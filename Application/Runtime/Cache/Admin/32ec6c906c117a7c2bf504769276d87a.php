<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo ($bar["curpos"]); ?> - <?php echo ($bar["menu"]); ?></title>
		<meta name="description" content="<?php echo ($cfgs["seodescription"]); ?>" />
		<meta name="keywords" content="<?php echo ($cfgs["seokeywords"]); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<script type="text/javascript" src="/Public/js/lib/require.js"></script>
		<script type="text/javascript" src="/Public/js/lib/config.js"></script>
		<link rel="stylesheet" href="/Public/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/Public/css/font-awesome.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-fonts.css" />
		<link rel="stylesheet" href="/Public/css/ace.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-skins.min.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="css/ace-ie.min.css" />
		  <script src="/Public/js/excanvas.min.js"></script>
		<![endif]-->
		<!-- inline styles related to this page -->
		<!-- ace settings handler -->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="/Public/js/html5shiv.js"></script>
		<script src="/Public/js/respond.min.js"></script>
		<![endif]-->
		<script type="text/javascript">
		if(navigator.appName == 'Microsoft Internet Explorer'){
			if(navigator.userAgent.indexOf("MSIE 5.0")>0 || 
				navigator.userAgent.indexOf("MSIE 6.0")>0 || 
				navigator.userAgent.indexOf("MSIE 7.0")>0) {
				alert('您使用的 IE 浏览器版本过低,无法获得最好的体验且可能出现无法预期的错误, 推荐使用 Chrome 浏览器或 IE8 及以上版本浏览器.');
			}
		}
		</script>
	</head>
	
	
	<body class="login-layout">
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center">
								<div class="row">
									<div class="col-xs-12 col-md-10">
										<div class="navbar navbar-default" style="background:transparent;">
											<div class="navbar-container">
												<div class="navbar-header">
													<a class="navbar-brand">
														<img src="<?php echo ($cfgs["logo"]); ?>" class="img-rounded img-thumbnail" style="height:45px;padding:0">
														<samll><?php echo ($cfgs["sitename"]); ?></samll>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="position-relative">
								<div id="login-box" class="login-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header blue lighter bigger">
												<i class="icon-coffee green"></i>
												请登录
											</h4>
											<div class="space-6"></div>
											<div class="has-error" id="login_err_tip">
												<span class="help-block"></span>
											</div>
											<form>
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" name="account" class="form-control" placeholder="账号/手机/邮箱" />
															<i class="icon-user"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" name="password" class="form-control" placeholder="请输入密码" />
															<i class="icon-lock"></i>
														</span>
													</label>

													<div class="space"></div>

													<div class="clearfix">
														<label class="inline">
															<input type="checkbox" class="ace" name="holder" value="1" />
															<span class="lbl">保持登录7天</span>
														</label>

														<button type="button" class="width-35 pull-right btn btn-sm btn-primary" id="dologin">
															<i class="icon-key"></i>
															登录
														</button>
													</div>

													<div class="space-4"></div>
												</fieldset>
											</form>
										</div><!-- /widget-main -->
									</div><!-- /widget-body -->
								</div><!-- /login-box -->
							</div><!-- /position-relative -->
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div>
		</div><!-- /.main-container -->
		<script type="text/javascript">
		var referr = '<?php echo ($uri); ?>';
		if(!referr && document.referrer){
			if(document.referrer.indexOf(document.location.protocol + '//' + document.location.host) === 0){
				referr = document.referrer;
			}
		}
		if(!referr){
			referr = "<?php echo U('Index/index');?>";
		}
		require(['jquery'], function($){
			$(document.body).keyup(function(e){
				if(e.keyCode == 13){
					$('#dologin').trigger('click');
				}
			});
			$('#dologin').click(function(){
				$(this).addClass('disabled').html('正在登录');
				$('#login_err_tip span').html('');
				var account = $('input[name="account"]').val();
				var password = $('input[name="password"]').val();
				if(!account){
					$('#login_err_tip span').html('登录名不能为空');
					$(this).removeClass('disabled').html('<i class="icon-key"></i>登录');
					return false;
				}
				if(account.length < 6 || account.length > 20){
					$('#login_err_tip span').html('账号格式错误');
					$(this).removeClass('disabled').html('<i class="icon-key"></i>登录');
					return false;
				}
				if(!password){
					$('#login_err_tip span').html('密码不能为空');
					$(this).removeClass('disabled').html('<i class="icon-key"></i>登录');
					return false;
				}
				if(!/^[^\/\\*\-#]{6,20}$/.test(password)){
					$('#login_err_tip span').html('密码格式错误');
					$(this).removeClass('disabled').html('<i class="icon-key"></i>登录');
					return false;
				}
				var $this = $(this);
				$.post(
					"<?php echo U('Login/doLogin');?>",
					{ account: account, password: password, holder: $('input[name="holder"]').prop('checked')?1:0},
					function(data){
						$this.removeClass('disabled');
						if(data.status == 1){
							$this.html('<i class="icon-ok"></i>登录成功');
							setTimeout(function(){
								location.href = referr;
							}, 500);
						} else {
							$this.html('<i class="icon-key"></i>登录');
							$('#login_err_tip span').html(data.msg);
						}
					},
					"json"
				);
			});
		});
		</script>
	
	</body>
</html>
<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo ($bar["curpos"]); ?> - <?php echo ($bar["menu"]); ?></title>
		<meta name="description" content="<?php echo ($cfgs["seodescription"]); ?>" />
		<meta name="keywords" content="<?php echo ($cfgs["seokeywords"]); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<script type="text/javascript" src="/Public/js/lib/require.js"></script>
		<script type="text/javascript" src="/Public/js/lib/config.js"></script>
		<link rel="stylesheet" href="/Public/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/Public/css/font-awesome.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-fonts.css" />
		<link rel="stylesheet" href="/Public/css/ace.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-skins.min.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="css/ace-ie.min.css" />
		  <script src="/Public/js/excanvas.min.js"></script>
		<![endif]-->
		<!-- inline styles related to this page -->
		<!-- ace settings handler -->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="/Public/js/html5shiv.js"></script>
		<script src="/Public/js/respond.min.js"></script>
		<![endif]-->
		<script type="text/javascript">
		if(navigator.appName == 'Microsoft Internet Explorer'){
			if(navigator.userAgent.indexOf("MSIE 5.0")>0 || 
				navigator.userAgent.indexOf("MSIE 6.0")>0 || 
				navigator.userAgent.indexOf("MSIE 7.0")>0) {
				alert('您使用的 IE 浏览器版本过低,无法获得最好的体验且可能出现无法预期的错误, 推荐使用 Chrome 浏览器或 IE8 及以上版本浏览器.');
			}
		}
		</script>
	</head>
	
	
<body>
	<div class="navbar navbar-default" id="navbar">
			<div class="navbar-container" id="navbar-container">
				<div class="navbar-header pull-left">
					<a href="<?php echo U('Index/index');?>" class="navbar-brand" style="padding:0px">
						<img src="<?php echo ($cfgs["logo"]); ?>" class="img-rounded img-thumbnail" style="height:45px;padding:0">
						<small>
							<?php echo ($cfgs["sitename"]); ?>后台管理系统
						</small>
					</a><!-- /.brand -->
				</div><!-- /.navbar-header -->

				<div class="navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="<?php echo ($_SESSION['admin']['avatar']); ?>" alt="<?php echo (session('admin_name')); ?>" />
								<span class="user-info">
									<small>欢迎使用<?php echo ($cfgs["sitename"]); ?>后台管理系统后台管理系统后台管理系统后台管理系统</small>
									<?php echo (session('admin_name')); ?>
								</span>
								<i class="icon-caret-down"></i>
							</a>
							<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="<?php echo U('Admin/profile');?>">
										<i class="icon-user"></i>
										个人资料
									</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="<?php echo U('Login/logout');?>">
										<i class="icon-off"></i>
										退出登录
									</a>
								</li>
							</ul>
						</li>
					</ul><!-- /.ace-nav -->
				</div><!-- /.navbar-header -->
			</div><!-- /.container -->
		</div>
		<div class="main-container" id="main-container">
			<div class="main-container-inner">
				<a class="menu-toggler" id="menu-toggler" href="#">
					<span class="menu-text"></span>
				</a>

				<div class="sidebar" id="sidebar">
					<script type="text/javascript">
					require(['jquery', 'acemy'], function($){
						try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
					});
					</script>
					<ul class="nav nav-list">
						<?php if(is_array($authMenus)): $i = 0; $__LIST__ = $authMenus;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu): $mod = ($i % 2 );++$i;?><li class="<?php echo ($menu["class"]); ?>">
							<a class="dropdown-toggle" href="<?php if(empty($menu["child"])): echo U($menu['url']); endif; ?>">
								<?php if(!empty($menu["icon"])): ?><i class="<?php echo ($menu["icon"]); ?>"></i><?php endif; ?>
								<span class="menu-text"><?php echo ($menu["title"]); ?></span>
							</a>
							<?php if(!empty($menu["child"])): ?><ul class="submenu">
							<?php if(is_array($menu["child"])): $i = 0; $__LIST__ = $menu["child"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li <?php if(!empty($vo["class"])): ?>class="<?php echo ($vo["class"]); ?>"<?php endif; ?>>
									<a href="<?php echo U($vo['url']);?>">
										<i class="icon-double-angle-right"></i>
										<?php echo ($vo["title"]); ?>
									</a>
								</li><?php endforeach; endif; else: echo "" ;endif; ?>
							</ul><?php endif; ?>
						</li><?php endforeach; endif; else: echo "" ;endif; ?>
					</ul><!-- /.nav-list -->

					<div class="sidebar-collapse" id="sidebar-collapse">
						<i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right" data-toggle="tooltip" data-placement="right" title="收起面板"></i>
					</div>

					<script type="text/javascript">
					require(['jquery', 'acemy'], function($){
						try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
					});
					</script>
				</div>
				<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
						require(['jquery', 'acemy'], function($){
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						});
						</script>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<?php echo U('Index/index');?>">首页</a>
							</li>
							<?php if(!empty($bar["menu"])): ?><li>
								<a href="<?php echo ($bar["url"]); ?>"><?php echo ($bar["menu"]); ?></a>
							</li><?php endif; ?>
							<li class="active"><?php echo ($bar["curpos"]); ?></li>
						</ul><!-- .breadcrumb -->
					</div>
<style>
select{padding:0;}
select option, select.form-control option {padding:0 4px;}
</style>
<div class="page-content">
	<div class="page-header" style="position:relative;">
		<h1>
			交通信息
			<small>
				<i class="icon-double-angle-right"></i>
				添加
				<a href="javascript:history.go(-1);" class="btn btn-link">返回列表</a>
			</small>
		</h1>
	</div>
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="table-responsive panel-body">
				<form name="form1" class="form form-horizontal" method="post" action="">
                    <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th width="6%">往/返</th>
                                        <th width="10%">交通方式</th>
                                        <th width="10%">出发地区</th>
                                        <th width="10%">到达地区</th>
                                        <th width="10%">班次</th>
                                        <th width="14%">出发时间</th>
                                        <th width="14%">到达时间</th>
                                        <th width="10%">方式</th>
                                        <th width="6%">排序</th>
                                        <th width="10%">操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="10">
                                            <a class="btn btn-link" onClick="addAttr(this,0);">
                                                <i class="icon-plus"></i>
                                                新增一行
                                            </a>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>

                    <div class="form-group">
						<label class="control-label col-xs-12 col-md-2 col-sm-3">&nbsp;</label>
						<div class="col-sm-9 col-xs-12">
							<button type="submit" class="btn btn-success">确认提交</button>
						</div>
				  	</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
function removeAttr(obj){
	require(['jquery'],function($){
		$(obj).parent().parent().fadeOut(function(){
			$(this).remove();
		})
	})
}
function addAttr(obj, idx){
	require(['jquery'], function($){
		var tpl = $('<tr><td><select name="roundtrip[]"><option value="往">往</option><option value="返">返</option></select></td><td><select name="trafficbs[]"><?php if(is_array($traffics)): $i = 0; $__LIST__ = $traffics;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data): $mod = ($i % 2 );++$i;?><option value="<?php echo ($data["id"]); ?>"><?php echo ($data["title"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?></select></td><td><input type="text" name="start_district[]" class="col-xs-10 col-sm-12"></td><td><input type="text" name="arrive_district[]" class="col-xs-10 col-sm-12"></td><td><input type="text" name="classes[]" class="col-xs-10 col-sm-12"></td><td><select name="start_hour[]"><option value="" selected></option><?php $__FOR_START_5661__=0;$__FOR_END_5661__=24;for($i=$__FOR_START_5661__;$i < $__FOR_END_5661__;$i+=1){ ?><option value="<?php if($i < 10 ): ?>0<?php endif; echo ($i); ?>"><?php if($i < 10 ): ?>0<?php endif; echo ($i); ?></option><?php } ?></select> : <select name="start_minute[]"><option value="" selected></option><?php $__FOR_START_28818__=0;$__FOR_END_28818__=60;for($i=$__FOR_START_28818__;$i < $__FOR_END_28818__;$i+=5){ ?><option value="<?php if($i < 10 ): ?>0<?php endif; echo ($i); ?>"><?php if($i < 10 ): ?>0<?php endif; echo ($i); ?></option><?php } ?></select></td><td><select name="arrive_hour[]"><option value="" selected></option><?php $__FOR_START_6755__=0;$__FOR_END_6755__=24;for($i=$__FOR_START_6755__;$i < $__FOR_END_6755__;$i+=1){ ?><option value="<?php if($i < 10 ): ?>0<?php endif; echo ($i); ?>"><?php if($i < 10 ): ?>0<?php endif; echo ($i); ?></option><?php } ?></select> : <select name="arrive_minute[]"><option value="" selected></option><?php $__FOR_START_5728__=0;$__FOR_END_5728__=60;for($i=$__FOR_START_5728__;$i < $__FOR_END_5728__;$i+=5){ ?><option value="<?php if($i < 10 ): ?>0<?php endif; echo ($i); ?>"><?php if($i < 10 ): ?>0<?php endif; echo ($i); ?></option><?php } ?></select></td><td><select name="directbs[]"><option value="" selected></option><?php if(is_array($directs)): $i = 0; $__LIST__ = $directs;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data3): $mod = ($i % 2 );++$i;?><option value="<?php echo ($data3["id"]); ?>"><?php echo ($data3["title"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?></select></td><td><input type="text" name="ordid[]" class="col-xs-10 col-sm-12"></td><td><a class="btn btn-link" onClick="removeAttr(this);"><i class="icon-remove"></i>删除</a></td></tr>');
		$(obj).parent().parent().parent().prev('tbody').append(tpl);
	})
}
</script>
</div><!-- /.main-content -->
			</div><!-- /.main-container-inner -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->
		<script type="text/javascript">
		require(['bootstrap'],function($){
			$('[data-toggle="tooltip"]').tooltip();
		})
		</script>
		
	</body>
</html>
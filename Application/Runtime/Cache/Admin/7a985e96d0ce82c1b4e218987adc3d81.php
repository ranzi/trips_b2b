<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title><?php echo ($bar["curpos"]); ?> - <?php echo ($cfgs["sitename"]); ?></title>
		<meta name="description" content="<?php echo ($cfgs["seodescription"]); ?>" />
		<meta name="keywords" content="<?php echo ($cfgs["seokeywords"]); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!-- basic styles -->
		<script type="text/javascript" src="/Public/js/lib/require.js"></script>
		<script type="text/javascript" src="/Public/js/lib/config.js"></script>
		<link rel="stylesheet" href="/Public/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/Public/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="css/font-awesome-ie7.min.css" />
		<![endif]-->
		<!-- page specific plugin styles -->
		<!-- fonts -->
		<link rel="stylesheet" href="/Public/css/ace-fonts.css" />
		<!-- ace styles -->
		<link rel="stylesheet" href="/Public/css/ace.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-skins.min.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="css/ace-ie.min.css" />
		  <script src="/Public/js/excanvas.min.js"></script>
		<![endif]-->
		
		<!-- inline styles related to this page -->
		<!-- ace settings handler -->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="/Public/js/html5shiv.js"></script>
		<script src="/Public/js/respond.min.js"></script>
		<![endif]-->
	</head>
	
	
<body>
<div class="navbar navbar-default" id="navbar">
			<script type="text/javascript">
			require(['jquery', 'acemy'], function($){
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			});
			</script>
			<div class="navbar-container" id="navbar-container">
				<div class="navbar-header pull-left">
					<a href="<?php echo U('Index/index');?>" class="navbar-brand" style="padding:0px">
						<img src="<?php echo ($cfgs["logo"]); ?>" class="img-rounded img-thumbnail" style="height:45px;padding:0">
						<small>
							<?php echo ($cfgs["sitename"]); ?>后台管理系统
						</small>
					</a><!-- /.brand -->
				</div><!-- /.navbar-header -->

				<div class="navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						<li class="grey">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="icon-tasks"></i>
								<span class="badge badge-grey">4</span>
							</a>

							<ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="icon-ok"></i>
									4 Tasks to complete
								</li>

								<li>
									<a href="#">
										<div class="clearfix">
											<span class="pull-left">Software Update</span>
											<span class="pull-right">65%</span>
										</div>

										<div class="progress progress-mini ">
											<div style="width:65%" class="progress-bar "></div>
										</div>
									</a>
								</li>

								<li>
									<a href="#">
										<div class="clearfix">
											<span class="pull-left">Hardware Upgrade</span>
											<span class="pull-right">35%</span>
										</div>

										<div class="progress progress-mini ">
											<div style="width:35%" class="progress-bar progress-bar-danger"></div>
										</div>
									</a>
								</li>

								<li>
									<a href="#">
										<div class="clearfix">
											<span class="pull-left">Unit Testing</span>
											<span class="pull-right">15%</span>
										</div>

										<div class="progress progress-mini ">
											<div style="width:15%" class="progress-bar progress-bar-warning"></div>
										</div>
									</a>
								</li>

								<li>
									<a href="#">
										<div class="clearfix">
											<span class="pull-left">Bug Fixes</span>
											<span class="pull-right">90%</span>
										</div>

										<div class="progress progress-mini progress-striped active">
											<div style="width:90%" class="progress-bar progress-bar-success"></div>
										</div>
									</a>
								</li>

								<li>
									<a href="#">
										See tasks with details
										<i class="icon-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>

						<li class="purple">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="icon-bell-alt icon-animated-bell"></i>
								<span class="badge badge-important">8</span>
							</a>

							<ul class="pull-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="icon-warning-sign"></i>
									8 Notifications
								</li>

								<li>
									<a href="#">
										<div class="clearfix">
											<span class="pull-left">
												<i class="btn btn-xs no-hover btn-pink icon-comment"></i>
												New Comments
											</span>
											<span class="pull-right badge badge-info">+12</span>
										</div>
									</a>
								</li>

								<li>
									<a href="#">
										<i class="btn btn-xs btn-primary icon-user"></i>
										Bob just signed up as an editor ...
									</a>
								</li>

								<li>
									<a href="#">
										<div class="clearfix">
											<span class="pull-left">
												<i class="btn btn-xs no-hover btn-success icon-shopping-cart"></i>
												New Orders
											</span>
											<span class="pull-right badge badge-success">+8</span>
										</div>
									</a>
								</li>

								<li>
									<a href="#">
										<div class="clearfix">
											<span class="pull-left">
												<i class="btn btn-xs no-hover btn-info icon-twitter"></i>
												Followers
											</span>
											<span class="pull-right badge badge-info">+11</span>
										</div>
									</a>
								</li>

								<li>
									<a href="#">
										See all notifications
										<i class="icon-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>

						<li class="green">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="icon-envelope icon-animated-vertical"></i>
								<span class="badge badge-success">5</span>
							</a>

							<ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="icon-envelope-alt"></i>
									5 Messages
								</li>

								<li>
									<a href="#">
										<img src="/Public/avatars/avatar.png" class="msg-photo" alt="Alex's Avatar" />
										<span class="msg-body">
											<span class="msg-title">
												<span class="blue">Alex:</span>
												Ciao sociis natoque penatibus et auctor ...
											</span>

											<span class="msg-time">
												<i class="icon-time"></i>
												<span>a moment ago</span>
											</span>
										</span>
									</a>
								</li>

								<li>
									<a href="#">
										<img src="/Public/avatars/avatar3.png" class="msg-photo" alt="Susan's Avatar" />
										<span class="msg-body">
											<span class="msg-title">
												<span class="blue">Susan:</span>
												Vestibulum id ligula porta felis euismod ...
											</span>

											<span class="msg-time">
												<i class="icon-time"></i>
												<span>20 minutes ago</span>
											</span>
										</span>
									</a>
								</li>

								<li>
									<a href="#">
										<img src="/Public/avatars/avatar4.png" class="msg-photo" alt="Bob's Avatar" />
										<span class="msg-body">
											<span class="msg-title">
												<span class="blue">Bob:</span>
												Nullam quis risus eget urna mollis ornare ...
											</span>

											<span class="msg-time">
												<i class="icon-time"></i>
												<span>3:15 pm</span>
											</span>
										</span>
									</a>
								</li>

								<li>
									<a href="inbox.html">
										See all messages
										<i class="icon-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>

						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="<?php echo ($_SESSION['admin']['avatar']); ?>" alt="<?php echo (session('admin_name')); ?>" />
								<span class="user-info">
									<small>欢迎使用<?php echo ($cfgs["sitename"]); ?>后台管理系统后台管理系统后台管理系统后台管理系统</small>
									<?php echo (session('admin_name')); ?>
								</span>
								<i class="icon-caret-down"></i>
							</a>
							<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="<?php echo U('Admin/profile');?>">
										<i class="icon-user"></i>
										个人资料
									</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="<?php echo U('Login/logout');?>">
										<i class="icon-off"></i>
										退出登录
									</a>
								</li>
							</ul>
						</li>
					</ul><!-- /.ace-nav -->
				</div><!-- /.navbar-header -->
			</div><!-- /.container -->
		</div>
		<div class="main-container" id="main-container">
			<script type="text/javascript">
			// require(['jquery', 'ace'], function($){
			// 	try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			// });
			</script>

			<div class="main-container-inner">
				<a class="menu-toggler" id="menu-toggler" href="#">
					<span class="menu-text"></span>
				</a>

				<div class="sidebar" id="sidebar">
					<script type="text/javascript">
					require(['jquery', 'acemy'], function($){
						try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
					});
					</script>

					<div class="sidebar-shortcuts" id="sidebar-shortcuts">
						<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
							<button class="btn btn-success">
								<i class="icon-signal"></i>
							</button>

							<button class="btn btn-info">
								<i class="icon-pencil"></i>
							</button>

							<button class="btn btn-warning">
								<i class="icon-group"></i>
							</button>

							<button class="btn btn-danger">
								<i class="icon-cogs"></i>
							</button>
						</div>

						<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
							<span class="btn btn-success"></span>

							<span class="btn btn-info"></span>

							<span class="btn btn-warning"></span>

							<span class="btn btn-danger"></span>
						</div>
					</div><!-- #sidebar-shortcuts -->

					<ul class="nav nav-list">
						<?php if(is_array($authMenus)): $i = 0; $__LIST__ = $authMenus;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu): $mod = ($i % 2 );++$i;?><li class="<?php echo ($menu["class"]); ?>">
							<a class="dropdown-toggle" href="<?php if(empty($menu["child"])): echo U($menu['url']); endif; ?>">
								<?php if(!empty($menu["icon"])): ?><i class="<?php echo ($menu["icon"]); ?>"></i><?php endif; ?>
								<span class="menu-text"><?php echo ($menu["title"]); ?></span>
							</a>
							<?php if(!empty($menu["child"])): ?><ul class="submenu">
							<?php if(is_array($menu["child"])): $i = 0; $__LIST__ = $menu["child"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li <?php if(!empty($vo["class"])): ?>class="<?php echo ($vo["class"]); ?>"<?php endif; ?>>
									<a href="<?php echo U($vo['url']);?>">
										<i class="icon-double-angle-right"></i>
										<?php echo ($vo["title"]); ?>
									</a>
								</li><?php endforeach; endif; else: echo "" ;endif; ?>
							</ul><?php endif; ?>
						</li><?php endforeach; endif; else: echo "" ;endif; ?>
					</ul><!-- /.nav-list -->

					<div class="sidebar-collapse" id="sidebar-collapse">
						<i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right" data-toggle="tooltip" data-placement="right" title="收起面板"></i>
					</div>

					<script type="text/javascript">
					require(['jquery', 'acemy'], function($){
						try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
					});
					</script>
				</div>
				<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
						require(['jquery', 'acemy'], function($){
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						});
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<?php echo U('Index/index');?>">首页</a>
							</li>
							<?php if(!empty($bar["menu"])): ?><li>
								<a href="<?php echo ($bar["url"]); ?>"><?php echo ($bar["menu"]); ?></a>
							</li><?php endif; ?>
							<li class="active"><?php echo ($bar["curpos"]); ?></li>
						</ul><!-- .breadcrumb -->

						<!-- <div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="icon-search nav-search-icon"></i>
								</span>
							</form>
						</div> --><!-- #nav-search -->
					</div>
<div class="page-content">
	<div class="panel panel-default">
		<div class="panel-heading">
			<?php echo ($types["title"]); ?>&nbsp;筛选项管理
		</div>
		<div class="panel-body">
			<div class="col-xs-12 col-sm-12">
				<form class="form form-horizontal" role="form" action="" method="POST">
					<?php if(!empty($list)): if(is_array($list)): $_k = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($_k % 2 );++$_k;?><input type="hidden" name="ids[<?php echo ($_k); ?>]" value="<?php echo ($item["id"]); ?>">
					<div class="form-group">
						<div class="col-xs-12">
							<h3 class="header smaller lighter green">
								筛选项
								<a class="btn btn-link" onClick="removeItem(this);">
									<i class="icon-trash"></i>
									删除筛选项
								</a>
							</h3>
							<div class="form-group">
								<label class="control-label col-xs-12 col-sm-3 col-md-2">名称</label>
								<div class="col-xs-12 col-sm-9">
									<input type="text" name="title[<?php echo ($_k); ?>]" value="<?php echo ($item["title"]); ?>" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-xs-12 col-sm-3 col-md-2">排序</label>
								<div class="col-xs-12 col-sm-9">
									<input type="text" name="ordid[<?php echo ($_k); ?>]" value="<?php echo ($item['ordid']); ?>" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-xs-12 col-sm-3 col-md-2">状态</label>
								<div class="col-xs-12 col-sm-9">
									<input type="checkbox" name="status[<?php echo ($_k); ?>]" <?php if(($item["status"]) == "1"): ?>checked="checked"<?php endif; ?> value="1" class="ace ace-switch">
									<span class="lbl"></span>
								</div>
							</div>
						</div>
						<div class="col-xs-12">
							<h3 class="header smaller lighter green">属性</h3>
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th class="width-30">排序</th>
											<th class="width-60">名称</th>
											<th class="width-10">操作</th>
										</tr>
									</thead>
									<tbody>
										<?php if(is_array($item["attr"])): $i = 0; $__LIST__ = $item["attr"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
											<td><input type="text" name="attr[ordid][<?php echo ($_k); ?>][]" value="<?php echo ($vo["ordid"]); ?>" class="form-control"></td>
											<td><input type="text" name="attr[title][<?php echo ($_k); ?>][]" value="<?php echo ($vo["title"]); ?>" class="form-control"></td>
											<td><a class="btn btn-link" onClick="removeAttr(this);"><i class="icon-remove"></i>删除</a></td>
										</tr><?php endforeach; endif; else: echo "" ;endif; ?>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="3">
												<a class="btn btn-link" onClick="addAttr(this,0);">
													<i class="icon-plus"></i>
													添加
												</a>
											</td>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="text-right col-xs-12 row">
							<a class="btn btn-link" onClick="addItem(this);">
								<i class="icon-plus"></i>
								添加筛选项
							</a>
						</div>
					</div><?php endforeach; endif; else: echo "" ;endif; ?>
					<?php else: ?>
					<div class="form-group">
						<div class="col-xs-12">
							<h3 class="header smaller lighter green">
								筛选项
								<a class="btn btn-link" onClick="removeItem(this);">
									<i class="icon-trash"></i>
									删除筛选项
								</a>
							</h3>
							<div class="form-group">
								<label class="control-label col-xs-12 col-sm-3 col-md-2">名称</label>
								<div class="col-xs-12 col-sm-9">
									<input type="text" name="title[0]" value="" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-xs-12 col-sm-3 col-md-2">排序</label>
								<div class="col-xs-12 col-sm-9">
									<input type="text" name="ordid[0]" value="" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-xs-12 col-sm-3 col-md-2">状态</label>
								<div class="col-xs-12 col-sm-9">
									<input type="checkbox" name="status[0]" checked="checked" value="1" class="ace ace-switch">
									<span class="lbl"></span>
								</div>
							</div>
						</div>
						<div class="col-xs-12">
							<h3 class="header smaller lighter green">属性</h3>
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th class="width-30">排序</th>
											<th class="width-60">名称</th>
											<th class="width-10">操作</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><input type="text" name="attr[ordid][0][]" class="form-control"></td>
											<td><input type="text" name="attr[title][0][]" class="form-control"></td>
											<td><a class="btn btn-link" onClick="removeAttr(this);"><i class="icon-remove"></i>删除</a></td>
										</tr>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="3">
												<a class="btn btn-link" onClick="addAttr(this,0);">
													<i class="icon-plus"></i>
													添加
												</a>
											</td>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="text-right col-xs-12 row">
							<a class="btn btn-link" onClick="addItem(this);">
								<i class="icon-plus"></i>
								添加筛选项
							</a>
						</div>
					</div><?php endif; ?>
					<div class="clearfix form-actions">
						<input type="hidden" name="type_id" value="<?php echo ($types["id"]); ?>">
						<button class="btn btn-info" type="submit">
							<i class="icon-ok"></i>
							保存
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
function removeAttr(obj){
	require(['jquery'],function($){
		$(obj).parent().parent().fadeOut(function(){
			$(this).remove();
		})
	})
}
function addAttr(obj, idx){
	require(['jquery'], function($){
		var tpl = $('<tr><td><input type="text" name="attr[ordid]['+idx+'][]" class="form-control"></td><td><input type="text" name="attr[title]['+idx+'][]" class="form-control"></td><td><a class="btn btn-link" onClick="removeAttr(this);"><i class="icon-remove"></i>删除</a></td></tr>');
		$(obj).parent().parent().parent().prev('tbody').append(tpl);
	})
}
var index = <?php echo (count($list)); ?>+1;
function addItem(obj){
	require(['jquery'], function($){
		var tpl = '<div class="form-group">'
					+'<div class="col-xs-12">'
						+'<h3 class="header smaller lighter green">'
							+'筛选项'
							+'<a class="btn btn-link" onClick="removeItem(this);">'
								+'<i class="icon-trash"></i>'
								+'删除筛选项'
							+'</a>'
						+'</h3>'
						+'<div class="form-group">'
							+'<label class="control-label col-xs-12 col-sm-3 col-md-2">名称</label>'
							+'<div class="col-xs-12 col-sm-9">'
								+'<input type="text" name="title['+index+']" value="" class="form-control">'
							+'</div>'
						+'</div>'
						+'<div class="form-group">'
							+'<label class="control-label col-xs-12 col-sm-3 col-md-2">排序</label>'
							+'<div class="col-xs-12 col-sm-9">'
								+'<input type="text" name="ordid['+index+']" value="" class="form-control">'
							+'</div>'
						+'</div>'
						+'<div class="form-group">'
							+'<label class="control-label col-xs-12 col-sm-3 col-md-2">状态</label>'
							+'<div class="col-xs-12 col-sm-9">'
								+'<input type="checkbox" name="status['+index+']" checked="checked" value="1" class="ace ace-switch">'
								+'<span class="lbl"></span>'
							+'</div>'
						+'</div>'
					+'</div>'
					+'<div class="col-xs-12">'
						+'<h3 class="header smaller lighter green">属性</h3>'
						+'<div class="table-responsive">'
							+'<table class="table table-striped table-bordered table-hover">'
								+'<thead>'
									+'<tr>'
										+'<th class="width-30">排序</th>'
										+'<th class="width-60">名称</th>'
										+'<th class="width-10">操作</th>'
									+'</tr>'
								+'</thead>'
								+'<tbody>'
									+'<tr>'
										+'<td><input type="text" name="attr[ordid]['+index+'][]" class="form-control"></td>'
										+'<td><input type="text" name="attr[title]['+index+'][]" class="form-control"></td>'
										+'<td><a class="btn btn-link" onClick="removeAttr(this);"><i class="icon-remove"></i>删除</a></td>'
									+'</tr>'
								+'</tbody>'
								+'<tfoot>'
									+'<tr>'
										+'<td colspan="3">'
											+'<a class="btn btn-link" onClick="addAttr(this,'+index+');">'
												+'<i class="icon-plus"></i>'
												+'添加'
											+'</a>'
										+'</td>'
									+'</tr>'
								+'</tfoot>'
							+'</table>'
						+'</div>'
					+'</div>'
				+'</div>';
		$(tpl).insertBefore($(obj).parent().parent());
		index ++;
	})
}
function removeItem(obj){
	require(['jquery'],function($){
		$(obj).parent().parent().parent().slideUp(function(){
			$(this).remove();
		});
	})
}
</script>
</div><!-- /.main-content -->
				<div class="ace-settings-container" id="ace-settings-container">
					<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
						<i class="icon-cog bigger-150"></i>
					</div>

					<div class="ace-settings-box" id="ace-settings-box">
						<div>
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="default" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; Choose Skin</span>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-navbar" />
							<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-sidebar" />
							<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-breadcrumbs" />
							<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" />
							<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-add-container" />
							<label class="lbl" for="ace-settings-add-container">
								Inside
								<b>.container</b>
							</label>
						</div>
					</div>
				</div><!-- /#ace-settings-container -->
			</div><!-- /.main-container-inner -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->
		<script type="text/javascript">
		require(['bootstrap'],function($){
			$('[data-toggle="tooltip"]').tooltip();
		})
		</script>
		
	</body>
</html>
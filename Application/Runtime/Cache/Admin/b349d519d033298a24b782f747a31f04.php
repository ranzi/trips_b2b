<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo ($bar["curpos"]); ?> - <?php echo ($bar["menu"]); ?></title>
		<meta name="description" content="<?php echo ($cfgs["seodescription"]); ?>" />
		<meta name="keywords" content="<?php echo ($cfgs["seokeywords"]); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<script type="text/javascript" src="/Public/js/lib/require.js"></script>
		<script type="text/javascript" src="/Public/js/lib/config.js"></script>
		<link rel="stylesheet" href="/Public/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/Public/css/font-awesome.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-fonts.css" />
		<link rel="stylesheet" href="/Public/css/ace.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-skins.min.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="css/ace-ie.min.css" />
		  <script src="/Public/js/excanvas.min.js"></script>
		<![endif]-->
		<!-- inline styles related to this page -->
		<!-- ace settings handler -->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="/Public/js/html5shiv.js"></script>
		<script src="/Public/js/respond.min.js"></script>
		<![endif]-->
		<script type="text/javascript">
		if(navigator.appName == 'Microsoft Internet Explorer'){
			if(navigator.userAgent.indexOf("MSIE 5.0")>0 || 
				navigator.userAgent.indexOf("MSIE 6.0")>0 || 
				navigator.userAgent.indexOf("MSIE 7.0")>0) {
				alert('您使用的 IE 浏览器版本过低,无法获得最好的体验且可能出现无法预期的错误, 推荐使用 Chrome 浏览器或 IE8 及以上版本浏览器.');
			}
		}
		</script>
	</head>
	
	
<style type="text/css">
.main-content{margin-left: 0;}
</style>
<div class="navbar navbar-default" id="navbar">
	<div class="navbar-container" id="navbar-container">
		<div class="navbar-header pull-left">
			<a class="navbar-brand">供应商:<?php echo ($cfg["sitename"]); ?></a>
		</div>
		<div class="navbar-header pull-right" role="navigation">
			<ul class="nav ace-nav">
				<li class="light-blue">
					<a data-toggle="dropdown" href="#" class="dropdown-toggle"><i class="icon-tasks"></i>在线客服</a>
					<ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
						<li><a href="#">877395880</a></li>
					</ul>
				</li>
				<li class="light-blue">
					<a data-toggle="dropdown" href="#" class="dropdown-toggle"><i class="icon-phone"></i>联系方式</a>
					<ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
						<li><a href="#">qiisyhx：13108661515</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="main-container">
	<div class="main-container-inner">
		<div class="main-content">
			<div class="page-content">
				<div class="col-xs-12">
					<div class="panel panel-success">
						<div class="panel-body">
							<h3><?php echo ($line["title"]); ?></h3>
							<div class="col-xs-12">
								<div class="col-xs-4">
									<div class="form-group">
										<a class="btn btn-link">线路编号</a>：<span class="form-control-static"><?php echo ($line["id"]); ?></span>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="form-group">
										<a class="btn btn-link">出团日期</a>：<span class="form-control-static"><?php echo (date("Y-m-d",$expand["dateline"])); ?></span>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="form-group">
										&nbsp;
									</div>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="col-xs-4">
									<div class="form-group">
										<a class="btn btn-link">旅游天数</a>：<span class="form-control-static"><?php echo ($line["days"]); ?></span>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="form-group">
										<a class="btn btn-link">集合时间</a>：<span class="form-control-static"><?php echo (date("Y-m-d H:i",$expand["dateline"])); ?></span>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="form-group">
										<a class="btn btn-link">送团电话</a>：<span class="form-control-static"><?php echo ($expand["open_mobile"]); ?></span>
									</div>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="col-xs-4">
									<div class="form-group">
										<a class="btn btn-link">集合地点</a>：<span class="form-control-static"><?php echo ($expand["place"]); ?></span>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="form-group">
										<a class="btn btn-link">送团标志</a>：<span class="form-control-static"><?php echo ($expand["together"]); ?></span>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="form-group">
										<a class="btn btn-link">送团人</a>：<span class="form-control-static"><?php echo ($expand["local_man"]); ?></span>
									</div>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="col-xs-12">
									<div class="form-group">
										<a class="btn btn-link">线路说明</a>
										<p><?php echo ($expand["feature"]); ?></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12">
					<a class="btn btn-info" style="border:0;">普通行程</a>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover dataTable">
							<thead>
								<tr>
									<th>日期</th>
									<th>行程</th>
									<th>图片</th>
									<th>餐</th>
									<th>酒店</th>
								</tr>
							</thead>
							<tbody>
								<?php if(is_array($trips)): $day = 0; $__LIST__ = $trips;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($day % 2 );++$day;?><tr class="<?php if(($mod) == "0"): ?>odd<?php else: ?>even<?php endif; ?>">
									<td>
										<span class="label label-lg label-primary arrowed-right">D<?php echo ($day); ?></span>
									</td>
									<td><?php echo ($vo["contents"]); ?></td>
									<td>
										<div class="col-xs-12" style="width:100px;">
											<div class="row-fluid">
												<ul class="ace-thumbnails">
													<?php if(is_array($vo["imgs"])): $ii = 0; $__LIST__ = $vo["imgs"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vi): $mod = ($ii % 2 );++$ii;?><li <?php if(($ii) != "1"): ?>style="display:none;"<?php endif; ?>>
														<a href="<?php echo ($vi["src"]); ?>" data-rel="colorbox" class="cboxElement">
															<img src="<?php echo ($vi["thumb"]); ?>" width="80" />
														</a>
													</li><?php endforeach; endif; else: echo "" ;endif; ?>
												</ul>
											</div>
										</div>
									</td>
									<td>
										<?php if(($vo["breakfast"]) == "1"): ?><span class="badge badge-info">早</span><br/><?php endif; ?>
										<?php if(($vo["lunch"]) == "1"): ?><span class="badge badge-info">中</span><br/><?php endif; ?>
										<?php if(($vo["dinner"]) == "1"): ?><span class="badge badge-info">晚</span><br/><?php endif; ?>
									</td>
									<td><?php echo ($vo["hotel"]); ?></td>
								</tr><?php endforeach; endif; else: echo "" ;endif; ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="space-6"></div>
					<a class="btn btn-info" style="border:0;">相关事宜</a>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover dataTable">
							<tbody>
								<tr>
									<th class="width-20 text-right">费用包含</th>
									<td><?php echo ($expand["feeinclude"]); ?></td>
								</tr>
								<tr>
									<th class="width-20 text-right">费用不包含</th>
									<td><?php echo ($expand["feeexclude"]); ?></td>
								</tr>
								<tr>
									<th class="width-20 text-right">儿童安排</th>
									<td><?php echo ($expand["children"]); ?></td>
								</tr>
								<tr>
									<th class="width-20 text-right">购物安排</th>
									<td><?php echo ($expand["shopping"]); ?></td>
								</tr>
								<tr>
									<th class="width-20 text-right">可选项目</th>
									<td><?php echo ($expand["optional"]); ?></td>
								</tr>
								<tr>
									<th class="width-20 text-right">赠送项目</th>
									<td><?php echo ($expand["gift"]); ?></td>
								</tr>
								<tr>
									<th class="width-20 text-right">注意事项</th>
									<td><?php echo ($expand["notice"]); ?></td>
								</tr>
								<tr>
									<th class="width-20 text-right">温馨提醒</th>
									<td><?php echo ($expand["tips"]); ?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="space-8"></div>
					<div class="text-center">&copy;-2015 All Right Reserved</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"> 
require(["colorbox","acemy"],function($){
	var colorbox_params = {
		reposition:true,
		scalePhotos:true,
		scrolling:false,
		previous:'<i class="icon-arrow-left"></i>',
		next:'<i class="icon-arrow-right"></i>',
		close:'&times;',
		current:'{current} / {total}',
		maxWidth:'100%',
		maxHeight:'100%',
		onOpen:function(){
			document.body.style.overflow = 'hidden';
		},
		onClosed:function(){
			document.body.style.overflow = 'auto';
		},
		onComplete:function(){
			$.colorbox.resize();
		}
	};
	$('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
});
</script>
	
	</body>
</html>
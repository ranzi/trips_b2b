<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title><?php echo ($bar["curpos"]); ?> - <?php echo ($cfgs["sitename"]); ?></title>
		<meta name="description" content="<?php echo ($cfgs["seodescription"]); ?>" />
		<meta name="keywords" content="<?php echo ($cfgs["seokeywords"]); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!-- basic styles -->
		<script type="text/javascript" src="/Public/js/lib/require.js"></script>
		<script type="text/javascript" src="/Public/js/lib/config.js"></script>
		<link rel="stylesheet" href="/Public/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/Public/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="css/font-awesome-ie7.min.css" />
		<![endif]-->
		<!-- page specific plugin styles -->
		<!-- fonts -->
		<link rel="stylesheet" href="/Public/css/ace-fonts.css" />
		<!-- ace styles -->
		<link rel="stylesheet" href="/Public/css/ace.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-skins.min.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="css/ace-ie.min.css" />
		  <script src="/Public/js/excanvas.min.js"></script>
		<![endif]-->
		
		<!-- inline styles related to this page -->
		<!-- ace settings handler -->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="/Public/js/html5shiv.js"></script>
		<script src="/Public/js/respond.min.js"></script>
		<![endif]-->
	</head>
	
	
<body>
<div class="navbar navbar-default" id="navbar">
			<script type="text/javascript">
			require(['jquery', 'acemy'], function($){
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			});
			</script>
			<div class="navbar-container" id="navbar-container">
				<div class="navbar-header pull-left">
					<a href="<?php echo U('Index/index');?>" class="navbar-brand" style="padding:0px">
						<img src="<?php echo ($cfgs["logo"]); ?>" class="img-rounded img-thumbnail" style="height:45px;padding:0">
						<small>
							<?php echo ($cfgs["sitename"]); ?>后台管理系统
						</small>
					</a><!-- /.brand -->
				</div><!-- /.navbar-header -->

				<div class="navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						<li class="grey">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="icon-tasks"></i>
								<span class="badge badge-grey">4</span>
							</a>

							<ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="icon-ok"></i>
									4 Tasks to complete
								</li>

								<li>
									<a href="#">
										<div class="clearfix">
											<span class="pull-left">Software Update</span>
											<span class="pull-right">65%</span>
										</div>

										<div class="progress progress-mini ">
											<div style="width:65%" class="progress-bar "></div>
										</div>
									</a>
								</li>

								<li>
									<a href="#">
										<div class="clearfix">
											<span class="pull-left">Hardware Upgrade</span>
											<span class="pull-right">35%</span>
										</div>

										<div class="progress progress-mini ">
											<div style="width:35%" class="progress-bar progress-bar-danger"></div>
										</div>
									</a>
								</li>

								<li>
									<a href="#">
										<div class="clearfix">
											<span class="pull-left">Unit Testing</span>
											<span class="pull-right">15%</span>
										</div>

										<div class="progress progress-mini ">
											<div style="width:15%" class="progress-bar progress-bar-warning"></div>
										</div>
									</a>
								</li>

								<li>
									<a href="#">
										<div class="clearfix">
											<span class="pull-left">Bug Fixes</span>
											<span class="pull-right">90%</span>
										</div>

										<div class="progress progress-mini progress-striped active">
											<div style="width:90%" class="progress-bar progress-bar-success"></div>
										</div>
									</a>
								</li>

								<li>
									<a href="#">
										See tasks with details
										<i class="icon-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>

						<li class="purple">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="icon-bell-alt icon-animated-bell"></i>
								<span class="badge badge-important">8</span>
							</a>

							<ul class="pull-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="icon-warning-sign"></i>
									8 Notifications
								</li>

								<li>
									<a href="#">
										<div class="clearfix">
											<span class="pull-left">
												<i class="btn btn-xs no-hover btn-pink icon-comment"></i>
												New Comments
											</span>
											<span class="pull-right badge badge-info">+12</span>
										</div>
									</a>
								</li>

								<li>
									<a href="#">
										<i class="btn btn-xs btn-primary icon-user"></i>
										Bob just signed up as an editor ...
									</a>
								</li>

								<li>
									<a href="#">
										<div class="clearfix">
											<span class="pull-left">
												<i class="btn btn-xs no-hover btn-success icon-shopping-cart"></i>
												New Orders
											</span>
											<span class="pull-right badge badge-success">+8</span>
										</div>
									</a>
								</li>

								<li>
									<a href="#">
										<div class="clearfix">
											<span class="pull-left">
												<i class="btn btn-xs no-hover btn-info icon-twitter"></i>
												Followers
											</span>
											<span class="pull-right badge badge-info">+11</span>
										</div>
									</a>
								</li>

								<li>
									<a href="#">
										See all notifications
										<i class="icon-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>

						<li class="green">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="icon-envelope icon-animated-vertical"></i>
								<span class="badge badge-success">5</span>
							</a>

							<ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="icon-envelope-alt"></i>
									5 Messages
								</li>

								<li>
									<a href="#">
										<img src="/Public/avatars/avatar.png" class="msg-photo" alt="Alex's Avatar" />
										<span class="msg-body">
											<span class="msg-title">
												<span class="blue">Alex:</span>
												Ciao sociis natoque penatibus et auctor ...
											</span>

											<span class="msg-time">
												<i class="icon-time"></i>
												<span>a moment ago</span>
											</span>
										</span>
									</a>
								</li>

								<li>
									<a href="#">
										<img src="/Public/avatars/avatar3.png" class="msg-photo" alt="Susan's Avatar" />
										<span class="msg-body">
											<span class="msg-title">
												<span class="blue">Susan:</span>
												Vestibulum id ligula porta felis euismod ...
											</span>

											<span class="msg-time">
												<i class="icon-time"></i>
												<span>20 minutes ago</span>
											</span>
										</span>
									</a>
								</li>

								<li>
									<a href="#">
										<img src="/Public/avatars/avatar4.png" class="msg-photo" alt="Bob's Avatar" />
										<span class="msg-body">
											<span class="msg-title">
												<span class="blue">Bob:</span>
												Nullam quis risus eget urna mollis ornare ...
											</span>

											<span class="msg-time">
												<i class="icon-time"></i>
												<span>3:15 pm</span>
											</span>
										</span>
									</a>
								</li>

								<li>
									<a href="inbox.html">
										See all messages
										<i class="icon-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>

						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="<?php echo ($_SESSION['admin']['avatar']); ?>" alt="<?php echo (session('admin_name')); ?>" />
								<span class="user-info">
									<small>欢迎使用<?php echo ($cfgs["sitename"]); ?>后台管理系统后台管理系统后台管理系统后台管理系统</small>
									<?php echo (session('admin_name')); ?>
								</span>
								<i class="icon-caret-down"></i>
							</a>
							<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="<?php echo U('Admin/profile');?>">
										<i class="icon-user"></i>
										个人资料
									</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="<?php echo U('Login/logout');?>">
										<i class="icon-off"></i>
										退出登录
									</a>
								</li>
							</ul>
						</li>
					</ul><!-- /.ace-nav -->
				</div><!-- /.navbar-header -->
			</div><!-- /.container -->
		</div>
		<div class="main-container" id="main-container">
			<script type="text/javascript">
			// require(['jquery', 'ace'], function($){
			// 	try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			// });
			</script>

			<div class="main-container-inner">
				<a class="menu-toggler" id="menu-toggler" href="#">
					<span class="menu-text"></span>
				</a>

				<div class="sidebar" id="sidebar">
					<script type="text/javascript">
					require(['jquery', 'acemy'], function($){
						try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
					});
					</script>

					<div class="sidebar-shortcuts" id="sidebar-shortcuts">
						<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
							<button class="btn btn-success">
								<i class="icon-signal"></i>
							</button>

							<button class="btn btn-info">
								<i class="icon-pencil"></i>
							</button>

							<button class="btn btn-warning">
								<i class="icon-group"></i>
							</button>

							<button class="btn btn-danger">
								<i class="icon-cogs"></i>
							</button>
						</div>

						<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
							<span class="btn btn-success"></span>

							<span class="btn btn-info"></span>

							<span class="btn btn-warning"></span>

							<span class="btn btn-danger"></span>
						</div>
					</div><!-- #sidebar-shortcuts -->

					<ul class="nav nav-list">
						<?php if(is_array($authMenus)): $i = 0; $__LIST__ = $authMenus;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu): $mod = ($i % 2 );++$i;?><li class="<?php echo ($menu["class"]); ?>">
							<a class="dropdown-toggle" href="<?php if(empty($menu["child"])): echo U($menu['url']); endif; ?>">
								<?php if(!empty($menu["icon"])): ?><i class="<?php echo ($menu["icon"]); ?>"></i><?php endif; ?>
								<span class="menu-text"><?php echo ($menu["title"]); ?></span>
							</a>
							<?php if(!empty($menu["child"])): ?><ul class="submenu">
							<?php if(is_array($menu["child"])): $i = 0; $__LIST__ = $menu["child"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li <?php if(!empty($vo["class"])): ?>class="<?php echo ($vo["class"]); ?>"<?php endif; ?>>
									<a href="<?php echo U($vo['url']);?>">
										<i class="icon-double-angle-right"></i>
										<?php echo ($vo["title"]); ?>
									</a>
								</li><?php endforeach; endif; else: echo "" ;endif; ?>
							</ul><?php endif; ?>
						</li><?php endforeach; endif; else: echo "" ;endif; ?>
					</ul><!-- /.nav-list -->

					<div class="sidebar-collapse" id="sidebar-collapse">
						<i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right" data-toggle="tooltip" data-placement="right" title="收起面板"></i>
					</div>

					<script type="text/javascript">
					require(['jquery', 'acemy'], function($){
						try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
					});
					</script>
				</div>
				<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
						require(['jquery', 'acemy'], function($){
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						});
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<?php echo U('Index/index');?>">首页</a>
							</li>
							<?php if(!empty($bar["menu"])): ?><li>
								<a href="<?php echo ($bar["url"]); ?>"><?php echo ($bar["menu"]); ?></a>
							</li><?php endif; ?>
							<li class="active"><?php echo ($bar["curpos"]); ?></li>
						</ul><!-- .breadcrumb -->

						<!-- <div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="icon-search nav-search-icon"></i>
								</span>
							</form>
						</div> --><!-- #nav-search -->
					</div>
<div class="page-content">
	<!-- <div class="page-header" style="position:relative;">
		<h1>
			网站设置
			<small>
				<i class="icon-double-angle-right"></i>
				网站设置
			</small>
		</h1>
	</div> -->
	<div class="row col-xs-12">
		<div class="main">
			<!-- <div class="panel panel-default">
				<div class="panel-heading">网站基础设置</div>
				<div class="panel-body"> -->
					<form class="form form-horizontal" role="form" method="post" action="<?php echo U('Syscfg/save');?>">
						<div class="row col-xs-12 col-md-12 col-sm-12">
							<div class="tabtab">
								<ul class="nav nav-tabs">
									<li class="active">
										<a href="#settings-base" data-toggle="tab">基础设置</a>
									</li>
									<li>
										<a href="#settings-image" data-toggle="tab">图片设置</a>
									</li>
									<li>
										<a href="#settings-advance" data-toggle="tab">高级设置</a>
									</li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="settings-base">
										<div class="form-group">
											<label class="col-xs-12 col-sm-3 col-md-2 control-label">网站名称</label>
											<div class="col-xs-12 col-sm-9">
												<input type="text" class="form-control" name="sitename" value="<?php echo ($cfgs["sitename"]); ?>" >
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-12 col-sm-3 col-md-2 control-label">网站链接</label>
											<div class="col-xs-12 col-sm-9">
												<input type="text" class="form-control" name="domain" value="<?php echo ($cfgs["domain"]); ?>" >
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-12 col-sm-3 col-md-2 control-label">网站LOGO</label>
											<div class="col-xs-12 col-sm-9">
												<div class="input-group "> <input type="text" name="logo" value="/uploads/logo/20150810/55c819ad32778_150x150.jpg" class="form-control" autocomplete="off" readonly="true"> <span class="input-group-btn"> <button class="btn btn-default btn-sm" type="button" onclick="showImageDialog(this);" data-nowater="1" data-dir="logo" data-size="150x150">选择图片</button> </span> </div> <div class="input-group " style=""> <img src="/uploads/logo/20150810/55c819ad32778_150x150.jpg" onerror="this.src='/Public/images/nopic.jpg'; this.title='图片未找到.'" class="img-responsive img-thumbnail" style="max-width:150px;" /> </div><script type="text/javascript">
            function showImageDialog(ele){
                require(["jquery", "bootstrap", "bootbox", "filestyle"], function($){
                    var btn = $(ele);
                    var dialog = bootbox.dialog({
                        message: '<iframe width="0" height="0" name="__image_file_uploader" style="display:none;"></iframe>'
                        +'<ul class="nav nav-tabs">'
                        +'<li class="active"><a data-toggle="tab" href="#local">本地图片</a></li>'
                        +'<li> <a data-toggle="tab" href="#remote">远程图片 </a> </li>'
                        +'</ul>'
                        +'<div class="tab-content">'
                        +'<div id="local" class="tab-pane in active"> '
                        +'<form action="/index.php/Admin/Base/frameUpload/callback/uploadLocalPic" enctype="multipart/form-data" method="post" target="__image_file_uploader">'
                        +'<input type="file" name="upfile" >'
                        +'<input type="hidden" name="dir" value="">'
                        +'<input type="hidden" name="size" value="">'
                        +'<input type="hidden" name="nowater" value="">'
                        +'</form></div>'
                        +' <div id="remote" class="tab-pane">'
                        +'<form action="/index.php/Admin/Base/frameUpload/callback/uploadLocalPic" method="post" target="__image_file_uploader">'
                        +'<input type="txt" name="url" class="form-control" placeholder="请输入图片URL" >'
                        +'<input type="hidden" name="dir" value="">'
                        +'<input type="hidden" name="size" value="">'
                        +'<input type="hidden" name="nowater" value="">'
                        +'</form></div> </div>',
                        title: "上传图片",
                        buttons:{
                            "cancel": {
                                label: "取消",
                                className: "btn btn-default"
                            },
                            "ok":{
                                label: "确定",
                                className: "btn btn-success",
                                callback: function(){
                                    var form;
                                    if(dialog.find(".nav.nav-tabs li").eq(0).hasClass("active")){
                                        form=dialog.find("form")[0];
                                    } else {
                                        form=dialog.find("form")[1];
                                    }
                                    $(form).find('input[name="dir"]').val(btn.attr("data-dir"));
                                    $(form).find('input[name="size"]').val(btn.attr("data-size"));
                                    $(form).find('input[name="nowater"]').val(btn.attr("data-nowater"));
                                    form.submit();
                                    return false;
                                }
                            }
                        }
                    });
                    dialog.find(":file").filestyle({buttonText: "选择图片", buttonName: "btn-sm"});
                    window.uploadLocalPic = function(data){
                        if(data.status == 1){
                            btn.parent().parent().next().children().attr("src", data.file);
                            btn.parent().prev().val(data.file); dialog.modal("hide");
                        } else {
                            alert(data.msg);
                        }
                    }
                });
            } </script>
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-12 col-sm-3 col-md-2 control-label">SEO标题</label>
											<div class="col-xs-12 col-sm-9">
												<input type="text" class="form-control" name="seotitle" value="<?php echo ($cfgs["seotitle"]); ?>" >
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-12 col-sm-3 col-md-2 control-label">SEO关键字</label>
											<div class="col-xs-12 col-sm-9">
												<textarea id="_txtbox" name="seokeywords" class="autosize-transition form-control ">b2c</textarea><script type="text/javascript">
            require(["jquery", "autosize"],function($){
                $("textarea[class*=autosize]").autosize({append: "\n"});
            });
            </script>
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-12 col-sm-3 col-md-2 control-label">SEO描述</label>
											<div class="col-xs-12 col-sm-9">
												<textarea id="_txtbox" name="seodescription" class="autosize-transition form-control ">b2c</textarea>
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-12 col-sm-3 col-md-2 control-label">关闭站点</label>
											<div class="col-xs-12 col-sm-9">
												<input type="checkbox" class="ace ace-switch" name="closesite" value="1"<?php if(($cfgs["closesite"]) == "1"): ?>checked="checked"<?php endif; ?>>
												<span class="lbl"></span>
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-12 col-sm-3 col-md-2 control-label">关闭原因</label>
											<div class="col-xs-12 col-sm-9">
												<textarea id="_txtbox" name="closereason" class="autosize-transition form-control "></textarea>
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-12 col-sm-3 col-md-2 control-label">备案号</label>
											<div class="col-xs-12 col-sm-9">
												<input type="text" name="icp" value="<?php echo ($cfgs["icp"]); ?>" class="form-control">
											</div>
										</div>
									</div>
									<div class="tab-pane" id="settings-image">
										<div class="form-group">
											<label class="col-xs-12 col-sm-3 col-md-2 control-label">开启水印</label>
											<div class="col-xs-12 col-sm-9">
												<input type="checkbox" class="ace ace-switch" name="watermark" value="1"<?php if(($cfgs["watermark"]) == "1"): ?>checked="checked"<?php endif; ?>>
												<span class="lbl"></span>
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-12 col-sm-3 col-md-2 control-label">是否保留原图</label>
											<div class="col-xs-12 col-sm-9">
												<input type="checkbox" class="ace ace-switch" name="saveorignal" value="1"<?php if(($cfgs["saveorignal"]) == "1"): ?>checked="checked"<?php endif; ?>>
												<span class="lbl"></span>
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-12 col-sm-3 col-md-2 control-label">缩略图尺寸</label>
											<div class="col-xs-12 col-sm-9">
												<div class="col-xs-6 col-sm-6">
													<div class="input-group">
														<span class="input-group-addon">长</span>
														<input type="text" name="thumb_width" class="form-control" value="<?php echo ($cfgs["thumb_width"]); ?>">
													</div>
												</div>
												<div class="col-xs-6 col-sm-6">
													<div class="input-group">
														<span class="input-group-addon">高</span>
														<input type="text" name="thumb_height" class="form-control" value="<?php echo ($cfgs["thumb_height"]); ?>">
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-12 col-sm-3 col-md-2 control-label">水印类型</label>
											<div class="col-xs-12 col-sm-9">
												<div class="btn-group" data-toggle="buttons">
													<label class="btn btn-primary <?php if(($cfgs["watertype"]) == "text"): ?>active<?php endif; ?>">
														<input type="radio" name="watertype" value="text" autocomplete="off" <?php if(($cfgs["watertype"]) == "text"): ?>checked="checked"<?php endif; ?>>文字
													</label>
													<label class="btn btn-primary <?php if(($cfgs["watertype"]) == "image"): ?>active<?php endif; ?>">
														<input type="radio" name="watertype" value="image" autocomplete="off" <?php if(($cfgs["watertype"]) == "image"): ?>checked="checked"<?php endif; ?>>图片
													</label>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-12 col-sm-3 col-md-2 control-label">水印文字</label>
											<div class="col-xs-12 col-sm-9">
												<input type="text" name="watertext" value="<?php echo ($cfgs["watertext"]); ?>" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-12 col-sm-3 col-md-2 control-label">水印图片</label>
											<div class="col-xs-12 col-sm-9">
												<div class="input-group "> <input type="text" name="waterimage" value="/uploads/admin/ajax/20150810/55c819924d3c2.jpg" class="form-control" autocomplete="off" readonly="true"> <span class="input-group-btn"> <button class="btn btn-default btn-sm" type="button" onclick="showImageDialog(this);" data-nowater="1" data-dir="watermark" data-size="">选择图片</button> </span> </div> <div class="input-group " style=""> <img src="/uploads/admin/ajax/20150810/55c819924d3c2.jpg" onerror="this.src='/Public/images/nopic.jpg'; this.title='图片未找到.'" class="img-responsive img-thumbnail" style="max-width:150px;" /> </div>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="settings-advance">
										advance
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 space-6"></div>
						<div class="row col-xs-12 col-sm-12 col-md-12">
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-lg-1">
								<input type="submit" name="submit" value="保存" class="btn btn-primary">
							</div>
						</div></div>
					</form>
				<!-- </div>
			</div> -->
		</div>
	</div>
</div>
</div><!-- /.main-content -->
				<div class="ace-settings-container" id="ace-settings-container">
					<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
						<i class="icon-cog bigger-150"></i>
					</div>

					<div class="ace-settings-box" id="ace-settings-box">
						<div>
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="default" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; Choose Skin</span>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-navbar" />
							<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-sidebar" />
							<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-breadcrumbs" />
							<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" />
							<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-add-container" />
							<label class="lbl" for="ace-settings-add-container">
								Inside
								<b>.container</b>
							</label>
						</div>
					</div>
				</div><!-- /#ace-settings-container -->
			</div><!-- /.main-container-inner -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->
		<script type="text/javascript">
		require(['bootstrap'],function($){
			$('[data-toggle="tooltip"]').tooltip();
		})
		</script>
		
	</body>
</html>
<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>系统提示 - <?php echo ($cfgs["sitename"]); ?></title>
	<meta name="description" content="overview &amp; stats" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<!-- basic styles -->
	<script type="text/javascript" src="/Public/js/lib/require.js"></script>
	<script type="text/javascript" src="/Public/js/lib/config.js"></script>
	<link rel="stylesheet" href="/Public/css/bootstrap.min.css" />
	<link rel="stylesheet" href="/Public/css/font-awesome.min.css" />
	<!--[if IE 7]>
	<link rel="stylesheet" href="css/font-awesome-ie7.min.css" />
	<![endif]-->
	<!-- page specific plugin styles -->
	<!-- fonts -->
	<link rel="stylesheet" href="/Public/css/ace-fonts.css" />
	<!-- ace styles -->
	<link rel="stylesheet" href="/Public/css/ace.min.css" />
	<link rel="stylesheet" href="/Public/css/ace-rtl.min.css" />
	<link rel="stylesheet" href="/Public/css/ace-skins.min.css" />
	<!--[if lte IE 8]>
	<link rel="stylesheet" href="css/ace-ie.min.css" />
	<script src="/Public/js/excanvas.min.js"></script>
	<![endif]-->

	<!-- inline styles related to this page -->
	<!-- ace settings handler -->
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="/Public/js/html5shiv.js"></script>
	<script src="/Public/js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<div class="container-fluid">
		<div>
			<div class="jumbotron clearfix alert alert-success">
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
						<i class="fa fa-5x icon-check-circle" style="font-size:15rem;"></i>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-10">
						<h2>操作成功</h2>
						<p class="lead"><?php echo ($message); ?></p>
						<?php if(!empty($jumpUrl)): ?><p><a href="<?php echo ($jumpUrl); ?>" class="alert-link">如果你的浏览器没有自动跳转，请点击此链接</a></p>
						<script type="text/javascript">
							setTimeout(function () {
								location.href = "<?php echo ($jumpUrl); ?>";
							}, 3000);
						</script>
						<?php else: ?>
							<p>[<a href="javascript:history.go(-1);" class="alert-link">点击这里返回上一页</a>] &nbsp; [<a href="<?php echo U('Index/index');?>" class="alert-link">首页</a>]</p><?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
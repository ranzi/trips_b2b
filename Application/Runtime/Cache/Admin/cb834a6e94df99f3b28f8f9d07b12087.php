<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo ($bar["curpos"]); ?> - <?php echo ($bar["menu"]); ?></title>
		<meta name="description" content="<?php echo ($cfgs["seodescription"]); ?>" />
		<meta name="keywords" content="<?php echo ($cfgs["seokeywords"]); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<script type="text/javascript" src="/Public/js/lib/require.js"></script>
		<script type="text/javascript" src="/Public/js/lib/config.js"></script>
		<link rel="stylesheet" href="/Public/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/Public/css/font-awesome.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-fonts.css" />
		<link rel="stylesheet" href="/Public/css/ace.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-skins.min.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="css/ace-ie.min.css" />
		  <script src="/Public/js/excanvas.min.js"></script>
		<![endif]-->
		<!-- inline styles related to this page -->
		<!-- ace settings handler -->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="/Public/js/html5shiv.js"></script>
		<script src="/Public/js/respond.min.js"></script>
		<![endif]-->
		<script type="text/javascript">
		if(navigator.appName == 'Microsoft Internet Explorer'){
			if(navigator.userAgent.indexOf("MSIE 5.0")>0 || 
				navigator.userAgent.indexOf("MSIE 6.0")>0 || 
				navigator.userAgent.indexOf("MSIE 7.0")>0) {
				alert('您使用的 IE 浏览器版本过低,无法获得最好的体验且可能出现无法预期的错误, 推荐使用 Chrome 浏览器或 IE8 及以上版本浏览器.');
			}
		}
		</script>
	</head>
	
	
<body>
	<div class="navbar navbar-default" id="navbar">
			<div class="navbar-container" id="navbar-container">
				<div class="navbar-header pull-left">
					<a href="<?php echo U('Index/index');?>" class="navbar-brand" style="padding:0px">
						<img src="<?php echo ($cfgs["logo"]); ?>" class="img-rounded img-thumbnail" style="height:45px;padding:0">
						<small>
							<?php echo ($cfgs["sitename"]); ?>后台管理系统
						</small>
					</a><!-- /.brand -->
				</div><!-- /.navbar-header -->

				<div class="navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="<?php echo ($_SESSION['admin']['avatar']); ?>" alt="<?php echo (session('admin_name')); ?>" />
								<span class="user-info">
									<small>欢迎使用<?php echo ($cfgs["sitename"]); ?>后台管理系统后台管理系统后台管理系统后台管理系统</small>
									<?php echo (session('admin_name')); ?>
								</span>
								<i class="icon-caret-down"></i>
							</a>
							<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="<?php echo U('Admin/profile');?>">
										<i class="icon-user"></i>
										个人资料
									</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="<?php echo U('Login/logout');?>">
										<i class="icon-off"></i>
										退出登录
									</a>
								</li>
							</ul>
						</li>
					</ul><!-- /.ace-nav -->
				</div><!-- /.navbar-header -->
			</div><!-- /.container -->
		</div>
		<div class="main-container" id="main-container">
			<div class="main-container-inner">
				<a class="menu-toggler" id="menu-toggler" href="#">
					<span class="menu-text"></span>
				</a>

				<div class="sidebar" id="sidebar">
					<script type="text/javascript">
					require(['jquery', 'acemy'], function($){
						try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
					});
					</script>
					<ul class="nav nav-list">
						<?php if(is_array($authMenus)): $i = 0; $__LIST__ = $authMenus;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu): $mod = ($i % 2 );++$i;?><li class="<?php echo ($menu["class"]); ?>">
							<a class="dropdown-toggle" href="<?php if(empty($menu["child"])): echo U($menu['url']); endif; ?>">
								<?php if(!empty($menu["icon"])): ?><i class="<?php echo ($menu["icon"]); ?>"></i><?php endif; ?>
								<span class="menu-text"><?php echo ($menu["title"]); ?></span>
							</a>
							<?php if(!empty($menu["child"])): ?><ul class="submenu">
							<?php if(is_array($menu["child"])): $i = 0; $__LIST__ = $menu["child"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li <?php if(!empty($vo["class"])): ?>class="<?php echo ($vo["class"]); ?>"<?php endif; ?>>
									<a href="<?php echo U($vo['url']);?>">
										<i class="icon-double-angle-right"></i>
										<?php echo ($vo["title"]); ?>
									</a>
								</li><?php endforeach; endif; else: echo "" ;endif; ?>
							</ul><?php endif; ?>
						</li><?php endforeach; endif; else: echo "" ;endif; ?>
					</ul><!-- /.nav-list -->

					<div class="sidebar-collapse" id="sidebar-collapse">
						<i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right" data-toggle="tooltip" data-placement="right" title="收起面板"></i>
					</div>

					<script type="text/javascript">
					require(['jquery', 'acemy'], function($){
						try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
					});
					</script>
				</div>
				<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
						require(['jquery', 'acemy'], function($){
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						});
						</script>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<?php echo U('Index/index');?>">首页</a>
							</li>
							<?php if(!empty($bar["menu"])): ?><li>
								<a href="<?php echo ($bar["url"]); ?>"><?php echo ($bar["menu"]); ?></a>
							</li><?php endif; ?>
							<li class="active"><?php echo ($bar["curpos"]); ?></li>
						</ul><!-- .breadcrumb -->
					</div>
<div class="page-content">
	<div class="page-header" style="position:relative;">
		<h1>
			合作单位
			<small>
				<i class="icon-double-angle-right"></i>
				<?php if($sch["schbs"] == 1 ): ?><a href="<?php echo U('Partners/index');?>">返回列表</a>
				<?php else: ?>列表<?php endif; ?>
			</small>
		</h1>
		<div class="nav-search">
			<a class="btn btn-sm btn-primary" href="<?php echo U('Partners/add');?>">添加合作单位</a>
		</div>
	</div>
	<form class="form-inline" name="form1" method="get" action="">
		<input type="hidden" name="schbs" value="1" />
		<input class="input-medium" type="text" name="title" value="<?php echo ($sch["title"]); ?>" placeholder="单位名称">
		<select name="industry">
			<option value="" selected="selected">行业类型</option>
			<?php if(is_array($industrys)): $i = 0; $__LIST__ = $industrys;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data): $mod = ($i % 2 );++$i;?><option value="<?php echo ($data["id"]); ?>" <?php if($sch["industry"] == $data["id"] ): ?>selected='selected'<?php endif; ?>><?php echo ($data["title"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
		</select>
		<select name="settlement">
			<option value="" selected="selected">结算方式</option>
			<?php if(is_array($settlements)): $i = 0; $__LIST__ = $settlements;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data2): $mod = ($i % 2 );++$i;?><option value="<?php echo ($data2["id"]); ?>" <?php if($sch["settlement"] == $data2["id"] ): ?>selected='selected'<?php endif; ?>><?php echo ($data2["title"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
		</select>
		信用额度
		<input class="input-small" type="text" name="start_creditquota" value="<?php echo ($sch["start_creditquota"]); ?>"> 至 
		<input class="input-small" type="text" name="end_creditquota" value="<?php echo ($sch["end_creditquota"]); ?>">
		<button class="btn btn-info btn-sm" type="submit">
		<i class="i bigger-110"></i>
		搜索
		</button>
	</form>
	<div class="row col-xs-12">
		<!-- Think 系统列表组件开始 -->
<script type="text/javascript">
            require(["jquery", "bootbox"], function($){
                $(".check_all_list").click(function(){
                    if($(this).prop("checked") == true){
                        $(this).closest("table").find("input:checkbox.check_item").prop("checked", true);
                    }else{
                        $(this).closest("table").find("input:checkbox.check_item").prop("checked", false);
                    }
                });
                $("span.editfield").click(function(){
                    var val=$(this).html();
                    var obj = $(this);
                    var dialog=bootbox.dialog({
                        title: "<span style=\"color:rgb(134, 181, 88)\"><i class=\"icon-check-circle\"></i>系统提示</span>",
                        message: '<input type="text" value="'+val+'" class="form-control">',
                        buttons:{
                            cancel: {
                                label: "取消",
                                className: "btn btn-sm btn-default"
                            },
                            ok:{
                                label:"提交",
                                className: "btn btn-sm btn-success",
                                callback: function(){
                                    var newVal = dialog.find("input").val();
                                    if(newVal != val){
                                        $.post(
                                            "/index.php/Admin/Partners/edit.html",
                                            { pk: obj.attr("data-pk"),id:obj.attr("data-val"),field: obj.attr("data-field"), value: newVal},
                                            function(json){
                                                if(json.status==1){
                                                    obj.html(newVal);
                                                } else{
                                                    dialog.modal("hide");
                                                    require(["util"],function(util){
                                                        util.error(json.msg);
                                                    });
                                                }
                                            },
                                            "json"
                                        );
                                    }
                                }
                            }
                        }
                    });
                });
            });
            function edit(id){
                location.href="/index.php/Admin/Partners/edit.html?id="+id;
            }
            function del(id, obj){
                require(["jquery", "util"], function($, util){
                    util.warning("你确定要删除本条数据吗？", function(){
                        $.post("/index.php/Admin/Partners/delete.html", { id: id}, function(data){
                            if(data.status == 1){
                                $(obj).parent().parent().slideUp(function(){
                                    $(this).remove();
                                });
                            }else{
                                util.error(data.msg);
                            }
                        }, "json")
                    })
                });
            }
            function sortBy(sort, model, action){
                var href = location.href;
                if(href.indexOf("_order")===-1){
                    if(href.indexOf("?")===-1){
                        href+="?_order="+sort;
                    }else{
                        href+="&_order="+sort;
                    }
                } else {
                    href = href.replace(/_order(\/|=)[^&\/\.]*/,"_order$1"+sort);
                }
                var _sort = "desc";
                if(sort==""){
                    _sort = ""=="desc"? "asc":"desc";
                }
                if(href.indexOf("_sort")===-1){
                    if(href.indexOf("?")===-1){
                        href+="?_sort="+_sort;
                    } else{
                        href+="&_sort="+_sort;
                    }
                }else{
                    href=href.replace(/_sort(\/|=)[^&\/\.]*/,"_sort$1"+_sort);
                }
                location.href=href;
            }
            function delAll(obj){
                require(["util"], function(util){
                    var ids = [];
                    var table = $(obj).parent().parent().parent().prev("table");
                    table.find(".check_item:checked").each(function(){
                        ids.push($(this).val());
                    });
                    if(ids.length <= 0){
                        util.error("请选择数据");
                        return;
                    }
                    util.warning("确定要删除这些数据吗？", function(){
                        $.post(
                            "/index.php/Admin/Partners/delete.html",
                            {id: ids.join(",")},
                            function(data){
                                if(data.status == 1){
                                    util.success("删除成功");
                                    table.find(".check_item:checked").parent().parent().slideUp(function(){
                                        $(this).remove();
                                    });
                                } else {
                                    util.error(data.msg);
                                }
                            },
                            "json"
                        );
                    })
                })
            }
            function changePageSize(obj){
                if(location.href.indexOf("psize")===-1){
                    if(location.href.indexOf("?")===-1){
                        location.href=location.href+"?psize="+$(obj).val();
                    }else{
                        location.href=location.href+"&psize="+$(obj).val();
                    }
                } else {
                    location.href=location.href.replace(/psize(\/|=)\d+/,"psize$1"+$(obj).val());
                }
            }
            </script><table id="" class="table table-striped table-hover " ><thead><tr class="row" ><th width="8"><input type="checkbox" class="check_all_list" ></th><th>ID</th><th>单位名称</th><th>行业</th><th>结算方式</th><th>信用额度</th><th>排序</th><th>状态</th><th >操作</th></tr></thead><?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$admin): $mod = ($i % 2 );++$i;?><tr class="row" ><td><input type="checkbox" class="check_item" name="key" value="<?php echo ($admin["id"]); ?>"></td><td><?php echo ($admin["id"]); ?></td><td><span class="editfield " data-toggle="tooltip" data-placement="top" title="点击编辑" data-field="title" data-pk="id" data-val="<?php echo ($admin["id"]); ?>"><?php echo ($admin["title"]); ?></span></td><td><?php echo ($admin["t_type_id"]); ?></td><td><?php echo ($admin["t_settlement_id"]); ?></td><td><?php echo ($admin["creditquota"]); ?></td><td><span class="editfield " data-toggle="tooltip" data-placement="top" title="点击编辑" data-field="ordid" data-pk="id" data-val="<?php echo ($admin["id"]); ?>"><?php echo ($admin["ordid"]); ?></span></td><td><?php echo ($admin["status"]); ?></td><td><a href="javascript:;" onClick="edit('<?php echo ($admin["id"]); ?>', this)">编辑</a>&nbsp;<a href="javascript:;" onClick="bank('<?php echo ($admin["id"]); ?>', this)">结算账号</a>&nbsp;<a href="javascript:;" onClick="contact('<?php echo ($admin["id"]); ?>', this)">联系方式</a>&nbsp;<a href="javascript:;" onClick="del('<?php echo ($admin["id"]); ?>', this)">删除</a>&nbsp;</td></tr><?php endforeach; endif; else: echo "" ;endif; ?></table><div class="row">  <div class="col-xs-12">    <div class="pull-left">      <a class="btn btn-xs btn-danger" onClick="delAll(this);" data-toggle="tooltip" data-placement="top" title="删除">        <i class="icon-trash icon-2x"></i>      </a>     <a class="btn btn-xs btn-info" onClick="location.reload();" data-toggle="tooltip" data-placement="top" title="刷新">        <i class="icon-refresh icon-2x"></i></a>      &nbsp;<label class="control-label">显示</label>      <select onchange="changePageSize(this);">        <option value="15">15</option>        <option value="20">20</option>        <option value="50">50</option>      </select>条/页    </div>    <div class="pull-right"><div class="col-xs-12"><div class="dataTables_paginate paging_bootstrap"><ul class="pagination"> <li class="prev disable"><a><i class="icon-double-angle-left"></i></a></li>  <li class="next disable"><a><i class="icon-double-angle-right"></i></a></li> </ul></div></div></div>  </div></div>
<!-- Think 系统列表组件结束 -->

	</div>
</div>
<script type="text/javascript">
function bank(id){
	location.href = '<?php echo U("partners/bank");?>?id='+id;
}
function contact(id){
	location.href = '<?php echo U("partners/contact");?>?id='+id;
}
</script>
</div><!-- /.main-content -->
			</div><!-- /.main-container-inner -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->
		<script type="text/javascript">
		require(['bootstrap'],function($){
			$('[data-toggle="tooltip"]').tooltip();
		})
		</script>
		
	</body>
</html>
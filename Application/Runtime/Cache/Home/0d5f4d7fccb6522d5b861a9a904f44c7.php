<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>首页 - 素材火 Admin</title>
		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!-- basic styles -->
		<script type="text/javascript" src="/Public/js/lib/require.js"></script>
		<script	type="text/javascript" src="/Public/js/lib/config.js"></script>
		<link href="/Public/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/Public/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="css/font-awesome-ie7.min.css" />
		<![endif]-->
		<!-- page specific plugin styles -->
		<!-- fonts -->
		<link rel="stylesheet" href="/Public/css/ace-fonts.css" />
		<!-- ace styles -->
		<link rel="stylesheet" href="/Public/css/ace.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-skins.min.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="css/ace-ie.min.css" />
		<![endif]-->
		<!-- inline styles related to this page -->
		<!-- ace settings handler -->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="/Public/js/html5shiv.js"></script>
		<script src="/Public/js/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>
		
		
		<div class="row">
			<div class="col-xs-12">
				<div id="hightchart"></div>
				<div id="hightchart2"></div>
			</div>
		</div>
		<script type="text/javascript">
		require(['chart'], function(chart){
			var xData = ['一月','二月','三月','四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'];
			var data = <?php echo (json_encode($data)); ?>;
			var data2 = <?php echo (json_encode($data2)); ?>;
			chart.line('hightchart', '2015年销售统计', xData, data, '套');
			chart.column('hightchart2', '5月份销售统计', data2, '万元');
		})
		</script>
	</body>
</html>
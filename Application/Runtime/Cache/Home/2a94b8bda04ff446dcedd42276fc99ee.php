<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>首页 - 素材火 Admin</title>
		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!-- basic styles -->
		<script type="text/javascript" src="/Public/js/lib/require.js" data-main="/Public/js/lib/config"></script>
		<link href="/Public/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/Public/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="css/font-awesome-ie7.min.css" />
		<![endif]-->
		<!-- page specific plugin styles -->
		<!-- fonts -->
		<link rel="stylesheet" href="/Public/css/ace-fonts.css" />
		<!-- ace styles -->
		<link rel="stylesheet" href="/Public/css/ace.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-skins.min.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="css/ace-ie.min.css" />
		<![endif]-->
		<!-- inline styles related to this page -->
		<!-- ace settings handler -->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="/Public/js/html5shiv.js"></script>
		<script src="/Public/js/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>
		
		<div class="main-container">
			<div class="page-content">
				<div class="page-header"><h1>Example</h1></div>
				<div class="row">
					<div class="col-xs-12">
						<a href="<?php echo U('index/image');?>" class="btn btn-link">image</a>
						<a href="<?php echo U('index/editor');?>" class="btn btn-link">editor</a>
						<a href="<?php echo U('index/select');?>" class="btn btn-link">select</a>
						<a href="<?php echo U('index/txtbox');?>" class="btn btn-link">auto size textarea</a>
						<a href="<?php echo U('index/date');?>" class="btn btn-link">date</a>
						<a href="<?php echo U('index/datetime');?>" class="btn btn-link">date time</a>
						<a href="<?php echo U('index/daterange');?>" class="btn btn-link">date range</a>
						<a href="<?php echo U('Admin/Example/download');?>" class="btn btn-link">export csv</a>
						<a href="<?php echo U('index/dialog');?>" class="btn btn-link">dialog</a>
						<a href="<?php echo U('index/chart');?>" class="btn btn-link">chart</a>
						<a href="<?php echo U('index/chosen');?>" class="btn btn-link">chosen</a>
						<a href="<?php echo U('index/seltree');?>" class="btn btn-link">seltree</a>
						<a href="<?php echo U('index/ztree');?>" class="btn btn-link">ztree</a>
						<a href="<?php echo U('index/jqgrid');?>" class="btn btn-link">jqgrid</a>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
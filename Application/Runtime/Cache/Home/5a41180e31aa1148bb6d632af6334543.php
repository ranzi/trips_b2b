<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>首页 - 素材火 Admin</title>
		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!-- basic styles -->
		<script type="text/javascript" src="/Public/js/lib/require.js" data-main="/Public/js/lib/config"></script>
		<link href="/Public/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/Public/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="css/font-awesome-ie7.min.css" />
		<![endif]-->
		<!-- page specific plugin styles -->
		<!-- fonts -->
		<link rel="stylesheet" href="/Public/css/ace-fonts.css" />
		<!-- ace styles -->
		<link rel="stylesheet" href="/Public/css/ace.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-skins.min.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="css/ace-ie.min.css" />
		<![endif]-->
		<!-- inline styles related to this page -->
		<!-- ace settings handler -->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="/Public/js/html5shiv.js"></script>
		<script src="/Public/js/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>
		
		
		<div class="row">
			<div class="col-xs-10">
				<h1>说明：</h1>
				<p class="lead">
					与原THINKPHP的HTML相比，有2个地方加强.
					1.支持多维数组，如array(array('id' => 1, 'name' => 'name1')),此时，需要指定key和val。
					2.选中项支持2维数组,只需传入var.key即可。
				</p>
				<form class="form-horizontal from">
					<div class="from-group">
						<label class="control-label col-sm-3">default select</label>
						<div class="col-sm-9">
							<!--default select start--><select name="myselect" id="myselect" onClick="" onChange="" class="form-control " ><option value="">==请选择==</option><?php foreach($single as $key => $val){ ?><option value="<?php echo $key;?>"><?php echo $val;?></option><?php }?></select><!--default select end-->
						</div>
					</div>
					<div class="from-group">
						<label class="control-label col-sm-3">default select</label>
						<div class="col-sm-9">
							<!--default select start--><select name="myselect" id="myselect" onClick="" onChange="" class="form-control " ><option value="">==请选择==</option><?php foreach($nest as $key => $val){ ?><option value="<?php echo $val["id"]; ?>"<?php if($val["id"] == $info["id"]){ echo ' selected="selected"';}?>><?php echo $val["name"];?></option><?php }?></select><!--default select end-->
						</div>
					</div>
				</form>
				
			</div>
		</div>
	</body>
</html>
<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>首页 - 素材火 Admin</title>
		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!-- basic styles -->
		<script type="text/javascript" src="/Public/js/lib/require.js"></script>
		<script type="text/javascript" src="/Public/js/lib/config.js"></script>
		<link href="/Public/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/Public/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="css/font-awesome-ie7.min.css" />
		<![endif]-->
		<!-- page specific plugin styles -->
		<!-- fonts -->
		<link rel="stylesheet" href="/Public/css/ace-fonts.css" />
		<!-- ace styles -->
		<link rel="stylesheet" href="/Public/css/ace.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-skins.min.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="css/ace-ie.min.css" />
		<![endif]-->
		<!-- inline styles related to this page -->
		<!-- ace settings handler -->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="/Public/js/html5shiv.js"></script>
		<script src="/Public/js/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>
		
		
		<div class="row">
			<div class="col-xs-10">
				
				<form class="form-horizontal from"> 
					<div class="from-group">
						<label class="control-label col-sm-3">today</label>
						<div class="col-sm-9">
							<?php if(($time) == "1"): ?><input name="mydaterange[start]" type="hidden" value="2015-08-03 09:11" /> <input name="mydaterange[end]" type="hidden" value="2015-08-03 09:11" /> <button class="btn btn-info daterange daterange-time" type="button"><span class="date-title">2015-08-03 09:11至2015-08-03 09:11</span> <i class="fa fa-calendar"></i></button><script type="text/javascript">
                require(["daterangepicker"], function($){
                    $(function(){
                        $(".daterange.daterange-time").each(function(){
                            var elm = this;
                            $(this).daterangepicker({
                                startDate: $(elm).prev().prev().val(),
                                endDate: $(elm).prev().val(),
                                format: "YYYY-MM-DD HH:mm",
                                timePicker: true,
                                timePicker12Hour : false,
                                timePickerIncrement: 1,
                                minuteStep: 1
                            }, function(start, end){
                                $(elm).find(".date-title").html(start.toDateTimeStr() + " 至 " + end.toDateTimeStr());
                                $(elm).prev().prev().val(start.toDateTimeStr());
                                $(elm).prev().val(end.toDateTimeStr());
                            });
                        });
                    });
                });
            </script>
							<?php else: ?>
							<input name="mydaterange[start]" type="hidden" value="2015-08-03" /> <input name="mydaterange[end]" type="hidden" value="2015-08-03" /> <button class="btn btn-info daterange daterange-date" type="button"><span class="date-title">2015-08-03至2015-08-03</span> <i class="fa fa-calendar"></i></button><script type="text/javascript">
                require(["daterangepicker"], function($){
                    $(function(){
                        $(".daterange.daterange-date").each(function(){
                            var elm = this;
                            $(this).daterangepicker({
                                startDate: $(elm).prev().prev().val(),
                                endDate: $(elm).prev().val(),
                                format: "YYYY-MM-DD",
                            }, function(start, end){
                                $(elm).find(".date-title").html(start.toDateStr() + " 至 " + end.toDateStr());
                                $(elm).prev().prev().val(start.toDateStr());
                                $(elm).prev().val(end.toDateStr());
                            });
                        });
                    });
                });
                </script><?php endif; ?>
						</div>
					</div>
				</form>
				
			</div>
		</div>
	</body>
</html>
<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>首页 - 素材火 Admin</title>
		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!-- basic styles -->
		<script type="text/javascript" src="/Public/js/lib/require.js"></script>
		<script	type="text/javascript" src="/Public/js/lib/config.js"></script>
		<link href="/Public/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/Public/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="css/font-awesome-ie7.min.css" />
		<![endif]-->
		<!-- page specific plugin styles -->
		<!-- fonts -->
		<link rel="stylesheet" href="/Public/css/ace-fonts.css" />
		<!-- ace styles -->
		<link rel="stylesheet" href="/Public/css/ace.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-skins.min.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="css/ace-ie.min.css" />
		<![endif]-->
		<!-- inline styles related to this page -->
		<!-- ace settings handler -->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="/Public/js/html5shiv.js"></script>
		<script src="/Public/js/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>
		
		
		<div class="row">
			<div class="col-xs-10">
				
				<form class="form-horizontal from">
					<div class="from-group">
						<label class="control-label col-sm-3">deafult image</label>
						<div class="col-sm-9">
							<input type="file" id="myimage" name="avatar" value="https://ss0.baidu.com/6ONWsjip0QIZ8tyhnq/it/u=1694481588,4130346406&fm=58" style="height:auto;width:auto;" /><script type="text/javascript"> require(["jquery", "ace-elements"], function($){$("#myimage").ace_file_input({no_file:"请选择图片", btn_choose:"上传图片", btn_change:"重新上传", droppable:false, onchange:null, thumbnail:false, whitelist:"gif|png|jpg|jpeg",  }); }) </script>
						</div>
					</div>
					<div class="from-group">
						<label class="control-label col-sm-3">drag image</label>
						<div class="col-sm-9">
							<input type="file" id="mydragimage" name="pic" value=""  style="width:auto;height:auto;" /><script type="text/javascript"> require(["jquery", "ace-elements"], function($){$("#mydragimage").ace_file_input({style:"well", btn_choose:"请选择图片", btn_change:null, no_icon:"icon-cloud-upload", droppable:true, thumbnail:"small",  preview_error : function(filename, error_code) {alert("文件预览失败"); } });}); </script>
						</div>
					</div>
					<div class="from-group">
						<label class="control-label col-sm-3">ajax image</label>
						<div class="col-sm-9">
							<div class="input-group "> <input type="text" name="_pvimage" value="/Public/images/nopic.jpg" class="form-control" autocomplete="off" readonly="true"> <span class="input-group-btn"> <button class="btn btn-default btn-sm" type="button" onclick="showImageDialog(this);">选择图片</button> </span> </div> <div class="input-group " style=""> <img src="/Public/images/nopic.jpg" onerror="this.src='/Public/images/nopic.jpg'; this.title='图片未找到.'" class="img-responsive img-thumbnail" width="150" /> </div><script type="text/javascript"> function showImageDialog(ele){require(["jquery", "bootstrap", "bootbox", "filestyle"], function($){var btn = $(ele); var dialog = bootbox.dialog({message: '<iframe width="0" height="0" name="__image_file_uploader" style="display:none;"></iframe><ul class="nav nav-tabs"><li class="active"><a data-toggle="tab" href="#local">本地图片</a></li><li> <a data-toggle="tab" href="#remote">远程图片 </a> </li></ul><div class="tab-content"> <div id="local" class="tab-pane in active"> <form action="/index.php/Admin/Base/frameUpload/callback/uploadLocalPic/" enctype="multipart/form-data" method="post" target="__image_file_uploader"><input type="file" name="upfile" ></form></div> <div id="remote" class="tab-pane"><form action="/index.php/Admin/Base/frameUpload/callback/uploadLocalPic" method="post" target="__image_file_uploader"><input type="txt" name="url" class="form-control" placeholder="请输入图片URL" ></form></div> </div>', title: "上传图片", buttons:{"cancel": {label: "取消", className: "btn btn-default"}, "ok":{label: "确定", className: "btn btn-success", callback: function(){if(dialog.find(".nav.nav-tabs li").eq(0).hasClass("active")){dialog.find("form")[0].submit(); } else {dialog.find("form")[1].submit(); } return false; } } } }); dialog.find(":file").filestyle({buttonText: "选择图片", buttonName: "btn-sm"}); window.uploadLocalPic = function(data){if(data.status == 1){btn.parent().parent().next().children().attr("src", data.file); btn.parent().prev().val(data.file); dialog.modal("hide"); } else {alert(data.msg); } } });} </script>
						</div>
					</div>
					<div class="from-group">
						<label class="control-label col-sm-3">multi image</label>
						<div class="col-sm-9">
							<div class="input-group"> <input type="text" class="form-control" readonly="readonly" value="" placeholder="批量上传图片" autocomplete="off"> <span class="input-group-btn"> <button class="btn btn-default btn-sm" type="button" onclick="uploadMultiImage(this);">选择图片</button> </span> </div> <div class="input-group multi-img-details"> <?php foreach($pics as $val){?><div class="multi-item" style="height: 150px; position:relative; float: left; margin-right: 18px;"><img style="max-width: 150px; max-height: 150px;" onerror="this.src='/Public/images/nopic.jpg'; this.title='图片未找到.'" src="<?php echo $val; ?>" class="img-responsive img-thumbnail"><input type="hidden" name="mpics[]" value="<?php echo $val; ?>"><em class="close" style="position:absolute; top: 0px; right: -14px;" title="删除这张图片" onclick="deleteMultiImage(this)">×</em></div><?php }?></div><script type="text/javascript"> function uploadMultiImage(elm) {require(["jquery", "bootbox", "filestyle"], function($){var dialog = bootbox.dialog({title: "上传图片", message: '<iframe width="0" height="0" name="__image_file_uploader" style="display:none;"></iframe><ul class="nav nav-tabs"><li class="active"><a data-toggle="tab" href="#local">本地图片</a></li><li> <a data-toggle="tab" href="#remote">远程图片 </a> </li></ul><div class="tab-content"> <div id="local" class="tab-pane in active"> <form action="/index.php/Admin/Base/frameUpload/callback/uploadLocalPic/" enctype="multipart/form-data" method="post" target="__image_file_uploader"><input type="file" name="upfile" ></form></div> <div id="remote" class="tab-pane"><form action="/index.php/Admin/Base/frameUpload/callback/uploadLocalPic" method="post" target="__image_file_uploader"><input type="txt" name="url" class="form-control" placeholder="请输入图片URL" ></form></div> </div>', buttons:{"cancel": {label: "取消", className: "btn btn-default"}, "ok": {label: "确定", className: "btn btn-success", callback: function(){if(dialog.find(".nav.nav-tabs li").eq(0).hasClass("active")){dialog.find("form")[0].submit(); } else {dialog.find("form")[1].submit(); } return false; } } } }); dialog.find(":file").filestyle({buttonText: "选择图片", buttonName: "btn-sm"}); window.uploadLocalPic = function(data){if(data.status == 1){$(elm).parent().parent().next().append('<div class="multi-item" style="height: 150px; position:relative; float: left; margin-right: 18px;"><img style="max-width: 150px; max-height: 150px;" onerror="this.src="/Public/images/nopic.jpg"; this.title="图片未找到." src="'+data.file+'" class="img-responsive img-thumbnail"><input type="hidden" name="mpics[]" value="'+data.file+'"><em class="close" style="position:absolute; top: 0px; right: -14px;" title="删除这张图片" onclick="deleteMultiImage(this)">×</em></div>'); dialog.modal("hide"); } else {alert(data.msg); } } }); } function deleteMultiImage(elm){require(["jquery"], function($){$(elm).parent().remove(); }); } </script>
						</div>
					</div>
				</form>
				
			</div>
		</div>
	</body>
</html>
<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>首页 - 素材火 Admin</title>
		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!-- basic styles -->
		<script type="text/javascript" src="/Public/js/lib/require.js"></script>
		<script	type="text/javascript" src="/Public/js/lib/config.js"></script>
		<link href="/Public/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/Public/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="css/font-awesome-ie7.min.css" />
		<![endif]-->
		<!-- page specific plugin styles -->
		<!-- fonts -->
		<link rel="stylesheet" href="/Public/css/ace-fonts.css" />
		<!-- ace styles -->
		<link rel="stylesheet" href="/Public/css/ace.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-skins.min.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="css/ace-ie.min.css" />
		<![endif]-->
		<!-- inline styles related to this page -->
		<!-- ace settings handler -->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="/Public/js/html5shiv.js"></script>
		<script src="/Public/js/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>
		
		
		<div class="row">
			<div class="col-xs-12">
				<form class="from form-horizontal">
					<div class="form-group">
						<label class="control-label col-sm-3">select</label>
						<div class="col-sm-9">
							<select class="chosen-select form-control" name="singlechosen" id="" onchange=""><option value="">please chosen</option><?php if(!empty($data) && is_array($data)){ foreach($data as $key => $val){?><option value="<?php echo $key; ?>"<?php if($selected && $selected==$key){?> selected="selected"<?php }?>><?php echo $val;?></option><?php } }?></select><script type="text/javascript">
            require(["chosen"], function(){
                $(".chosen-select").chosen();
            })
            </script>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-3">select</label>
						<div class="col-sm-9">
							<select class="chosen-select form-control tag-input-style" name="multichosen" id="" multiple="" data-placeholder="请选择" onchange=""><?php if(!empty($data) && is_array($data)){ foreach($data as $key => $val){?><option value="<?php echo $key; ?>"<?php if($selected2 && (is_array($selected2)?in_array($key,$selected2):$selected2==$key)){?> selected="selected"<?php }?>><?php echo $val;?></option><?php } }?></select>
						</div>
					</div>
				</form>
			</div>
		</div>
	</body>
</html>
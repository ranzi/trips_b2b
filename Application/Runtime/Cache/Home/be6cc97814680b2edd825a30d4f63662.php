<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>首页 - 素材火 Admin</title>
		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!-- basic styles -->
		<script type="text/javascript" src="/Public/js/lib/require.js"></script>
		<script	type="text/javascript" src="/Public/js/lib/config.js"></script>
		<link href="/Public/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/Public/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="css/font-awesome-ie7.min.css" />
		<![endif]-->
		<!-- page specific plugin styles -->
		<!-- fonts -->
		<link rel="stylesheet" href="/Public/css/ace-fonts.css" />
		<!-- ace styles -->
		<link rel="stylesheet" href="/Public/css/ace.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="/Public/css/ace-skins.min.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="css/ace-ie.min.css" />
		<![endif]-->
		<!-- inline styles related to this page -->
		<!-- ace settings handler -->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="/Public/js/html5shiv.js"></script>
		<script src="/Public/js/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>
		
		<div class="row">
			<div class="col-xs-12">
				doc: <a href="http://bootstrap-datepicker.readthedocs.org/en/latest/options.html#datesdisabled">http://bootstrap-datepicker.readthedocs.org/en/latest/options.html#datesdisabled</a>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-10">

				<form class="form-horizontal from">
					<div class="from-group">
						<label class="control-label col-sm-3">Date Picker</label>
						<div class="col-sm-9">
							<div class="input-group col-sm-5"> 
								<!--datepicker start--><input class="form-control date-picker " id="mydatepicker" name="begindate" type="text" value="<?php echo ($startdate); ?>" /> <span class="input-group-addon"> <i class="icon-calendar bigger-110"></i> </span><script type="text/javascript"> require(["jquery", "datepicker", "datepicker-lang"], function($){$(".date-picker").datepicker({autoclose:true,startDate:"2015-08-03",language:"zh-CN"}).next().on("click", function(){$(this).prev().focus(); }); }) </script><!--datepicker end-->
							</div>
						</div>
					</div>
				</form>
				
			</div>
		</div>
	</body>
</html>
define(['hcharts'], function(require){
	var module = {};

	module.line = function(id, title, xAxis, data, unit, prefix){
		var _data = [];
		$.each(data, function(i, e){
			_data.push({name: i, data: data[i], pointPlacement: 'on'});
		});
		return new Highcharts.Chart({   
	        chart: {
	            renderTo: id,
	            polar: true,
	            type: 'line'
	        },
	        title: {
	            text: title,
	            x: 0
	        },
	        pane: {
	            size: '90%'
	        },
	        xAxis: {
	            categories: xAxis,
	            tickmarkPlacement: 'on',
	            lineWidth: 0
	        },  
	        yAxis: {
	            gridLineInterpolation: 'polygon',
	            lineWidth: 0,
	            min: 0
	        },
	        tooltip: {
	            shared: false,
	            valuePrefix: prefix,
	            valueSuffix: unit
	        },
	        legend: {
	            align: 'right',
	            verticalAlign: 'top',
	            y: 100,
	            layout: 'vertical'
	        },
	        series: _data
	    });
	};

	module.column = function(id, title, data, unit){
		var _data = [];
		$.each(data, function(i, e){
			_data.push({name: i, y: data[i]});
		});
		return new Highcharts.Chart({
			chart: {
					renderTo: id,
                    type: 'column'
                },
                title: {
                    text: title
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: ''
                    }
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: ''
                        }
                    }
                },

                tooltip: {
                    headerFormat: '',
                    pointFormat: '<span >{point.name}:{point.y}'+unit+'</span>'
                },

                series: [{
                    // name: 'xxx',
                    colorByPoint: true,
                    data: _data
                }]
		});
	}

	return module;
});
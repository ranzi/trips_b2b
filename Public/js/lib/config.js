require.config({
	baseUrl: '/Public/js/lib',
	urlArgs: "v=" +  (new Date()).getTime(),
	paths: {
		'css': 'css.min',
		'jquery': 'jquery-1.11.1.min', 
		'bootstrap': 'bootstrap.min',
		'domReady': 'domReady',
		'typeahead': 'typeahead-bs2.min',
		'jquery.touch': 'jquery.ui.touch-punch.min',
		'jquery.custom': 'jquery-ui-1.10.3.custom.min',
		'jquery.slimscroll': 'jquery.slimscroll.min',
		'jquery.piechart': 'jquery.easy-pie-chart.min',
		'jquery.sparkline': 'jquery.sparkline.min',
		'jquery.flot': 'flot/jquery.flot.min',
		'jquery.flot.pie': 'flot/jquery.flot.pie.min',
		'jquery.flot.resize': 'flot/jquery.flot.resize.min',
		'ace-elements': 'ace-elements.min',
		'acemy': 'ace',
		'wysiwyg': 'bootstrap-wysiwyg.min',
		'baidueditor': 'ueditor1_4_3-utf8-php/uemy',
		'bdlang': 'ueditor1_4_3-utf8-php/lang/zh-cn/zh-cn',
		'zeroclipboard': 'ueditor1_4_3-utf8-php/third-party/zeroclipboard/ZeroClipboard.min',
		'datepicker': 'date-time/bootstrap-datepicker.min',
		'datepicker-lang': 'date-time/locales/bootstrap-datepicker.zh-CN',
		'autosize': 'jquery.autosize.min',
		'at':'autosize.min',
		'util': 'util',
		'filestyle': 'bootstrap-filestyle.min',
		'bootbox': 'bootbox.min',
		'fullcalendar': 'fullcalendar-2.3.2/fullcalendar.min', 
		'fullcalendarzh':'fullcalendar-2.3.2/lang/zh-cn',
		'moment': 'fullcalendar-2.3.2/lib/moment.min',
		'moment2.0': 'daterangepicker/moment',
		'dateprice': 'date_price',
		'daterangepicker': 'daterangepicker/daterangepicker',
		'chart': 'chart',
		'hcharts': [
			'hightchars/highcharts',
			'hightchars/highcharts-more',
			'hightchars/modules/exporting'
		],
		'chosen': 'chosen.jquery.min',
		'ztreecore': 'zTree_v3/js/jquery.ztree.core-3.5.min',
		'ztreecheck': 'zTree_v3/js/jquery.ztree.excheck-3.5.min',
		'jqgrid': 'jqGrid/jquery.jqGrid.min',
		'jqgridlang':'jqGrid/i18n/grid.locale-cn',
		'dist':'district',
		'datetimepicker': 'datetimepicker/bootstrap-datetimepicker.min',
		'colorbox': 'jquery.colorbox-min',
		'uploadify': 'uploadify/jquery.uploadify.min',
		'lhgdialog': 'lhgdialog/lhgcore.lhgdialog.min',
		'art': 'dialog-min'
	},
	shim:{
		'bootstrap': { 
			deps: ['jquery'],
			exports: '$'
		},
		'typeahead': { 
			deps: ['jquery']
		},
		'jquery.touch': { 
			deps: ['jquery']
		},
		'jquery.custom': { 
			deps: ['jquery']
		},
		'jquery.slimscroll': { 
			deps: ['jquery']
		},
		'jquery.piechart':{ 
			deps: ['jquery']
		},
		'jquery.sparkline':{ 
			deps: ['jquery']
		},
		'jquery.flot':{ 
			deps: ['jquery']
		},
		'jquery.flot.pie':{ 
			deps: ['jquery', 'jquery.flot']
		},
		'jquery.flot.resize':{ 
			deps: ['jquery', 'jquery.flot']
		},
		'ace-elements': { 
			deps: ['acemy']
		},
		'acemy': { 
			deps: ['bootstrap']
		},
		'wysiwyg': { 
			deps: ['jquery', 'bootstrap']
		},
		'baidueditor': {
			deps: ['ueditor1_4_3-utf8-php/ueditor.config']
		},
		'bdlang':{
			deps: ['baidueditor']
		},
		'datepicker': {
			deps: ['jquery', 'css!../../css/datepicker'],
			exports: '$'
		},
		'datepicker-lang': {
			deps: ['jquery', 'datepicker']
		},
		'daterangepicker':{
			deps: ['bootstrap', 'moment2.0', 'css!daterangepicker.css'],
			exports: '$'
		},
		'autosize': {
			deps: ['jquery']
		},
		'bootbox': {
			exports:'$',
			deps: ['bootstrap']
		},
		'filestyle':{
			deps: ['jquery']
		},
		'fullcalendar':{
			deps:['jquery', 'css!fullcalendar-2.3.2/fullcalendar.min'],
			exports: '$'
		},
		'fullcalendarzh':{
			deps:['fullcalendar']
		},
		'dateprice': {
			deps: ['jquery', 'css!../../css/date_price', 'css!../../css/form']
		},
		'hcharts': {
			deps: ['jquery']
		},
		'chosen': {
			deps: ['jquery', 'css!../../css/chosen', 'css!../../css/ace.min']
		},
		'ztreecore': {
			deps:['jquery', 'css!zTree_v3/css/zTreeStyle/zTreeStyle'],
			exports: '$'
		},
		'ztreecheck':{
			deps:['ztreecore'],
			exports: '$'
		},
		'jqgridlang': {
			deps: ['jquery']
		},
		'jqgrid':{
			deps:['jqgridlang'],
			exports: '$'
		},
		'dist':{
			exports: '$',
			deps: ['css!../../css/district']
		},
		'datetimepicker':{
			exports: '$',
			deps:['bootstrap', 'css!datetimepicker']
		},
		'colorbox':{
			deps:['jquery', 'css!../../css/colorbox'],
			exports:'$'
		},
		'uploadify':{
			deps:['jquery', 'css!../lib/uploadify/uploadify.css'],
			exports:'$'
		},
		'lhgdialog':{
			deps:['jquery'],
			exports: '$'
		},
		'art':{
			deps:['css!../../css/ui-dialog.css']
		}
	}
});
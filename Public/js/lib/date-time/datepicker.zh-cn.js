$.fn.datepicker.dates['zh-cn'] = {
    days: ["日", "一", "二", "三", "四", "五", "六"],
    daysShort: ["日", "一", "二", "三", "四", "五", "六"],
    daysMin: ["日", "一", "二", "三", "四", "五", "六"],
    months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
    monthsShort: ["一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二"],
    today: "今天",
    clear: "清空",
    format: "yyyy-MM-dd",
    titleFormat: "yyyy MM", 
    weekStart: 0
};
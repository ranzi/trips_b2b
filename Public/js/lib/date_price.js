require(["jquery", "util", "bootbox"], function($, util){ 
    var caigou_type = false;
    if($('#J_caigou_line_type').length){
        caigou_type = true;
    }
    
    var new_date = function(str) { 
        var date = new Date(); 
        date.setUTCFullYear(parseInt(str[0],10), parseInt(str[1],10) - 1, parseInt(str[2],10)); 
        date.setUTCHours(-8, 0, 0, 0); 
        return date;
    }
    var stop_bubble = function(event){
        if (event.stopPropagation) {
            event.stopPropagation();
        } else {
            event.cancelBubble = true;
        }
    }
    var check_price = function(youhui_price){
        var price = $('[name=menshijia]').val();
        if(price.length == 0){
            return true;
        }
        price = parseInt(price,10);
        if(price == 0){
            return true;
        }
        if(youhui_price > price){
            util.error('预订价不能高过门市价～');
            return false;
        }
        return true;
    }

    function get_section_tpl(){
        return '<div class="month_row" style="z-index:$z_index">'
                    +'<div class="sd">'
                         +'<p class="year">$year年</p>'
                         +'<p class="month">$month月</p>'
                    +' </div>'
                     +'<div class="hd" $is_display>'
                         +'<ul class="list-unstyled">'
                            +' <li>星期日</li>'
                             +'<li>星期一</li>'
                             +'<li>星期二</li>'
                             +'<li>星期三</li>'
                             +'<li>星期四</li>'
                             +'<li>星期五</li>'
                             +'<li>星期六</li>'
                         +'</ul>'
                     +'</div>'
                     +'<div class="bd">'
                         +'<ul class="clearfix list-unstyled">$lis</ul>'
                     +'</div>'
                 +'</div>';
    }
    
    $('.J_clear_all_price').click(function(){
        util.warning("确认清空所有报价么？", function(){
            $('.month_row .bd li:not(.disable)').each(function(){
                var self = $(this);
                if(self.find('.li_wrap').length === 1){
                    if(self.data('youhui_price')){
                        self.data({ youhui_price:'', child_price:'', kucun:''})
                            .find('.li_wrap')
                            .append('<a class="add_date_price J_add_date_price" href="javascript:;">添加价格</a>')
                            .find('.price')
                            .remove();
                    }
                }else{
                    var li_wrap = self.find('.li_wrap').eq(1);
                    li_wrap.find('.youhui').val('').removeClass('on');
                    li_wrap.find('.child').val('').removeClass('on');
                    li_wrap.find('.dijie').val('').removeClass('on');
                    li_wrap.find('.dijie_child').val('').removeClass('on');
                    li_wrap.find('.kucun').val('').removeClass('on');
                }
            })
        })
    });
    $('.J_week_all_select').click(function(){
        var val = this.checked;
        $('.J_week_select').each(function(){
            this.checked = val;
        })
    })
    $('.J_week_select').click(function(){
        var val = this.checked;
        if(!val){
            $('.J_week_all_select')[0].checked = val;
        }
    })
    $('#J_batch_add_by_week').click(function(){
        var inputs = $(this).parent().prev().find('input')
        var youhui_price = inputs.eq(0).val();
        var child_price = inputs.eq(1).val();
        var dijie = inputs.eq(2).val();
        var dijie_child = inputs.eq(3).val();
        if(!youhui_price){
            util.error('没输入预订价～');
            return;
        }
        if(!check_price(parseInt(youhui_price,10))){
            return;
        }
        var week_select_doms = $('.J_week_select:checked');
        if(week_select_doms.length === 0){
            util.error('要勾选日期哦～');
            return;
        }
        week_select_doms.each(function(){
            var val = this.value;
            $('.month_row .bd li').each(function(){
                var self = $(this);
                var index = self.index();
                if(index%7+'' === val+''&&!self.hasClass('disable')){
                    self.data({
                        youhui_price:youhui_price,
                        child_price:child_price,
                        dijie: dijie,
                        dijie_child: dijie_child
                    })
                    var price_add_btn = self.find('.J_add_date_price');
                    if(price_add_btn.length>0){
                        price_add_btn.remove();
                        self.find('.li_wrap').append('<a class="price" href="javascript:;">￥'+youhui_price+'<i class="J_del_price"></i></a>');
                    }else{
                        var quick_edit_inputs = self.find('.J_quick_edit_input');
                        if(quick_edit_inputs.length>0){
                            quick_edit_inputs.eq(0).val(youhui_price);
                            quick_edit_inputs.eq(1).val(child_price);
                            quick_edit_inputs.eq(2).val(dijie);
                            quick_edit_inputs.eq(3).val(dijie_child);
                            quick_edit_inputs.addClass('on');
                        }else{
                            self.find('.price').html('￥'+youhui_price+'<i class="J_del_price"></i>');
                        }
                    }
                }
            })
            this.checked = false;
        });
        $('.J_week_all_select')[0].checked = false;
        inputs.val('');
    })
    
    $('#J_batch_add_by_dateandweek').click(function(){
        var price_inputs = $(this).parent().prev().find('input');
        var youhui_price = price_inputs.eq(0).val();
        var child_price = price_inputs.eq(1).val();
        var dijie = price_inputs.eq(2).val();
        var dijie_child = price_inputs.eq(3).val();
	    var start_date = $('input[name="rangetime[start]"]').val();
        var end_date = $('input[name="rangetime[end]"]').val();
	
        if(!youhui_price){
            util.error('没输入预订价～');
            return;
        }
        if(!check_price(parseInt(youhui_price,10))){
            return;
        }
        if(!start_date){
            util.error('没选择开始时间哦～');
            return;
        }
        if(!end_date){
            util.error('没选择结束时间哦～');
            return;
        }
        var start_date_t = new_date(start_date.split('-')).getTime();
        var end_date_t = new_date(end_date.split('-')).getTime();
        if(start_date_t >= end_date_t){
            util.error('开始日期比结束日期大，要重新选择～');
            return;
        }
        
        var week_select_doms1 = $('.J_week_select1:checked');
        if(week_select_doms1.length === 0){
            util.error('没有选择周几哦～');
            return;
        }

        week_select_doms1.each(function(){
            var val = this.value;
            $('.month_row .bd li').each(function(){
                var self = $(this);
                if(self.hasClass('disable')){
                    return;
                }
                var index = self.index();
                if(index%7+'' === val+''&&!self.hasClass('disable')){
                    var cur_date_t = new_date(self.data('date').split('-')).getTime();
                    if(cur_date_t >= start_date_t && cur_date_t <= end_date_t){
                        self.data({
                            youhui_price:youhui_price,
                            child_price:child_price,
                            dijie: dijie,
                            dijie_child: dijie_child
                        })
                        var price_add_btn = self.find('.J_add_date_price');
                        if(price_add_btn.length>0){
                            price_add_btn.remove();
                            self.find('.li_wrap').append('<a class="price" href="javascript:;">￥'+youhui_price+'<i class="J_del_price"></i></a>');
                        }else{
                            var quick_edit_inputs = self.find('.J_quick_edit_input');
							
                            if(quick_edit_inputs.length>0){
                                quick_edit_inputs.eq(0).val(youhui_price);
                                quick_edit_inputs.eq(1).val(child_price);
                                quick_edit_inputs.eq(2).val(dijie);
                                quick_edit_inputs.eq(3).val(dijie_child);
                                quick_edit_inputs.addClass('on');
                            }else{
                                self.find('.price').html(youhui_price+'<i class="J_del_price"></i>');
                            }
                        }
                    }
                }
            })
        })
        price_inputs.val('');
    })
    
    $('#J_batch_add_by_date').click(function(){
        var price_inputs = $(this).parent().find('input');
        var youhui_price = price_inputs.eq(0).val();
        var child_price = price_inputs.eq(1).val();
        var dijie = price_inputs.eq(2).val();
        var dijie_child = price_inputs.eq(3).val();
        var start_date = $('input[name="rangetime[start]"]').val();
        var end_date = $('input[name="rangetime[end]"]').val();
        if(!youhui_price){
            util.error('没输入预订价～');
            return;
        }
        if(!check_price(parseInt(youhui_price,10))){
            return;
        }
        if(!start_date){
            util.error('没选择开始时间哦～');
            return;
        }
        if(!end_date){
            util.error('没选择结束时间哦～');
            return;
        }
        var start_date_t = new_date(start_date.split('-')).getTime();
        var end_date_t = new_date(end_date.split('-')).getTime();
        if(start_date_t >= end_date_t){
            util.error('开始日期比结束日期大，要重新选择～');
            return;
        }
        $('.month_row .bd li').each(function(){
            var self = $(this);
            if(self.hasClass('disable')){
                return;
            }
            var cur_date_t = new_date(self.data('date').split('-')).getTime();
            if(cur_date_t >= start_date_t && cur_date_t <= end_date_t){
                self.data({
                    youhui_price:youhui_price,
                    child_price:child_price,
                    dijie:dijie,
                    dijie_child:dijie_child
                })
                var price_add_btn = self.find('.J_add_date_price');
                if(price_add_btn.length>0){
                    price_add_btn.remove();
                    self.find('.li_wrap').append('<a class="price" href="javascript:;">￥'+youhui_price+'<i class="J_del_price"></i></a>');
                }else{
                    var quick_edit_inputs = self.find('.J_quick_edit_input');
                    if(quick_edit_inputs.length>0){
                        quick_edit_inputs.eq(0).val(youhui_price);
                        quick_edit_inputs.eq(1).val(child_price);
                        quick_edit_inputs.eq(2).val(dijie);
                        quick_edit_inputs.eq(3).val(dijie_child);
                        quick_edit_inputs.addClass('on');
                    }else{
                        self.find('.price').html(youhui_price+'<i class="J_del_price"></i>');
                    }
                }
            }
        })
        price_inputs.val('');
    })
    
    //事件代理
    $('#J_date_table').on('click','.J_add_date_price,.price',function(event){
        stop_bubble(event);
        var self = $(this);
        var parent = self.parents('li');
        var content = '<div class="row"><div class="col-xs-12 form form-horizontal">';
        content += '<div class="form-group">';
        content += '<label class="col-xs-12 col-sm-3 control-label">预订价</label>';
        content += '<div class="col-xs-12 col-sm-9">';
        content += '<input type="text" class="form-control youhui" value="'+(parent.data('youhui_price')||'')+'">';
        content += '</div>';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label class="col-xs-12 col-sm-3 control-label">预订儿童价</label>';
        content += '<div class="col-xs-12 col-sm-9">';
        content += '<input type="text" class="form-control child" value="'+(parent.data('child_price')||'')+'">';
        content += '</div>';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label class="col-xs-12 col-sm-3 control-label">地接价</label>';
        content += '<div class="col-xs-12 col-sm-9">';
        content += '<input type="text" class="form-control dijie" value="'+(parent.data('dijie')||'')+'">';
        content += '</div>';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label class="col-xs-12 col-sm-3 control-label">地接儿童价</label>';
        content += '<div class="col-xs-12 col-sm-9">';
        content += '<input type="text" class="form-control dijie_child" value="'+(parent.data('dijie_child')||'')+'">';
        content += '</div>';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label class="col-xs-12 col-sm-3 control-label">库存</label>';
        content += '<div class="col-xs-12 col-sm-9">';
        content += '<input type="text" class="form-control kucun" value="'+(parent.data('kucun')||'')+'">';
        content += '</div>';
        content += '</div>';
        content += '</div></div>';
        var $dialog = bootbox.dialog({
            title: '添加价格',
            message: content,
            buttons: {
                cancel: {
                    label: '取消',
                    className: 'btn btn-default'
                },
                ok: {
                    label: '确定',
                    className: 'btn btn-success',
                    callback: function(){
                        var li = parent;
                        li.data('youhui_price',$dialog.find('.youhui').val());
                        li.data('child_price',$dialog.find('.child').val());
                        li.data('dijie',$dialog.find('.dijie').val());
                        li.data('dijie_child',$dialog.find('.dijie_child').val());
                        li.data('kucun',$dialog.find('.kucun').val());
                        li.trigger('data_insert');
                    }
                }
            }
        });
    })
    $('#J_date_table').on('click','.J_del_price',function(event){
        stop_bubble(event)
        var self = $(this);
        var parent = self.parents('li');
        var position = parent.position();
        var li = self.parents('li');
        li.data('youhui_price','');
        li.data('child_price','');
        li.data('dijie','');
        li.data('dijie_child','');
        li.data('kucun','');
        li.find('.price').remove();
        li.find('.li_wrap').append('<a class="add_date_price J_add_date_price" href="javascript:;">添加价格</a>');
    })
    $('#J_date_table').on('click','.J_quick_edit_input',function(){
        $(this).addClass('on').off('blur').blur(function(){
            var self = $(this);
            if(self.val() === ''){
                self.removeClass('on')
            }
        })
    })
    
    //表格html生成，依赖今天的日期，今后3个月的价格数据
    var earlier_date = 0;
    var today = new Date();
    var today_arr=[];
    today_arr[0] = today.getFullYear();
    today_arr[1] = today.getMonth();
    today_arr[2] = today.getDate();
    var expire_day_arr=[];
    expire_day_arr[0] = (today_arr[1] + 3) > 12 ? today_arr[0] + 1 : today_arr[0];
    expire_day_arr[1] = (today_arr[1] + 3) > 12 ? today_arr[1] - 9 : today_arr[1] + 3;
    expire_day_arr[2] = today_arr[2];
    var new_date_today = new_date(today_arr);
    var new_date_expire_day = new_date(expire_day_arr);
    var data_arr = $('#date_price_data').val();
    data_arr = data_arr?$.parseJSON(data_arr):[];
    // for(var i = 0;i<data_arr.length;i++){
    //     var arr = data_arr[i].split('|');
    //     data_arr['date_'+new_date(arr[0].split('-')).getTime()] = arr[1].split('-');
    // }
    var html = '';
    var section_month_template = get_section_tpl();
    //判断今天是否月末最后一天，是的话就只排列3个月份的表格
    var y_m_arr = [];//构建月份表格[[2013,11],[2013,12]]
    y_m_arr.push([new_date_today.getFullYear(),new_date_today.getMonth()+1])
    //对今天的date对象重置
    new_date_today = new_date(today_arr);
    new_date_today.setDate(15);//避免出现月头和月尾，月头，如果月尾是31号那么在setMonth+1后可能会出现跳月的现象
    new_date_today.setMonth(new_date_today.getMonth() + 1);//月份+1
    while(
        new_date_today.getTime() < new_date_expire_day.getTime() &&
        new_date_today.getMonth() !== new_date_expire_day.getMonth()
    ){
        y_m_arr.push([new_date_today.getFullYear(),new_date_today.getMonth()+1]);
        new_date_today.setMonth(new_date_today.getMonth() + 1);//月份+1
    }
    if(!(today_arr[0] === expire_day_arr[0] && today_arr[1] === expire_day_arr[1])){
        y_m_arr.push([new_date_expire_day.getFullYear(),new_date_expire_day.getMonth()+1]);//装载结束日的年份和月份
    }
    
    //对今天的date对象重置
    new_date_today = new_date(today_arr);
    for(var i = 0; i < y_m_arr.length; i++){
        var lis = '';
        var from = new_date([y_m_arr[i][0],y_m_arr[i][1],1]);
        var hd_is_display = 'style="display:none;"';
        //判断循环的月份是否是今天的月份
        if(today_arr[0] === y_m_arr[i][0] && today_arr[1] === y_m_arr[i][1]){
            from = new_date_today;
            hd_is_display = '';
        }
        var to = new_date([y_m_arr[i][0],y_m_arr[i][1]+1,1]);
        to.setDate(to.getDate() - 1);
        //判断循环的月份是否是结束日的月份
        if(expire_day_arr[0] === y_m_arr[i][0] && expire_day_arr[1] === y_m_arr[i][1]){
            to = new_date_expire_day;
        }
        //对月始补全
        var week = from.getDay();
        var complete_start_date = new_date([from.getFullYear(),from.getMonth()+1,from.getDate()]);//克隆from
        complete_start_date.setDate(complete_start_date.getDate() - week - 1);
        // if(to.getDate() - from.getDate() < 7){
            // complete_start_date.setDate(complete_start_date.getDate() - 7);
            // week = week+7;
        // }
        for(k = 0; k < week; k++){
            complete_start_date.setDate(complete_start_date.getDate() + 1);
            if(complete_start_date.getMonth()+1 === y_m_arr[i][1]){
                lis += '<li class="disable"><div class="li_wrap"><p>'+complete_start_date.getDate()+'</p></div></li>';
            }else{
                lis += '<li class="disable"><div class="li_wrap"></div></li>';
            }
        }
        var temp_today = new_date(today_arr);
        temp_today.setDate(temp_today.getDate() + earlier_date - 1);
        for(j = from; j <= to; j.setDate(j.getDate() + 1)){
            var date_text = j.getFullYear()+'-'+(j.getMonth()+1)+'-'+j.getDate();
            var price = data_arr[date_text];
            var price_date_string = '';
            if(price){
                price_date_string = 'data-youhui_price="'+price.booking+'" data-child_price="'+price.booking_child+'" data-dijie="'+price.dijie+'"  data-dijie_child="'+price.dijie_child+'" data-kucun="'+price.stock+'"';
            }
            var J_batch_li_str = 'J_batch_li';
            //是否在有效期内
            if(j <= temp_today){
                lis += '<li class="disable" data-date="'+date_text+'" '+price_date_string+'>';
                J_batch_li_str = '';
            }else{
                lis += '<li data-date="'+date_text+'" '+price_date_string+'>';
            }
            //是否节日
            if(false){
                lis += '<div class="li_wrap '+J_batch_li_str+' jieri">';
            }else{
                lis += '<div class="li_wrap '+J_batch_li_str+'">';
            }
            lis += '<p>'+j.getDate()+'</p>';
            //是否今天
            if(j.toString() === new_date(today_arr).toString()){
                lis += '<p>今天</p>';
            }
            //当前日期是否有价格
            if(j > temp_today){
                if(price){
                    lis += '<a class="price" href="javascript:;">￥'+price.booking+(caigou_type?'':'<i class="J_del_price"></i>')+'</a>';
                }else{
                    if(!caigou_type){
                        lis += '<a class="add_date_price J_add_date_price" href="javascript:;">添加价格</a>'
                    }
                }
            }
            //'<i class="jieri_tit">中秋节</i>'
            lis += '</div>'
            lis += '</li>'
        }
        html += section_month_template.replace('$year',y_m_arr[i][0])
                                      .replace('$month',y_m_arr[i][1])
                                      .replace('$lis',lis)
                                      .replace('$is_display',hd_is_display)
                                      .replace('$z_index',10-i);
    }
    $('#J_date_table').append(html);
    //日期表格事件绑定
    $('.month_row .bd li').on('data_insert',function(){
        var self = $(this);
        var youhui_price = self.data('youhui_price');
        if(youhui_price){
            self.find('.J_add_date_price').remove();
            if(self.find('.price').length==0){
                self.find('.li_wrap').append('<a class="price" href="javascript:;">￥'+youhui_price+'<i class="J_del_price"></i></a>');
            }else{
                self.find('.price').html('￥'+youhui_price+'<i class="J_del_price"></i>');
            }
        }else{
            self.find('.price').remove();
            if(self.find('.J_add_date_price').length==0){
                self.find('.li_wrap').append('<a class="add_date_price J_add_date_price" href="javascript:;">添加价格</a>');
            }
        }
    });
});
define(["bootstrap"], function($){
	$('input.dist-input').focus(function(){
		var dist = $(this).next('.dist-main');
		dist.css('display','block');
		dist.find('.hide').removeClass('hide');
		dist.find('.tab-pane.active .dist-main').css('display','block');
	});
	$('.dist-close').click(function(){
		$(this).parent('.dist-main').css('display','none');
	});
	$('.dist-main .dist-item li a').click(function(){
		var p=$(this).closest('.dist-container');
		p.find('input.dist-input').val($(this).attr('data-val'));
		p.find('input.dist-input').next('.dist-main').css('display','none');
		return false;
	});
	$('input.dist-input').keyup(function(evt){
		var val = $(this).val();
		var dist = $(this).next('.dist-main');
		if(val == ''){
			dist.find('.hide').removeClass('hide');
			return;
		}
		val = val.toUpperCase();
		// var cur = $('.dist-main .nav-tabs li.active a');
		var cur = dist.find('.nav-tabs li.active a');
		var id = cur.attr('href');
		var index = cur.html();
		if(evt.keyCode >= 65 && evt.keyCode <= 90){
			if(val.length == 1 && index.indexOf(val) === -1){
				// $('.dist-main .nav-tabs li').each(function(){
				dist.find('.nav-tabs li').each(function(){
					if($(this).find('a').html().indexOf(val) !== -1){
						cur = $(this).find('a');
						index = cur.html();
						id = cur.attr('href');
						cur.trigger('click');
						return;
					}
				});
			}
			var reg = '.*';
			for(var i = 0, l = val.length; i < l; i++){
				reg+=val.charAt(i) + '.*';
			}
			reg = new RegExp(reg, "i");
			// console.log(reg)
			dist.find(id).find('.dist-item li').each(function(){
				var self = $(this);
				var item = self.find('a');
				var py = item.attr('data-py').toUpperCase();
				// if(py.indexOf(val) !== 0){
				if(!reg.test(py)){
					self.addClass('hide');
				}
			});
			dist.find(id).find('.dist-item').each(function(){
				if($(this).find('li').length == $(this).find('li.hide').length){
					$(this).parent().parent().addClass('hide');
				}
			});
		} else if(evt.keyCode == 8){
			if(/^[a-zA-Z]+$/.test(val)){
				dist.find(id).find('.dist-item li').each(function(){
					var self = $(this);
					var item = self.find('a');
					var py = item.attr('data-py').toUpperCase();
					self.removeClass('hide');
					if(py.indexOf(val) !== 0){
						self.addClass('hide');
					}
				});
				dist.find(id).find('.dist-item').each(function(){
					if($(this).find('li').length == $(this).find('li.hide').length){
						$(this).parent().parent().addClass('hide');
					} else {
						$(this).parent().parent().removeClass('hide');
					}
				});
			}
		}
	});
});
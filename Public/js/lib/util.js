define(['bootstrap', 'bootbox'], function($){
	var module = {};
	
	module.error = function(msg){
		var icons = {
			success : 'check-circle',
			error :'times-circle',
			info : 'info-circle',
			warning : 'exclamation-triangle'
		};
		bootbox.dialog({
			title: '<span style="color:rgb(165, 31, 28) "><i class="icon-times-circle"></i>系统提示</span>',
			message: msg,
			buttons: {
				ok: {
					label: '确定',
					className: 'btn btn-success'
				}
			}
		});
	};

	module.success = function(msg){
		bootbox.dialog({
			title: '<span style="color:rgb(134, 181, 88)"><i class="icon-check-circle"></i>系统提示</span>',
			message: msg,
			buttons: {
				ok: {
					label: '确定',
					className: 'btn btn-success'
				}
			}
		});
	}

	module.warning = function(msg, callback){
		bootbox.dialog({
			title: '<span style="color:rgb(229, 151, 41)"><i class="icon-exclamation-triangle"></i>系统提示</span>',
			message: msg,
			buttons: {
				cancel: {
					label: '取消',
					className: 'btn btn-default'
				},
				ok: {
					label: '确定',
					className: 'btn btn-success', 
					callback: typeof callback === 'function' ? callback : ''
				}
			}
		});
	}

	module.cookie = {
		'prefix' : '',
		// 保存 Cookie
		'set' : function(name, value, seconds) {
			expires = new Date();
			expires.setTime(expires.getTime() + (1000 * seconds));
			document.cookie = this.name(name) + "=" + escape(value) + "; expires=" + expires.toGMTString() + "; path=/";
		},
		// 获取 Cookie
		'get' : function(name) {
			cookie_name = this.name(name) + "=";
			cookie_length = document.cookie.length;
			cookie_begin = 0;
			while (cookie_begin < cookie_length)
			{
				value_begin = cookie_begin + cookie_name.length;
				if (document.cookie.substring(cookie_begin, value_begin) == cookie_name)
				{
					var value_end = document.cookie.indexOf ( ";", value_begin);
					if (value_end == -1)
					{
						value_end = cookie_length;
					}
					return unescape(document.cookie.substring(value_begin, value_end));
				}
				cookie_begin = document.cookie.indexOf ( " ", cookie_begin) + 1;
				if (cookie_begin == 0)
				{
					break;
				}
			}
			return null;
		},
		// 清除 Cookie
		'del' : function(name) {
			var expireNow = new Date();
			document.cookie = this.name(name) + "=" + "; expires=Thu, 01-Jan-70 00:00:01 GMT" + "; path=/";
		},
		'name' : function(name) {
			return this.prefix + name;
		}
	};//end cookie
	return module;
});

<?php
/**
 *@author Chunlei Wang<sihangshi2011@gmail.com>
 *@version $Id 2015-07-29
 *
 *槛菊愁烟兰泣露
 *罗幕轻寒
 *燕子双飞去
 *明月不谙离恨苦
 *斜光到晓穿朱户
 *昨夜西风凋碧树
 *独上高楼
 *望断天涯路
 *欲寄彩笺兼尺素
 *山长水阔知何处
 **/
namespace Think\Template\TagLib;
use Think\Template\TagLib;

class Form extends TagLib {
    protected $tags = array(
        'editor'     => array('attr' => 'id,name,style,width,height,type,onerror', 'close' => 1),
        'calendar'   => array('attr' => 'id,name,style,type,startdate,lang', 'close' => 0),
        'image'      => array('attr' => 'id,name,src,onchange,chose,rechose,nofile', 'close' => 0),
        'dragimage'  => array('attr' => 'id,name,src,onchange,chose,rechose,nofile', 'close' => 0),
        'select'     => array('attr' => 'id,name,style,click,change,type,options', 'close' => 0),
        'txtbox'     => array('attr' => 'id,name,style', 'close' => 0),
        'datetime'   => array('attr' => 'id,name,style', 'close' => 0),
        'ajaximage'  => array('attr' => 'name,style,value,default', 'close' => 0),
        'multimage'  => array('attr' => 'name,style,values,default', 'close' => 0),
        'daterange'  => array('attr' => 'name,style,startdate,enddate,time', 'close' => 0),
        'chosen'     => array('attr' => 'id,name,style,options,type,onchange,multiple,default', 'close' => 0),
        'list'       => array('attr' => 'id,pk,style,action,actionlist,show,datasource,checkbox', 'close' => 0),
        'seltree'    => array('attr' => 'id,name,style,default', 'close' => 0),
        'district'   => array('attr' => 'id,level,target', 'close' => 0),
        'attachment' => array('attr' => 'name,value,size,limit', 'close' => 0),
    );

    public function _editor($tag, $content) {
        $id      = !empty($tag['id']) ? $tag['id'] : '_editor';
        $name    = !empty($tag['name']) ? $tag['name'] : 'contents';
        $style   = !empty($tag['style']) ? $tag['style'] : 'wysiwyg-editor';
        $type    = !empty($tag['type']) ? $tag['type'] : 'wysiwyg';
        $onerror = !empty($tag['onerror']) ? $tag['onerror'] : 'showErrorAlert';
        $content = $content ? $content : '请在这里输入内容';
        $height  = $tag['height'] ?: 300;
        $width   = $tag['width'] ?: 980;
        switch ($type) {
            case 'wysiwyg':
                $parseStr = '<!--wysiwyg editor start--><div class="' . $style . '" id="' . $id . '">' . $content . '</div><script type="text/javascript"> require(["jquery", "bootstrap", "acemy", "ace-elements", "wysiwyg"], function($){function showErrorAlert (reason, detail) {var msg=""; if (reason==="unsupported-file-type") { msg = "不支持的文件类型" +detail; } else {console.log("error uploading file", reason, detail); } $("<div class=\"alert\"> <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button><strong>File upload error</strong> "+msg+" </div>").prependTo("#alerts"); } $("#' . $id . '").ace_wysiwyg({toolbar: ["font", null, "fontSize", null, {name:"bold", className:"btn-info"}, {name:"italic", className:"btn-info"}, {name:"strikethrough", className:"btn-info"}, {name:"underline", className:"btn-info"}, null, {name:"insertunorderedlist", className:"btn-success"}, {name:"insertorderedlist", className:"btn-success"}, {name:"outdent", className:"btn-purple"}, {name:"indent", className:"btn-purple"}, null, {name:"justifyleft", className:"btn-primary"}, {name:"justifycenter", className:"btn-primary"}, {name:"justifyright", className:"btn-primary"}, {name:"justifyfull", className:"btn-inverse"}, null, {name:"createLink", className:"btn-pink"}, {name:"unlink", className:"btn-pink"}, null, {name:"insertImage", className:"btn-success"}, null, "foreColor", null, {name:"undo", className:"btn-grey"}, {name:"redo", className:"btn-grey"} ], "wysiwyg": {fileUploadError: ' . $onerror . ' } }); }); </script><!--wysiwyg editor end-->';
                break;
            case 'baidu':
                $parseStr = '<!--baidu editor start--><script  type="text/plain" id="' . $id . '" name="' . $name . '" style="height:' . $height . 'px;width:' . $width . '">' . $content . '</script> <script type="text/javascript"> require(["baidueditor", "zeroclipboard", "bdlang"], function(UE, zcl){window.ZeroClipboard = zcl; var ue = UE.getEditor("' . $id . '"); }) </script><!--baidu editor end-->';
                break;
        }
        return $parseStr;
    }

    public function _calendar($tag, $content) {
        $id        = $tag['id'] ?: '_calendar';
        $name      = $tag['name'] ?: '_calendar';
        $style     = $tag['style'] ?: '';
        $type      = $tag['type'] ?: 'date';
        $startDate = $tag['startdate'] ? $this->tpl->get($tag['startdate']) : '';
        $lang      = $tag['lang'] ?: 'zh-CN';
        switch ($type) {
            case 'date':
                $parseStr = '<!--datepicker start--><input class="form-control date-picker ' . $style . '" id="' . $id . '" name="' . $name . '" type="text" value="' . $content . '" /> <span class="input-group-addon"> <i class="icon-calendar bigger-110"></i> </span><script type="text/javascript"> require(["jquery", "datepicker", "datepicker-lang"], function($){$(".date-picker").datepicker({autoclose:true,startDate:"' . $startDate . '",language:"' . $lang . '"}).next().on("click", function(){$(this).prev().focus(); }); }) </script><!--datepicker end-->';
                break;
            case 'daterange':
                $parseStr = '<!--daterange start--><!--daterange end-->';
                break;
        }
        return $parseStr;
    }

    public function _select($tag) {
        $id       = $tag['id'] ?: '_select';
        $name     = $tag['name'] ?: '_select';
        $style    = $tag['style'] ? $tag['style'] : 'form-control';
        $change   = $tag['onchange'] ? $tag['onchange'] . '(this);' : '';
        $click    = $tag['click'];
        $_key     = $tag['key'];
        $_val     = $tag['val'];
        $multiple = isset($tag['multiple']);
        $options  = $tag['options'];
        $values   = $tag['values'];
        $output   = $tag['output'];
        $selected = $tag['selected'];

        $holder = isset($tag['holder']) ? $tag['holder'] : '==请选择==';

        $parseStr = '<!--default select start--><select name="' . $name . '" id="' . $id . '" onClick="' . $click . '" onChange="' . $change . '" class="' . $style . '" ' . ($multiple ? ' multiple="multiple"' : '') . '>';
        $options  = $options ? $this->tpl->get($options) : array();
        $selected = $selected ? $this->tpl->get($selected) : '';
        $parseStr .= '<option value="">' . $holder . '</option>';
        if (!empty($options)) {
            if (!empty($_key) && !empty($_val)) {
                foreach ($options as $key => $val) {
                    $parseStr .= '<option value="' . $val[$_key] . '"' . ($val[$_key] == $selected ? ' selected="selected"' : '') . '>' . $val[$_val] . '</option>';
                }
            } else {
                foreach ($options as $key => $val) {
                    $parseStr .= '<option value="' . $key . '" ' . ($selected == $key ? ' selected="selected"' : '') . '>' . $val . '</option>';
                }
            }
        } else if (!empty($values)) {
            $values = $this->tpl->get($values);
            $output = $this->tpl->get($output);
            for ($i = 0, $len = count($values); $i < $len; $i++) {
                $parseStr .= '<option value="' . $values[$i] . '"' . ((is_array($selected) && in_array($values[$i], $selected)) || $values[$i] == $selected ? ' selected="selected"' : '') . '>' . $output[$i] . '</option>';
            }
        }
        $parseStr .= '</select><!--default select end-->';

        return $parseStr;
    }

    public function _image($tag) {
        $id      = $tag['id'] ?: '_simage';
        $name    = $tag['name'] ?: '_simage';
        $change  = $tag['onchange'] ?: '';
        $nofile  = $tag['nofile'] ?: '请选择图片';
        $chose   = $tag['chose'] ?: '上传图片';
        $rechose = $tag['rechose'] ?: '重新上传';
        $src     = $tag['src'];

        $parseStr = '<input type="file" id="' . $id . '" name="' . $name . '" value="' . $src . '" style="height:auto;width:auto;" /><script type="text/javascript"> require(["jquery", "ace-elements"], function($){$("#' . $id . '").ace_file_input({no_file:"' . $nofile . '", btn_choose:"' . $chose . '", btn_change:"' . $rechose . '", droppable:false, onchange:null, thumbnail:false, whitelist:"gif|png|jpg|jpeg", ' . ($change ? 'onchange: ' . $change : '') . ' }); }) </script>';
        return $parseStr;
    }

    public function _dragimage($tag) {
        $id       = $tag['id'] ?: '_simage';
        $name     = $tag['name'] ?: '_simage';
        $change   = $tag['onchange'] ?: '';
        $chose    = $tag['chose'] ?: '请选择图片';
        $src      = $tag['src'];
        $multiple = $tag['multiple'];

        $parseStr = '<input type="file" id="' . $id . '" name="' . $name . '" value="' . $src . '" ' . ($multiple ? ' multiple="" ' : '') . ' style="width:auto;height:auto;" /><script type="text/javascript"> require(["jquery", "ace-elements"], function($){$("#' . $id . '").ace_file_input({style:"well", btn_choose:"' . $chose . '", btn_change:null, no_icon:"icon-cloud-upload", droppable:true, thumbnail:"small", ' . ($change ? 'onchange:' . $change . ',' : '') . ' preview_error : function(filename, error_code) {alert("文件预览失败"); } });}); </script>';
        return $parseStr;
    }

    public function _txtbox($tag) {
        $id      = $tag['id'] ?: '_txtbox';
        $name    = $tag['name'] ?: '_txtbox';
        $style   = $tag['style'];
        $content = $this->tpl->get($tag['content']);

        $parseStr = '<textarea id="' . $id . '" name="' . $name . '" class="autosize-transition form-control ' . $style . '">' . $content . '</textarea>';
        if (!defined('AUTOSIZE_TEXTAREA')) {
            $parseStr .= '<script type="text/javascript">
            var ta;
            require(["jquery", "at"],function($, at){
                ta=at($("textarea[class*=autosize]"));
            });
            </script>';
        }
        define('AUTOSIZE_TEXTAREA', true);
        return $parseStr;
    }

    public function _datetime($tag) {
        $id       = $tag['id'] ?: '_datetime';
        $name     = $tag['name'] ?: '_datetime';
        $lang     = $tag['lang'] ?: 'zh-CN';
        $time     = $tag['time'];
        $parseStr = '';
        if ($time && !defined('TPL_INIT_DATE_TIME')) {
            $parseStr .= '<script type="text/javascript">
                require(["datetimepicker"], function($){
                    $(function(){
                        $(".datetimepicker.datetime").each(function(){
                            var opt = {
                                language: "zh-CN",
                                minView: 0,
                                autoclose: true,
                                format : "yyyy-mm-dd hh:ii",
                                todayBtn: true,
                                minuteStep: 5
                            };
                            $(this).datetimepicker(opt);
                        });
                    });
                });
            </script>';
            define('TPL_INIT_DATE_TIME', true);
        }
        if (!$time && !defined('TPL_INIT_DATE')) {
            $parseStr .= '<script type="text/javascript">
                require(["datetimepicker"], function($){
                    $(function(){
                        $(".datetimepicker.date").each(function(){
                            var opt = {
                                language: "zh-CN",
                                minView: 2,
                                format: "yyyy-mm-dd",
                                autoclose: true,
                                todayBtn: true
                            };
                            $(this).datetimepicker(opt);
                        });
                    });
                });
            </script>';
            define('TPL_INIT_DATE', true);
        }
        $class       = $time ? 'datetime' : 'date';
        $placeholder = $time ? '日期时刻' : '日期';
        $value       = $tag['value'] ? $this->tpl->get($tag['value']) : '';
        $value       = date('Y-m-d' . ($time ? ' H:i' : ''), $value ?: NOW_TIME);
        $parseStr .= '<div class="input-group"><input type="text" name="' . $name . '" value="' . $value . '" placeholder="' . $placeholder . '"  readonly="readonly" class="datetimepicker ' . $class . ' form-control" id="' . $id . '"/><span class="input-group-addon"><i class="icon-calendar"></i></span></div>';
        return $parseStr;
    }

    /**
     *说明：
     *图片上传
     *@param $dir String 指定文件上传目录,如传入 mydir/mysubdir,将变成 ./uploads/mydir/mysubdir
     *@param $size String 指定文件尺寸，格式：32x32，若不指定，将使用系统设置的缩略图尺寸
     *@param $nowater Integer 不添加水印,如上传的图片不需要水印，则设置为1，否则留空.若系统开启水印功能，默认将添加水印
     */
    public function _ajaximage($tag) {
        $name    = $tag['name'] ?: '_ajaximage';
        $style   = $tag['style'];
        $default = $tag['default'] ?: '/Public/images/nopic.jpg';
        $value   = $this->tpl->get($tag['value']) ?: $default;
        $dir     = $tag['dir'];
        $size    = $tag['size'];
        $nowater = $tag['nowater'];

        $parseStr .= '<div class="input-group "> <input type="text" name="' . $name . '" value="' . $value . '" class="form-control" autocomplete="off" readonly="true"> <span class="input-group-btn"> <button class="btn btn-default btn-sm" type="button" onclick="showImageDialog(this);" data-nowater="' . $nowater . '" data-dir="' . $dir . '" data-size="' . $size . '">选择图片</button> </span> </div> <div class="input-group " style=""> <img src="' . $value . '" onerror="this.src=\'' . $default . '\'; this.title=\'图片未找到.\'" class="img-responsive img-thumbnail" style="max-width:150px;" /> </div>';
        if (!defined('TPL_INIT_IMAGE')) {
            $types = D('Admin/Zsysdata')->getAll(array('type' => 'images'));
            $parseStr .= '<script type="text/javascript">
            function showImageDialog(ele){
                require(["jquery", "bootstrap", "bootbox", "filestyle"], function($){
                    var btn = $(ele);
                    var dialog = bootbox.dialog({
                        message: \'<iframe width="0" height="0" name="__image_file_uploader" style="display:none;"></iframe>\'
                        +\'<ul class="nav nav-tabs">\'
                        +\'<li class="active"><a data-toggle="tab" href="#local">本地图片</a></li>\'
                        +\'<li> <a data-toggle="tab" href="#remote">远程图片 </a> </li>\'
                        +\'<li><a data-toggle="tab" href="#uploaded">浏览附件</a></li>\'
                        +\'</ul>\'
                        +\'<div class="tab-content">\'
                        +\'<div id="local" class="tab-pane in active"> \'
                        +\'<form action="/index.php/Admin/Base/frameUpload/callback/uploadLocalPic" enctype="multipart/form-data" method="post" target="__image_file_uploader">\'
                        +\'<input type="file" name="upfile" >\'
                        +\'<input type="hidden" name="dir" value="">\'
                        +\'<input type="hidden" name="size" value="">\'
                        +\'<input type="hidden" name="nowater" value="">\'
                        +\'</form></div>\'
                        +\' <div id="remote" class="tab-pane">\'
                        +\'<form action="/index.php/Admin/Base/frameUpload/callback/uploadLocalPic" method="post" target="__image_file_uploader">\'
                        +\'<input type="txt" name="url" class="form-control" placeholder="请输入图片URL" >\'
                        +\'<input type="hidden" name="dir" value="">\'
                        +\'<input type="hidden" name="size" value="">\'
                        +\'<input type="hidden" name="nowater" value="">\'
                        +\'</form></div>\'
                        +\'<div class="tab-pane" id="uploaded">\'
                        +\'<div class="row">\'
                        +\'<div class="col-xs-12">\'
                        +\'类型：<label class="radio-inline"><input type="radio" name="type" value="1" checked>按分类</label>\'
                        +\'<label class="radio-inline"><input type="radio" name="type" value="2">按目录</label>\'
                        +\'</div>\'
                        +\'<div class="col-xs-12">分类：\'';
            foreach ($types as $key => $row) {
                $parseStr .= '+\'<label class="radio-inline"><input type="radio" name="type2" value="' . $row['id'] . '"' . ($key == 0 ? ' checked' : '') . '>' . $row['title'] . '</label>\'';
            }
            $parseStr .= '+\'</div>\'
                        +\'<div class="col-xs-12" id="uploaded-content"></div>\'
                        +\'</div>\'
                        +\'</div>\'
                        +\'</div>\',
                        title: "上传图片",
                        buttons:{
                            "cancel": {
                                label: "取消",
                                className: "btn btn-default"
                            },
                            "ok":{
                                label: "确定",
                                className: "btn btn-success",
                                callback: function(){
                                    var form;
                                    var idx = dialog.find(".nav.nav-tabs li.active").index();
                                    if(idx == 0){
                                        form=dialog.find("form")[0];
                                    } else if(idx == 1) {
                                        form=dialog.find("form")[1];
                                    } else if(idx == 2){
                                        var src = dialog.find("#uploaded-content .ccbox-img-item.selected").attr("data-rel");
                                        btn.parent().parent().next().children().attr("src", src);
                                        btn.parent().prev().val(src); dialog.modal("hide");
                                        dialog.modal("hide");
                                        return false;
                                    }
                                    $(form).find(\'input[name="dir"]\').val(btn.attr("data-dir"));
                                    $(form).find(\'input[name="size"]\').val(btn.attr("data-size"));
                                    $(form).find(\'input[name="nowater"]\').val(btn.attr("data-nowater"));
                                    form.submit();
                                    return false;
                                }
                            }
                        }
                    });
                    dialog.find("input[name=\'type\']").on("click",function(){
                        var val=$(this).val();
                        if(val == 1){
                            $(this).parent().parent().next().css("display","block");
                        }else{
                            $(this).parent().parent().next().css("display","none");
                            $.post(
                                "' . U('Admin/Base/loadDirs') . '",
                                { dir:"images"},
                                function(json){
                                    if(json.status == 1){
                                        var htm = \'\';
                                        for(var i = 0, data = json.data, l = data.length; i < l; i ++){
                                            htm += \'<a href="javascript:;" class="btn btn-app btn-lg img-album-dir" data-dir="\'+data[i].name+\'" data-parent="\'+data[i].parent+\'" data-rel="\'+(data[i].isdir==true?"dir":"file")+\'"><i class="icon-folder-close blue"></i>\'+data[i].name+\'</a>\';
                                        }
                                        $("#uploaded-content").html(htm);
                                    }else {
                                        alert("加载文件夹失败");
                                    }
                                },
                                "json"
                            );
                        }
                    });
                    dialog.find("input[name=\'type2\']").click(function(){
                        var type=$(this).val();
                        if(itype[type] != null){
                            var html = \'\';
                            for(var i = 0, data = itype[type], l = data.length; i < l; i++){
                                html += \'<div class="col-xs-12 col-sm-3"><a class="btn btn-link img-album-item" data-rel="\'+data[i].id+\'">\'+data[i].title+\'</a></div>\';
                            }
                            $("#uploaded-content").html(html);
                        } else {
                            $.post(
                                "' . U('Admin/Base/loadImages') . '",
                                { id: type},
                                function(json){
                                    itype[type]=json.data;
                                    var html = \'\';
                                    if(json.status == 1 && json.data.length > 0){
                                        for(var i = 0, data = json.data, l = data.length; i < l; i++){
                                            html += \'<div class="col-xs-12 col-sm-3"><a class="btn btn-link img-album-item" data-rel="\'+data[i].id+\'">\'+data[i].title+\'</a></div>\';
                                        }
                                    }
                                    $("#uploaded-content").html(html);
                                },"JSON"
                            );
                        }
                    }).eq(0).click();
                    $("#uploaded-content").delegate(".img-album-item","click",function(){
                        var self=$(this),id=self.attr("data-rel");
                        $.post(
                            "' . U('Admin/Base/loadImagesDetail') . '",
                            { id: id},
                            function(json){
                                if(json.status == 1){
                                    var htm = \'<div class="ace-thumbnails">\';
                                    for(var i = 0, data = json.data, l = data.length; i < l; i ++){
                                        htm += \'<li style="border:2px solid transparent;" class="ccbox-img-item" data-rel="\'+data[i].thumb+\'"><i style="position:absolute;top:0;right:0;" class="icon-ok red hide"></i><a href="javascript://"><img src="\'+data[i].thumb+\'" width="80" height="80"></a></li>\';
                                    }
                                    htm += \'</div>\';
                                    $("#uploaded-content").html(htm);
                                } else {
                                    alert("加载失败!");
                                }
                            },"json"
                        );
                    });
                    $("#uploaded-content").delegate(".ccbox-img-item", "click", function(){
                        var self=$(this);
                        self.addClass("selected").css("borderColor","#dd5a43").find("i.icon-ok").removeClass("hide");
                        self.siblings(".selected").removeClass("selected").css("borderColor","transparent").find("i.icon-ok").addClass("hide");
                    });
                    $("#uploaded-content").delegate(".img-album-dir", "click", function(){
                        var self=$(this),dir=self.attr("data-dir"),parent=self.attr("data-parent");
                        $.post(
                            "' . U("Admin/Base/loadDirs") . '",
                            { dir: dir, parent: parent},
                            function(json){
                                if(json.status == 1){
                                    var htm = \'<div class="ace-thumbnails">\';
                                    for(var i = 0, data = json.data, l = data.length; i < l; i ++){
                                        htm += \'<li style="border:2px solid transparent;" class="ccbox-img-item" data-rel="/uploads/attachment\'+data[i].parent+"/"+data[i].name+\'"><i style="position:absolute;top:0;right:0;" class="icon-ok red hide"></i><a href="javascript://"><img src="/uploads/attachment\'+data[i].parent+"/"+data[i].name+\'" width="80" height="80"></a></li>\';
                                    }
                                    htm += \'</div>\';
                                    $("#uploaded-content").html(htm);
                                } else {
                                    alert("加载失败!");
                                }
                            },"json"
                        );
                    });
                    dialog.find(":file").filestyle({buttonText: "选择图片", buttonName: "btn-sm"});
                    window.uploadLocalPic = function(data){
                        if(data.status == 1){
                            btn.parent().parent().next().children().attr("src", data.file);
                            btn.parent().prev().val(data.file); dialog.modal("hide");
                        } else {
                            alert(data.msg);
                        }
                    }
                });
            } </script>';
            define('TPL_INIT_IMAGE', true);
        }
        return $parseStr;
    }

    public function _multimage($tag) {
        $name     = $tag['name'] ?: '_pvimage';
        $style    = $tag['style'];
        $default  = $tag['default'] ?: '/Public/images/nopic.jpg';
        $values   = $tag['values'];
        $dir      = $tag['dir'];
        $size     = $tag['size'];
        $nowater  = $tag['nowater'];
        $parseStr = '<div class="input-group"> <input type="text" class="form-control" readonly="readonly" value="" placeholder="批量上传图片" autocomplete="off"> <span class="input-group-btn"> <button class="btn btn-default btn-sm" type="button" onclick="uploadMultiImage(this, \'' . $name . '\');" data-nowater="' . $nowater . '" data-dir="' . $dir . '" data-size="' . $size . '">选择图片</button> </span> </div> <div class="input-group multi-img-details"> ';
        if ($values) {
            $values = $this->tpl->get($values);
            if (is_array($values) && !empty($values)) {
                foreach ($values as $val) {
                    $parseStr .= '<div class="multi-item" style="height: 150px; position:relative; float: left; margin-right: 18px;"><img style="max-width: 150px; max-height: 150px;" onerror="this.src=\'/Public/images/nopic.jpg\'; this.title=\'图片未找到.\'" src="' . $val . '" class="img-responsive img-thumbnail"><input type="hidden" name="' . $name . '[]" value="' . $val . '"><em class="close" style="position:absolute; top: 0px; right: -14px;" title="删除这张图片" onclick="deleteMultiImage(this)">×</em></div>';
                }
            }
        }
        $parseStr .= '</div>';
        if (!defined('TPL_INIT_MULTI_IMAGE')) {
            $types = D('Admin/Zsysdata')->getAll(array('type' => 'images'));
            $parseStr .= '<script type="text/javascript">
            var itype=[];
            function uploadMultiImage(elm, nm) {
                require(["jquery", "bootbox", "filestyle"], function($){
                    var dialog = bootbox.dialog({
                        title: "上传图片",
                        message: \'<iframe width="0" height="0" name="__image_file_uploader" style="display:none;"></iframe>\'
                        +\'<ul class="nav nav-tabs">\'
                        +\'<li class="active"><a data-toggle="tab" href="#local">本地图片</a></li>\'
                        +\'<li> <a data-toggle="tab" href="#remote">远程图片 </a> </li>\'
                        +\'<li><a data-toggle="tab" href="#uploaded">浏览附件</a></li>\'
                        +\'</ul>\'
                        +\'<div class="tab-content"> <div id="local" class="tab-pane in active"> \'
                        +\'<form action="/index.php/Admin/Base/frameUpload/callback/uploadLocalPic" enctype="multipart/form-data" method="post" target="__image_file_uploader">\'
                        +\'<input type="file" name="upfile" >\'
                        +\'<input type="hidden" name="dir" value="">\'
                        +\'<input type="hidden" name="size" value="">\'
                        +\'<input type="hidden" name="nowater" value="">\'
                        +\'</form></div> \'
                        +\'<div id="remote" class="tab-pane">\'
                        +\'<form action="/index.php/Admin/Base/frameUpload/callback/uploadLocalPic" method="post" target="__image_file_uploader">\'
                        +\'<input type="txt" name="url" class="form-control" placeholder="请输入图片URL" >\'
                        +\'<input type="hidden" name="dir" value="">\'
                        +\'<input type="hidden" name="size" value="">\'
                        +\'<input type="hidden" name="nowater" value="">\'
                        +\'</form></div>\'
                        +\'<div class="tab-pane" id="uploaded">\'
                        +\'<div class="row">\'
                        +\'<div class="col-xs-12">\'
                        +\'类型：<label class="radio-inline"><input type="radio" name="type" value="1" checked>按分类</label>\'
                        +\'<label class="radio-inline"><input type="radio" name="type" value="2">按目录</label>\'
                        +\'</div>\'
                        +\'<div class="col-xs-12">分类：\'';
            foreach ($types as $key => $row) {
                $parseStr .= '+\'<label class="radio-inline"><input type="radio" name="type2" value="' . $row['id'] . '"' . ($key == 0 ? ' checked' : '') . '>' . $row['title'] . '</label>\'';
            }
            $parseStr .= '+\'</div>\'
                        +\'<div class="col-xs-12" id="uploaded-content"></div>\'
                        +\'</div>\'
                        +\'</div>\'
                        +\'</div>\',
                        buttons:{
                            "cancel": {
                                label: "取消",
                                className: "btn btn-default"
                            },
                            "ok": {
                                label: "确定",
                                className: "btn btn-success",
                                callback: function(){
                                    var form;
                                    var idx = dialog.find(".nav.nav-tabs li.active").index();
                                    if(idx == 0){
                                        form=dialog.find("form")[0];
                                    } else if(idx == 1) {
                                        form=dialog.find("form")[1];
                                    } else if(idx == 2){
                                        dialog.find("#uploaded-content .ccbox-img-item.selected").each(function(){
                                             $(elm).parent().parent().next().append(\'<div class="multi-item" style="height: 150px; position:relative; float: left; margin-right: 18px;"><img style="max-width: 150px; max-height: 150px;" onerror="this.src=\\"/Public/images/nopic.jpg\\"; this.title=\\"图片未找到.\\"" src="\'+$(this).attr("data-rel")+\'" class="img-responsive img-thumbnail"><input type="hidden" name="\'+nm+\'[]" value="\'+$(this).attr("data-rel")+\'"><em class="close" style="position:absolute; top: 0px; right: -14px;" title="删除这张图片" onclick="deleteMultiImage(this)">×</em></div>\');
                                        });
                                        dialog.modal("hide");
                                        return false;
                                    }
                                    $(form).find(\'input[name="dir"]\').val($(elm).attr("data-dir"));
                                    $(form).find(\'input[name="size"]\').val($(elm).attr("data-size"));
                                    $(form).find(\'input[name="nowater"]\').val($(elm).attr("data-nowater"));
                                    form.submit();
                                    return false;
                                }
                            }
                        }
                    });
                    dialog.find("input[name=\'type\']").on("click",function(){
                        var val=$(this).val();
                        if(val == 1){
                            $(this).parent().parent().next().css("display","block");
                        }else{
                            $(this).parent().parent().next().css("display","none");
                            $.post(
                                "' . U('Admin/Base/loadDirs') . '",
                                { dir:"images"},
                                function(json){
                                    if(json.status == 1){
                                        var htm = \'\';
                                        for(var i = 0, data = json.data, l = data.length; i < l; i ++){
                                            htm += \'<a href="javascript:;" class="btn btn-app btn-lg img-album-dir" data-dir="\'+data[i].name+\'" data-parent="\'+data[i].parent+\'" data-rel="\'+(data[i].isdir==true?"dir":"file")+\'"><i class="icon-folder-close blue"></i>\'+data[i].name+\'</a>\';
                                        }
                                        $("#uploaded-content").html(htm);
                                    }else {
                                        alert("加载文件夹失败");
                                    }
                                },
                                "json"
                            );
                        }
                    });
                    dialog.find("input[name=\'type2\']").click(function(){
                        var type=$(this).val();
                        if(itype[type] != null){
                            var html = \'\';
                            for(var i = 0, data = itype[type], l = data.length; i < l; i++){
                                html += \'<div class="col-xs-12 col-sm-3"><a class="btn btn-link img-album-item" data-rel="\'+data[i].id+\'">\'+data[i].title+\'</a></div>\';
                            }
                            $("#uploaded-content").html(html);
                        } else {
                            $.post(
                                "' . U('Admin/Base/loadImages') . '",
                                { id: type},
                                function(json){
                                    itype[type]=json.data;
                                    var html = \'\';
                                    if(json.status == 1 && json.data.length > 0){
                                        for(var i = 0, data = json.data, l = data.length; i < l; i++){
                                            html += \'<div class="col-xs-12 col-sm-3"><a class="btn btn-link img-album-item" data-rel="\'+data[i].id+\'">\'+data[i].title+\'</a></div>\';
                                        }
                                    }
                                    $("#uploaded-content").html(html);
                                },"JSON"
                            );
                        }
                    }).eq(0).click();
                    $("#uploaded-content").delegate(".img-album-item","click",function(){
                        var self=$(this),id=self.attr("data-rel");
                        $.post(
                            "' . U('Admin/Base/loadImagesDetail') . '",
                            { id: id},
                            function(json){
                                if(json.status == 1){
                                    var htm = \'<div class="ace-thumbnails">\';
                                    for(var i = 0, data = json.data, l = data.length; i < l; i ++){
                                        htm += \'<li style="border:2px solid transparent;" class="ccbox-img-item" data-rel="\'+data[i].thumb+\'"><i style="position:absolute;top:0;right:0;" class="icon-ok red hide"></i><a href="javascript://"><img src="\'+data[i].thumb+\'" width="80" height="80"></a></li>\';
                                    }
                                    htm += \'</div>\';
                                    $("#uploaded-content").html(htm);
                                } else {
                                    alert("加载失败!");
                                }
                            },"json"
                        );
                    });
                    $("#uploaded-content").delegate(".ccbox-img-item", "click", function(){
                        var self=$(this), selected=self.hasClass("selected");
                        if(selected){
                            self.removeClass("selected").css("borderColor","transparent").find("i.icon-ok").addClass("hide");
                        }else{
                            self.addClass("selected").css("borderColor","#dd5a43").find("i.icon-ok").removeClass("hide");
                        }
                    });
                    $("#uploaded-content").delegate(".img-album-dir", "click", function(){
                        var self=$(this),dir=self.attr("data-dir"),parent=self.attr("data-parent");
                        $.post(
                            "' . U("Admin/Base/loadDirs") . '",
                            { dir: dir, parent: parent},
                            function(json){
                                if(json.status == 1){
                                    var htm = \'<div class="ace-thumbnails">\';
                                    for(var i = 0, data = json.data, l = data.length; i < l; i ++){
                                        htm += \'<li style="border:2px solid transparent;" class="ccbox-img-item" data-rel="/uploads/attachment\'+data[i].parent+"/"+data[i].name+\'"><i style="position:absolute;top:0;right:0;" class="icon-ok red hide"></i><a href="javascript://"><img src="/uploads/attachment\'+data[i].parent+"/"+data[i].name+\'" width="80" height="80"></a></li>\';
                                    }
                                    htm += \'</div>\';
                                    $("#uploaded-content").html(htm);
                                } else {
                                    alert("加载失败!");
                                }
                            },"json"
                        );
                    });
                    dialog.find(":file").filestyle({buttonText: "选择图片", buttonName: "btn-sm"});
                    window.uploadLocalPic = function(data){
                        if(data.status == 1){
                            $(elm).parent().parent().next().append(\'<div class="multi-item" style="height: 150px; position:relative; float: left; margin-right: 18px;"><img style="max-width: 150px; max-height: 150px;" onerror="this.src=\\"/Public/images/nopic.jpg\\"; this.title=\\"图片未找到.\\" src="\'+data.file+\'" class="img-responsive img-thumbnail"><input type="hidden" name="\'+nm+\'[]" value="\'+data.file+\'"><em class="close" style="position:absolute; top: 0px; right: -14px;" title="删除这张图片" onclick="deleteMultiImage(this)">×</em></div>\');
                            dialog.modal("hide");
                        } else {
                            alert(data.msg);
                        }
                    }
                });
            }
            function deleteMultiImage(elm){
                require(["jquery"], function($){
                    $(elm).parent().remove();
                });
            }
            </script>';
            define('TPL_INIT_MULTI_IMAGE', true);
        }
        return $parseStr;
    }

    public function _daterange($tag) {
        $name      = $tag['name'] ?: 'daterange';
        $time      = !empty($tag['time']) && $tag['time'] == 'true' ? TRUE : FALSE;
        $startdate = $tag['startdate'] ? $this->tpl->get($tag['startdate']) : '';
        $enddate   = $tag['enddate'] ? $this->tpl->get($tag['enddate']) : '';
        if ($startdate) {
            $startdate = $time ? date('Y-m-d H:i', strtotime($tag['startdate'])) : date('Y-m-d', strtotime($tag['startdate']));
        } else {
            $startdate = $time ? date('Y-m-d H:i') : date('Y-m-d');
        }
        if ($enddate) {
            $enddate = $time ? date('Y-m-d H:i', strtotime($tag['enddate'])) : date('Y-m-d', strtotime($tag['enddate']));
        } else {
            $enddate = $time ? date('Y-m-d H:i') : date('Y-m-d');
        }
        $parseStr = '<input name="' . $name . '[start]" type="hidden" value="' . $startdate . '" /> <input name="' . $name . '[end]" type="hidden" value="' . $enddate . '" /> <button class="btn btn-info daterange ' . ($time ? 'daterange-time' : 'daterange-date') . '" type="button" style="border:0;"><span class="date-title">' . $startdate . '至' . $enddate . '</span> <i class="fa fa-calendar"></i></button>';
        if (!$time && !defined('TPL_INIT_DATERANGE_DATE')) {
            $parseStr .= '<script type="text/javascript">
                require(["daterangepicker"], function($){
                    $(function(){
                        $(".daterange.daterange-date").each(function(){
                            var elm = this;
                            $(this).daterangepicker({
                                startDate: $(elm).prev().prev().val(),
                                endDate: $(elm).prev().val(),
                                format: "YYYY-MM-DD",
                            }, function(start, end){
                                $(elm).find(".date-title").html(start.toDateStr() + " 至 " + end.toDateStr());
                                $(elm).prev().prev().val(start.toDateStr());
                                $(elm).prev().val(end.toDateStr());
                            });
                        });
                    });
                });
                </script>';
            define('TPL_INIT_DATERANGE_DATE', TRUE);
        } elseif ($time && !defined('TPL_INIT_DATERANGE_TIME')) {
            $parseStr .= '<script type="text/javascript">
                require(["daterangepicker"], function($){
                    $(function(){
                        $(".daterange.daterange-time").each(function(){
                            var elm = this;
                            $(this).daterangepicker({
                                startDate: $(elm).prev().prev().val(),
                                endDate: $(elm).prev().val(),
                                format: "YYYY-MM-DD HH:mm",
                                timePicker: true,
                                timePicker12Hour : false,
                                timePickerIncrement: 1,
                                minuteStep: 1
                            }, function(start, end){
                                $(elm).find(".date-title").html(start.toDateTimeStr() + " 至 " + end.toDateTimeStr());
                                $(elm).prev().prev().val(start.toDateTimeStr());
                                $(elm).prev().val(end.toDateTimeStr());
                            });
                        });
                    });
                });
            </script>';
            define('TPL_INIT_DATERANGE_TIME', true);
        }
        return $parseStr;
    }

    public function _chosen($tag) {
        $id       = $tag['id'];
        $name     = $tag['name'] ?: '_chosen';
        $style    = $tag['style'];
        $onchange = $tag['onchange'];
        $options  = $tag['options'];
        $multiple = $tag['multiple'];
        $default  = $tag['default'] ?: '请选择';
        $selected = $tag['selected'];
        if (strpos($selected, '.') !== FALSE) {
            //plus selected
            list($var1, $var2) = explode('.', $selected);
            $selected          = $var1 . '["' . $var2 . '"]';
        }
        $parseStr = '<select class="chosen-select ' . $style . ($multiple ? ' tag-input-style' : '') . '" name="' . $name . ($multiple ? '[]' : '') . '" id="' . $id . '"' . ($multiple ? ' multiple="" data-placeholder="' . $default . '"' : '') . ' onchange="' . $onchange . '">';
        if (empty($multiple)) {
            $parseStr .= '<option value="">' . $default . '</option>';
        }
        if (!empty($options)) {
            if (empty($multiple)) {
                $parseStr .= '<?php if(!empty($' . $options . ') && is_array($' . $options . ')){?>';
                $parseStr .= '<?php foreach($' . $options . ' as $key => $val){?>';
                $parseStr .= '<option value="<?php echo $key; ?>"' . (!empty($selected) ? '<?php if($' . $selected . ' && $' . $selected . '==$key){?>' . ' selected="selected"' . '<?php }?>' : '') . '><?php echo $val;?></option>';
                $parseStr .= '<?php }?>';
                $parseStr .= '<?php }?>';
            } else {
                $parseStr .= '<?php if(!empty($' . $options . ') && is_array($' . $options . ')){?>';
                $parseStr .= '<?php foreach($' . $options . ' as $key => $val){?>';
                $parseStr .= '<option value="<?php echo $key; ?>"' . (!empty($selected) ? '<?php if($' . $selected . ' && (is_array($' . $selected . ')?in_array($key,$' . $selected . '):$' . $selected . '==$key)){?> selected="selected"<?php }?>' : '') . '><?php echo $val;?></option>';
                $parseStr .= '<?php }?>';
                $parseStr .= '<?php }?>';
            }
        }
        $parseStr .= '</select>';
        if (!defined('TPL_INIT_CHOSEN')) {
            $parseStr .= '<script type="text/javascript">
            require(["chosen"], function(){
                $(".chosen-select").chosen({width: "100%", search_contains: true});
            })
            </script>';
            define('TPL_INIT_CHOSEN', TRUE);
        }
        return $parseStr;
    }

    public function _seltree($tag) {
        $name     = $tag['name'] ?: '_seltree';
        $style    = $tag['style'] ?: '';
        $onchange = $tag['onchange'] ?: '';
        $options  = $tag['options'];
        $multiple = $tag['multiple'];
        $default  = $tag['default'] ?: '请选择';
        $selected = $tag['selected'];
        $_key     = $tag['key'] ?: 'id';
        $_val     = $tag['val'] ?: 'title';
        $isleaf   = $tag['leaf'] == 1 ? true : false;
        $parseStr = '';
        $selected = $this->tpl->get($selected);

        $parseStr = '<select class="chosen-select  ' . $style . ($multiple ? ' tag-input-style' : '') . '" name="' . $name . ($multiple ? '[]' : '') . '" id="' . $id . '"' . ($multiple ? ' multiple="" data-placeholder="' . $default . '"' : '') . ' onchange="' . $onchange . '">';

        if (empty($multiple)) {
            $parseStr .= '<option value="">' . $default . '</option>';
        }
        $options = $this->tpl->get($options);
        if (!empty($options) && is_array($options)) {
            $parseStr .= $this->createOptions($options, 0, $_key, $_val, $selected, $isleaf);
        }

        $parseStr .= '</select>';
        if (!defined('TPL_INIT_CHOSEN')) {
            $parseStr .= '<script type="text/javascript">
            require(["chosen"], function(){
                $(".chosen-select").chosen({width: "100%", search_contains: true});
            })
            </script>';
            define('TPL_INIT_CHOSEN', TRUE);
        }
        return $parseStr;
    }

    private function createOptions($options, $level, $_key, $_val, $selected, $isleaf = false) {
        $parseStr = '';
        $repeat   = $level;
        foreach ($options as $key => $val) {
            $parseStr .= '<option value="' . $val[$_key] . '"' . ($val[$_key] == $selected || (is_array($selected) && in_array($val[$_key], $selected)) ? ' selected="selected"' : '') . ($isleaf && !empty($val['child']) ? ' disabled' : '') . '>' . ($level ? '|' . str_repeat('——', $repeat) : '') . $val[$_val] . '</option>';
            if ($val['child'] && is_array($val['child'])) {
                $parseStr .= $this->createOptions($val['child'], $level + 1, $_key, $_val, $selected, $isleaf);
            }
        }
        return $parseStr;
    }

    /**
     * list标签解析
     * 格式： <html:list datasource="" show="" />
     * @access public
     * @param array $tag 标签属性
     * @return string
     */
    public function _list($tag) {
        $id         = $tag['id']; //表格ID
        $datasource = $tag['datasource']; //列表显示的数据源VoList名称
        $pk         = empty($tag['pk']) ? 'id' : $tag['pk']; //主键名，默认为id
        $style      = $tag['style']; //样式名
        $name       = !empty($tag['name']) ? $tag['name'] : 'vo'; //Vo对象名
        $action     = $tag['action'] == 'true' ? true : false; //是否显示功能操作
        $key        = !empty($tag['key']) ? true : false;
        $sort       = $tag['sort'] == 'false' ? false : true; //是否开启排序
        $checkbox   = $tag['checkbox']; //是否显示Checkbox
        $editable   = $tag['editable'] ? explode(',', $tag['editable']) : array(); //可编辑字段
        if (isset($tag['actionlist'])) {
            if (substr($tag['actionlist'], 0, 1) == '$') {
                $actionlist = $this->tpl->get(substr($tag['actionlist'], 1));
            } else {
                $actionlist = $tag['actionlist'];
            }
            $actionlist = explode(',', trim($actionlist)); //指定功能列表
        }

        if (substr($tag['show'], 0, 1) == '$') {
            $show = $this->tpl->get(substr($tag['show'], 1));
        } else {
            $show = $tag['show'];
        }
        $show = explode(',', $show); //列表显示字段列表

        //计算表格的列数
        $colNum = count($show);
        if (!empty($checkbox)) {
            $colNum++;
        }

        if (!empty($action)) {
            $colNum++;
        }

        if (!empty($key)) {
            $colNum++;
        }

        //显示开始
        $parseStr = "<!-- Think 系统列表组件开始 -->\n";
        if (!defined('TPL_INIT_TABLE_LIST')) {
            $parseStr .= '<script type="text/javascript">
            require(["jquery", "bootbox"], function($){
                $(".check_all_list").click(function(){
                    if($(this).prop("checked") == true){
                        $(this).closest("table").find("input:checkbox.check_item").prop("checked", true);
                    }else{
                        $(this).closest("table").find("input:checkbox.check_item").prop("checked", false);
                    }
                });
                $("span.editfield").click(function(){
                    var val=$(this).html();
                    var obj = $(this);
                    var dialog=bootbox.dialog({
                        title: "<span style=\"color:rgb(134, 181, 88)\"><i class=\"icon-check-circle\"></i>系统提示</span>",
                        message: \'<input type="text" value="\'+val+\'" class="form-control">\',
                        buttons:{
                            cancel: {
                                label: "取消",
                                className: "btn btn-sm btn-default"
                            },
                            ok:{
                                label:"提交",
                                className: "btn btn-sm btn-success",
                                callback: function(){
                                    var newVal = dialog.find("input").val();
                                    if(newVal != val){
                                        $.post(
                                            "' . U(CONTROLLER_NAME . '/edit') . '",
                                            { pk: obj.attr("data-pk"),id:obj.attr("data-val"),field: obj.attr("data-field"), value: newVal},
                                            function(json){
                                                if(json.status==1){
                                                    obj.html(newVal);
                                                } else{
                                                    dialog.modal("hide");
                                                    require(["util"],function(util){
                                                        util.error(json.msg);
                                                    });
                                                }
                                            },
                                            "json"
                                        );
                                    }
                                }
                            }
                        }
                    });
                });
            });
            function edit(id){
                location.href="' . U(CONTROLLER_NAME . '/edit') . '?id="+id;
            }
            function del(id, obj){
                require(["jquery", "util"], function($, util){
                    util.warning("你确定要删除本条数据吗？", function(){
                        $.post("' . U(CONTROLLER_NAME . '/delete') . '", { id: id}, function(data){
                            if(data.status == 1){
                                $(obj).parent().parent().slideUp(function(){
                                    $(this).remove();
                                });
                            }else{
                                util.error(data.msg);
                            }
                        }, "json")
                    })
                });
            }
            function sortBy(sort, model, action){
                var href = location.href;
                if(href.indexOf("_order")===-1){
                    if(href.indexOf("?")===-1){
                        href+="?_order="+sort;
                    }else{
                        href+="&_order="+sort;
                    }
                } else {
                    href = href.replace(/_order(\/|=)[^&\/\.]*/,"_order$1"+sort);
                }
                var _sort = "desc";
                if(sort=="' . $this->tpl->get('_order') . '"){
                    _sort = "' . $this->tpl->get('_sort') . '"=="desc"? "asc":"desc";
                }
                if(href.indexOf("_sort")===-1){
                    if(href.indexOf("?")===-1){
                        href+="?_sort="+_sort;
                    } else{
                        href+="&_sort="+_sort;
                    }
                }else{
                    href=href.replace(/_sort(\/|=)[^&\/\.]*/,"_sort$1"+_sort);
                }
                location.href=href;
            }
            function delAll(obj){
                require(["util"], function(util){
                    var ids = [];
                    var table = $(obj).parent().parent().parent().prev("table");
                    table.find(".check_item:checked").each(function(){
                        ids.push($(this).val());
                    });
                    if(ids.length <= 0){
                        util.error("请选择数据");
                        return;
                    }
                    util.warning("确定要删除这些数据吗？", function(){
                        $.post(
                            "' . U(CONTROLLER_NAME . '/delete') . '",
                            {id: ids.join(",")},
                            function(data){
                                if(data.status == 1){
                                    util.success("删除成功");
                                    table.find(".check_item:checked").parent().parent().slideUp(function(){
                                        $(this).remove();
                                    });
                                } else {
                                    util.error(data.msg);
                                }
                            },
                            "json"
                        );
                    })
                })
            }
            function changePageSize(obj){
                if(location.href.indexOf("psize")===-1){
                    if(location.href.indexOf("?")===-1){
                        location.href=location.href+"?psize="+$(obj).val();
                    }else{
                        location.href=location.href+"&psize="+$(obj).val();
                    }
                } else {
                    location.href=location.href.replace(/psize(\/|=)\d+/,"psize$1"+$(obj).val());
                }
            }
            </script>';
            define('TPL_INIT_TABLE_LIST', TRUE);
        }
        $parseStr .= '<table id="' . $id . '" class="table table-striped table-hover ' . $style . '" >';
        $parseStr .= '<thead>';
        $parseStr .= '<tr class="row" >';
        //列表需要显示的字段
        $fields = array();
        foreach ($show as $val) {
            $fields[] = explode(':', $val);
        }
        if (!empty($checkbox) && 'true' == strtolower($checkbox)) {
//如果指定需要显示checkbox列
            $parseStr .= '<th width="8"><input type="checkbox" class="check_all_list" ></th>';
        }
        if (!empty($key)) {
            $parseStr .= '<th width="12">No</th>';
        }
        foreach ($fields as $field) {
//显示指定的字段
            $property = explode('|', $field[0]);
            $showname = explode('|', $field[1]);
            if (isset($showname[1])) {
                $parseStr .= '<th width="' . $showname[1] . '">';
            } else {
                $parseStr .= '<th>';
            }
            $showname[2] = isset($showname[2]) ? $showname[2] : $showname[0];
            if ($sort) {
                $parseStr .= '<a href="javascript:sortBy(\'' . $property[0] . '\',\'{$sort}\',\'' . ACTION_NAME . '\')" title="按照' . $showname[2] . '{$sortType} ">' . $showname[0] . '<eq name="_order" value="' . $property[0] . '" ><i class="icon-arrow-<eq name="_sort" value="desc">up<else/>down</eq> green"></i></eq></a></th>';
            } else {
                $parseStr .= $showname[0] . '</th>';
            }

        }

        if (!empty($action)) {
//如果指定显示操作功能列
            $parseStr .= '<th >操作</th>';
        }

        $parseStr .= '</tr></thead>';
        $parseStr .= '<volist name="' . $datasource . '" id="' . $name . '" ><tr class="row" '; //支持鼠标移动单元行颜色变化 具体方法在js中定义

        $parseStr .= '>';
        if (!empty($checkbox)) {
//如果需要显示checkbox 则在每行开头显示checkbox
            $parseStr .= '<td><input type="checkbox" class="check_item" name="key" value="{$' . $name . '.' . $pk . '}"></td>';
        }
        if (!empty($key)) {
            $parseStr .= '<td>{$i}</td>';
        }
        foreach ($fields as $field) {
            //显示定义的列表字段
            $parseStr .= '<td>';
            if (!empty($field[2])) {
                // 支持列表字段链接功能 具体方法由JS函数实现
                $href = explode('|', $field[2]);
                if (count($href) > 1) {
                    //指定链接传的字段值
                    // 支持多个字段传递
                    $array = explode('^', $href[1]);
                    if (count($array) > 1) {
                        foreach ($array as $a) {
                            $temp[] = '\'{$' . $name . '.' . $a . '|addslashes}\'';
                        }
                        $parseStr .= '<a href="javascript:' . $href[0] . '(' . implode(',', $temp) . ')">';
                    } else {
                        $parseStr .= '<a href="javascript:' . $href[0] . '(\'{$' . $name . '.' . $href[1] . '|addslashes}\')">';
                    }
                } else {
                    //如果没有指定默认传编号值
                    $parseStr .= '<a href="javascript:' . $field[2] . '(\'{$' . $name . '.' . $pk . '|addslashes}\')">';
                }
            }
            if (strpos($field[0], '^')) {
                $property = explode('^', $field[0]);
                foreach ($property as $p) {
                    $unit = explode('|', $p);
                    if (count($unit) > 1) {
                        $parseStr .= '{$' . $name . '.' . $unit[0] . '|' . $unit[1] . '} ';
                    } else {
                        $parseStr .= '{$' . $name . '.' . $p . '} ';
                    }
                }
            } else {
                $property = explode('|', $field[0]);
                if (count($property) > 1) {
                    $parseStr .= '{$' . $name . '.' . $property[0] . '|' . $property[1] . '}';
                } else {
                    if (in_array($field[0], $editable)) {
                        $parseStr .= '<span class="editfield " data-toggle="tooltip" data-placement="top" title="点击编辑" data-field="' . $field[0] . '" data-pk="' . $pk . '" data-val="{$' . $name . '.' . $pk . '}">{$' . $name . '.' . $field[0] . '}</span>';
                    } else {
                        $parseStr .= '{$' . $name . '.' . $field[0] . '}';
                    }
                }
            }
            if (!empty($field[2])) {
                $parseStr .= '</a>';
            }
            $parseStr .= '</td>';

        }
        if (!empty($action)) {
            //显示功能操作
            if (!empty($actionlist[0])) {
                //显示指定的功能项
                $parseStr .= '<td>';
                foreach ($actionlist as $val) {
                    if (strpos($val, ':')) {
                        $a = explode(':', $val);
                        if (count($a) > 2) {
                            $parseStr .= '<a href="javascript:;" onClick="' . $a[0] . '(\'{$' . $name . '.' . $a[2] . '}\', this)">' . $a[1] . '</a>&nbsp;';
                        } else {
                            $parseStr .= '<a href="javascript:;" onClick="' . $a[0] . '(\'{$' . $name . '.' . $pk . '}\', this)">' . $a[1] . '</a>&nbsp;';
                        }
                    } else {
                        $array = explode('|', $val);
                        if (count($array) > 2) {
                            $parseStr .= ' <a href="javascript:;" onClick="' . $array[1] . '(\'{$' . $name . '.' . $array[0] . '}\', this)">' . $array[2] . '</a>&nbsp;';
                        } else {
                            $parseStr .= ' {$' . $name . '.' . $val . '}&nbsp;';
                        }
                    }
                }
                $parseStr .= '</td>';
            }
        }
        $parseStr .= '</tr></volist></table>';
        $parseStr .= '<div class="row">';
        $parseStr .= '  <div class="col-xs-12">';
        $parseStr .= '    <div class="pull-left">';
        $parseStr .= '      <a class="btn btn-xs btn-danger" onClick="delAll(this);" data-toggle="tooltip" data-placement="top" title="删除">';
        $parseStr .= '        <i class="icon-trash icon-2x"></i>';
        $parseStr .= '      </a>';
        $parseStr .= '     <a class="btn btn-xs btn-info" onClick="location.reload();" data-toggle="tooltip" data-placement="top" title="刷新">';
        $parseStr .= '        <i class="icon-refresh icon-2x"></i></a>';
        $parseStr .= '      &nbsp;<label class="control-label">显示</label>';
        $parseStr .= '      <select onchange="changePageSize(this);">';
        $parseStr .= '        <option value="15"' . (I('get.psize') == 15 ? ' selected="selected"' : '') . '>15</option>';
        $parseStr .= '        <option value="20"' . (I('get.psize') == 20 ? ' selected="selected"' : '') . '>20</option>';
        $parseStr .= '        <option value="50"' . (I('get.psize') == 50 ? ' selected="selected"' : '') . '>50</option>';
        $parseStr .= '      </select>条/页';
        $parseStr .= "    </div>";
        $parseStr .= '    <div class="pull-right">' . $this->tpl->get('page') . '</div>';
        $parseStr .= '  </div>';
        $parseStr .= '</div>';
        $parseStr .= "\n<!-- Think 系统列表组件结束 -->\n";
        return $parseStr;
    }

    /**
     *desc: quick select district
     *@param $tag Array options
     *@author qiisyhx<sihangshi2011@gmail.com>
     */
    public function _district($tag) {
        $name   = $tag['name'];
        $value  = $tag['value'] ? $this->tpl->get($tag['value']) : '';
        $change = $tag['onchange'] ?: '';
        $click  = $tag['onclick'] ?: '';
        $id     = $tag['id'];

        $districts = D('District')->field('id,title,letter,en,level,ishot,type')->where(array('status' => 1))->order('ordid DESC')->select();
        $result    = array();
        foreach ($districts as $row) {
            if ($row['level'] == 1) {
                continue;
            }
            if ($row['ishot'] == 1) {
                $result[$row['type']]['热门'][] = $row;
            }
            switch ($row['letter']) {
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                    $result[$row['type']]['ABCD'][] = $row;
                    break;
                case 'E':
                case 'F':
                case 'G':
                case 'H':
                    $result[$row['type']]['EFGH'][] = $row;
                    break;
                case 'J':
                case 'K':
                case 'L':
                case 'M':
                    $result[$row['type']]['JKLM'][] = $row;
                    break;
                case 'N':
                case 'O':
                case 'P':
                case 'Q':
                case 'R':
                case 'S':
                    $result[$row['type']]['NOPQRS'][] = $row;
                    break;
                case 'T':
                case 'U':
                case 'V':
                case 'W':
                case 'X':
                    $result[$row['type']]['TUVWX'][] = $row;
                    break;
                case 'Y':
                case 'Z':
                    $result[$row['type']]['YZ'][] = $row;
                    break;
            }
        }
        foreach ($result as $key => $row) {
            $temp = array();
            if (isset($row['热门'])) {
                $temp['热门'] = $row['热门'];
                unset($row['热门']);
            }
            ksort($row);
            $result[$key] = array_merge($temp, $row);
        }
        $rand0    = mt_rand();
        $parseStr = '<div class="row">';
        $parseStr .= '<div class="col-xs-12 dist-container">';
        $parseStr .= '<input type="text" class="form-control dist-input" name="' . $name . '" value="' . $value . '" id="' . $id . '" autocomplete="off" placeholder="输入拼音快速定位" onchange="' . $change . '" onfocus="showDist(this);" onclick="' . $click . '">';
        $parseStr .= '<div class="tabtab dist-main" style="display:none;">';
        $parseStr .= '<span class="dist-close">X</span>';
        $parseStr .= '<ul class="nav nav-tabs">';
        $parseStr .= '<li class="active">';
        $parseStr .= '<a data-toggle="tab" href="#type1' . $rand0 . '">国内</a>';
        $parseStr .= '</li>';
        $parseStr .= '<li>';
        $parseStr .= '<a data-toggle="tab" href="#type2' . $rand0 . '">境外</a>';
        $parseStr .= '</li>';
        $parseStr .= '</ul>';
        $parseStr .= '<div class="tab-content">';
        $parseStr .= '<div class="tab-pane active" id="type1' . $rand0 . '">';
        $parseStr .= '<div class="row">';
        $parseStr .= '<div class="tabtab dist-main">';
        $parseStr .= '<ul class="nav nav-tabs">';
        $rand1 = mt_rand();
        foreach (array_keys($result[1]) as $key => $item) {
            $parseStr .= '<li' . (0 == $key ? ' class="active"' : '') . '>';
            $parseStr .= '<a data-toggle="tab" href="#district_' . $key . $rand1 . '">' . $item . '</a>';
            $parseStr .= '</li>';
        }
        $parseStr .= '</ul>';
        $parseStr .= '<div class="tab-content">';
        $index = 0;
        foreach ($result[1] as $row) {
            usort($row, function ($a, $b) {
                if ($a['letter'] == $b['letter']) {
                    return 0;
                }
                return $a['letter'] > $b['letter'] ? 1 : -1;
            });
            $parseStr .= '<div class="tab-pane' . ($index == 0 ? ' active' : '') . '" id="district_' . $index . $rand1 . '">';
            $parseStr .= '<div class="row">';
            $letter = '';
            foreach ($row as $item) {
                if ($letter == '') {
                    $parseStr .= '<dl><dt>' . $item['letter'] . '</dt><dd><ul class="list-inline dist-item">';
                } elseif ($letter != $item['letter']) {
                    $parseStr .= '</ul></dd></dl><dl><dt>' . $item['letter'] . '</dt><dd><ul class="list-inline dist-item">';
                }
                $letter = $item['letter'];
                $parseStr .= '<li><a href="#" data-id="' . $item['id'] . '" data-letter="' . $item['letter'] . '" data-py="' . $item['en'] . '" data-val="' . $item['title'] . '">' . $item['title'] . '</a></li>';
            }
            $parseStr .= '</ul></dd></dl></div></div>';
            ++$index;
        }
        $parseStr .= '</div>';
        $parseStr .= '</div>';
        $parseStr .= '</div>';
        $parseStr .= '</div>';
        $parseStr .= '<div class="tab-pane" id="type2' . $rand0 . '">';
        $parseStr .= '<div class="row">';
        $parseStr .= '<div class="tabtab dist-main">';
        $parseStr .= '<ul class="nav nav-tabs">';
        $rand2 = mt_rand();
        foreach (array_keys($result[2]) as $key => $item) {
            $parseStr .= '<li' . (0 == $key ? ' class="active"' : '') . '>';
            $parseStr .= '<a data-toggle="tab" href="#district_' . $key . $rand2 . '">' . $item . '</a>';
            $parseStr .= '</li>';
        }
        $parseStr .= '</ul>';
        $parseStr .= '<div class="tab-content">';
        $index = 0;
        foreach ($result[2] as $row) {
            usort($row, function ($a, $b) {
                if ($a['letter'] == $b['letter']) {
                    return 0;
                }
                return $a['letter'] > $b['letter'] ? 1 : -1;
            });
            $parseStr .= '<div class="tab-pane' . ($index == 0 ? ' active' : '') . '" id="district_' . $index . $rand2 . '">';
            $parseStr .= '<div class="row">';
            $letter = '';
            foreach ($row as $item) {
                if ($letter == '') {
                    $parseStr .= '<dl><dt>' . $item['letter'] . '</dt><dd><ul class="list-inline dist-item">';
                } elseif ($letter != $item['letter']) {
                    $parseStr .= '</ul></dd></dl><dl><dt>' . $item['letter'] . '</dt><dd><ul class="list-inline dist-item">';
                }
                $letter = $item['letter'];
                $parseStr .= '<li><a href="#" data-id="' . $item['id'] . '" data-letter="' . $item['letter'] . '" data-py="' . $item['en'] . '" data-val="' . $item['title'] . '">' . $item['title'] . '</a></li>';
            }
            $parseStr .= '</ul></dd></dl></div></div>';
            ++$index;
        }
        $parseStr .= '</div>';
        $parseStr .= '</div>';
        $parseStr .= '</div>';
        $parseStr .= '</div>';
        $parseStr .= '</div>';
        $parseStr .= '</div>';
        $parseStr .= '</div>';
        $parseStr .= '</div>';
        if (!defined('TPL_INIT_DIST')) {
            $parseStr .= '<script type="text/javascript">require(["dist"]);</script>';
            define('TPL_INIT_DIST', TRUE);
        }
        return $parseStr;
    }

    public function _attachment($tag) {
        $name  = $tag['name'];
        $id    = $tag['name'];
        $size  = $tag['size'] ?: '2MB';
        $limit = $tag['limit'] ?: 5;
        $value = $tag['value'] ? intval($this->tpl->get($tag['value'])) : 0;
        if ($value) {
            $atts = D('AttachmentUrl')->getAll(array('status' => 1, 'pid' => $value));
            $limit -= count($atts);
        }
        $parseStr = '<input type="hidden" name="' . $name . '" value="' . $value . '">
                    <input type="file" id="' . $name . '" class="uploadify-attachment" data-limit="' . $limit . '">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <ul class="list-unstyled">';
        if ($atts) {
            foreach ($atts as $item) {
                $parseStr .= '<li><span class="label label-info">' . $item['title'] . '</span><span class="label">' . $item['sizeshow'] . '</span><span><a href="javascript:;" onclick="delAttachment(' . $value . ',' . $item['id'] . ',this);">删除</a></span><span><a href="' . U('Admin/Base/downloadAttachment', array('i' => $value, 'j' => $item['id'])) . '" target="_blank">下载(' . $item['downloads'] . '次)</a></span></li>';
            }
        }
        $parseStr .= '</ul>
                        </div>
                    </div>';
        if (!defined('TPL_UPLOADIFY_INIT')) {
            $parseStr .= '<script type="text/javascript">
            require(["uploadify", "util"], function($, util){
                $(".uploadify-attachment").uploadify({
                    swf: "__PUBLIC__/js/lib/uploadify/uploadify.swf",
                    buttonText:"选择文件",
                    uploader: "' . U('Admin/Base/uploadify') . '",
                    fileSizeLimit: "' . $size . '",
                    fileTypeExts: "*.jpg;*.gif;*.png;*.xls;*.xlsx;*.doc;*.docx;*.txt;",
                    fileTypeDesc: "图片，Excel，Word或者文本",
                    queueSizeLimit: ' . $limit . ',
                    overrideEvents: ["onDialogClose","onUploadError"],
                    uploadLimit: -1,
                    onInit:function(obj){
                        $(".uploadify-queue").hide();
                        obj.settings._uploadLimit=parseInt(this.attr("data-limit"),10);
                    },
                    onDialogOpen:function(){
                        if(this.settings.uploadLimit==-1){
                            this.setFileUploadLimit(this.settings._uploadLimit);
                            this.settings.uploadLimit=this.settings._uploadLimit;
                        }
                    },
                    onSelectError:function(file, errorCode){
                        console.log(errorCode)
                        var errorMsg = "文件添加失败：";
                        if(errorCode == -110){
                            errorMsg="文件大小超过限制,最大' . $size . '";
                        }else if(errorCode == -100){
                            errorMsg="文件数量超过限制，最多可上传"+this.settings.uploadLimit+"个文件";
                        }else if(errorCode == -130){
                            errorMsg="不支持的文件类型";
                        } else if(errorCode == -120){
                            errorMsg="文件大小为0，请重新选择文件";
                        } else {
                            errorMsg="未知错误!";
                        }
                        util.error(errorMsg);
                    },
                    onUploadStart:function(){
                        this.wrapper.uploadify("settings","formData",{attachment: this.wrapper.prev().val()});
                    },
                    onUploadError: function(){
                    },
                    onUploadSuccess:function(file, data, response){
                        data = $.parseJSON(data);
                        if(data.status == 1){
                            this.wrapper.prev().val(data.data.attachment);
                            this.wrapper.next().next().find(".list-unstyled").append(\'<li><span class="label label-info">\'+data.data.showname+\'</span><span class="label">\'+data.data.size+\'</span><span><a href="javascript:;" class="btn btn-link" onclick="delAttachment(\'+data.data.attachment+\',\'+data.data.subid+\', this);">删除</a></span></li>\');
                        } else {
                            util.error("文件上传失败");
                        }
                    }
                });
            });
            function delAttachment(i,j, obj){
                require(["util"],function(util){
                    $.post(
                        "' . U('Admin/Base/delAttachment') . '",
                        { i: i, j: j},
                        function(json){
                            if(json.status == 1){
                                util.success("附件删除成功");
                                $(obj).parent().parent().remove();
                            }
                        },"JSON"
                    );
                })
            }
            </script>';
            define('TPL_UPLOADIFY_INIT', TRUE);
        }
        return $parseStr;
    }
}